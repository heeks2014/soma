package com.hehe.soma;

import java.io.IOException;

import org.andengine.engine.Engine;
import org.andengine.engine.FixedStepEngine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.ui.activity.BaseGameActivity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.view.KeyEvent;
import android.widget.Toast;

import com.hehe.soma.manager.ResourcesManager;
import com.hehe.soma.manager.SceneManager;

/**
 * @authors: Amiri Mugarura and Sixbert Uwiringiyimana
 * @Credits: Thanks to Richard Rusa, the Hehe Team and other artists that help 
 * this project implementation in terms of graphic designs/audio, and healthy project management
 * @web www.heheltd.com
 * @version 0.0.0
 * @About this Class "Soma"
 * ----------------------
 * This class is the main activity that Android OS calls up.
 * It initializes the Andengine with options of :
 * 1. Scenes Camera's size(width and height) and position
 * 2. Notify Andengine that the Soma involves Audio resources
 * 3. Allowing the screen not to got to sleep while the game is on.
 * 4. The engine forces the game to have uniform performance across devices with different CPU/GPU performances
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
@SuppressLint("NewApi")
public class Soma extends BaseGameActivity
{
	// ===========================================================
	// Constants
	// ===========================================================

	private static final int CAMERA_WIDTH = 800;
	private static final int CAMERA_HEIGHT = 480;
	private static final Soma INSTANCE = new Soma();
	private Camera camera ; 
	private Context somaContext;
	private NotificationManager mNotificationManager;
	private final String onPauseNotification="kugirango ufashe bateri wibuke kuyifunga.";
	@Override
	public EngineOptions onCreateEngineOptions()
	{   
		camera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);  //scene camera view

		somaContext=getApplicationContext();
		if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
		mNotificationManager =(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		}
		/*prepare the engine for screen resolution policy by taking ratio width/height
		 --this will be pain in neck when applied to tablets with SS>10.1 in*/
		EngineOptions engineOptions =new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT),camera);
		engineOptions.getAudioOptions().setNeedsMusic(true); //prepare system to use music--background music
		engineOptions.getAudioOptions().setNeedsSound(true); //prepare system for sound usage ---simple FX
		engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON); //never put the screen off
		engineOptions.getRenderOptions().setDithering(true);
		return engineOptions;
	}

	@Override
	public Engine onCreateEngine(EngineOptions pEngineOptions) {
		/*Create a fixed step engine updating at 60 steps per second
	 in order to enforce same game's performance cross-devices
		 */
		return new FixedStepEngine(pEngineOptions, 60);
	}


	@Override
	public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws IOException
	{    
		//load all objects for resources needed for the game
		ResourcesManager.prepareManager(mEngine, this, camera, getVertexBufferObjectManager(),somaContext);
		ResourcesManager.getInstance();
		pOnCreateResourcesCallback.onCreateResourcesFinished(); //notify engine when done loading resources
	}



	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) throws IOException
	{
		SceneManager.getInstance().createSplashScene(pOnCreateSceneCallback); //create splash screen scene
	}

	//populate scene with  splash screen while we load menu resources in background 

	@Override
	public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) throws IOException
	{
		mEngine.registerUpdateHandler(new TimerHandler(5f, new ITimerCallback() //register splash screen timer
		{
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler) //function to handle timer's expiration
			{
				mEngine.unregisterUpdateHandler(pTimerHandler); //unregister time when splash finishes
				SceneManager.getInstance().createMenuScene(); //create home menu scene when splash screen is done.
			}
		}));
		pOnPopulateSceneCallback.onPopulateSceneFinished(); // callback to notify that splash has finished loading
	}

	//to ensure that SOMA completely exits after closing
	@Override
	protected void onDestroy()
	{
		removeNotification(0);
		super.onDestroy();
		System.exit(0);	
	}

	@Override
	protected void onPause()
	{   

		super.onPause();
		
		if (isGameLoaded()) {
			if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
			if(SceneManager.getInstance().getCurrentScene().getSceneType().equals(SceneManager.SceneType.SCENE_MENU))
				SceneManager.getInstance().pauseMenuBackMusic();
			if(SceneManager.getInstance().getCurrentScene().getSceneType().equals(SceneManager.SceneType.BUILD_SCENE_GAME))
				SceneManager.getInstance().pauseGameModeBackMusic();
			if(SceneManager.getInstance().getCurrentScene().getSceneType().equals(SceneManager.SceneType.EGG_SCENE_GAME))
				SceneManager.getInstance().pauseGameModeBackMusic();
			if(SceneManager.getInstance().getCurrentScene().getSceneType().equals(SceneManager.SceneType.PAINT_SCENE_GAME))
				SceneManager.getInstance().pauseGameModeBackMusic();
			if(SceneManager.getInstance().getCurrentScene().getSceneType().equals(SceneManager.SceneType.POP_SCENE_GAME))
				SceneManager.getInstance().pauseGameModeBackMusic();
			if(SceneManager.getInstance().getCurrentScene().getSceneType().equals(SceneManager.SceneType.SCENE_ABOUT))
				SceneManager.getInstance().pauseMenuBackMusic();
			}
			
			else{
				/*I tried what I could...but no pause for less than 3.0
				 * just to avoid monkey business */
				super.onDestroy();
				System.exit(0);		
			}
		}

		showNotification("SOMA iracyakora",onPauseNotification,0);
		
	}


	public void showNotification(String title,String message,int id){

		if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){

			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
			.setSmallIcon(R.drawable.ic_launcher)
			.setContentTitle(title)
			.setContentText(message);
			// Creates an explicit intent for an Activity 
			Intent resultIntent = new Intent();
			TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
			stackBuilder.addParentStack(this);
			stackBuilder.addNextIntent(resultIntent);
			PendingIntent resultPendingIntent =stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
			mBuilder.setContentIntent(resultPendingIntent);
			// mId allows  to update the notification later on.
			mNotificationManager.notify(id, mBuilder.build());
		}

		else{

			Toast.makeText(this,title+"\n"+message,Toast.LENGTH_LONG).show();	
		}
	}



	public void removeNotification(int id){

		if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
			if(mNotificationManager!=null){
				mNotificationManager.cancel(id);
			}
		} 
	}


	@Override
	protected synchronized void onResume()
	{
		super.onResume();
		
		if(isGameLoaded()) {
			if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
			if(SceneManager.getInstance().getCurrentScene().getSceneType().equals(SceneManager.SceneType.SCENE_MENU))
				SceneManager.getInstance().resumeMenuBackMusic();
			if(SceneManager.getInstance().getCurrentScene().getSceneType().equals(SceneManager.SceneType.BUILD_SCENE_GAME))
				SceneManager.getInstance().resumeGameModeBackMusic();
			if(SceneManager.getInstance().getCurrentScene().getSceneType().equals(SceneManager.SceneType.EGG_SCENE_GAME))
				SceneManager.getInstance().resumeGameModeBackMusic();
			if(SceneManager.getInstance().getCurrentScene().getSceneType().equals(SceneManager.SceneType.PAINT_SCENE_GAME))
				SceneManager.getInstance().resumeGameModeBackMusic();
			if(SceneManager.getInstance().getCurrentScene().getSceneType().equals(SceneManager.SceneType.POP_SCENE_GAME))
				SceneManager.getInstance().resumeGameModeBackMusic();
			if(SceneManager.getInstance().getCurrentScene().getSceneType().equals(SceneManager.SceneType.SCENE_ABOUT))
				SceneManager.getInstance().resumeMenuBackMusic();
			removeNotification(0);
		  }
			
		}
		else{
			//this wont serve nothing...
		}


	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{  
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			SceneManager.getInstance().getCurrentScene().onBackKeyPressed();
		}

		if (keyCode == KeyEvent.KEYCODE_MENU){
			SceneManager.getInstance().getCurrentScene().onMenuKeyPressed();
		}
		return false; 
	}


	public static Soma getInstance(){

		return INSTANCE;
	}


}
