/**
 @company: Hehe Ltd
 @Project: Soma
 @Date: mm/dd/yyyy
 @Author: Amiri Mugarura and Sixbert Uwiringiyimana
 @Credits: thanks given to Richard Rusa and other artists for graphic designs ....

 @About this Class "GameManager.java":
   ----------------------------------------
 This class manages game in terms of level, scores, and/or any data related to entity on game screen

 */
package com.hehe.soma.manager;


import java.util.HashMap;
import java.util.Locale;
import java.util.Random;

import org.andengine.util.color.Color;

import android.util.SparseArray;


public class GameManager {

	private static GameManager INSTANCE;

	private int gameLevel = 0;
	protected static boolean music_on = true;
	protected static boolean sound_on = true;
	protected static boolean vibration_on = true;
	private boolean isGamePaused=false;
	
	Random r=new Random(System.nanoTime());


	//egg letter variables
	private String CurrentCapitalEggLetter=".";
	private String saidLetter=".";

	private static int max=20;

	private HashMap<String, String> SystemMsg = new HashMap<String, String>();

	private SparseArray<String> eggLetters = new SparseArray<String>();

	private  Color fill[]= {Color.BLUE,Color.CYAN,Color.GREEN,Color.ORANGE,Color.RED,
			Color.BROWN,Color.REDFONT,Color.VIOLET,Color.AMAJU,Color.GAKUBA,Color.SIXBERT,
			Color.MUGARURA,Color.CLARISSE,Color.IRIBAGIZA,Color.RICHARD,Color.RUSA,
			Color.HEHE,Color.IVAN,Color.MUJERE,Color.JURU,Color.GREENFONT};
	
	private  Color CircleColor[]= {Color.CircleColor0,Color.CircleColor1,Color.CircleColor2,
			Color.CircleColor3,Color.CircleColor4,Color.CircleColor5,Color.CircleColor6,Color.CircleColor7};

	private  String LetterList[]= {"a","b","d","e","f","g","h","i","j","l","m",
			"n","r","t","u","A","B","D","E","F","G","H","I","J","L","M","N","R","T","U"};
	private String SpecialLetterList[]={"s","S","p","P","o","O","y","Y","w","W","c","C","z","Z","v","V","k","K"};


	private Color colors[]= {null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null};

	private String object2Paint;
	private String[] agasekeLetters= {"A","S","K","I","F"};
	private String[] radioLetters= {"Z","I","N","G","A","L","O"};
	private String[] agapulisoLetters={"S","O","M","A"};
	private String[] circleLetters={"B","I","N","E","G","O","P"};

	/*dummy constructor--no need to init values...
	 *  constructor does not do anything for this game management*/
	GameManager() {
	}

	//return GameManager object instance in terms of method trying to avoid the "new" everywhere
	public static GameManager getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new GameManager();

		}
		return INSTANCE;
	}




	// Resetting the game simply means we must revert back to initial values.
	public void resetPaintGame() {

		for(int i=0;i<max;i++)
			colors[i]= null;
	}


	public void resetPopGame() {
		saidLetter=" ";
	}

	/*============== setters and  getters=====================*/



	public void setMusic(boolean M_option) {
		music_on = M_option;
	}

	public void setSound(boolean S_option) {
		sound_on = S_option;
	}

	public void setGameLevel(int level) {
		gameLevel = level;
	}

	public void setVibration(boolean V_option) {
		vibration_on = V_option;
	}

	public boolean isMusicEnabled() {
		return music_on;
	}

	public boolean isSoundEnabled() {
		return sound_on;
	}

	public boolean isVibrationEnabled() {
		return vibration_on;
	}

	public int getGameLevel() {
		return gameLevel;
	}

	public void createMessage(String key, String message) {
		SystemMsg.put(key.toLowerCase(Locale.ENGLISH), message);
	}


	public String getMessage(String msgKey) {
		if (SystemMsg.containsKey(msgKey.toLowerCase(Locale.ENGLISH)))
			return SystemMsg.get(msgKey);
		else
			return "ubutumwa nti bwabonetse...";
	}


	public void removeMessage(String MessageKey) {

		if (SystemMsg.containsKey(MessageKey.toLowerCase(Locale.ENGLISH)))
			SystemMsg.remove(MessageKey);

	}

	public void clearMessages() {
		if (!SystemMsg.isEmpty())
			SystemMsg.clear();
	}
	//put on top the object to paint
	public void setObject2Paint(String object) {
		this.object2Paint = object;
	}

	//method to return currently being painted object
	public String getObject2Paint() {
		return object2Paint;
	}

	//method to register each color circle with its specific parameters(color and id)
	public void setPaintCircle(Color color,int id) {
		this.colors[id]=color;
	}

	public void setCircleLetter(int circleID,String letter) {
		this.circleLetters[circleID]=letter;
	}

	//method to return color circle
	public Color getPaintCircleColor(int id) {
		return colors[id];
	}

	//method to return letter associated with color circle
	public String getPaintCircleLetter(int id) {
		return circleLetters[id];
	}





	public void setObjectPartLetter(String object,int partID,String PartLetter) {
		if("agaseke".equalsIgnoreCase(object)) {
			if(partID<agasekeLetters.length) 
				agasekeLetters[partID]=PartLetter;   

		}
		else if("radio".equalsIgnoreCase(object)) {
			if(partID<radioLetters.length) 
				radioLetters[partID]=PartLetter;   

		}
		else if("agapuliso".equalsIgnoreCase(object)) {
			if(partID<agapulisoLetters.length) 
				agapulisoLetters[partID]=PartLetter;   

		}
		else {
			//well this will never happen unless someone tempers with the PaintLetterScene's setObjectPartLetter calling
		}



	}
	public String getObjectPartLetter(String ItemOnTop,int ID) {
		if("agaseke".equalsIgnoreCase(ItemOnTop)&&ID<agasekeLetters.length) {
			return agasekeLetters[ID];
		}
		else if("radio".equalsIgnoreCase(ItemOnTop)&&ID<radioLetters.length)
			return radioLetters[ID];
		else if("agapuliso".equalsIgnoreCase(ItemOnTop)&&ID<agapulisoLetters.length)
			return agapulisoLetters[ID];
		else {
			return "P"; //again this will never happen as I know of
		}

	}

	public String getRandomObjectLetter(int nbr) {

		return circleLetters[nbr];	

	}

	public Color getRandomColor(String context,int random) {
        if("siga".equals(context))
        return CircleColor[random];
        else
		return fill[random];

	}

	public String getRandomCircleLetter(int ListID,int randomIndex) {
		if(ListID==0){
			return LetterList[randomIndex];
		}
		else{
			return SpecialLetterList[randomIndex];
		}

	}

	public void setPopSaidLetter(String letter) {
		saidLetter=letter;
	}

	public String getPopSaidLetter() {

		return saidLetter;
	}


	public void setCurrentCapitalEggLetter(String letter) {

		this.CurrentCapitalEggLetter=letter;
	}

	public void setCurrentLowerCaseEggLetter(String letter,int EggID) {

		this.eggLetters.put(EggID, letter);
	}

	public String getCurrentCapitalEggLetter() {
		return this.CurrentCapitalEggLetter;
	}

	public String getCurrentLowerCaseEggLetter(int targetEgg) {

		return eggLetters.get(targetEgg);

	}

	public void resetEggGame() {
		// TODO Auto-generated method stub

	}

	public boolean isSubMenuUp(){
		
		if(isGamePaused)
			return true;
		else 
			return false;
	}

	public void setPaused(boolean state){

		this.isGamePaused=state;

		
	}
}
