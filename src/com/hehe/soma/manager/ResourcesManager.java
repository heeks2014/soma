/**
 @company: Hehe Ltd
 @Project: Soma
 @Date:
 @Author: Amiri Mugarura and Sixbert Uwiringiyimana
 @Credits: thanks given to Richard Rusa and other artists for graphic designs ....

 @About this Class "ResourcesManager.java":
   ----------------------------------------
 This class is the central manager of any resources to be loaded in game.
 it loads, manages, and assign graphics, audio,fonts or any other resources of the game

 */

package com.hehe.soma.manager;

import java.io.IOException;

import org.andengine.audio.music.Music;
import org.andengine.audio.music.MusicFactory;
import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.debug.Debug;

import android.content.Context;
import android.graphics.Color;

import com.hehe.soma.Soma;


public class ResourcesManager
{
	//---------------------------------------------
	// VARIABLES
	//---------------------------------------------

	private static final ResourcesManager INSTANCE = new ResourcesManager();

	public Engine engine;
	public Soma activity;
	public Context context;
	public Camera camera;
	public VertexBufferObjectManager vbom;




	//---------------------------------------------
	// FONTS TEXTURES & TEXTURE REGIONS
	//---------------------------------------------

	// splash screen resources
	private BitmapTextureAtlas splashTextureAtlas;
	public ITextureRegion splash_region;

	//game mode menu buttons
	private BuildableBitmapTextureAtlas subMenuTextureAtlas;
	public ITextureRegion reset_region;
	public ITextureRegion back_region;
	public ITextureRegion cancel_region,canvas_region,showmenu_region,showwarn_region;

	//==========================================================
	//Soma main menu texture items bng addons + buttons
	private BuildableBitmapTextureAtlas menuTextureAtlas; //main map
	public ITextureRegion menu_background_region;
	public ITextureRegion build_menubutton_region;
	public ITextureRegion egg_menubutton_region;
	public ITextureRegion paint_menubutton_region;
	public ITextureRegion pop_menubutton_region;
	public ITextureRegion options_region;
	public ITextureRegion about_region;
	public ITextureRegion exit_region;
	public ITextureRegion menu_arrow_region;
	public ITextureRegion siga_show_region;
	public ITextureRegion ubaka_show_region;
	public ITextureRegion huza_show_region;
	public ITextureRegion fora_show_region;


	//menu sprite sheet

	//========================================================
	//build letter graphic resources
	private BuildableBitmapTextureAtlas build_0_TextureAtlas;
	private BuildableBitmapTextureAtlas build_1_TextureAtlas;
	private BuildableBitmapTextureAtlas build_2_TextureAtlas;
	private BuildableBitmapTextureAtlas build_3_TextureAtlas;
	public ITextureRegion build_background_region;
	public ITextureRegion build_hangar0_region;
	public ITextureRegion build_hangar1_region;
	public ITextureRegion build_hangar2_region;
	public ITextureRegion build_hangar3_region;
	public ITextureRegion build_hangar4_region;
	public ITextureRegion build_hangar5_region;
	public ITextureRegion build_roof_region;

	public ITextureRegion build_house1_region;


	//letter a parts
	public ITextureRegion build_a_region;
	public ITextureRegion build_a1_region;
	public ITextureRegion build_a2_region;
	public ITextureRegion build_a3_region;
	//letter A parts
	public ITextureRegion build_A_region;
	public ITextureRegion build_A1_region;
	public ITextureRegion build_A2_region;
	public ITextureRegion build_A3_region;

	//letter b parts
	public ITextureRegion build_b_region;
	public ITextureRegion build_b1_region;
	public ITextureRegion build_b2_region;
	public ITextureRegion build_b3_region;
	//letter B parts
	public ITextureRegion build_B_region;
	public ITextureRegion build_B1_region;
	public ITextureRegion build_B2_region;
	public ITextureRegion build_B3_region;
	//letter c parts
	public ITextureRegion build_c_region;
	public ITextureRegion build_c1_region;
	public ITextureRegion build_c2_region;
	public ITextureRegion build_c3_region;
	//letter C parts
	public ITextureRegion build_C_region;
	public ITextureRegion build_C1_region;
	public ITextureRegion build_C2_region;
	public ITextureRegion build_C3_region;
	//letter d parts
	public ITextureRegion build_d_region;
	public ITextureRegion build_d1_region;
	public ITextureRegion build_d2_region;
	public ITextureRegion build_d3_region;
	//letter D parts
	public ITextureRegion build_D_region;
	public ITextureRegion build_D1_region;
	public ITextureRegion build_D2_region;
	public ITextureRegion build_D3_region;
	//letter e parts
	public ITextureRegion build_e_region;
	public ITextureRegion build_e1_region;
	public ITextureRegion build_e2_region;
	public ITextureRegion build_e3_region;
	//letter E parts
	public ITextureRegion build_E_region;
	public ITextureRegion build_E1_region;
	public ITextureRegion build_E2_region;
	public ITextureRegion build_E3_region;
	//letter f parts
	public ITextureRegion build_f_region;
	public ITextureRegion build_f1_region;
	public ITextureRegion build_f2_region;
	public ITextureRegion build_f3_region;
	//letter F parts
	public ITextureRegion build_F_region;
	public ITextureRegion build_F1_region;
	public ITextureRegion build_F2_region;
	public ITextureRegion build_F3_region;

	//letter g parts
	public ITextureRegion build_g_region;
	public ITextureRegion build_g1_region;
	public ITextureRegion build_g2_region;
	public ITextureRegion build_g3_region;
	//letter G parts
	public ITextureRegion build_G_region;
	public ITextureRegion build_G1_region;
	public ITextureRegion build_G2_region;
	public ITextureRegion build_G3_region;
	//letter h parts
	public ITextureRegion build_h_region;
	public ITextureRegion build_h1_region;
	public ITextureRegion build_h2_region;
	public ITextureRegion build_h3_region;
	//letter H parts
	public ITextureRegion build_H_region;
	public ITextureRegion build_H1_region;
	public ITextureRegion build_H2_region;
	public ITextureRegion build_H3_region;


	//letter k parts
	public ITextureRegion build_k_region;
	public ITextureRegion build_k1_region;
	public ITextureRegion build_k2_region;
	public ITextureRegion build_k3_region;
	//letter K parts
	public ITextureRegion build_K_region;
	public ITextureRegion build_K1_region;
	public ITextureRegion build_K2_region;
	public ITextureRegion build_K3_region;
	//letter l parts
	public ITextureRegion build_l_region;
	public ITextureRegion build_l1_region;
	public ITextureRegion build_l2_region;
	public ITextureRegion build_l3_region;
	//letter L parts
	public ITextureRegion build_L_region;
	public ITextureRegion build_L1_region;
	public ITextureRegion build_L2_region;
	public ITextureRegion build_L3_region;
	//letter m parts
	public ITextureRegion build_m_region;
	public ITextureRegion build_m1_region;
	public ITextureRegion build_m2_region;
	public ITextureRegion build_m3_region;
	//letter M parts
	public ITextureRegion build_M_region;
	public ITextureRegion build_M1_region;
	public ITextureRegion build_M2_region;
	public ITextureRegion build_M3_region;

	//letter n parts
	public ITextureRegion build_n_region;
	public ITextureRegion build_n1_region;
	public ITextureRegion build_n2_region;
	public ITextureRegion build_n3_region;
	//letter N parts
	public ITextureRegion build_N_region;
	public ITextureRegion build_N1_region;
	public ITextureRegion build_N2_region;
	public ITextureRegion build_N3_region;
	//letter o parts
	public ITextureRegion build_o_region;
	public ITextureRegion build_o1_region;
	public ITextureRegion build_o2_region;
	public ITextureRegion build_o3_region;
	//letter O parts
	public ITextureRegion build_O_region;
	public ITextureRegion build_O1_region;
	public ITextureRegion build_O2_region;
	public ITextureRegion build_O3_region;

	//letter r parts
	public ITextureRegion build_r_region;
	public ITextureRegion build_r1_region;
	public ITextureRegion build_r2_region;
	public ITextureRegion build_r3_region;
	//letter R parts
	public ITextureRegion build_R_region;
	public ITextureRegion build_R1_region;
	public ITextureRegion build_R2_region;
	public ITextureRegion build_R3_region;
	//letter s parts
	public ITextureRegion build_s_region;
	public ITextureRegion build_s1_region;
	public ITextureRegion build_s2_region;
	public ITextureRegion build_s3_region;
	//letter S parts
	public ITextureRegion build_S_region;
	public ITextureRegion build_S1_region;
	public ITextureRegion build_S2_region;
	public ITextureRegion build_S3_region;

	//letter u parts
	public ITextureRegion build_u_region;
	public ITextureRegion build_u1_region;
	public ITextureRegion build_u2_region;
	public ITextureRegion build_u3_region;
	//letter U parts
	public ITextureRegion build_U_region;
	public ITextureRegion build_U1_region;
	public ITextureRegion build_U2_region;
	public ITextureRegion build_U3_region;

	//letter v parts
	public ITextureRegion build_v_region;
	public ITextureRegion build_v1_region;
	public ITextureRegion build_v2_region;
	public ITextureRegion build_v3_region;
	//letter V parts
	public ITextureRegion build_V_region;
	public ITextureRegion build_V1_region;
	public ITextureRegion build_V2_region;
	public ITextureRegion build_V3_region;
	//letter w parts
	public ITextureRegion build_w_region;
	public ITextureRegion build_w1_region;
	public ITextureRegion build_w2_region;
	public ITextureRegion build_w3_region;
	//letter W parts
	public ITextureRegion build_W_region;
	public ITextureRegion build_W1_region;
	public ITextureRegion build_W2_region;
	public ITextureRegion build_W3_region;

	//letter z parts
	public ITextureRegion build_z_region;
	public ITextureRegion build_z1_region;
	public ITextureRegion build_z2_region;
	public ITextureRegion build_z3_region;
	//letter Z parts
	public ITextureRegion build_Z_region;
	public ITextureRegion build_Z1_region;
	public ITextureRegion build_Z2_region;
	public ITextureRegion build_Z3_region;






	//then sprite sheets
	private BitmapTextureAtlas buildTileBitmapTextureAtlas;
	public TiledTextureRegion build_supernka_region;

	//=========================================================

	//egg letter graphic resources
	private BuildableBitmapTextureAtlas eggTextureAtlas;
	public ITextureRegion egg_background_region;
	public ITextureRegion egg_region,mother_region,balloonEgg_region,car0_region,car1_region,bird,feed_region;


	public BitmapTextureAtlas  eggTileBitmapTextureAtlas;
	public BitmapTextureAtlas  flyTileBitmapTextureAtlas;
	public TiledTextureRegion birdfly_region;
	public TiledTextureRegion crane_region;
	//=========================================================== 
	//paint letter graphic resources
	private BuildableBitmapTextureAtlas paintTextureAtlas;
	public ITextureRegion paint_background_region;
	public ITextureRegion Finger_region;
	public ITextureRegion paint_arrow_region;
	public ITextureRegion RadioPart0_region;
	public ITextureRegion RadioPart1_region;
	public ITextureRegion RadioPart2_region;
	public ITextureRegion RadioPart3_region;
	public ITextureRegion RadioPart4_region;
	public ITextureRegion RadioPart5_region;
	public ITextureRegion RadioPart6_region;

	public ITextureRegion floater0_region;
	public ITextureRegion floater1_region;
	public ITextureRegion floater2_region;
	public ITextureRegion floater3_region;

	public ITextureRegion basket0_region;
	public ITextureRegion basket1_region;
	public ITextureRegion basket2_region;
	public ITextureRegion basket3_region;
	public ITextureRegion basket4_region;

	public ITextureRegion palette_region;
	public ITextureRegion circle_region;
	public BitmapTextureAtlas  SupaNkaTileBitmapTextureAtlas;
	public TiledTextureRegion SupaNka_region;
	//==========================================================

	//pop letter graphic resources

	private BuildableBitmapTextureAtlas popTextureAtlas;
	public ITextureRegion pop_background_region;
	public ITextureRegion pop_cloud_region;
	public ITextureRegion pop_drum_region;
	public ITextureRegion arrowleft_region;
	public ITextureRegion arrowright_region;
	//then sprite sheets
	private BitmapTextureAtlas popTileBitmapTextureAtlas;
	public TiledTextureRegion pop_supernka_region;
	//=============================================================
	//soma option button textures
	private BuildableBitmapTextureAtlas optionTextureAtlas;
	public ITextureRegion option_back_region;
	public ITextureRegion music_button_region;
	public ITextureRegion sound_button_region;
	//public ITextureRegion level_button_region;
	public ITextureRegion vibration_button_region;
	public ITextureRegion nomusic_button_region;
	public ITextureRegion nosound_button_region;
	public ITextureRegion novibration_button_region;
	public ITextureRegion rate_button_region,rate_region;

	//soma about button textures
	private BuildableBitmapTextureAtlas aboutTextureAtlas;
	public ITextureRegion about_back_region;
	public ITextureRegion en_button_region;
	public ITextureRegion fr_button_region;
	public ITextureRegion kin_button_region;
	public ITextureRegion HeHe_Logo_region;
	public ITextureRegion _100Hills_Logo_region;

	//============================================================
	//other resources...fonts, sound and music

	//general font 
	public Font font;
	//option menu font
	public Font optionFont;
	//about scene fonts
	public Font aboutFont;

	//build scene font

	//egg letter font
	public Font Eggfont;

	//paint letter fonts
	public Font CirclepaintFont,ObjectpaintFont;
	//pop letter scene  fonts
	public Font PopFont;

	//declare warn font 
	public Font WarnFont;

	//Soma main menu audio
	public Sound menuButtonSound,subMenuButtonSound;
	public Music menuBackMusic,GamePlayBackMusic;

	//ack and nack

	public Sound ack,nack,ApplauseSound;
	//build letter audio

	public Sound buildClickSound,build_ok,HammerSfx,buildVictory;
	public Music build_guide;
	//egg letter audio
	public Sound eggInstruction,honk,eggAck,eggNack,eggVictory;
	public Music egg_guide;
	//paint letter audio
	public Sound paintDemoSFX,Object0SFX,Object1SFX,Object2SFX,paintok;
	public Music paint_guide;
	//pop letter audio
	public Sound PopLetterSound[]= {null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null};
	public Sound a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,r,s,t,u,v,w,y,z;


	public Sound arrow,drum,popVictory;
	public Music pop_guide;
	//---------------------------------------------
	// CLASS LOGIC
	//---------------------------------------------


	/* methods to load and unload textures ,audio,
	 * and fonts for egg letter are nullified after here*/


	public void loadMenuTextures(){
		menuTextureAtlas.load();
		 
	}

	public void unloadMenuTextures()
	{
		menuTextureAtlas.unload();
		menuButtonSound=null;
		
	}


	public void unloadBuildTextures()
	{
		subMenuTextureAtlas.unload();
		build_0_TextureAtlas.unload();
		build_1_TextureAtlas.unload();
		build_2_TextureAtlas.unload();
		build_3_TextureAtlas.unload();
		buildTileBitmapTextureAtlas.unload();
		
		subMenuButtonSound=null;;
        


		buildClickSound=null;;
		build_ok=null;;
		HammerSfx=null;;
		buildVictory=null;;
		build_guide=null;;

		ack=null;;
		nack=null;;
		ApplauseSound=null;;
		a=null;;
		b=null;;
		c=null;;
		d=null;;
		e=null;;
		f=null;;
		g=null;;
		h=null;;
		i=null;;
		j=null;;
		k=null;;
		l=null;;
		m=null;;
		n=null;;
		o=null;;
		p=null;;
		r=null;;
		s=null;;
		t=null;;
		u=null;;
		v=null;;
		w=null;;
		y=null;;
		z=null;;
		PopLetterSound=null;

		if(font!=null)
			font.unload();
		if(optionFont!=null)
			optionFont.unload();
		if(aboutFont!=null)
			aboutFont.unload();
		if(Eggfont!=null)
			Eggfont.unload();
		if(CirclepaintFont!=null)
			CirclepaintFont.unload();
		if(ObjectpaintFont!=null)
			ObjectpaintFont.unload();
		if(PopFont!=null)
			PopFont.unload();



	}



	public void unloadEggTextures()
	{
		subMenuTextureAtlas.unload();
		eggTextureAtlas.unload();
		eggTileBitmapTextureAtlas.unload();
		flyTileBitmapTextureAtlas.unload();
		subMenuButtonSound=null;;


		eggInstruction=null;;
		honk=null;;
		eggAck=null;;
		eggNack=null;;
		eggVictory=null;;
		egg_guide=null;;
		ack=null;;
		nack=null;;
		ApplauseSound=null;;

		if(font!=null)
			font.unload();
		if(optionFont!=null)
			optionFont.unload();
		if(aboutFont!=null)
			aboutFont.unload();
		if(Eggfont!=null)
			Eggfont.unload();
		if(CirclepaintFont!=null)
			CirclepaintFont.unload();
		if(ObjectpaintFont!=null)
			ObjectpaintFont.unload();
		if(PopFont!=null)
			PopFont.unload();
		a=null;;
		b=null;;
		c=null;;
		d=null;;
		e=null;;
		f=null;;
		g=null;;
		h=null;;
		i=null;;
		j=null;;
		k=null;;
		l=null;;
		m=null;;
		n=null;;
		o=null;;
		p=null;;
		r=null;;
		s=null;;
		t=null;;
		u=null;;
		v=null;;
		w=null;;
		y=null;;
		z=null;;
		PopLetterSound=null;



	}


	public void unloadPaintTextures()
	{
		subMenuTextureAtlas.unload();
		paintTextureAtlas.unload();
		SupaNkaTileBitmapTextureAtlas.unload();
		subMenuButtonSound=null;;


		ack=null;;
		nack=null;;
		ApplauseSound=null;;
		paintDemoSFX=null;;
		Object0SFX=null;;
		Object1SFX=null;;
		Object2SFX=null;;
		paintok=null;;
		paint_guide=null;;

		if(font!=null)
			font.unload();
		if(optionFont!=null)
			optionFont.unload();
		if(aboutFont!=null)
			aboutFont.unload();
		if(Eggfont!=null)
			Eggfont.unload();
		if(CirclepaintFont!=null)
			CirclepaintFont.unload();
		if(ObjectpaintFont!=null)
			ObjectpaintFont.unload();
		if(PopFont!=null)
			PopFont.unload();


	}

	public void unloadPopTextures()
	{
		subMenuTextureAtlas.unload();
		popTextureAtlas.unload();
		popTileBitmapTextureAtlas.unload();
		subMenuButtonSound=null;;


		ack=null;;
		nack=null;;
		ApplauseSound=null;;
		arrow=null;;
		drum=null;;
		popVictory=null;;
		pop_guide=null;;

		if(font!=null)
			font.unload();
		if(optionFont!=null)
			optionFont.unload();
		if(aboutFont!=null)
			aboutFont.unload();
		if(Eggfont!=null)
			Eggfont.unload();
		if(CirclepaintFont!=null)
			CirclepaintFont.unload();
		if(ObjectpaintFont!=null)
			ObjectpaintFont.unload();
		if(PopFont!=null)
			PopFont.unload();
		a=null;;
		b=null;;
		c=null;;
		d=null;;
		e=null;;
		f=null;;
		g=null;;
		h=null;;
		i=null;;
		j=null;;
		k=null;;
		l=null;;
		m=null;;
		n=null;;
		o=null;;
		p=null;;
		r=null;;
		s=null;;
		t=null;;
		u=null;;
		v=null;;
		w=null;;
		y=null;;
		z=null;;
		PopLetterSound=null;

	}




	public void unloadOptionTextures() {

		optionTextureAtlas.unload();
		if(font!=null)
			font.unload();
		if(optionFont!=null)
			optionFont.unload();
		if(aboutFont!=null)
			aboutFont.unload();
		if(Eggfont!=null)
			Eggfont.unload();
		if(CirclepaintFont!=null)
			CirclepaintFont.unload();
		if(ObjectpaintFont!=null)
			ObjectpaintFont.unload();
		if(PopFont!=null)
			PopFont.unload();
	}


	public void unloadAboutTextures() {

		aboutTextureAtlas.unload();
		if(font!=null)
			font.unload();
		if(optionFont!=null)
			optionFont.unload();
		if(aboutFont!=null)
			aboutFont.unload();
		if(Eggfont!=null)
			Eggfont.unload();
		if(CirclepaintFont!=null)
			CirclepaintFont.unload();
		if(ObjectpaintFont!=null)
			ObjectpaintFont.unload();
		if(PopFont!=null)
			PopFont.unload();
	}





	public void loadMenuResources()
	{   

		loadMenuGraphics();
		loadMenuAudio();
		loadFonts();
		loadWarnFont();
	}

	public void loadBuildResources()
	{
		loadBuildLetterGraphics();
		loadSubMenuGraphics();
		loadGeneralAudio();
		loadBuildLetterAudio();
		loadLetterSounds();
		loadFonts();
		loadWarnFont();

	}

	public void loadEggResources()
	{
		loadEggLetterGraphics();
		loadSubMenuGraphics();
		loadGeneralAudio();
		loadEggLetterAudio();
		loadLetterSounds();
		loadEggFonts();
		loadWarnFont();

	}

	public void loadPaintResources()
	{
		loadPaintLetterGraphics();
		loadSubMenuGraphics();
		loadGeneralAudio();
		loadPaintLetterAudio();
		loadpaintFonts();
		loadWarnFont();

	}

	public void loadPopResources()
	{
		loadPopLetterGraphics();
		loadSubMenuGraphics();
		loadGeneralAudio();
		loadLetterSounds();
		loadPopFonts();
		loadWarnFont();

	}

	public void loadOptionResources()
	{
		loadOptionGraphics();
		loadOptionFonts();

	}

	public void loadAboutResources()
	{
		loadAboutGraphics();
		loadAboutFonts();

	}



	private void loadMenuGraphics()
	{
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/menu/");
		menuTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.BILINEAR);
		menu_background_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "menu_background.png");
		build_menubutton_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "Ubaka.png");
		egg_menubutton_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "Huza.png");
		paint_menubutton_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "Siga.png");
		pop_menubutton_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "Fora.png");
		about_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "about.png");
		exit_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "exit.png");
		options_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "options.png");
		siga_show_region=BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "Siga_illustration.png");
		ubaka_show_region=BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "ubaka_illustration.png");
		fora_show_region=BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "Fora_illustration.png");
		huza_show_region=BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "huza_illustration.png");
		menu_arrow_region=BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "arrow.png");
		showwarn_region=BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "warning.png");

		try 
		{
			menuTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
			menuTextureAtlas.load();
		} 
		catch (final TextureAtlasBuilderException e)
		{
			e.printStackTrace();
			Debug.e(" SOMA failed to load Menu  graphic textures::i.e. "+e.getMessage());
		} 

	}


	private void loadSubMenuGraphics(){

		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		subMenuTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(),512,512, TextureOptions.BILINEAR);
		subMenuTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(),512,512, TextureOptions.NEAREST);

		reset_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(subMenuTextureAtlas, activity, "menu_reset.png");
		back_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(subMenuTextureAtlas, activity, "menu_back.png");
		cancel_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(subMenuTextureAtlas, activity, "menu_cancel.png");
		canvas_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(subMenuTextureAtlas, activity, "canvas.png");
		showmenu_region= BitmapTextureAtlasTextureRegionFactory.createFromAsset(subMenuTextureAtlas, activity, "menu.png");
		showwarn_region= BitmapTextureAtlasTextureRegionFactory.createFromAsset(subMenuTextureAtlas, activity, "warning.png");
		try 
		{
			subMenuTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
			subMenuTextureAtlas.load();

		} 
		catch (final Exception e)
		{
			e.printStackTrace();
			Debug.e(" SOMA failed to load build letter graphic textures::i.e. "+e.getMessage());
		}  

	}
	private void loadBuildLetterGraphics()
	{
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/build/");
		build_0_TextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.BILINEAR);
		build_0_TextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.NEAREST);

		build_1_TextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.BILINEAR);
		build_1_TextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.NEAREST);

		build_2_TextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.BILINEAR);
		build_2_TextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.NEAREST);

		build_3_TextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.BILINEAR);
		build_3_TextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.NEAREST);

		build_background_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_0_TextureAtlas, activity, "build_background.png");

		build_hangar0_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_0_TextureAtlas, activity, "hangar0.png");
		build_hangar1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_0_TextureAtlas, activity, "hangar1.png");
		build_hangar2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_0_TextureAtlas, activity, "hangar2.png");
		build_hangar3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_0_TextureAtlas, activity, "hangar3.png");
		build_hangar4_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_0_TextureAtlas, activity, "hangar4.png");
		build_hangar5_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_0_TextureAtlas, activity, "hangar5.png");
		build_roof_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_0_TextureAtlas, activity, "roof.png");
		build_house1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_0_TextureAtlas, activity, "house1.png");
		Finger_region=BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_0_TextureAtlas, activity, "finger.png");	



		build_a_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_0_TextureAtlas, activity, "a.png");
		build_a1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_0_TextureAtlas, activity, "a1.png");
		build_a2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_0_TextureAtlas, activity, "a2.png");
		build_a3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_0_TextureAtlas, activity, "a3.png");

		build_A_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_0_TextureAtlas, activity, "amaj.png");
		build_A1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_0_TextureAtlas, activity, "amaj1.png");
		build_A2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_0_TextureAtlas, activity, "amaj2.png");
		build_A3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_0_TextureAtlas, activity, "amaj3.png");

		build_b_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "b.png");
		build_b1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "b1.png");
		build_b2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "b2.png");
		build_b3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "b3.png");

		build_B_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "Bmaj.png");
		build_B1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "Bmaj1.png");
		build_B2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "Bmaj2.png");
		build_B3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "Bmaj3.png");

		build_c_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "c.png");
		build_c1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "c1.png");
		build_c2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "c2.png");
		build_c3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "c3.png");

		build_C_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "cmaj.png");
		build_C1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "cmaj1.png");
		build_C2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "cmaj2.png");
		build_C3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "cmaj3.png");

		build_d_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "d.png");
		build_d1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "d1.png");
		build_d2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "d2.png");
		build_d3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "d3.png");

		build_D_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "dmaj.png");
		build_D1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "dmaj1.png");
		build_D2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "dmaj2.png");
		build_D3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "dmaj3.png");

		build_e_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "e.png");
		build_e1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "e1.png");
		build_e2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "e2.png");
		build_e3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "e3.png");

		build_E_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "emaj.png");
		build_E1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "emaj1.png");
		build_E2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "emaj2.png");
		build_E3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "emaj3.png");

		build_f_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "f.png");
		build_f1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "f1.png");
		build_f2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "f2.png");
		build_f3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "f3.png");

		build_F_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "fmaj.png");
		build_F1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "fmaj1.png");
		build_F2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "fmaj2.png");
		build_F3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "fmaj3.png");


		build_g_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "g.png");
		build_g1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "g1.png");
		build_g2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "g2.png");
		build_g3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "g3.png");

		build_G_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "gmaj.png");
		build_G1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "gmaj1.png");
		build_G2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "gmaj2.png");
		build_G3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "gmaj3.png");


		build_h_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "h.png");
		build_h1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "h1.png");
		build_h2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "h2.png");
		build_h3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "h3.png");

		build_H_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "hmaj.png");
		build_H1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "hmaj1.png");
		build_H2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "hmaj2.png");
		build_H3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "hmaj3.png");


		build_k_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "k.png");
		build_k1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "k1.png");
		build_k2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "k2.png");
		build_k3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "k3.png");

		build_K_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "kmaj.png");
		build_K1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "kmaj1.png");
		build_K2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "kmaj2.png");
		build_K3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "kmaj3.png");

		build_l_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "l.png");
		build_l1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "l1.png");
		build_l2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "l2.png");
		build_l3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "l3.png");

		build_L_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "lmaj.png");
		build_L1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "lmaj1.png");
		build_L2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "lmaj2.png");
		build_L3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_1_TextureAtlas, activity, "lmaj3.png");

		//
		build_m_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "m.png");
		build_m1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "m1.png");
		build_m2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "m2.png");
		build_m3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "m3.png");

		build_M_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "Mmaj.png");
		build_M1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "Mmaj1.png");
		build_M2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "Mmaj2.png");
		build_M3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "Mmaj3.png");


		build_n_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "n.png");
		build_n1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "n1.png");
		build_n2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "n2.png");
		build_n3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "n3.png");

		build_N_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "Nmaj.png");
		build_N1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "Nmaj1.png");
		build_N2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "Nmaj2.png");
		build_N3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "Nmaj3.png");

		build_o_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "o.png");
		build_o1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "o1.png");
		build_o2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "o2.png");
		build_o3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "o3.png");

		build_O_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "omaj.png");
		build_O1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "omaj1.png");
		build_O2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "omaj2.png");
		build_O3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "omaj3.png");


		build_r_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "r.png");
		build_r1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "r1.png");
		build_r2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "r2.png");
		build_r3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "r3.png");

		build_R_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "rmaj.png");
		build_R1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "rmaj1.png");
		build_R2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "rmaj2.png");
		build_R3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "rmaj3.png");

		build_s_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "s.png");
		build_s1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "s1.png");
		build_s2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "s2.png");
		build_s3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_2_TextureAtlas, activity, "s3.png");

		build_S_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "smaj.png");
		build_S1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "smaj1.png");
		build_S2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "smaj2.png");
		build_S3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "smaj3.png");

		build_u_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "u.png");
		build_u1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "u1.png");
		build_u2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "u2.png");
		build_u3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "u3.png");

		build_U_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "umaj.png");
		build_U1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "umaj1.png");
		build_U2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "umaj2.png");
		build_U3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "umaj3.png");

		build_v_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "v.png");
		build_v1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "v1.png");
		build_v2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "v2.png");
		build_v3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "v3.png");

		build_V_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "vmaj.png");
		build_V1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "vmaj1.png");
		build_V2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "vmaj2.png");
		build_V3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "vmaj3.png");

		build_w_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "w.png");
		build_w1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "w1.png");
		build_w2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "w2.png");
		build_w3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "w3.png");

		build_W_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "wmaj.png");
		build_W1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "wmaj1.png");
		build_W2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "wmaj2.png");
		build_W3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "wmaj3.png");

		build_z_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "z.png");
		build_z1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "z1.png");
		build_z2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "z2.png");
		build_z3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "z3.png");

		build_Z_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "zmaj.png");
		build_Z1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "zmaj1.png");
		build_Z2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "zmaj2.png");
		build_Z3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(build_3_TextureAtlas, activity, "zmaj3.png");


		buildTileBitmapTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.BILINEAR);
		build_supernka_region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(buildTileBitmapTextureAtlas,activity, "superinka.png", 0, 0, 5, 2);


		try 
		{
			build_0_TextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
			build_0_TextureAtlas.load();

			build_1_TextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
			build_1_TextureAtlas.load();

			build_2_TextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
			build_2_TextureAtlas.load();

			build_3_TextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
			build_3_TextureAtlas.load();

			engine.getTextureManager().loadTexture(buildTileBitmapTextureAtlas);
		} 
		catch (final Exception e)
		{
			e.printStackTrace();
			Debug.e(" SOMA failed to load build letter graphic textures::i.e. "+e.getMessage());
		}  

	}

	private void loadEggLetterGraphics(){

		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/egg/");
		eggTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.NEAREST);
		eggTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.BILINEAR);
		egg_background_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(eggTextureAtlas, activity, "egg_background.png");
		egg_region= BitmapTextureAtlasTextureRegionFactory.createFromAsset(eggTextureAtlas, activity, "egg.png");
		balloonEgg_region= BitmapTextureAtlasTextureRegionFactory.createFromAsset(eggTextureAtlas, activity, "balloon.png");
		mother_region= BitmapTextureAtlasTextureRegionFactory.createFromAsset(eggTextureAtlas, activity, "mother.png");
		car0_region= BitmapTextureAtlasTextureRegionFactory.createFromAsset(eggTextureAtlas, activity, "car0.png");
		car1_region= BitmapTextureAtlasTextureRegionFactory.createFromAsset(eggTextureAtlas, activity, "car1.png");
		bird= BitmapTextureAtlasTextureRegionFactory.createFromAsset(eggTextureAtlas, activity, "bird.png");
		feed_region=BitmapTextureAtlasTextureRegionFactory.createFromAsset(eggTextureAtlas, activity, "feed.png");

		eggTileBitmapTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.BILINEAR);
		flyTileBitmapTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.BILINEAR);

		birdfly_region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(flyTileBitmapTextureAtlas,activity, "flybird.png", 0, 0, 6, 1);
		crane_region=BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(eggTileBitmapTextureAtlas,activity, "crane.png", 0, 0, 10, 1);

		try 
		{

			eggTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
			eggTextureAtlas.load();
			engine.getTextureManager().loadTexture(eggTileBitmapTextureAtlas);
			engine.getTextureManager().loadTexture(flyTileBitmapTextureAtlas);
		} 
		catch (final Exception e)
		{
			e.printStackTrace();
			Debug.e(" SOMA failed to load egg letter graphic textures::i.e. "+e.getMessage());
		}  
	}

	private void loadPaintLetterGraphics()
	{
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/paint/");
		paintTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.NEAREST);
		paintTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.BILINEAR);
		paint_background_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(paintTextureAtlas, activity, "paint_background.png");
		Finger_region=BitmapTextureAtlasTextureRegionFactory.createFromAsset(paintTextureAtlas, activity, "finger.png");
		paint_arrow_region=BitmapTextureAtlasTextureRegionFactory.createFromAsset(paintTextureAtlas, activity, "arrow.png");;
		RadioPart0_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(paintTextureAtlas, activity, "front.png");
		RadioPart1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(paintTextureAtlas, activity, "leftspeaker.png");
		RadioPart2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(paintTextureAtlas, activity, "rightspeaker.png");
		RadioPart3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(paintTextureAtlas, activity, "dial.png");
		RadioPart4_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(paintTextureAtlas, activity, "tapedrive.png");
		RadioPart5_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(paintTextureAtlas, activity, "top.png");
		RadioPart6_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(paintTextureAtlas, activity, "right.png");

		//
		floater0_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(paintTextureAtlas, activity, "floater0.png");
		floater1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(paintTextureAtlas, activity, "floater1.png");
		floater2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(paintTextureAtlas, activity, "floater2.png");
		floater3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(paintTextureAtlas, activity, "floater3.png");

		basket0_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(paintTextureAtlas, activity, "bask0.png");
		basket1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(paintTextureAtlas, activity, "bask1.png");
		basket2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(paintTextureAtlas, activity, "bask2.png");
		basket3_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(paintTextureAtlas, activity, "bask3.png");
		basket4_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(paintTextureAtlas, activity, "bask4.png");
		palette_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(paintTextureAtlas, activity, "palette.png");
		circle_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(paintTextureAtlas, activity, "circle.png");


		SupaNkaTileBitmapTextureAtlas= new BitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.BILINEAR);
		SupaNka_region= BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(SupaNkaTileBitmapTextureAtlas,activity, "paint_superinka.png", 0, 0, 3, 2);
		try 
		{
			paintTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
			paintTextureAtlas.load();
			engine.getTextureManager().loadTexture(SupaNkaTileBitmapTextureAtlas);

		} 
		catch (final Exception e)
		{
			e.printStackTrace();
			Debug.e(" SOMA failed to load paint letter graphic textures::i.e. "+e.getMessage());
		}  

	}


	private void loadWarnFont(){

		try{
			FontFactory.setAssetBasePath("fonts/");
			final ITexture mainFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

			WarnFont = FontFactory.createFromAsset(activity.getFontManager(), mainFontTexture, activity.getAssets(), "rockout.ttf",22, true, Color.WHITE);
			WarnFont.load();
		}
		catch(Exception exc)
		{
			exc.printStackTrace();
			Debug.e(" SOMA failed to load about's fonts from "+FontFactory.getAssetBasePath()+"/fonts/ ::i.e. "+exc.getMessage());
		}
	}
	
	private void loadPopLetterGraphics()
	{
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/pop/");
		popTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.NEAREST);
		popTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.BILINEAR);
		pop_background_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(popTextureAtlas, activity, "pop_background.png");
		pop_cloud_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(popTextureAtlas, activity, "cloud.png");
		pop_drum_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(popTextureAtlas, activity, "drum.png");
		arrowleft_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(popTextureAtlas, activity, "arrowleft.png");
		arrowright_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(popTextureAtlas, activity, "arrowright.png");
		popTileBitmapTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.BILINEAR);
		pop_supernka_region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(popTileBitmapTextureAtlas,activity, "pop_superinka.png", 0, 0, 3, 2);


		try 
		{
			popTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
			popTextureAtlas.load();
			engine.getTextureManager().loadTexture(popTileBitmapTextureAtlas);

		} 
		catch (final Exception e)
		{
			e.printStackTrace();
			Debug.e(" SOMA failed to load pop letter graphic textures::i.e. "+e.getMessage());
		}  
	}


	public void loadOptionGraphics() {

		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/option/");
		optionTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.BILINEAR);
		music_button_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(optionTextureAtlas, activity, "music.png");
		option_back_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(optionTextureAtlas, activity, "option_background.png");
		sound_button_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(optionTextureAtlas, activity, "sound.png");
		nomusic_button_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(optionTextureAtlas, activity, "nomusic.png");
		nosound_button_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(optionTextureAtlas, activity, "nosound.png");
		novibration_button_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(optionTextureAtlas, activity,"novibration.png");
		//level_button_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(optionTextureAtlas, activity, "level.png");
		vibration_button_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(optionTextureAtlas, activity,"vibration.png");
		rate_button_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(optionTextureAtlas, activity, "rating.png");
		canvas_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(optionTextureAtlas, activity, "canvas.png");
		rate_region=BitmapTextureAtlasTextureRegionFactory.createFromAsset(optionTextureAtlas, activity, "rate.png");


		try 
		{
			optionTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
			optionTextureAtlas.load();
		} 
		catch (final TextureAtlasBuilderException e)
		{
			e.printStackTrace();
			Debug.e(" Damn...SOMA failed to load options menu::i.e. "+e.getMessage());
		}  

	}



	public void loadAboutGraphics() {

		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/about/");
		aboutTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.BILINEAR);
		about_back_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(aboutTextureAtlas, activity, "aboutback.png");
		en_button_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(aboutTextureAtlas, activity, "eng.png");
		fr_button_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(aboutTextureAtlas, activity, "fr.png");
		kin_button_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(aboutTextureAtlas, activity, "kinya.png");
		HeHe_Logo_region=BitmapTextureAtlasTextureRegionFactory.createFromAsset(aboutTextureAtlas, activity, "logo.png");
		_100Hills_Logo_region=BitmapTextureAtlasTextureRegionFactory.createFromAsset(aboutTextureAtlas, activity, "thl_logo.png");
		try 
		{
			aboutTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
			aboutTextureAtlas.load();
		} 
		catch (final TextureAtlasBuilderException e)
		{
			e.printStackTrace();
			Debug.e(" Damn...SOMA failed to load about resources::i.e. "+e.getMessage());
		}  

	}





	private void loadFonts()
	{   
		try{
			FontFactory.setAssetBasePath("fonts/");
			final ITexture mainFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

			font = FontFactory.createStrokeFromAsset(activity.getFontManager(), mainFontTexture, activity.getAssets(), "comic.ttf",60, true, Color.WHITE,3, Color.BLUE);
			font.load();
		}
		catch(Exception exc)
		{
			exc.printStackTrace();
			Debug.e(" SOMA failed to load fonts from "+FontFactory.getAssetBasePath()+"/fonts/ ::i.e. "+exc.getMessage());
		}
	}

	private void loadEggFonts() {

		try{
			FontFactory.setAssetBasePath("fonts/");
			final ITexture mainFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 512, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

			Eggfont = FontFactory.createStrokeFromAsset(activity.getFontManager(), mainFontTexture, activity.getAssets(), "comic.ttf", 50, true, Color.WHITE, 1, Color.BLUE);
			Eggfont.load();
		}
		catch(Exception exc)
		{
			exc.printStackTrace();
			Debug.e(" SOMA failed to load fonts from "+FontFactory.getAssetBasePath()+"/fonts/ ::i.e. "+exc.getMessage());
		}

	}


	private void loadOptionFonts()
	{   
		try{
			FontFactory.setAssetBasePath("fonts/");
			final ITexture mainFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

			optionFont = FontFactory.createFromAsset(activity.getFontManager(), mainFontTexture, activity.getAssets(), "about.ttf",30, true, Color.WHITE);
			optionFont.load();
		}
		catch(Exception exc)
		{
			exc.printStackTrace();
			Debug.e(" SOMA failed to load option's fonts from "+FontFactory.getAssetBasePath()+"/fonts/ ::i.e. "+exc.getMessage());
		}
	}

	private void loadAboutFonts()
	{   
		try{
			FontFactory.setAssetBasePath("fonts/");
			final ITexture mainFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

			aboutFont = FontFactory.createFromAsset(activity.getFontManager(), mainFontTexture, activity.getAssets(), "about.ttf",22, true, Color.WHITE);
			aboutFont.load();
		}
		catch(Exception exc)
		{
			exc.printStackTrace();
			Debug.e(" SOMA failed to load about's fonts from "+FontFactory.getAssetBasePath()+"/fonts/ ::i.e. "+exc.getMessage());
		}
	}

	private void loadpaintFonts()
	{   
		try{
			FontFactory.setAssetBasePath("fonts/");
			final ITexture paintCircleFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 512, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			final ITexture paintObjectFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 512, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

			CirclepaintFont = FontFactory.createFromAsset(activity.getFontManager(), paintCircleFontTexture, activity.getAssets(), "comic.ttf",35, true, Color.WHITE);
			CirclepaintFont.load();

			ObjectpaintFont = FontFactory.createFromAsset(activity.getFontManager(), paintObjectFontTexture, activity.getAssets(), "comic.ttf",35, true,Color.rgb(207,149,28));
			ObjectpaintFont.load();

		}
		catch(Exception exc)
		{
			exc.printStackTrace();
			Debug.e(" SOMA failed to load fonts from "+FontFactory.getAssetBasePath()+"/fonts/ ::i.e. "+exc.getMessage());
		}
	}


	private void loadPopFonts()
	{   
		try{
			FontFactory.setAssetBasePath("fonts/");

			final ITexture mainFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

			PopFont = FontFactory.createStrokeFromAsset(activity.getFontManager(), mainFontTexture, activity.getAssets(),  "rockout.ttf",50, true, Color.WHITE,3, Color.GREEN);
			PopFont.load();		
		}
		catch(Exception exc)
		{
			exc.printStackTrace();
			Debug.e(" SOMA failed to load fonts from "+FontFactory.getAssetBasePath()+"/fonts/ ::i.e. "+exc.getMessage());
		}
	}





	private void loadMenuAudio()
	{
		SoundFactory.setAssetBasePath("sfx/menu/");
		MusicFactory.setAssetBasePath("sfx/menu/");
		// Load our "sound" file into a Sound object
		try {
			menuButtonSound = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity, "menu_button_sound.ogg");

		} catch (IOException e) {
			e.printStackTrace();
			Debug.e(" SOMA failed to load Menu sound audio: i.e. "+e.getMessage());
		}
		// Load our "music" file into a music object
		try {
			menuBackMusic = MusicFactory.createMusicFromAsset(engine.getMusicManager(), activity, "menu_back_music.ogg");
		} catch (IOException e) {
			e.printStackTrace();
			Debug.e(" SOMA failed to load Menu music audio::i.e. "+e.getMessage());
		}

	}

	private void loadGeneralAudio() {


		SoundFactory.setAssetBasePath("sfx/");
		MusicFactory.setAssetBasePath("sfx/");

		//other sound objects go here
		try {
			ack = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity, "good.ogg");
			nack=SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity, "bad.ogg");
			ApplauseSound=SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity, "applause.ogg");
			subMenuButtonSound=SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity, "submenubutton.ogg");

		} catch (IOException e) {
			e.printStackTrace();
		}	

		try {
			GamePlayBackMusic = MusicFactory.createMusicFromAsset(engine.getMusicManager(), activity, "background_music.ogg");
		} catch (IOException e) {
			e.printStackTrace();
			Debug.e(" SOMA failed to load background music audio::i.e. "+e.getMessage());
		}
	}


	private void loadBuildLetterAudio()
	{
		SoundFactory.setAssetBasePath("sfx/build/");
		MusicFactory.setAssetBasePath("sfx/build/");
		// Load our build letter scene sound  files into a Sound object

		//other sound objects go here
		try {

			build_ok=SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity, "ok.ogg");
			HammerSfx=SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity, "hummer.ogg");
			buildVictory = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity, "buildvictory.ogg");

		} catch (IOException e) {
			e.printStackTrace();
		}
		// Load our "music" file into a music object
		try {

			build_guide = MusicFactory.createMusicFromAsset(engine.getMusicManager(), activity, "build_guidance.ogg");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void loadEggLetterAudio()
	{
		SoundFactory.setAssetBasePath("sfx/egg/");
		MusicFactory.setAssetBasePath("sfx/egg/");
		// Load our egg letter scene file into a Sound object
		try {

			eggInstruction = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity, "touch_egg.ogg");
			honk = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity, "horn.ogg");
			eggVictory = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity, "eggvictory.ogg");
			eggAck=SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity, "eggack.ogg");
			eggNack=SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity, "eggnack.ogg");
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Load our "music" file into a music object...this is typically for background music
		try {
			egg_guide = MusicFactory.createMusicFromAsset(engine.getMusicManager(), activity, "egg_guidance.ogg");

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void loadPaintLetterAudio()
	{
		SoundFactory.setAssetBasePath("sfx/paint/");
		MusicFactory.setAssetBasePath("sfx/paint/");

		// Load our "music" file into a music object
		try {	
			paintDemoSFX=SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"aftersigademo.ogg");
			Object0SFX=SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"object0.ogg");
			Object1SFX=SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"object1.ogg");
			Object2SFX=SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"object2.ogg");
			paintok = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity, "paintok.ogg");
			paint_guide = MusicFactory.createMusicFromAsset(engine.getMusicManager(), activity, "paint_guidance.ogg");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void loadLetterSounds()
	{
		SoundFactory.setAssetBasePath("sfx/pop/");
		MusicFactory.setAssetBasePath("sfx/pop/");
		// Load our pop letter scene sound files into a Sound object
		try {

			arrow = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"arrowsound.ogg");
			drum = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"drum.ogg");
			popVictory = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"foravictory.ogg");
			a = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"a.ogg");
			PopLetterSound[0]=a;
			b = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"b.ogg");
			PopLetterSound[1]=b;
			c = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"c.ogg");
			PopLetterSound[2]=c;
			d = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"d.ogg");
			PopLetterSound[3]=d;
			e = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"e.ogg");
			PopLetterSound[4]=e;
			f = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"f.ogg");
			PopLetterSound[5]=f;
			g = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"g.ogg");
			PopLetterSound[6]=g;
			h = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"h.ogg");
			PopLetterSound[7]=h;
			i = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"i.ogg");
			PopLetterSound[8]=i;
			j = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"j.ogg");
			PopLetterSound[9]=j;
			k = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"k.ogg");
			PopLetterSound[10]=k;
			l = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"l.ogg");
			PopLetterSound[11]=l;
			m = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"m.ogg");
			PopLetterSound[12]=m;
			n = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"n.ogg");
			PopLetterSound[13]=n;
			o = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"o.ogg");
			PopLetterSound[14]=o;
			p = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"p.ogg");
			PopLetterSound[15]=p;
			r = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"r.ogg");
			PopLetterSound[16]=r;
			s = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"s.ogg");
			PopLetterSound[17]=s;
			t = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"t.ogg");
			PopLetterSound[18]=t;
			u = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"u.ogg");
			PopLetterSound[19]=u;
			v = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"v.ogg");
			PopLetterSound[20]=v;
			w = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"w.ogg");
			PopLetterSound[21]=w;
			y = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"y.ogg");
			PopLetterSound[22]=y;
			z = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity,"z.ogg");
			PopLetterSound[23]=z;


		} catch (IOException e) {
			e.printStackTrace();
			Debug.d("The resources of letter audio may be missing in this location: "+SoundFactory.getAssetBasePath()+"\n\n"+e.getMessage());
		}
		// Load our "music" file() into a music object
		try {
			pop_guide = MusicFactory.createMusicFromAsset(engine.getMusicManager(), activity, "pop_guidance.ogg");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}


	//load splash screen texture
	public void loadSplashScreen()
	{
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		splashTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(),1024,1024, TextureOptions.BILINEAR);
		splash_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(splashTextureAtlas, activity, "splash.png", 0, 0);
		splashTextureAtlas.load();
	}

	//method to release memory used by Splash texture
	public void unloadSplashScreen()
	{
		splashTextureAtlas.unload();
		splash_region = null;
	}

	/*
	 * @param engine
	 * @param activity
	 * @param camera
	 * @param vbom
	 * We use this method at beginning of game loading, to prepare Resources Manager properly,
	 * setting all needed parameters, so we can latter access them from different classes (eg. scenes)
	 */
	public static void prepareManager(Engine engine, Soma activity, Camera camera, VertexBufferObjectManager vbom,Context Somacontext)
	{
		getInstance().engine = engine;
		getInstance().activity = activity;
		getInstance().context = Somacontext;
		getInstance().camera = camera;
		getInstance().vbom = vbom;
	}

	//---------------------------------------------
	// GETTERS AND SETTERS
	//---------------------------------------------
	//we will return ResourceManager Object instance using this method
	public static ResourcesManager getInstance()
	{
		return INSTANCE;
	}


}
