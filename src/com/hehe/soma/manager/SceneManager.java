/**
 @company: Hehe Ltd
 @Project: Soma
 @Date:
 @Author: Amiri Mugarura and Sixbert Uwiringiyimana
 @Credits: thanks given to Richard Rusa and other artists for graphic designs ....

 @About this Class "SceneManager.java":
   ----------------------------------------
   This class is responsible for managing the flow between scenes....( by creating and disposing menus and game modes)
   it handles Soma scene from as soon as the splash screen  
   starts to when the user exits the game.
 */


package com.hehe.soma.manager;

import org.andengine.engine.Engine;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.ui.IGameInterface.OnCreateSceneCallback;

import com.hehe.soma.scene.AboutScene;
import com.hehe.soma.scene.BuildLetterScene;
import com.hehe.soma.scene.EggLetterScene;
import com.hehe.soma.scene.LoadingScene;
import com.hehe.soma.scene.MainMenuScene;
import com.hehe.soma.scene.OptionScene;
import com.hehe.soma.scene.PaintLetterScene;
import com.hehe.soma.scene.ParentScene;
import com.hehe.soma.scene.PopLetterScene;

public class SceneManager
{
	//---------------------------------------------
	// SCENES
	//---------------------------------------------

	private ParentScene splashScene;
	private ParentScene menuScene;
	private ParentScene buildGameScene;
	private ParentScene eggGameScene;
	private ParentScene paintGameScene;
	private ParentScene popGameScene;
	private ParentScene loadingScene;
	private ParentScene optionScene;
	private ParentScene aboutScene;

	//---------------------------------------------
	// VARIABLES
	//---------------------------------------------

	private static final SceneManager INSTANCE = new SceneManager();

	private SceneType currentSceneType = SceneType.SCENE_SPLASH;

	private ParentScene currentScene;

	private Engine engine = ResourcesManager.getInstance().engine;

	public enum SceneType
	{
		SCENE_SPLASH,
		SCENE_MENU,
		BUILD_SCENE_GAME,
		EGG_SCENE_GAME,
		PAINT_SCENE_GAME,
		POP_SCENE_GAME,
		SCENE_LOADING,
		SCENE_OPTION,
		SCENE_ABOUT,
	}

	//---------------------------------------------
	// CLASS LOGIC
	//---------------------------------------------

	//set current scene to which camera is focused
	public void setScene(ParentScene scene)
	{
		engine.setScene(scene);
		currentScene = scene;
		currentSceneType = scene.getSceneType();
	}

	public void setScene(SceneType sceneType)
	{
		switch (sceneType)
		{  
		case SCENE_SPLASH:
			setScene(splashScene);
			break;
		case SCENE_LOADING:
			setScene(loadingScene);
			break;
		case SCENE_MENU:
			setScene(menuScene);
			break;
		case BUILD_SCENE_GAME:
			setScene(buildGameScene);
			break;
		case EGG_SCENE_GAME:
			setScene(eggGameScene);
			break;
		case PAINT_SCENE_GAME:
			setScene(paintGameScene);
			break;
		case POP_SCENE_GAME:
			setScene(popGameScene);
			break;
		case SCENE_OPTION:
			setScene(optionScene);
			break;
		case SCENE_ABOUT:
			setScene(aboutScene);
			break;
		default:
			break;
		}
	}

	//---------------------------------------------
	// GETTERS AND SETTERS
	//---------------------------------------------

	public static SceneManager getInstance()
	{
		return INSTANCE;
	}

	public SceneType getCurrentSceneType()
	{
		return currentSceneType;
	}

	public ParentScene getCurrentScene()
	{
		return currentScene;
	}


	//METHODS

	public void createSplashScene(OnCreateSceneCallback pOnCreateSceneCallback)
	{
		ResourcesManager.getInstance().loadSplashScreen();
		splashScene = new com.hehe.soma.scene.SplashScene();
		currentScene = splashScene;
		pOnCreateSceneCallback.onCreateSceneFinished(splashScene);

	}




	public void createMenuScene()
	{
		ResourcesManager.getInstance().loadMenuResources();
		menuScene = new MainMenuScene();
		loadingScene = new LoadingScene();
		SceneManager.getInstance().setScene(menuScene);
		disposeSplashScene();

	}

	public void createBuildLetterScene()
	{
		ResourcesManager.getInstance().loadBuildResources();
		buildGameScene = new BuildLetterScene();
		setScene(buildGameScene);
		//unload menu scene
		disposeMenuScene();
	}

	public void createEggLetterScene()
	{
		ResourcesManager.getInstance().loadEggResources();
		eggGameScene = new EggLetterScene();
		setScene(eggGameScene);
		//unload menu scene
		disposeMenuScene();
	}

	public void createPaintLetterScene()
	{
		ResourcesManager.getInstance().loadPaintResources();
		paintGameScene = new PaintLetterScene();
		setScene(paintGameScene);
		disposeMenuScene();
	}
	public void createPopLetterScene()
	{
		ResourcesManager.getInstance().loadPopResources();
		popGameScene = new PopLetterScene();
		setScene(popGameScene);
		disposeMenuScene();
	}

	public void createOptionScene()
	{
		ResourcesManager.getInstance().loadOptionResources();
		optionScene = new OptionScene();
		setScene(optionScene);
		//disposeMenuScene();
	}

	public void createAboutScene()
	{
		ResourcesManager.getInstance().loadAboutResources();
		aboutScene = new AboutScene();
		setScene(aboutScene);
		//disposeMenuScene();
	}

	//when we go back to main menu, dispose current game mode
	public void loadMenuScene(final Engine mEngine)
	{   
		setScene(loadingScene);
		if(buildGameScene!=null)
			disposeBuildScene();

		if(eggGameScene!=null)
			disposeEggScene();

		if(paintGameScene!=null)
			disposePaintScene();


		if(popGameScene!=null)
			disposePopScene();

		if(optionScene!=null)
			disposeOptionScene();

		mEngine.registerUpdateHandler(new TimerHandler(0.71f, new ITimerCallback() 
		{
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler) 
			{
				mEngine.unregisterUpdateHandler(pTimerHandler);
				ResourcesManager.getInstance().loadMenuTextures();
				setScene(menuScene);
			}
		}));
	}

	public void loadBuildScene(final Engine mEngine)
	{
		setScene(loadingScene);
		ResourcesManager.getInstance().unloadMenuTextures();
		mEngine.registerUpdateHandler(new TimerHandler(0.71f, new ITimerCallback() 
		{
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler) 
			{
				mEngine.unregisterUpdateHandler(pTimerHandler);
				ResourcesManager.getInstance().loadBuildResources();
				BuildLetterScene build = new BuildLetterScene();
				setScene(build);
			}
		}));
	}

	public void loadEggScene(final Engine mEngine)
	{
		setScene(loadingScene);
		ResourcesManager.getInstance().unloadMenuTextures();
		mEngine.registerUpdateHandler(new TimerHandler(0.71f, new ITimerCallback() 
		{
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler) 
			{
				mEngine.unregisterUpdateHandler(pTimerHandler);
				ResourcesManager.getInstance().loadEggResources();
				EggLetterScene egg = new EggLetterScene();
				setScene(egg);
			}
		}));
	}

	public void loadPaintScene(final Engine mEngine)
	{
		setScene(loadingScene);
		ResourcesManager.getInstance().unloadMenuTextures();
		mEngine.registerUpdateHandler(new TimerHandler(1f, new ITimerCallback() 
		{
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler) 
			{
				mEngine.unregisterUpdateHandler(pTimerHandler);
				ResourcesManager.getInstance().loadPaintResources();
				PaintLetterScene paint = new PaintLetterScene();
				setScene(paint);
			}
		}));
	}

	public void loadPopScene(final Engine mEngine)
	{
		setScene(loadingScene);
		ResourcesManager.getInstance().unloadMenuTextures();
		mEngine.registerUpdateHandler(new TimerHandler(0.71f, new ITimerCallback() 
		{
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler) 
			{
				mEngine.unregisterUpdateHandler(pTimerHandler);
				ResourcesManager.getInstance().loadPopResources();
				PopLetterScene pop = new PopLetterScene();
				setScene(pop);
			}
		}));
	}

	public void loadOptionScene(final Engine mEngine)
	{
		//setScene(loadingScene);
		ResourcesManager.getInstance().unloadMenuTextures();
		mEngine.registerUpdateHandler(new TimerHandler(0.1f, new ITimerCallback() 
		{
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler) 
			{
				mEngine.unregisterUpdateHandler(pTimerHandler);
				ResourcesManager.getInstance().loadOptionResources();
				OptionScene option = new OptionScene();
				setScene(option);
			}
		}));
	}

	public void loadAboutScene(final Engine mEngine)
	{
		//setScene(loadingScene);
		ResourcesManager.getInstance().unloadMenuTextures();
		mEngine.registerUpdateHandler(new TimerHandler(0.01f, new ITimerCallback() 
		{
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler) 
			{
				mEngine.unregisterUpdateHandler(pTimerHandler);
				ResourcesManager.getInstance().loadAboutResources();
				AboutScene about = new AboutScene();
				setScene(about);
			}
		}));
	}

	// methods to dispose menu and play mode scenes

	private void disposeSplashScene()
	{
		ResourcesManager.getInstance().unloadSplashScreen();
		splashScene.disposeScene();
		splashScene = null;
	}


	private void disposeMenuScene(){
		ResourcesManager.getInstance().unloadMenuTextures();
		menuScene.disposeScene();
		menuScene = null; 

	}

	private void disposeBuildScene(){
		ResourcesManager.getInstance().unloadBuildTextures();
		buildGameScene.disposeScene();
		buildGameScene = null;

	}

	private void disposeEggScene(){
		ResourcesManager.getInstance().unloadEggTextures();
		eggGameScene.disposeScene();
		eggGameScene = null;

	}

	private void disposePaintScene(){
		ResourcesManager.getInstance().unloadPaintTextures();
		paintGameScene.disposeScene();
		paintGameScene = null;

	}

	private void disposePopScene(){
		ResourcesManager.getInstance().unloadPopTextures();
		popGameScene.disposeScene();
		popGameScene = null;


	}

	private void disposeOptionScene(){
		ResourcesManager.getInstance().unloadOptionTextures();
		optionScene.disposeScene();
		optionScene = null;

	}

	//--------------------------------------------------------------------
	public void playMenuBackMusic(){

		if(GameManager.getInstance().isMusicEnabled()) {
			if(ResourcesManager.getInstance().menuBackMusic!=null){
				ResourcesManager.getInstance().menuBackMusic.setLooping(true);
				ResourcesManager.getInstance().menuBackMusic.setVolume(1,1);
				ResourcesManager.getInstance().menuBackMusic.play();
			}
		}

	}

	public void playMenuButtonSound(){

		if(GameManager.getInstance().isSoundEnabled()) {
			if(ResourcesManager.getInstance().menuButtonSound!=null){
				ResourcesManager.getInstance().menuButtonSound.setVolume(0.6f,0.6f);
				ResourcesManager.getInstance().menuButtonSound.play();
			}
		}
	}

	public void pauseMenuBackMusic(){
		if(ResourcesManager.getInstance().menuBackMusic!=null){
			if(ResourcesManager.getInstance().menuBackMusic.isPlaying())
				ResourcesManager.getInstance().menuBackMusic.pause();
		}

	}

	public void resumeMenuBackMusic(){
		if(GameManager.getInstance().isMusicEnabled())
			if(ResourcesManager.getInstance().menuBackMusic!=null)
				ResourcesManager.getInstance().menuBackMusic.resume();
	}

	public void stopMenuBackMusic(){
		if(ResourcesManager.getInstance().menuBackMusic!=null){
			if(ResourcesManager.getInstance().menuBackMusic.isPlaying())
				ResourcesManager.getInstance().menuBackMusic.stop();
		}

	}
	//-----------------------------------------------------------------------
	

	public void playPaintOkSound(){

		if(GameManager.getInstance().isSoundEnabled()) {
			if(ResourcesManager.getInstance().paintok!=null){
				ResourcesManager.getInstance().paintok.setVolume(1,1);
				ResourcesManager.getInstance().paintok.play();
			}
		}
		else {

		}
	}

	public void playBuildOkSound(){
		if(GameManager.getInstance().isSoundEnabled()) {
			if(ResourcesManager.getInstance().build_ok!=null){
				ResourcesManager.getInstance().build_ok.setVolume(1,1);		
				ResourcesManager.getInstance().build_ok.play();
			}
		}
		else {
			//we will do some
		}
	}

	public void playHammerBuild(){
		if(GameManager.getInstance().isSoundEnabled()) {
			if(ResourcesManager.getInstance().HammerSfx!=null){
				ResourcesManager.getInstance().HammerSfx.setVolume(1f,1f);
				ResourcesManager.getInstance().HammerSfx.play();
			}
		}
		else {

		}
	}
	public void playGameModeBackMusic(){
		if(GameManager.getInstance().isMusicEnabled()) {
			if(ResourcesManager.getInstance().GamePlayBackMusic!=null){
				ResourcesManager.getInstance().GamePlayBackMusic.setLooping(true);
				ResourcesManager.getInstance().GamePlayBackMusic.setVolume(0.3f,0.3f);
				ResourcesManager.getInstance().GamePlayBackMusic.play();
			}
		}
		else {

		}
	}

	public void pauseGameModeBackMusic(){
		if(ResourcesManager.getInstance().GamePlayBackMusic!=null){
			if(ResourcesManager.getInstance().GamePlayBackMusic.isPlaying())
				ResourcesManager.getInstance().GamePlayBackMusic.pause();
		}
	}

	public void resumeGameModeBackMusic(){
		if(ResourcesManager.getInstance().GamePlayBackMusic!=null){
			if(GameManager.getInstance().isMusicEnabled()){
				if(!GameManager.getInstance().isSubMenuUp())
				ResourcesManager.getInstance().GamePlayBackMusic.resume();
			}
		}
	}

	public void stopGameModeBackMusic(){
		if(ResourcesManager.getInstance().GamePlayBackMusic!=null){
			if(ResourcesManager.getInstance().GamePlayBackMusic.isPlaying())
				ResourcesManager.getInstance().GamePlayBackMusic.stop();
		}

	}

	//------------------------------------------------------------
	
	public void playEggInstruction(){
		if(GameManager.getInstance().isSoundEnabled()) {
			if(ResourcesManager.getInstance().eggInstruction!=null){
				ResourcesManager.getInstance().eggInstruction.setVolume(0.9f,0.9f);
				ResourcesManager.getInstance().eggInstruction.play();
			}
		}
		else {

		}
	}
	
	

	public void playAckSound(){

		if(GameManager.getInstance().isSoundEnabled()) {
			if(ResourcesManager.getInstance().ack!=null){
				ResourcesManager.getInstance().ack.setVolume(1,1);
				ResourcesManager.getInstance().ack.play();
			}
		}
		else {

		}
	}

	public void playNacksound(){
		if(GameManager.getInstance().isSoundEnabled()) {
			if(ResourcesManager.getInstance().nack!=null){
				ResourcesManager.getInstance().nack.setVolume(1,1);
				ResourcesManager.getInstance().nack.play();
			}
			
		}
		else {

		}
	}

	public void playPaintCongratulorySound(boolean action){

		if(action){
			if(GameManager.getInstance().isSoundEnabled()) {
				if(ResourcesManager.getInstance().ApplauseSound!=null){
					ResourcesManager.getInstance().ApplauseSound.setVolume(1,1);
					ResourcesManager.getInstance().ApplauseSound.play();
				}
			}
		}
		else {
			if(ResourcesManager.getInstance().ApplauseSound!=null){
				if(ResourcesManager.getInstance().ApplauseSound.isLoaded())
					ResourcesManager.getInstance().ApplauseSound.stop();
			}
		}

	}
	
	
	//---------------------------------------------------------------

	

	public void playPopVictorySound(){
		if(GameManager.getInstance().isSoundEnabled()) {
			if(ResourcesManager.getInstance().popVictory!=null){
				ResourcesManager.getInstance().popVictory.setVolume(1,1);
				ResourcesManager.getInstance().popVictory.play();
			}
		}
	}

	public void playArrowSound(){
		if(GameManager.getInstance().isSoundEnabled()) {
			if(ResourcesManager.getInstance().arrow!=null){
				ResourcesManager.getInstance().arrow.setVolume(1,1);
				ResourcesManager.getInstance().arrow.play();
			}
		}
	}

	public void pauseArrowSound(){
		if(ResourcesManager.getInstance().arrow!=null){
			if(ResourcesManager.getInstance().arrow.isLoaded()){
				ResourcesManager.getInstance().arrow.pause();
			}
		}
	}

	public void resumeArrowSound(){
		if(ResourcesManager.getInstance().arrow!=null){
			ResourcesManager.getInstance().arrow.resume();

		}
	}


	public void playLetterSound(String letter){


		if("a".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[0]!=null) {
				ResourcesManager.getInstance().PopLetterSound[0].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[0].play();


			}


		}
		else if("b".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[1]!=null) {
				ResourcesManager.getInstance().PopLetterSound[1].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[1].play();


			}


		}
		else if("c".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[2]!=null) {
				ResourcesManager.getInstance().PopLetterSound[2].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[2].play();


			}


		}
		else if("d".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[3]!=null) {
				ResourcesManager.getInstance().PopLetterSound[3].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[3].play();


			}


		}
		else if("e".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[4]!=null) {
				ResourcesManager.getInstance().PopLetterSound[4].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[4].play();


			}


		}
		else if("f".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[5]!=null) {
				ResourcesManager.getInstance().PopLetterSound[5].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[5].play();


			}


		}
		else if("g".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[6]!=null) {
				ResourcesManager.getInstance().PopLetterSound[6].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[6].play();


			}


		}
		else if("h".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[7]!=null) {
				ResourcesManager.getInstance().PopLetterSound[7].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[7].play();


			}


		}

		else if("i".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[8]!=null) {
				ResourcesManager.getInstance().PopLetterSound[8].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[8].play();


			}


		}
		else if("j".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[9]!=null) {
				ResourcesManager.getInstance().PopLetterSound[9].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[9].play();


			}


		}
		else if("k".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[10]!=null) {
				ResourcesManager.getInstance().PopLetterSound[10].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[10].play();


			}


		}
		else if("l".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[11]!=null) {
				ResourcesManager.getInstance().PopLetterSound[11].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[11].play();


			}


		}
		else if("m".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[12]!=null) {
				ResourcesManager.getInstance().PopLetterSound[12].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[12].play();


			}


		}
		else if("n".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[13]!=null) {
				ResourcesManager.getInstance().PopLetterSound[13].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[13].play();


			}


		}
		else if("o".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[14]!=null) {
				ResourcesManager.getInstance().PopLetterSound[14].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[14].play();


			}


		}
		else if("p".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[15]!=null) {
				ResourcesManager.getInstance().PopLetterSound[15].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[15].play();


			}

		}
		else if("r".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[16]!=null) {
				ResourcesManager.getInstance().PopLetterSound[16].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[16].play();


			}


		}
		else if("s".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[17]!=null) {
				ResourcesManager.getInstance().PopLetterSound[17].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[17].play();


			}


		}
		else if("t".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[18]!=null) {
				ResourcesManager.getInstance().PopLetterSound[18].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[18].play();


			}


		}
		else if("u".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[19]!=null) {
				ResourcesManager.getInstance().PopLetterSound[19].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[19].play();


			}


		}
		else if("v".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[20]!=null) {
				ResourcesManager.getInstance().PopLetterSound[20].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[20].play();


			}


		}
		else if("w".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[21]!=null) {
				ResourcesManager.getInstance().PopLetterSound[21].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[21].play();


			}

		}
		else if("y".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[22]!=null) {
				ResourcesManager.getInstance().PopLetterSound[22].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[22].play();


			}

		}
		else if("z".equalsIgnoreCase(letter)) {
			if(ResourcesManager.getInstance().PopLetterSound[23]!=null) {
				ResourcesManager.getInstance().PopLetterSound[23].setVolume(1,1);
				ResourcesManager.getInstance().PopLetterSound[23].play();


			}
		}


	}

	public void playOnSubMenuButtonClickSound(){
		if(ResourcesManager.getInstance().subMenuButtonSound!=null) {
			ResourcesManager.getInstance().subMenuButtonSound.setVolume(1);
			ResourcesManager.getInstance().subMenuButtonSound.play();
		}
	}

	

	public void playBuildGuidanceAudio() {

		if(GameManager.getInstance().isSoundEnabled()) {
			if(ResourcesManager.getInstance().build_guide!=null){
				ResourcesManager.getInstance().build_guide.setVolume(1,1);
				ResourcesManager.getInstance().build_guide.play();
			}
		}

	}
	
	public void playBuildVictorySound() {

		if(GameManager.getInstance().isSoundEnabled()) {
			if(ResourcesManager.getInstance().buildVictory!=null){
				ResourcesManager.getInstance().buildVictory.setVolume(1,1);
				ResourcesManager.getInstance().buildVictory.play();
			}
		}

	}

	public void playEggGuidanceAudio() {


		if(GameManager.getInstance().isSoundEnabled()) {
			if(ResourcesManager.getInstance().egg_guide!=null){
				ResourcesManager.getInstance().egg_guide.setVolume(1,1);			
				ResourcesManager.getInstance().egg_guide.play();
			}
		}
	}

	public void playPaintGuidanceAudio() {

		if(GameManager.getInstance().isSoundEnabled()) {

			if(ResourcesManager.getInstance().paint_guide!=null){
				ResourcesManager.getInstance().paint_guide.setVolume(1,1);			
				ResourcesManager.getInstance().paint_guide.play();
			}			
		}
	}

	public void playEndPaintDemo() {

		if(GameManager.getInstance().isSoundEnabled()) {

			if(ResourcesManager.getInstance().paintDemoSFX!=null){
				ResourcesManager.getInstance().paintDemoSFX.setVolume(1,1);			
				ResourcesManager.getInstance().paintDemoSFX.play();
			}			
		}
	}
	
	public void playAfterFinishedObjectSound(int objectID) {

		if(GameManager.getInstance().isSoundEnabled()) {
       if(objectID==0){
			if(ResourcesManager.getInstance().Object0SFX!=null){
				ResourcesManager.getInstance().Object0SFX.setVolume(1,1);			
				ResourcesManager.getInstance().Object0SFX.play();
			}	
         }
       else if(objectID==1){
    	   if(ResourcesManager.getInstance().Object1SFX!=null){
				ResourcesManager.getInstance().Object1SFX.setVolume(1,1);			
				ResourcesManager.getInstance().Object1SFX.play();
			} 
       }
       else if(objectID==2){
    	   if(ResourcesManager.getInstance().Object2SFX!=null){
				ResourcesManager.getInstance().Object2SFX.setVolume(1,1);			
				ResourcesManager.getInstance().Object2SFX.play();
			}
       }
		}
	}

	public void playPopGuidanceAudio() {
		if(GameManager.getInstance().isSoundEnabled()) {

			if(ResourcesManager.getInstance().pop_guide!=null){
				ResourcesManager.getInstance().pop_guide.setVolume(1,1);			
				ResourcesManager.getInstance().pop_guide.play();
			}			
		}
	}

	public void eggHonk() {
		if(GameManager.getInstance().isSoundEnabled()) {
			if(ResourcesManager.getInstance().honk!=null){
				ResourcesManager.getInstance().honk.setVolume(1,1);
				ResourcesManager.getInstance().honk.play();
			}
		}
	}
	
	public void playEggFeedback(boolean feedback) {
		if(GameManager.getInstance().isSoundEnabled()) {
		if(feedback){
			if(ResourcesManager.getInstance().eggAck!=null){
				ResourcesManager.getInstance().eggAck.setVolume(1,1);
				ResourcesManager.getInstance().eggAck.play();
			}
		}
		else{
			if(ResourcesManager.getInstance().eggNack!=null){
				ResourcesManager.getInstance().eggNack.setVolume(1,1);
				ResourcesManager.getInstance().eggNack.play();
			}
		}
		}
	}

	public void playDrumSound(){
		if(GameManager.getInstance().isSoundEnabled()){
			if(ResourcesManager.getInstance().drum!=null){
				ResourcesManager.getInstance().drum.setVolume(1,1);
				ResourcesManager.getInstance().drum.play();	
			}
		}
	}
	public void vibrate(long time){
		if(GameManager.getInstance().isVibrationEnabled()){
			engine.vibrate(time);
		}
	}


}
