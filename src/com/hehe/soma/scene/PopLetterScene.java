

/**
 @company: Hehe Ltd
 @Project: Soma
 @Date: mm/dd/yyyy
 @Author: Amiri Mugarura and Sixbert Uwiringiyimana
 @Credits: thanks given to Richard Rusa and other artists for graphic designs ....

 @About this Class "PopLetterScene.java":
   ----------------------------------------

 */



package com.hehe.soma.scene;

import java.util.LinkedList;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.ColorModifier;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.JumpModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.RotationModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.util.GLState;
import org.andengine.util.math.MathUtils;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.opengl.GLES20;
import android.widget.Toast;

import com.hehe.soma.manager.GameManager;
import com.hehe.soma.manager.SceneManager;
import com.hehe.soma.manager.SceneManager.SceneType;

public class PopLetterScene extends ParentScene implements IOnMenuItemClickListener {


	protected MenuScene popMenuScene;
	protected static final int MENU_RESET = 0;
	protected static final int MENU_CANCEL=1;
	protected static final int MENU_QUIT = 2;

	private Sprite background,canvas,showmenu,arrowleft,arrowright;
	private AnimatedSprite popSupernka;
	private Sprite drum0,drum1,drum2;
	private Sprite cloudA,cloudB,cloudC,cloudD,
	cloudE,cloudF,cloudG,cloudH,cloudI,cloudJ,
	cloudK,cloudL,cloudM,cloudN,cloudO,cloudP,
	cloudR,cloudS,cloudT,cloudU,cloudV,cloudW,
	cloudY,cloudZ;

	private final float CloudInitY=20;
	private final float LetterXOffset=56;



	//our game timer ....
	TimerHandler  checkCollision,ShowLetterTimerHandler,ShootArrowTimer,
	WaitForreleaseDrum, animateWaitTimer,waitPopIn,waitForVictory,popSubMenu,waitForNackToFinish;

	private Text t,e0,g,e1,r,e2,z,a;

	private ParallelEntityModifier e0S,gS,e1S,rS,e2S,zS,aS, tS;; 

	//letter texts to display
	private Text letterA;
	private Text letterB;
	private Text letterC;
	private Text letterD;
	private Text letterE;
	private Text letterF;
	private Text letterG;
	private Text letterH;
	private Text letterI;
	private Text letterJ;
	private Text letterK;
	private Text letterL;
	private Text letterM;
	private Text letterN;
	private Text letterO;
	private Text letterP;
	private Text letterR;
	private Text letterS;
	private Text letterT;
	private Text letterU;
	private Text letterV;
	private Text letterW;
	private Text letterY;
	private Text letterZ;

	private Text WarnText,RAMinfo;
	private Sprite warning;
	
	private boolean isVictoryUp=false;
	private boolean isWaitMessageOn;
	private boolean playAgain=true;
	//make d
	private boolean isFlipLocked=false;
	private boolean isPositionLeft=false;
	private boolean isFirstShot=true;
	
	//hold said letter
	private String PronouncedLetter;
	//keep track of correct touched letters
	private int CatchedLetterCount=0;
	/*variable to keep track
	 *  of how many shots fired*/
	private int shotsCount=0;
	//index to get poped cloud from list
	private int shotLetterIndex=0;

/* linked list to hold cloud sprites  */
	private LinkedList<Sprite> CloudList0;
	private LinkedList<Sprite> CloudList1;
	private LinkedList<Sprite> CloudList2;

	/*boolean linked lsit to hold info whether
	 * a cloud has been touched before or not*/
	private LinkedList<Boolean>isCloudTouched0;
	private LinkedList<Boolean>isCloudTouched1;
	private LinkedList<Boolean>isCloudTouched2;


	private Text[] LetterList0;
	private Text[] LetterList1;
	private Text[] LetterList2;

	private String[] LetterCat0;
	private String[] LetterCat1;
	private String[] LetterCat2;

	private IEntityModifier moveLeftArrow;
	private IEntityModifier moveRightArrow;


	/*intial positions of letters and  clouds 
	 * are random and held in these arrays*/

	int[] randomLetterInitX1;
	int[] randomLetterInitX2;
	int[] randomLetterInitX3;



	@Override
	public void createScene()
	{ 
		engine.enableVibrator(activity);
		createMenuScene();
		SceneManager.getInstance().playGameModeBackMusic();
		SceneManager.getInstance().playPopGuidanceAudio();
		createBackground();	
		initSprites();
		sortChildren();
		displayCloudLetter();
		sortChildren();
		updateMemoryInfo();

	}

	private void createBackground()
	{
		
		
		background=new Sprite(0, 0, resourcesManager.pop_background_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		background.setScale(1.0f);
		background.setZIndex(0);
		background.setPosition(0,0);
		attachChild(background);
		warning=new Sprite(0, 0, resourcesManager.showwarn_region, vbom)
		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   

				if (pSceneTouchEvent.isActionUp())
				{   
					WarnText.setScale(0);
					RAMinfo.setScale(0);
					warning.setScale(0);
				

				}
				return true;
			};
		};
		warning.setPosition(100,camera.getHeight()-warning.getHeight()-25);
		attachChild(warning);
		registerTouchArea(warning);
		warning.setScale(0);

		WarnText=new Text(warning.getX()+warning.getWidth()-15,camera.getHeight()-warning.getHeight()-50, resourcesManager.WarnFont,
				"Telefone yawe ifite ibyangombwa bidahagije!\n Funga izindi apps udakeneye cyangwa\n ujye usubiramo SOMA nijya yifunga.", vbom);	
		attachChild(WarnText);	
		WarnText.setScale(0);

		RAMinfo=new Text(warning.getX()+warning.getWidth()-15,camera.getHeight()-warning.getHeight()-55+WarnText.getHeight(), resourcesManager.WarnFont,
				"  RAM isigaye: 100 MB,ikenewe: 128 MB ", vbom);	
		attachChild(RAMinfo);	
		RAMinfo.setScale(0);
		
		RAMinfo.setZIndex(4);
		WarnText.setZIndex(4);
		warning.setZIndex(4);
		
		this.canvas=new Sprite(0, 0, resourcesManager.canvas_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		canvas.setScale(0);
		canvas.setColor(0,0.95f, 0.95f,0.75f);
		canvas.setZIndex(100);
		canvas.setPosition(camera.getCenterX()-250,camera.getCenterY()-175);
		attachChild(canvas);

		this.showmenu=new Sprite(0, 0, resourcesManager.showmenu_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   

				if (pSceneTouchEvent.isActionUp())
				{   
					showPopMenu();
					SceneManager.getInstance().playOnSubMenuButtonClickSound();
					SceneManager.getInstance().vibrate(200);

				}

				if (pSceneTouchEvent.isActionDown())
				{  



				}
				return true;
			};
		};
		showmenu.setScale(0.5f);
		showmenu.setAlpha(0.5f);
		showmenu.setZIndex(4);
		showmenu.setPosition(camera.getWidth()-80,camera.getHeight()-80);
		attachChild(showmenu);
		registerTouchArea(showmenu);


		t=new Text(camera.getCenterX()-250,camera.getCenterY(), resourcesManager.PopFont,".", vbom);
		e0=new Text(camera.getCenterX()-200,camera.getCenterY(), resourcesManager.PopFont,".", vbom);
		g=new Text(camera.getCenterX()-150,camera.getCenterY(), resourcesManager.PopFont,".", vbom);
		e1=new Text(camera.getCenterX()-100,camera.getCenterY(), resourcesManager.PopFont,".", vbom);
		r=new Text(camera.getCenterX()-50,camera.getCenterY(), resourcesManager.PopFont,".", vbom);
		e2=new Text(camera.getCenterX(),camera.getCenterY(), resourcesManager.PopFont,".", vbom);
		z=new Text(camera.getCenterX()+50,camera.getCenterY(), resourcesManager.PopFont,".", vbom);
		a=new Text(camera.getCenterX()+100,camera.getCenterY(), resourcesManager.PopFont,".", vbom);
		t.setScale(0);
		e0.setScale(0);
		g.setScale(0);
		r.setScale(0);
		e1.setScale(0);
		r.setScale(0);
		e2.setScale(0);
		z.setScale(0);
		a.setScale(0);

		attachChild(t);
		attachChild(e0);
		attachChild(g);
		attachChild(e1);
		attachChild(r);
		attachChild(e2);
		attachChild(z);
		attachChild(a);

	}

	//method to load letter sprites

	private void initSprites() {

		initPRNG();
		//create instance of lists and arrays
		CloudList0=new LinkedList<Sprite>();
		CloudList1=new LinkedList<Sprite>();
		CloudList2=new LinkedList<Sprite>();
		playAgain=true;
		isPositionLeft=false;
		isFirstShot=true;


		LetterList0= new Text[] {null,null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,null,null,null};
		LetterList1= new Text[] {null,null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,null,null,null};		

		LetterList2= new Text[] {null,null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,null,null,null};		


		LetterCat0=new String[] {"A","B","C","D","E","F","G","H"};
		LetterCat1=new String[] {"I","J","K","L","M","N","O","P"};
		LetterCat2=new String[] {"R","S","T","U","V","W","Y","Z"};

		isCloudTouched0=new LinkedList<Boolean>();
		isCloudTouched1=new LinkedList<Boolean>();
		isCloudTouched2=new LinkedList<Boolean>();

		for(int i=0;i<8;i++) {
			isCloudTouched0.add(false);		
			isCloudTouched1.add(false);
			isCloudTouched2.add(false);
		}




		arrowleft=new Sprite(0, 0, resourcesManager.arrowleft_region, vbom);
		arrowleft.setPosition(camera.getCenterX()+200,camera.getCenterY()-90);
		arrowleft.setScale(0);
		arrowleft.setZIndex(6);

		arrowright=new Sprite(0, 0, resourcesManager.arrowright_region, vbom);
		arrowright.setPosition(350,camera.getCenterY()+100);
		arrowright.setScale(0);
		arrowright.setZIndex(6);


		drum0=new Sprite(0, 0, resourcesManager.pop_drum_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}


		};

		drum0.setPosition(20,camera.getHeight()-100);
		drum0.setScale(1.0f);
		drum0.setAlpha(0.5f);
		drum0.setZIndex(1);



		drum1=new Sprite(0, 0, resourcesManager.pop_drum_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

		};

		drum1.setPosition(85,camera.getHeight()-100);
		drum1.setScale(1.0f);
		drum1.setAlpha(0.5f);
		drum1.setZIndex(1);

		drum2=new Sprite(0, 0, resourcesManager.pop_drum_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}


		};

		drum2.setPosition(150,camera.getHeight()-100);
		drum2.setScale(1.0f);
		drum2.setAlpha(0.5f);
		drum2.setZIndex(1);

		popSupernka=new AnimatedSprite(0, 0, resourcesManager.pop_supernka_region, vbom);
		popSupernka.setPosition(camera.getCenterX()+200,camera.getCenterY());
		popSupernka.setScale(1);
		popSupernka.setZIndex(3);

		letterA=new Text(0,0, resourcesManager.PopFont, LetterCat0[0], vbom);
		LetterList0[0]=letterA;
		
		letterB=new Text(0,0, resourcesManager.PopFont,LetterCat0[1], vbom);
		LetterList0[1]=letterB;
		
		letterC=new Text(0,0, resourcesManager.PopFont,LetterCat0[2], vbom);
		LetterList0[2]=letterC;
       
		letterD=new Text(0,0, resourcesManager.PopFont,LetterCat0[3], vbom);
        LetterList0[3]=letterD;
        
        letterE=new Text(0,0, resourcesManager.PopFont, LetterCat0[4], vbom);
		LetterList0[4]=letterE;
		
		letterF=new Text(0,0, resourcesManager.PopFont,LetterCat0[5], vbom);
		LetterList0[5]=letterF;
		
		letterG=new Text(0,0, resourcesManager.PopFont,LetterCat0[6], vbom);
		LetterList0[6]=letterG;
       
		letterH=new Text(0,0, resourcesManager.PopFont,LetterCat0[7], vbom);
        LetterList0[7]=letterH;
		
        letterI=new Text(300,CloudInitY, resourcesManager.PopFont, LetterCat1[0], vbom);
		LetterList1[0]=letterI;
		
		letterJ=new Text(300,CloudInitY, resourcesManager.PopFont,LetterCat1[1], vbom);
		LetterList1[1]=letterJ;
		
		letterK=new Text(300,CloudInitY, resourcesManager.PopFont,LetterCat1[2], vbom);
		LetterList1[2]=letterK;
       
		letterL=new Text(300,CloudInitY, resourcesManager.PopFont,LetterCat1[3], vbom);
        LetterList1[3]=letterL;
        
        letterM=new Text(300,CloudInitY, resourcesManager.PopFont, LetterCat1[4], vbom);
		LetterList1[4]=letterM;
		
		letterN=new Text(300,CloudInitY, resourcesManager.PopFont,LetterCat1[5], vbom);
		LetterList1[5]=letterN;
		
		letterO=new Text(300,CloudInitY, resourcesManager.PopFont,LetterCat1[6], vbom);
		LetterList1[6]=letterO;
		
		letterP=new Text(300,CloudInitY, resourcesManager.PopFont,LetterCat1[7], vbom);
		LetterList1[7]=letterP;
       
		
        
        letterR=new Text(550,CloudInitY, resourcesManager.PopFont, LetterCat2[0], vbom);
		LetterList2[0]=letterR;
		
		letterS=new Text(550,CloudInitY, resourcesManager.PopFont,LetterCat2[1], vbom);
		LetterList2[1]=letterS;
		
		letterT=new Text(550,CloudInitY, resourcesManager.PopFont,LetterCat2[2], vbom);
		LetterList2[2]=letterT;
       
		letterU=new Text(550,CloudInitY, resourcesManager.PopFont,LetterCat2[3], vbom);
        LetterList2[3]=letterU;
        
        letterV=new Text(550,CloudInitY, resourcesManager.PopFont, LetterCat2[4], vbom);
		LetterList2[4]=letterV;
		
		letterW=new Text(550,CloudInitY, resourcesManager.PopFont,LetterCat2[5], vbom);
		LetterList2[5]=letterW;
		
		letterY=new Text(550,CloudInitY, resourcesManager.PopFont,LetterCat2[6], vbom);
		LetterList2[6]=letterY;
       
		letterZ=new Text(550,CloudInitY, resourcesManager.PopFont,LetterCat2[7], vbom);
        LetterList2[7]=letterZ;
        
        repositionLetters();
			

		//load the clouds with their specific letters and the add clouds to linked list for later retrieval

		
		cloudA=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   



				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{   


					if("a".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched0.get(0).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched0.add(0,true);
							releaseDrum();
						}

						shootArrow(letterA.getX(),letterA.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);

					}

				}

				return true;

			};


		};

		
		registerTouchArea(cloudA);
		CloudList0.add(cloudA);

		cloudB=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   



				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{   
					if("b".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched0.get(1).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched0.add(1,true);
							releaseDrum();
						}

						shootArrow(letterB.getX(),letterB.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}


				}

				return true;
			};


		};

		
		registerTouchArea(cloudB);
		
		CloudList0.add(cloudB);
		cloudC=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   

				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{
					if("c".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched0.get(2).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched0.add(2,true);
							releaseDrum();
						}

						shootArrow(letterC.getX(),letterC.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}

				}


				return true;
			};


		};

		
		registerTouchArea(cloudC);
		CloudList0.add(cloudC);

		cloudD=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{  

				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{   

					if("d".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched0.get(3).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched0.add(3,true);
							releaseDrum();
						}


						shootArrow(letterD.getX(),letterD.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}

				}

				return true;
			};


		};


		
		registerTouchArea(cloudD);
		LetterList0[3]=letterD;
		CloudList0.add(cloudD);

		cloudE=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   

				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{ 
					if("e".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched0.get(4).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched0.add(4,true);
							releaseDrum();
						}

						shootArrow(letterE.getX(),letterE.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}

				}

				return true;
			};


		};

		

		registerTouchArea(cloudE);
		CloudList0.add(cloudE);

		cloudF=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   


				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{   

					if("f".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched0.get(5).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched0.add(5,true);
							releaseDrum();
						}


						shootArrow(letterF.getX(),letterF.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}

				}


				return true;
			};


		};


		registerTouchArea(cloudF);
		CloudList0.add(cloudF);
		cloudG=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   


				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{   
					if("g".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched0.get(6).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched0.add(6,true);
							releaseDrum();
						}

						shootArrow(letterG.getX(),letterG.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}

				}

				return true;
			};


		};
		registerTouchArea(cloudG);
		CloudList0.add(cloudG);

	

		cloudH=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   


				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{  

					if("h".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched0.get(7).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched0.add(7,true);
							releaseDrum();
						}

						shootArrow(letterH.getX(),letterH.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}
				}

				return true;
			};


		};
	
		registerTouchArea(cloudH);
		CloudList0.add(cloudH);



		cloudI=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   


				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{   
					if("i".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched1.get(0).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched1.add(0,true);
							releaseDrum();
						}

						shootArrow(letterI.getX(),letterI.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}
				}

				return true;
			};


		};

		registerTouchArea(cloudI);
		CloudList1.add(cloudI);

		cloudJ=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{  

				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{ 
					if("j".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched1.get(1).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched1.add(1,true);
							releaseDrum();
						}

						shootArrow(letterJ.getX(),letterJ.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}

				}

				return true;
			};


		};

	
		registerTouchArea(cloudJ);
	
		CloudList1.add(cloudJ);

		cloudK=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{ 

				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{   

					if("k".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched1.get(2).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched1.add(2,true);
							releaseDrum();
						}

						shootArrow(letterK.getX(),letterK.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}
				}


				return true;
			};


		};

	
		registerTouchArea(cloudK);
		
		CloudList1.add(cloudK);

		cloudL=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{



				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{

					if("l".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched1.get(3).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched1.add(3,true);
							releaseDrum();
						}

						shootArrow(letterL.getX(),letterL.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}
				}


				return true;
			};


		};


		
		registerTouchArea(cloudL);
		
		CloudList1.add(cloudL);

		cloudM=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{ 


				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{   

					if("m".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched1.get(4).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched1.add(4,true);
							releaseDrum();
						}

						shootArrow(letterM.getX(),letterM.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}

				}


				return true;
			};


		};


		registerTouchArea(cloudM);
	
		CloudList1.add(cloudM);

		cloudN=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   


				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{   

					if("n".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched1.get(5).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched1.add(5,true);
							releaseDrum();
						}

						shootArrow(letterN.getX(),letterN.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}

				}

				return true;
			};


		};


		registerTouchArea(cloudN);
		
		CloudList1.add(cloudN);

		cloudO=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   


				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{ 
					if("o".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched1.get(6).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched1.add(6,true);
							releaseDrum();
						}

						shootArrow(letterO.getX(),letterO.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}

				}

				return true;
			};


		};

		registerTouchArea(cloudO);
		
		CloudList1.add(cloudO);

		cloudP=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{  


				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{   

					if("p".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched1.get(7).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched1.add(7,true);
							releaseDrum();
						}

						shootArrow(letterP.getX(),letterP.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}

				}


				return true;
			};


		};


	
		registerTouchArea(cloudP);
		
		CloudList1.add(cloudP);

		cloudR=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   


				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{   
					if("r".equalsIgnoreCase(PronouncedLetter)) {
						if(!isCloudTouched2.get(0).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched2.add(0,true);
							releaseDrum();
						}

						shootArrow(letterR.getX(),letterR.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}
				}


				return true;
			};


		};


		registerTouchArea(cloudR);
	
		CloudList2.add(cloudR);

		cloudS=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{  


				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{  

					if("s".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched2.get(1).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched2.add(1,true);
							releaseDrum();
						}

						shootArrow(letterS.getX(),letterS.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}

				}


				return true;
			};


		};

		registerTouchArea(cloudS);
		
		CloudList2.add(cloudS);

		cloudT=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{  


				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{   

					if("t".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched2.get(2).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched2.add(2,true);
							releaseDrum();
						}

						shootArrow(letterT.getX(),letterT.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}
				}


				return true;
			};


		};


		registerTouchArea(cloudT);
	
		CloudList2.add(cloudT);

		cloudU=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   


				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{   
					if("u".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched2.get(3).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched2.add(3,true);
							releaseDrum();
						}

						shootArrow(letterU.getX(),letterU.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}

				}


				return true;
			};


		};

		registerTouchArea(cloudU);
		
		CloudList2.add(cloudU);

		cloudV=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{ 


				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{   

					if("v".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched2.get(4).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched2.add(4,true);
							releaseDrum();
						}

						shootArrow(letterV.getX(),letterV.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}
				}


				return true;
			};


		};


		registerTouchArea(cloudV);
		
		CloudList2.add(cloudV);

		cloudW=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   

				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{   
					if("w".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched2.get(5).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched2.add(5,true);
							releaseDrum();

						}

						shootArrow(letterW.getX(),letterW.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}

				}

				return true;
			};


		};

		registerTouchArea(cloudW);
		
		CloudList2.add(cloudW);

		cloudY=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   


				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{   
					if("y".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched2.get(6).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched2.add(6,true);
							releaseDrum();


						}

						shootArrow(letterY.getX(),letterY.getY()+20);

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}

				}


				return true;
			};


		};

		registerTouchArea(cloudY);
	
		CloudList2.add(cloudY);

		cloudZ=new Sprite(0, 0, resourcesManager.pop_cloud_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   


				PronouncedLetter=GameManager.getInstance().getPopSaidLetter();
				if (pSceneTouchEvent.isActionUp())
				{   

					if("z".equalsIgnoreCase(PronouncedLetter)) {

						if(!isCloudTouched2.get(7).booleanValue()) {
							CatchedLetterCount++;
							isCloudTouched2.add(7,true);
							releaseDrum();


						}

						shootArrow(letterZ.getX(),letterZ.getY()+20);


					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}
				}


				return true;
			};


		};


		
		registerTouchArea(cloudZ);
		
		CloudList2.add(cloudZ);

		//attach sprites now
		setSprites();
		attachSprites();

	}


	private void displayWaitMsg(){

		unregisterTouchArea(this.showmenu);
		isWaitMessageOn=true;
		tS=new ParallelEntityModifier(new ScaleModifier(2f,0,1.75f),new ColorModifier(2,0,0,0,1,0.05f,0.75f));
		e0S=new ParallelEntityModifier(new ScaleModifier(1f,0,1.75f),new ColorModifier(1,0,0,0,1,0.05f,0.75f));
		gS=new ParallelEntityModifier(new ScaleModifier(1f,0,1.75f),new ColorModifier(1,0,0,0,1,0.05f,0.75f));
		e1S=new ParallelEntityModifier(new ScaleModifier(1f,0,1.75f),new ColorModifier(1,0,0,0,1,0.05f,0.75f));
		rS=new ParallelEntityModifier(new ScaleModifier(2f,0,1.75f),new ColorModifier(2,0,0,0,1,0.05f,0.75f));
		e2S=new ParallelEntityModifier(new ScaleModifier(1f,0,1.75f),new ColorModifier(1,0,0,0,1,0.05f,0.75f));
		zS=new ParallelEntityModifier(new ScaleModifier(1f,0,1.75f),new ColorModifier(1,0,0,0,1,0.05f,0.75f));
		aS=new ParallelEntityModifier(new ScaleModifier(2f,0,1.75f),new ColorModifier(2,0,0,0,1,0.05f,0.75f));	

		animateWaitTimer = new TimerHandler(0.5f,true,new ITimerCallback() {

			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {

				t.registerEntityModifier(tS);

				if(tS.isFinished()){
					e0.registerEntityModifier(e0S);	
				}
				if(e0S.isFinished()){
					g.registerEntityModifier(gS);	
				}
				if(gS.isFinished()){
					e1.registerEntityModifier(e1S);	
				}
				if(e1S.isFinished()){
					r.registerEntityModifier(rS);	
				}
				if(rS.isFinished()){
					e2.registerEntityModifier(e2S);	
				}
				if(e2S.isFinished()){
					z.registerEntityModifier(zS);	
				}
				if(zS.isFinished()){
					a.registerEntityModifier(aS);	
				}

			}

		});
		registerUpdateHandler(animateWaitTimer);



		TimerHandler	hideWaitMsg = new TimerHandler(13,false,new ITimerCallback() {

			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {


				t.setScale(0);
				e0.setScale(0);
				g.setScale(0);
				r.setScale(0);
				e1.setScale(0);
				r.setScale(0);
				e2.setScale(0);
				z.setScale(0);
				a.setScale(0);

				unregisterUpdateHandler(animateWaitTimer);

				unregisterEntityModifier(aS);
				unregisterEntityModifier(e0S);
				unregisterEntityModifier(e1S);
				unregisterEntityModifier(e2S);
				unregisterEntityModifier(tS);
				unregisterEntityModifier(gS);
				unregisterEntityModifier(rS);
				unregisterEntityModifier(zS);	
				registerTouchArea(showmenu);
				isWaitMessageOn=false;
			}

		});
		registerUpdateHandler(hideWaitMsg);


	}

	private void initPRNG(){

		randomLetterInitX1=new int[]{0,0,0,0,0,0,0,0};
		randomLetterInitX2=new int[]{300,300,300,300,300,300,300,300};
		randomLetterInitX3=new int[]{550,550,550,550,550,550,550,550};

		//randomly place cloud letters on screen in categories from A-P category 1, from R-f
		for(int i=0;i<7;i++){
			randomLetterInitX1[i]=MathUtils.random(0,100);
			randomLetterInitX2[i]=MathUtils.random(200,300);
			randomLetterInitX3[i]=MathUtils.random(550,650);
		}

	}

	private void setSprites(){

		for(int i=0;i<8;i++){
			CloudList0.get(i).setPosition(randomLetterInitX1[i],CloudInitY);
			CloudList1.get(i).setPosition(randomLetterInitX2[i],CloudInitY);
			CloudList2.get(i).setPosition(randomLetterInitX3[i],CloudInitY);

			CloudList0.get(i).setScale(0);
			CloudList1.get(i).setScale(0);
			CloudList2.get(i).setScale(0); 

			CloudList0.get(i).setZIndex(1);
			CloudList1.get(i).setZIndex(1);
			CloudList2.get(i).setZIndex(1);

			LetterList0[i].setZIndex(2);
			LetterList1[i].setZIndex(2);
			LetterList2[i].setZIndex(2);

			LetterList0[i].setScale(0);
			LetterList1[i].setScale(0);
			LetterList2[i].setScale(0);

		}
		
	}

	private void repositionLetters(){

		isFlipLocked=false;
		for(int i=0;i<8;i++){

			LetterList0[i].setPosition(randomLetterInitX1[i]+LetterXOffset,CloudInitY);	
			LetterList1[i].setPosition(randomLetterInitX2[i]+LetterXOffset,CloudInitY);	
			LetterList2[i].setPosition(randomLetterInitX3[i]+LetterXOffset,CloudInitY);	
		}

	}

	private void releaseDrum(){

		WaitForreleaseDrum = new TimerHandler(3.7f,false,new ITimerCallback() {

			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {

				if(CatchedLetterCount==3) {
					drum0.registerEntityModifier(new ParallelEntityModifier(new AlphaModifier(5,0.5f,1),new ScaleModifier(5,1,1.5f)));


					TimerHandler waitForDrum = new TimerHandler(2f,false,new ITimerCallback() {
						@Override
						public void onTimePassed(TimerHandler pTimerHandler) {	
							SceneManager.getInstance().playDrumSound();
						}

					});
					registerUpdateHandler(waitForDrum);

				}
				else if(CatchedLetterCount==6) {
					drum1.registerEntityModifier(new ParallelEntityModifier(new AlphaModifier(5,0.5f,1),new ScaleModifier(5,1,1.5f)));

					TimerHandler waitForDrum = new TimerHandler(2f,false,new ITimerCallback() {
						@Override
						public void onTimePassed(TimerHandler pTimerHandler) {	
							SceneManager.getInstance().playDrumSound();
						}

					});
					registerUpdateHandler(waitForDrum);
				}
				else if(CatchedLetterCount==8) {
					drum2.registerEntityModifier(new ParallelEntityModifier(new AlphaModifier(5,0.5f,1),new ScaleModifier(5,1,1.5f)));

					TimerHandler waitForDrum = new TimerHandler(2f,false,new ITimerCallback() {
						@Override
						public void onTimePassed(TimerHandler pTimerHandler) {	
							SceneManager.getInstance().playDrumSound();
						}

					});
					registerUpdateHandler(waitForDrum);

					onVictory();
				}

			}	

		});
		registerUpdateHandler(WaitForreleaseDrum);

	}


	private void animateDrums(){

		drum0.registerEntityModifier(new MoveModifier(1,20,drum0.getScaleX(),camera.getHeight()-100,drum0.getScaleY()));
		drum1.registerEntityModifier(new MoveModifier(1,85,drum1.getScaleX(),camera.getHeight()-100,drum1.getScaleY()));
		drum2.registerEntityModifier(new MoveModifier(1,150,drum2.getScaleX(),camera.getHeight()-100,drum2.getScaleY()));;
	
		TimerHandler AnimateDrum0=new TimerHandler(1f,false,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
						
				drum0.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(5f,1.5f,2.5f),new MoveModifier(5,drum0.getScaleX(),camera.getCenterX(),drum0.getScaleY(),camera.getCenterY())));
				
				
			}	

		});
		registerUpdateHandler(AnimateDrum0);
		
		TimerHandler AnimateDrum1=new TimerHandler(2f,false,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
						
				drum1.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(5f,1.5f,2.5f),new MoveModifier(5,drum1.getScaleX(),camera.getWidth()-200,drum1.getScaleY(),camera.getCenterY()+drum1.getScaleY())));
				
			}	

		});
		registerUpdateHandler(AnimateDrum1);
		
		TimerHandler AnimateDrum2=new TimerHandler(3f,false,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
		drum2.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(5f,1.5f,2.5f),new MoveModifier(5,drum2.getScaleX(),camera.getWidth()-300,drum2.getScaleY(),camera.getCenterY())));
		
			}	

		});
		registerUpdateHandler(AnimateDrum2);
	}


	private void displayCloudLetter() {


		displayWaitMsg();

		ShowLetterTimerHandler = new TimerHandler(13.7f,true,new ITimerCallback() {

			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {

				int saidLetter=MathUtils.random(0,2);
				int col0=MathUtils.random(0,7);
				int col1=MathUtils.random(0,7);
				int col2=MathUtils.random(0,7);
				if(!isVictoryUp)
					popLetter(saidLetter,col0,col1,col2);


			}	

		});
		registerUpdateHandler( ShowLetterTimerHandler);

	}

	private void popLetter(int random,final int letter0,final int letter1,final int letter2 ) {

		shotsCount=0;
		repositionLetters(); //when the cow shot 'em
		CloudList0.get(letter0).setScale(1);
		CloudList1.get(letter1).setScale(1);
		CloudList2.get(letter2).setScale(1);

		CloudList0.get(letter0).registerEntityModifier(new AlphaModifier(5,0,1f));	
		LetterList0[letter0].registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(5,0,1.2f),new AlphaModifier(5,0,1f)));


		CloudList1.get(letter1).registerEntityModifier(new AlphaModifier(5,0,1f));
		LetterList1[letter1].registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(5,0,1.2f),new AlphaModifier(5,0,1f)));

		CloudList2.get(letter2).registerEntityModifier(new AlphaModifier(5,0,1f));
		LetterList2[letter2].registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(5,0,1.2f),new AlphaModifier(5,0,1f)));


		switch(random){
		case 0:

			SceneManager.getInstance().playLetterSound(LetterCat0[letter0]);
			GameManager.getInstance().setPopSaidLetter(LetterCat0[letter0]);
			this.shotLetterIndex=letter0;
			break;
		case 1:

			SceneManager.getInstance().playLetterSound(LetterCat1[letter1]);
			GameManager.getInstance().setPopSaidLetter(LetterCat1[letter1]);
			this.shotLetterIndex=letter1;
			break;
		case 2:

			SceneManager.getInstance().playLetterSound(LetterCat2[letter2]);
			GameManager.getInstance().setPopSaidLetter(LetterCat2[letter2]);
			this.shotLetterIndex=letter2;

			break;
		default:
			break;
		}


		waitPopIn= new TimerHandler(8, false,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {

				CloudList0.get(letter0).registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(3,1.5f,0),new AlphaModifier(3,1,0)));	
				LetterList0[letter0].registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(3,1.2f,0),new ColorModifier(3,0.5f,1,0.5f,1,1,1)));


				CloudList1.get(letter1).registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(3,1.5f,0),new AlphaModifier(3,1,0)));
				LetterList1[letter1].registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(3,1.2f,0),new ColorModifier(3,0.5f,1,0.5f,1,1,1)));

				CloudList2.get(letter2).registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(3,1.5f,0),new AlphaModifier(3,1,0)));
				LetterList2[letter2].registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(3,1.2f,0),new ColorModifier(3,0.5f,1,0.5f,1,1,1)));




			}

		});
		registerUpdateHandler(waitPopIn); 

		TimerHandler lockLetterFlip	= new TimerHandler(10, false,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {		

				isFlipLocked=true;

			}

		});
		registerUpdateHandler(lockLetterFlip); 
	}



	private void attachSprites(){

		attachChild(popSupernka);
		attachChild(arrowleft);
		attachChild(arrowright);
		attachChild(drum0);
		attachChild(drum1);
		attachChild(drum2);

		for(int i=0;i<8;i++){
			attachChild(CloudList0.get(i));
			attachChild(CloudList1.get(i));
			attachChild(CloudList2.get(i));	

			attachChild(LetterList0[i]);
			attachChild(LetterList1[i]);
			attachChild(LetterList2[i]);
		}

	}

	private void detachSprites(){

		for(int i=0;i<8;i++){

			detachChild(CloudList0.get(i));
			detachChild(CloudList1.get(i));
			detachChild(CloudList2.get(i));	

			detachChild(LetterList0[i]);
			detachChild(LetterList1[i]);
			detachChild(LetterList2[i]);
		}

		detachChild(popSupernka);
		detachChild(arrowleft);
		detachChild(arrowright);
		detachChild(drum0);
		detachChild(drum1);
		detachChild(drum2);

	}

	private void cleanPopScene(){

		this.CatchedLetterCount=0;
		this.PronouncedLetter=" ";
		this.isVictoryUp=false;
		this.clearEntityModifiers();
		this.unregisterUpdateHandler(this.ShootArrowTimer);
		this.unregisterUpdateHandler(this.ShowLetterTimerHandler);
		this.unregisterUpdateHandler(waitPopIn);
		this.unregisterUpdateHandler(checkCollision);
		this.unregisterUpdateHandler(waitForVictory);
		this.unregisterUpdateHandler(popSubMenu);
		this.unregisterUpdateHandler(WaitForreleaseDrum);



	}



	private void shootArrow(final float arrowTx,final float arrowTy){

		popSupernka.setScale(1);
		
		if(arrowTx<camera.getCenterX()+LetterXOffset) {
			if(popSupernka.isFlippedHorizontal()){
				popSupernka.setFlippedHorizontal(false);
			}

			if(!isPositionLeft){
				popSupernka.registerEntityModifier(new JumpModifier(1, popSupernka.getX(), camera.getCenterX()+200, popSupernka.getY(), camera.getCenterY(), camera.getCenterY()));
				isPositionLeft=true;
			}
			isFirstShot=false;
			popSupernka.animate(new long[] {300,300,300,500,300,300}, 0,5,false);

			ShootArrowTimer = new TimerHandler(0.9f,false,new ITimerCallback() {				
				@Override
				public void onTimePassed(TimerHandler pTimerHandler) {
					SceneManager.getInstance().playArrowSound();
					moveLeftArrow=new MoveModifier(0.3f,camera.getCenterX()+190,arrowTx,camera.getCenterY()+100,arrowTy);
					arrowleft.registerEntityModifier(new ParallelEntityModifier(moveLeftArrow,new AlphaModifier(0.3f,0.98f,2),new ScaleModifier(0.3f,1.5f,1)));

				}
			});

			registerUpdateHandler(ShootArrowTimer);

			checkCollision = new TimerHandler(1.3f,false,new ITimerCallback() {				
				@Override
				public void onTimePassed(TimerHandler pTimerHandler) {

					if(arrowleft.collidesWith(CloudList0.get(shotLetterIndex))){

						if(shotsCount<2){
							LetterList0[shotLetterIndex].registerEntityModifier(new ParallelEntityModifier(new RotationModifier(1,0,360),new MoveModifier(1,LetterList0[shotLetterIndex].getX(),LetterList0[shotLetterIndex].getX()-25,LetterList0[shotLetterIndex].getY(),LetterList0[shotLetterIndex].getY()-25)));

						}
						else if(!isFlipLocked){

							LetterList0[shotLetterIndex].registerEntityModifier(new ParallelEntityModifier(new RotationModifier(3,0,360),new MoveModifier(5,LetterList0[shotLetterIndex].getX(),LetterList0[shotLetterIndex].getX()-50,CloudInitY-75,camera.getHeight())));	
						}
					}
					else if(arrowleft.collidesWith(CloudList1.get(shotLetterIndex))){

						if(shotsCount<2){
							LetterList1[shotLetterIndex].registerEntityModifier(new ParallelEntityModifier(new RotationModifier(1,0,360),new MoveModifier(1,LetterList1[shotLetterIndex].getX(),LetterList1[shotLetterIndex].getX()-25,LetterList1[shotLetterIndex].getY(),LetterList1[shotLetterIndex].getY()-25)));
						}
						else if(!isFlipLocked){
							LetterList1[shotLetterIndex].registerEntityModifier(new ParallelEntityModifier(new RotationModifier(3,0,360),new MoveModifier(5,LetterList1[shotLetterIndex].getX(),LetterList1[shotLetterIndex].getX()-50,CloudInitY-75,camera.getHeight())));	
						}
					}
				}
			});

			registerUpdateHandler(checkCollision);


			TimerHandler updateArrow = new TimerHandler(1.6f,false,new ITimerCallback() {				
				@Override
				public void onTimePassed(TimerHandler pTimerHandler) {


					if(moveLeftArrow.isFinished()){
						shotsCount++;
						arrowleft.setScale(0);
						arrowleft.setPosition(camera.getCenterX()+200,camera.getCenterY()+90);
					}
				}
			});

			registerUpdateHandler(updateArrow);
		}
		else {
			popSupernka.setScale(1);
			if(!popSupernka.isFlippedHorizontal()){
				popSupernka.setFlippedHorizontal(true);
			}
			if(isPositionLeft){
				popSupernka.registerEntityModifier(new JumpModifier(1, popSupernka.getX(),20, popSupernka.getY(), camera.getCenterY(), camera.getCenterY()));
				isPositionLeft=false;
			}
			else if(isFirstShot){
				popSupernka.registerEntityModifier(new JumpModifier(1, popSupernka.getX(),20, popSupernka.getY(), camera.getCenterY(), camera.getCenterY()));
				isPositionLeft=false;	
			}

			isFirstShot=false;
			popSupernka.animate(new long[] { 300,300,300,500,300,300}, 0,5,false);

			ShootArrowTimer = new TimerHandler(0.9f,false,new ITimerCallback() {				
				@Override
				public void onTimePassed(TimerHandler pTimerHandler) {
					SceneManager.getInstance().playArrowSound();
					moveRightArrow=new MoveModifier(0.3f,10,arrowTx-100,camera.getCenterY()+100,arrowTy);
					arrowright.registerEntityModifier(new ParallelEntityModifier(moveRightArrow,new AlphaModifier(0.3f,0.97f,2),new ScaleModifier(0.3f,1.5f,1)));

				}
			});

			registerUpdateHandler(ShootArrowTimer);

			checkCollision = new TimerHandler(1.3f,false,new ITimerCallback() {				
				@Override
				public void onTimePassed(TimerHandler pTimerHandler) {


					if(arrowright.collidesWith(CloudList1.get(shotLetterIndex))){

						if(shotsCount<2){
							LetterList1[shotLetterIndex].registerEntityModifier(new ParallelEntityModifier(new RotationModifier(1,360,0),new MoveModifier(1,LetterList1[shotLetterIndex].getX(),LetterList1[shotLetterIndex].getX()+25,LetterList1[shotLetterIndex].getY(),LetterList1[shotLetterIndex].getY()-25)));

						}
						else if(!isFlipLocked){
							LetterList1[shotLetterIndex].registerEntityModifier(new ParallelEntityModifier(new MoveModifier(3,LetterList1[shotLetterIndex].getX(),LetterList1[shotLetterIndex].getX()+50,CloudInitY-75,camera.getHeight()),new RotationModifier(5,360,0)));	
						}
					}
					else if(arrowright.collidesWith(CloudList2.get(shotLetterIndex))){

						if(shotsCount<2){
							LetterList2[shotLetterIndex].registerEntityModifier(new ParallelEntityModifier(new RotationModifier(1,360,0),new MoveModifier(1,LetterList2[shotLetterIndex].getX(),LetterList2[shotLetterIndex].getX()+25,LetterList2[shotLetterIndex].getY(),LetterList2[shotLetterIndex].getY()-25)));	
						}
						else if(!isFlipLocked){
							LetterList2[shotLetterIndex].registerEntityModifier(new ParallelEntityModifier(new MoveModifier(3,LetterList2[shotLetterIndex].getX(),LetterList2[shotLetterIndex].getX()+50,CloudInitY-75,camera.getHeight()),new RotationModifier(5,360,0)));	
						}
					}

				}
			});

			registerUpdateHandler(checkCollision);

			TimerHandler updateArrow = new TimerHandler(1.6f,false,new ITimerCallback() {				
				@Override
				public void onTimePassed(TimerHandler pTimerHandler) {

					if(moveRightArrow.isFinished()){
						shotsCount++;
						arrowright.setScale(0);
						arrowright.setPosition(350,camera.getCenterY()+100);
					}
				}
			});

			registerUpdateHandler(updateArrow);


		}




	}


	private void onVictory(){

		isVictoryUp=true;	

		TimerHandler waitForVictory = new TimerHandler(2f,false,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {	
				SceneManager.getInstance().playPopVictorySound();
			}

		});
		registerUpdateHandler(waitForVictory);

		TimerHandler waitForCongratsSFX = new TimerHandler(5f,false,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {	
				SceneManager.getInstance().playPaintCongratulorySound(true);
				animateDrums();
			}

		});
		registerUpdateHandler(waitForCongratsSFX);

		popSubMenu = new TimerHandler(13f,true,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {	
				if(isVictoryUp){
					showPopMenu();			
				}
			}

		});
		registerUpdateHandler(popSubMenu);


		waitForVictory = new TimerHandler(5.5f,false,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
				if(isVictoryUp){
					clearEntityModifiers();
					unregisterUpdateHandler(waitPopIn);
					for(int i=0;i<8;i++){
						CloudList0.get(i).setScale(0);
						CloudList1.get(i).setScale(0);
						CloudList2.get(i).setScale(0);	
						LetterList0[i].setScale(0);
						LetterList1[i].setScale(0);
						LetterList2[i].setScale(0);
					}

				}
			}

		});
		registerUpdateHandler(waitForVictory);
	}

	private void postMemInfo(){


		WarnText.setScale(0.75f);
		RAMinfo.setScale(0.75f);
		warning.setScale(1.25f);

		TimerHandler waitForWarning = new TimerHandler(17f,false,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
							
					warning.setScale(0);
				    WarnText.setScale(0);
				    RAMinfo.setScale(0);
			}

		});
		registerUpdateHandler(waitForWarning);
	}

	private void updateMemoryInfo(){

		TimerHandler MemCheckDaemon = new TimerHandler(20f,true,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {

				MemoryInfo mi = new MemoryInfo();
				ActivityManager activityManager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
				if(mi!=null){
					activityManager.getMemoryInfo(mi);
					long availableMegs = mi.availMem / 1048576L;

					final String badmemory="  RAM isigaye: "+availableMegs+" MB,ikenewe: 128 MB";
					if(availableMegs<128L){	
						RAMinfo.setText(badmemory);
						postMemInfo();
					}

				}

			}

		});
		registerUpdateHandler(MemCheckDaemon);
	}

	//=======================overridden methods=============================




	protected void createMenuScene() {

		this.popMenuScene = new MenuScene(camera);

		final SpriteMenuItem resetMenuItem = new SpriteMenuItem(MENU_RESET,resourcesManager.reset_region,vbom);
		resetMenuItem.setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

		final SpriteMenuItem quitMenuItem = new SpriteMenuItem(MENU_QUIT,resourcesManager.back_region, vbom);
		quitMenuItem.setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);


		final SpriteMenuItem cancelMenuItem = new SpriteMenuItem(MENU_CANCEL,resourcesManager.cancel_region, vbom);
		quitMenuItem.setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

		this.popMenuScene.addMenuItem(resetMenuItem);
		this.popMenuScene.addMenuItem(cancelMenuItem);
		this.popMenuScene.addMenuItem(quitMenuItem);

		this.popMenuScene.buildAnimations();
		this.popMenuScene.setBackgroundEnabled(false);

		this.popMenuScene.setOnMenuItemClickListener(this);
	}



	@Override
	public boolean onMenuItemClicked(final MenuScene pMenuScene, final IMenuItem pMenuItem, final float pMenuItemLocalX, final float pMenuItemLocalY) {
		switch(pMenuItem.getID()) {
		case MENU_RESET:				
			resetPopMenu();
			return true;
		case MENU_CANCEL:
			cancelMenu();
			return true;
		case MENU_QUIT:
			/* End game mode. */
			exitPopGame();
			return true;
		default:
			return false;
		}
	}


	@Override
	public void onBackKeyPressed()
	{  
		if(!isWaitMessageOn){
			showPopMenu();	
		}
		else{
			Toast.makeText(activity, "tegereza ntabwo Soma iritegura neza...", Toast.LENGTH_LONG+Toast.LENGTH_LONG).show();		
		}
	}


	@Override
	public SceneType getSceneType()
	{
		return SceneType.POP_SCENE_GAME;
	}

	@Override
	public void disposeScene()
	{
		this.clearEntityModifiers();
		this.clearUpdateHandlers();
		this.clearTouchAreas();
		this.clearChildScene();
		this.detachChildren(); //detach all children
		this.detachSelf();
		this.dispose();
		System.gc();


	}

	@Override
	public void onMenuKeyPressed() {

		if(!isWaitMessageOn){
			showPopMenu();	
		}
		else{
			Toast.makeText(activity, "tegereza ntabwo Soma iritegura neza...", Toast.LENGTH_LONG+Toast.LENGTH_LONG).show();		

		}
	}


	private void showPopMenu() {

		if(this.hasChildScene()) {
			// Remove the menu and reset it. 
			this.popMenuScene.back();
			GameManager.getInstance().setPaused(false);
			SceneManager.getInstance().resumeGameModeBackMusic();
			SceneManager.getInstance().resumeArrowSound();			
			canvas.setScale(0);
			showmenu.setScale(0.5f);
		} 
		else {
			// Attach the menu. 
			this.setChildScene(this.popMenuScene, false, true, true);
			SceneManager.getInstance().pauseGameModeBackMusic();
			SceneManager.getInstance().pauseArrowSound();
			GameManager.getInstance().setPaused(true);
			canvas.setScale(1);
			showmenu.setScale(0);
		}
	}

	private void exitPopGame() {

		//kick back to home menu
		GameManager.getInstance().resetPopGame();
		SceneManager.getInstance().stopGameModeBackMusic();
		SceneManager.getInstance().resumeMenuBackMusic();
		SceneManager.getInstance().loadMenuScene(engine);

	}

	private void resetPopMenu() {	

		SceneManager.getInstance().playPaintCongratulorySound(false);
		GameManager.getInstance().setPaused(false);
		GameManager.getInstance().resetPopGame();
		/* Remove the menu and reset it. */
		this.clearChildScene();
		this.popMenuScene.reset();	
		clearTouchAreas();
		cleanPopScene();
		detachSprites();
		initSprites();
		canvas.setScale(0);
		showmenu.setScale(0.5f);
		registerTouchArea(showmenu);
		displayCloudLetter();
		SceneManager.getInstance().resumeGameModeBackMusic();


	}
	private void cancelMenu(){

		/* Remove the menu and reset it. */
		this.clearChildScene();
		this.popMenuScene.reset();	
		GameManager.getInstance().setPaused(false);
		SceneManager.getInstance().resumeGameModeBackMusic(); 
		canvas.setScale(0);
		showmenu.setScale(0.5f);
	}

}
