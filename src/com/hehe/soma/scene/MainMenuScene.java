
/**
 @company: Hehe Ltd
 @Project: Soma
 @Date:
 @Author: Amiri Mugarura and Sixbert Uwiringiyimana
 @Credits: thanks given to Richard Rusa and other artists for graphic designs ....

 @About this Class "MainMenuScene.java":
   ----------------------------------------
 This class is the home menu or landing screen of the Soma game.
 It houses button textures and handles their listeners 

 */
package com.hehe.soma.scene;

import javax.microedition.khronos.opengles.GL10;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.ColorModifier;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.scene.menu.item.decorator.ScaleMenuItemDecorator;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.util.GLState;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.widget.Toast;

import com.hehe.soma.manager.GameManager;
import com.hehe.soma.manager.SceneManager;
import com.hehe.soma.manager.SceneManager.SceneType;

public class MainMenuScene extends ParentScene implements IOnMenuItemClickListener
{   
	//main menu entities
	private MenuScene menuChildScene;
	private Sprite background,siga,huza,fora,
	ubaka,Buildarrow,Eggarrow,Paintarrow,Poparrow;
	private Text WarnText,RAMinfo;
	private Sprite warning;
	int count=0;

	private LoopEntityModifier PointToUbaka,PointToHuza,PointToSiga,PointToFora;
	
	//game modes button IDs and other misc buttons
	private final int BUILD_PLAY = 0;
	private final int EGG_PLAY =1;
	private final int PAINT_PLAY =2;
	private final int POP_PLAY =3;
	private final int MENU_OPTIONS = 4;
	private final int ABOUT = 5;
	private final int EXIT_PLAY = 6;
	

	@Override
	public SceneType getSceneType()
	{
		return SceneType.SCENE_MENU;
	}

	@Override
	public void createScene()
	{   
		System.gc(); /*we were forced to do this gc since 
		small devices crash the game*/
		SceneManager.getInstance().playMenuBackMusic();
		engine.enableVibrator(activity);
		GameManager.getInstance().setPaused(false);
		createBackground();
		createMenuChildScene();
		initIllustrations();
		Menulooper();
		updateMemoryInfo();
	}



	private void createMenuChildScene()
	{
		menuChildScene = new MenuScene(camera);
		menuChildScene.setPosition(0, 0);

		final IMenuItem build_playMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(BUILD_PLAY, resourcesManager.build_menubutton_region, vbom),0.80f,1);
		final IMenuItem egg_playMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(EGG_PLAY, resourcesManager.egg_menubutton_region, vbom), 0.80f, 1);
		final IMenuItem paint_playMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(PAINT_PLAY, resourcesManager.paint_menubutton_region, vbom), 0.80f,1);
		final IMenuItem pop_playMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(POP_PLAY, resourcesManager.pop_menubutton_region, vbom), 0.80f, 1);

		final IMenuItem optionsMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_OPTIONS, resourcesManager.options_region, vbom), 0.85f,0.75f);
		final IMenuItem aboutMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(ABOUT, resourcesManager.about_region, vbom), 0.85f,0.75f);
		final IMenuItem exitMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(EXIT_PLAY, resourcesManager.exit_region, vbom), 0.85f,0.75f); 

		menuChildScene.addMenuItem(build_playMenuItem);
		menuChildScene.addMenuItem(pop_playMenuItem);
		menuChildScene.addMenuItem(paint_playMenuItem);
		menuChildScene.addMenuItem(egg_playMenuItem);
		menuChildScene.addMenuItem(optionsMenuItem);
		menuChildScene.addMenuItem(aboutMenuItem);
		menuChildScene.addMenuItem(exitMenuItem);
		menuChildScene.buildAnimations();
		menuChildScene.setBackgroundEnabled(false);

		//place buttons on scene
		build_playMenuItem.setPosition(520,20);
		pop_playMenuItem.setPosition(650,20);
		paint_playMenuItem.setPosition(520,150);
		egg_playMenuItem.setPosition(650,150);

		optionsMenuItem.setScale(0.75f);
		aboutMenuItem.setScale(0.75f);
		exitMenuItem.setScale(0.75f);
		optionsMenuItem.setPosition(560,320);
		aboutMenuItem.setPosition(560,375);
		exitMenuItem.setPosition(560,430);

		menuChildScene.setOnMenuItemClickListener(this);  
		setChildScene(menuChildScene);
	}

	//listen on button click
	@Override
	public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem, float pMenuItemLocalX, float pMenuItemLocalY)
	{      

		switch(pMenuItem.getID())
		{
		case BUILD_PLAY:

			SceneManager.getInstance().pauseMenuBackMusic();
			SceneManager.getInstance().playMenuButtonSound();
			if(GameManager.getInstance().isVibrationEnabled()) {
				engine.vibrate(100);
			}
			SceneManager.getInstance().loadBuildScene(engine);
			return true;
		case EGG_PLAY:

			SceneManager.getInstance().pauseMenuBackMusic();
			SceneManager.getInstance().playMenuButtonSound();
			if(GameManager.getInstance().isVibrationEnabled()) {
				engine.vibrate(100);
			}
			SceneManager.getInstance().loadEggScene(engine);
			return true;
		case PAINT_PLAY:

			SceneManager.getInstance().pauseMenuBackMusic();
			SceneManager.getInstance().playMenuButtonSound();
			if(GameManager.getInstance().isVibrationEnabled()) {
				engine.vibrate(100);
			}
			SceneManager.getInstance().loadPaintScene(engine);
			return true;
		case POP_PLAY:
			SceneManager.getInstance().pauseMenuBackMusic();
			SceneManager.getInstance().playMenuButtonSound();
			if(GameManager.getInstance().isVibrationEnabled()) {
				engine.vibrate(100);
			}
			SceneManager.getInstance().loadPopScene(engine);
			return true;
		case MENU_OPTIONS:
			SceneManager.getInstance().pauseMenuBackMusic();
			if(GameManager.getInstance().isVibrationEnabled()) {
				engine.vibrate(200);
			}
			SceneManager.getInstance().loadOptionScene(engine);
			return true;
		case ABOUT:
			SceneManager.getInstance().loadAboutScene(engine);
			return true;
		case EXIT_PLAY:        	
			activity.finish();
			System.exit(0); //complete exit
		default:
			return false;
		}
	}


	@Override
	public void disposeScene()
	{ 
		this.clearEntityModifiers();
		this.clearUpdateHandlers();
		this.clearTouchAreas();
		this.clearChildScene();
		this.detachChildren(); //detach all children
		this.detachSelf();
		this.dispose();
		System.gc();	
	}

	private void initIllustrations(){

		this.PointToUbaka=new LoopEntityModifier(new MoveModifier(4f,620,590,120,90));
		this.PointToHuza=new LoopEntityModifier(new MoveModifier(4f,640,680,130,160));

		this.PointToSiga=new LoopEntityModifier(new MoveModifier(4f,620,590,130,160));
		this.PointToFora=new LoopEntityModifier(new MoveModifier(4f,640,680,130,90));

		ubaka=new Sprite(0, 0, resourcesManager.ubaka_show_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		ubaka.setPosition(612,110);
		ubaka.setScale(0);
		ubaka.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		attachChild(ubaka);

		huza=new Sprite(0, 0, resourcesManager.huza_show_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		huza.setPosition(612,110);
		huza.setScale(0);
		huza.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		attachChild(huza);

		siga=new Sprite(0, 0, resourcesManager.siga_show_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		siga.setPosition(612,110);
		siga.setScale(0);
		siga.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		attachChild(siga);



		fora=new Sprite(0, 0, resourcesManager.fora_show_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		fora.setPosition(612,110);
		fora.setScale(0);
		fora.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		attachChild(fora);

		Buildarrow=new Sprite(0, 0, resourcesManager.menu_arrow_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		Buildarrow.setPosition(580,90);
		Buildarrow.setScale(0);
		Buildarrow.setRotation(-45);
		Buildarrow.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		Buildarrow.registerEntityModifier(new LoopEntityModifier(new ParallelEntityModifier(new AlphaModifier(3,1f,0.2f),new ColorModifier(3,1,1, 1, 0.3f, 0.7f,0.9f))));
		attachChild(Buildarrow);

		Eggarrow=new Sprite(0, 0, resourcesManager.menu_arrow_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		Eggarrow.setPosition(620,100);
		Eggarrow.setScale(0);
		Eggarrow.setRotation(150);
		Eggarrow.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		Eggarrow.registerEntityModifier(new LoopEntityModifier(new ParallelEntityModifier(new AlphaModifier(3,1f,0.2f),new ColorModifier(3,1,1, 1, 0.3f, 0.7f,0.9f))));
		attachChild(Eggarrow);

		Paintarrow=new Sprite(0, 0, resourcesManager.menu_arrow_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		Paintarrow.setPosition(620,100);
		Paintarrow.setScale(0);
		Paintarrow.setRotation(-150);
		Paintarrow.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		Paintarrow.registerEntityModifier(new LoopEntityModifier(new ParallelEntityModifier(new AlphaModifier(3,1f,0.2f),new ColorModifier(3,1,1, 1, 0.3f, 0.7f,0.9f))));
		attachChild(Paintarrow);

		Poparrow=new Sprite(0, 0, resourcesManager.menu_arrow_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		Poparrow.setPosition(600,90);
		Poparrow.setScale(0);
		Poparrow.setRotation(45);
		Poparrow.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		Poparrow.registerEntityModifier(new LoopEntityModifier(new ParallelEntityModifier(new AlphaModifier(3,1f,0.2f),new ColorModifier(3,1,1, 1, 0.3f, 0.7f,0.9f))));
		attachChild(Poparrow);

		Paintarrow.registerEntityModifier(PointToSiga);	
		Poparrow.registerEntityModifier(PointToFora);
		Buildarrow.registerEntityModifier(PointToUbaka);
		Eggarrow.registerEntityModifier(PointToHuza);

	}

	private void Menulooper(){

		displayIllustration(0);

		TimerHandler looper = new TimerHandler(4f,true,new ITimerCallback() {
			int position=0;
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
				position++;
				if(position==4){
					position=0;
				}

				displayIllustration(position);
			}

		});
		registerUpdateHandler(looper);  

	}



	private void displayIllustration(int gamemode){


		switch(gamemode){
		case 0:
			ubaka.registerEntityModifier(new AlphaModifier(4f,0,1));
			siga.setScale(0);
			fora.setScale(0);
			huza.setScale(0);
			ubaka.setScale(2);
			ubaka.setAlpha(0.75f);

			Buildarrow.setScale(1);
			Eggarrow.setScale(0);
			Paintarrow.setScale(0);
			Poparrow.setScale(0);

			break;
		case 2:
			huza.registerEntityModifier(new AlphaModifier(4f,0,1));
			siga.setScale(0);
			fora.setScale(0);
			huza.setScale(2);
			ubaka.setScale(0);
			Buildarrow.setScale(0);
			Eggarrow.setScale(1);
			Paintarrow.setScale(0);
			Poparrow.setScale(0);


			break;
		case 1:
			siga.registerEntityModifier(new AlphaModifier(4f,0,1));
			siga.setScale(2);
			fora.setScale(0);
			huza.setScale(0);
			ubaka.setScale(0);
			Buildarrow.setScale(0);
			Eggarrow.setScale(0);
			Paintarrow.setScale(1);
			Poparrow.setScale(0);

			break;
		case 3:
			fora.registerEntityModifier(new AlphaModifier(4f,0,1));
			siga.setScale(0);
			fora.setScale(2);
			huza.setScale(0);
			ubaka.setScale(0);
			Buildarrow.setScale(0);
			Eggarrow.setScale(0);
			Paintarrow.setScale(0);
			Poparrow.setScale(1);
			break;

		default:
			break;
		}
	}


	private void createBackground()
	{

		background=new Sprite(0, 0, resourcesManager.menu_background_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		background.setPosition(0,0);
		background.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		background.registerEntityModifier(new LoopEntityModifier(new ParallelEntityModifier(new AlphaModifier(10,0.7f,0.4f),new ColorModifier(10,1,0.9f, 1, 0.7f, 0.7f,0.9f))));
		attachChild(background);

		warning=new Sprite(0, 0, resourcesManager.showwarn_region, vbom)
		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   

				if (pSceneTouchEvent.isActionUp())
				{   
					WarnText.setScale(0);
					RAMinfo.setScale(0);
					warning.setScale(0);
				

				}
				return true;
			};
		};
		warning.setPosition(100,camera.getHeight()-warning.getHeight()-25);
		attachChild(warning);
		registerTouchArea(warning);
		warning.setScale(0);

		WarnText=new Text(warning.getX()+warning.getWidth()-15,camera.getHeight()-warning.getHeight()-50, resourcesManager.WarnFont,
				"Telefone yawe ifite ibyangombwa bidahagije!\n Funga izindi apps udakeneye cyangwa\n ujye usubiramo SOMA nijya yifunga.", vbom);	
		attachChild(WarnText);	
		WarnText.setScale(0);

		RAMinfo=new Text(warning.getX()+warning.getWidth()-15,camera.getHeight()-warning.getHeight()-55+WarnText.getHeight(), resourcesManager.WarnFont,
				"  RAM isigaye: 100 MB,ikenewe: 128 MB ", vbom);	
		attachChild(RAMinfo);	
		RAMinfo.setScale(0);

	}

	private void postMemInfo(){


		WarnText.setScale(0.75f);
		RAMinfo.setScale(0.75f);
		warning.setScale(1.25f);

		TimerHandler waitForWarning = new TimerHandler(17f,false,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
							
					warning.setScale(0);
				    WarnText.setScale(0);
				    RAMinfo.setScale(0);
			}

		});
		registerUpdateHandler(waitForWarning);
	}

	private void updateMemoryInfo(){

		TimerHandler MemCheckDaemon = new TimerHandler(20f,true,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {

				MemoryInfo mi = new MemoryInfo();
				ActivityManager activityManager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
				if(mi!=null){
					activityManager.getMemoryInfo(mi);
					long availableMegs = mi.availMem / 1048576L;

					final String badmemory="  RAM isigaye: "+availableMegs+" MB,ikenewe: 128 MB";
					if(availableMegs<128L){	
						RAMinfo.setText(badmemory);
						postMemInfo();
					}

				}

			}

		});
		registerUpdateHandler(MemCheckDaemon);
	}

	// handle Back and Menu Hw key presses

	@Override
	public void onBackKeyPressed()
	{
		count++;

		if(count>1) {
			activity.finish();
			System.exit(0); //in order to avoid monkey business
		}
		else
			Toast.makeText(activity, "ONGERA UKANDE KUGIRA NGO\nSOMA IBASHYE GUFUNGA",Toast.LENGTH_LONG).show();  

	}


	@Override
	public void onMenuKeyPressed() {
		// do same thing as if options button was pressed
		SceneManager.getInstance().pauseMenuBackMusic();
		if(GameManager.getInstance().isVibrationEnabled()) {
			engine.enableVibrator(activity);
			engine.vibrate(100);
		}
		SceneManager.getInstance().loadOptionScene(engine);
	}


}
