/**
 @company: Hehe Ltd
 @Project: Soma
 @Date: mm/dd/yyyy
 @Author: Amiri Mugarura and Sixbert Uwiringiyimana
 @Credits: thanks given to Richard Rusa and other artists for graphic designs ....

 @About this Class "BuildLetterScene.java":
 ----------------------------------------

 */

package com.hehe.soma.scene;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.ColorModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.util.GLState;
import org.andengine.util.math.MathUtils;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.opengl.GLES20;
import android.widget.Toast;

import com.hehe.soma.manager.GameManager;
import com.hehe.soma.manager.SceneManager;
import com.hehe.soma.manager.SceneManager.SceneType;

public class BuildLetterScene extends ParentScene implements IOnMenuItemClickListener {

	protected MenuScene buildMenuScene;
	protected static final int MENU_RESET = 0;
	protected static final int MENU_CANCEL = 1;
	protected static final int MENU_QUIT = 2;

	private MoveModifier toPart0, toPart1, toPart2, toPart3, toPart4, toPart5,
	toLetter0, toLetter1, toLetter2, toLetter3, toLetter4, toLetter5;

	private MoveModifier DragPart0, DragPart1, DragPart2, DragPart3, DragPart4,DragPart5;

	private TimerHandler postLetter;

	// variables
	private Sprite background, canvas, showmenu, house1, finger;

	private Sprite roof, hangar0, hangar1, hangar2, hangar3, hangar4, hangar5;
	// letter a
	private Sprite Letter_a, Letter_a1, Letter_a2, Letter_a3;
	// letter A
	private Sprite Letter_A, Letter_A1, Letter_A2, Letter_A3;

	// letter b
	private Sprite Letter_b, Letter_b1, Letter_b2, Letter_b3;
	// letter B
	private Sprite Letter_B, Letter_B1, Letter_B2, Letter_B3;

	// letter c
	private Sprite Letter_d, Letter_d1, Letter_d2, Letter_d3;
	// letter C
	private Sprite Letter_D, Letter_D1, Letter_D2, Letter_D3;
	// letter c
	private Sprite Letter_c, Letter_c1, Letter_c2, Letter_c3;
	// letter C
	private Sprite Letter_C, Letter_C1, Letter_C2, Letter_C3;

	// letter d
	private Sprite Letter_e, Letter_e1, Letter_e2, Letter_e3;
	// letter D
	private Sprite Letter_E, Letter_E1, Letter_E2, Letter_E3;

	// letter e
	private Sprite Letter_f, Letter_f1, Letter_f2, Letter_f3;
	// letter E
	private Sprite Letter_F, Letter_F1, Letter_F2, Letter_F3;
	// letter f
	private Sprite Letter_g, Letter_g1, Letter_g2, Letter_g3;
	// letter F
	private Sprite Letter_G, Letter_G1, Letter_G2, Letter_G3;
	// letter g
	private Sprite Letter_h, Letter_h1, Letter_h2, Letter_h3;
	// letter G
	private Sprite Letter_H, Letter_H1, Letter_H2, Letter_H3;


	// letter b
	private Sprite Letter_k, Letter_k1, Letter_k2, Letter_k3;
	// letter B
	private Sprite Letter_K, Letter_K1, Letter_K2, Letter_K3;

	// letter c
	private Sprite Letter_l, Letter_l1, Letter_l2, Letter_l3;
	// letter C
	private Sprite Letter_L, Letter_L1, Letter_L2, Letter_L3;
	// letter c
	private Sprite Letter_m, Letter_m1, Letter_m2, Letter_m3;
	// letter C
	private Sprite Letter_M, Letter_M1, Letter_M2, Letter_M3;

	// letter d
	private Sprite Letter_n, Letter_n1, Letter_n2, Letter_n3;
	// letter D
	private Sprite Letter_N, Letter_N1, Letter_N2, Letter_N3;

	// letter e
	private Sprite Letter_o, Letter_o1, Letter_o2, Letter_o3;
	// letter E
	private Sprite Letter_O, Letter_O1, Letter_O2, Letter_O3;

	private Sprite Letter_r, Letter_r1, Letter_r2, Letter_r3;
	// letter G
	private Sprite Letter_R, Letter_R1, Letter_R2, Letter_R3;
	// letter h
	private Sprite Letter_s, Letter_s1, Letter_s2, Letter_s3;

	private Sprite Letter_S, Letter_S1, Letter_S2, Letter_S3;
	// letter H


	private Sprite Letter_u, Letter_u1, Letter_u2, Letter_u3;

	// letter e
	private Sprite Letter_U, Letter_U1, Letter_U2, Letter_U3;
	// letter E
	private Sprite Letter_v, Letter_v1, Letter_v2, Letter_v3;
	// letter f
	private Sprite Letter_V, Letter_V1, Letter_V2, Letter_V3;
	// letter F
	private Sprite Letter_w, Letter_w1, Letter_w2, Letter_w3;
	// letter g
	private Sprite Letter_W, Letter_W1, Letter_W2, Letter_W3;
	// letter z
	private Sprite Letter_z, Letter_z1, Letter_z2, Letter_z3;
	// letter Z
	private Sprite Letter_Z, Letter_Z1, Letter_Z2, Letter_Z3;
	
	private Text WarnText,RAMinfo;
	private Sprite warning;

	private final float InitX = 550, InitLowY = 20, InitCapY = 250;

	private final float initLetterX1 = 20, initLetterX2 = 120,
			initLetterX3 = 220, InitCapX1 = 20, InitCapX2 = 150,
			InitCapX3 = 340;
	private final float initLetterY1 = 20, initLetterY2 = 80,
			initLetterY3 = 20, InitCapY1 = 200, InitCapY2 = 120,
			InitCapY3 = 240;

	// up row
	private final float InitHangarRow1 = 200;
	// down row
	private final float InitHangarRow2 = 130;

	private final float InitHangarX0 = 275;
	private final float InitHangarCapX0 = 225;

	private final float InitHangarX1 = 390;
	private final float InitHangarCapX1 = 350;

	private final float InitHangarX2 = 470;
	private final float InitHangarCapX2 = 420;

	private ParallelEntityModifier Hanger0Part0Mod;
	private ParallelEntityModifier Hanger0Part1Mod;

	private ParallelEntityModifier Hanger1Part0Mod;
	private ParallelEntityModifier Hanger1Part1Mod;

	private ParallelEntityModifier Hanger2Part0Mod;
	private ParallelEntityModifier Hanger2Part1Mod;

	private ParallelEntityModifier Hanger3Part0Mod;
	private ParallelEntityModifier Hanger3Part1Mod;

	private ParallelEntityModifier Hanger4Part0Mod;
	private ParallelEntityModifier Hanger4Part1Mod;

	private ParallelEntityModifier Hanger5Part0Mod;
	private ParallelEntityModifier Hanger5Part1Mod;
	private TimerHandler waitForMenu;

	private float Tolerance = 10;

	private boolean isHangarFree0 = true, isHangarFree1 = true,
			isHangarFree2 = true, isHangarFree3 = true, isHangarFree4 = true,
			isHangarFree5 = true;

	private boolean isDemoRunning; // //never initialize.
	private boolean isVictoryUp = false;
	private boolean isHanger0Part0Used = false;
	private boolean isHanger0Part1Used = false;

	private boolean isHanger1Part0Used = false;
	private boolean isHanger1Part1Used = false;

	private boolean isHanger2Part0Used = false;
	private boolean isHanger2Part1Used = false;

	private boolean isHanger3Part0Used = false;
	private boolean isHanger3Part1Used = false;

	private boolean isHanger4Part0Used = false;
	private boolean isHanger4Part1Used = false;

	private boolean isHanger5Part0Used = false;
	private boolean isHanger5Part1Used = false;

	private boolean Hangar0Ack = false;
	private boolean Hangar1Ack = false;
	private boolean Hangar2Ack = false;
	private boolean Hangar3Ack = false;
	private boolean Hangar4Ack = false;
	private boolean Hangar5Ack = false;

	private boolean hasVictorySequenceStarted=false;

	private AnimatedSprite buildSupernka;

	private boolean is_a_PartUp1 = false, is_a_PartUp2 = false,
			is_a_PartUp3 = false;
	private boolean is_A_PartUp1 = false, is_A_PartUp2 = false,
			is_A_PartUp3 = false;

	private boolean is_b_PartUp1 = false, is_b_PartUp2 = false,
			is_b_PartUp3 = false;
	private boolean is_B_PartUp1 = false, is_B_PartUp2 = false,
			is_B_PartUp3 = false;

	private boolean is_c_PartUp1 = false, is_c_PartUp2 = false,
			is_c_PartUp3 = false;
	private boolean is_C_PartUp1 = false, is_C_PartUp2 = false,
			is_C_PartUp3 = false;

	private boolean is_d_PartUp1 = false, is_d_PartUp2 = false,
			is_d_PartUp3 = false;
	private boolean is_D_PartUp1 = false, is_D_PartUp2 = false,
			is_D_PartUp3 = false;

	private boolean is_e_PartUp1 = false, is_e_PartUp2 = false,
			is_e_PartUp3 = false;
	private boolean is_E_PartUp1 = false, is_E_PartUp2 = false,
			is_E_PartUp3 = false;

	private boolean is_f_PartUp1 = false, is_f_PartUp2 = false,
			is_f_PartUp3 = false;
	private boolean is_F_PartUp1 = false, is_F_PartUp2 = false,
			is_F_PartUp3 = false;

	private boolean is_g_PartUp1 = false, is_g_PartUp2 = false,
			is_g_PartUp3 = false;
	private boolean is_G_PartUp1 = false, is_G_PartUp2 = false,
			is_G_PartUp3 = false;

	private boolean is_h_PartUp1 = false, is_h_PartUp2 = false,
			is_h_PartUp3 = false;
	private boolean is_H_PartUp1 = false, is_H_PartUp2 = false,
			is_H_PartUp3 = false;

	private boolean is_k_PartUp1 = false, is_k_PartUp2 = false,
			is_k_PartUp3 = false;
	private boolean is_K_PartUp1 = false, is_K_PartUp2 = false,
			is_K_PartUp3 = false;

	private boolean is_l_PartUp1 = false, is_l_PartUp2 = false,
			is_l_PartUp3 = false;
	private boolean is_L_PartUp1 = false, is_L_PartUp2 = false,
			is_L_PartUp3 = false;

	private boolean is_m_PartUp1 = false, is_m_PartUp2 = false,
			is_m_PartUp3 = false;
	private boolean is_M_PartUp1 = false, is_M_PartUp2 = false,
			is_M_PartUp3 = false;

	private boolean is_n_PartUp1 = false, is_n_PartUp2 = false,
			is_n_PartUp3 = false;
	private boolean is_N_PartUp1 = false, is_N_PartUp2 = false,
			is_N_PartUp3 = false;

	private boolean is_o_PartUp1 = false, is_o_PartUp2 = false,
			is_o_PartUp3 = false;
	private boolean is_O_PartUp1 = false, is_O_PartUp2 = false,
			is_O_PartUp3 = false;



	private boolean is_R_PartUp1 = false, is_R_PartUp2 = false,
			is_R_PartUp3 = false;
	private boolean is_r_PartUp1 = false, is_r_PartUp2 = false,
			is_r_PartUp3 = false;

	private boolean is_s_PartUp1 = false, is_s_PartUp2 = false,
			is_s_PartUp3 = false;
	private boolean is_S_PartUp1 = false, is_S_PartUp2 = false,
			is_S_PartUp3 = false;



	private boolean is_u_PartUp1 = false, is_u_PartUp2 = false,
			is_u_PartUp3 = false;
	private boolean is_U_PartUp1 = false, is_U_PartUp2 = false,
			is_U_PartUp3 = false;

	private boolean is_v_PartUp1 = false, is_v_PartUp2 = false,
			is_v_PartUp3 = false;
	private boolean is_V_PartUp1 = false, is_V_PartUp2 = false,
			is_V_PartUp3 = false;

	private boolean is_w_PartUp1 = false, is_w_PartUp2 = false,
			is_w_PartUp3 = false;
	private boolean is_W_PartUp1 = false, is_W_PartUp2 = false,
			is_W_PartUp3 = false;



	private boolean is_z_PartUp1 = false, is_z_PartUp2 = false,
			is_z_PartUp3 = false;
	private boolean is_Z_PartUp1 = false, is_Z_PartUp2 = false,
			is_Z_PartUp3 = false;

	private int randBuildLetterIndex0;
	private int randBuildLetterIndex1;
	private int randBuildLetterIndex2;
	private int randBuildLetterIndex3;
	private int randBuildLetterIndex4;
	private int randBuildLetterIndex5;

	@Override
	public void createScene() {
		createMenuScene();
		SceneManager.getInstance().playGameModeBackMusic();
		SceneManager.getInstance().playBuildGuidanceAudio();
		initLetterParts();
		doDemo();
		updateMemoryInfo();
	}

	private boolean avoidOffCameraDrag(Sprite sprite) {

		if (sprite.getX() >= camera.getWidth()|| sprite.getY() >= camera.getHeight() || sprite.getX() <= 0|| sprite.getY() <= 0) {
			if (sprite.getX() >= camera.getWidth())
				sprite.setPosition(camera.getWidth() - 10, sprite.getY());
			if (sprite.getX() <= 0)
				sprite.setPosition(10, sprite.getY());
			if (sprite.getY() >= camera.getHeight())
				sprite.setPosition(sprite.getX(), camera.getHeight() - 10);
			if (sprite.getY() <= 0)
				sprite.setPosition(sprite.getX(), 10);
			return false;
		}
		else
			return true;
	}

	private void initLetterParts() {

		randBuildLetterIndex0 = MathUtils.random(0, 2);
		randBuildLetterIndex1 = MathUtils.random(3, 5);
		randBuildLetterIndex2 = MathUtils.random(6, 9);
		randBuildLetterIndex3 = MathUtils.random(10, 12);
		randBuildLetterIndex4 = MathUtils.random(13, 15);
		randBuildLetterIndex5 = MathUtils.random(16, 18);

		warning=new Sprite(0, 0, resourcesManager.showwarn_region, vbom)
		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   

				if (pSceneTouchEvent.isActionUp())
				{   
					WarnText.setScale(0);
					RAMinfo.setScale(0);
					warning.setScale(0);
				

				}
				return true;
			};
		};
		warning.setPosition(100,camera.getHeight()-warning.getHeight()-25);
		attachChild(warning);
		registerTouchArea(warning);
		warning.setScale(0);

		WarnText=new Text(warning.getX()+warning.getWidth()-15,camera.getHeight()-warning.getHeight()-50, resourcesManager.WarnFont,
				"Telefone yawe ifite ibyangombwa bidahagije!\n Funga izindi apps udakeneye cyangwa\n ujye usubiramo SOMA nijya yifunga.", vbom);	
		attachChild(WarnText);	
		WarnText.setScale(0);

		RAMinfo=new Text(warning.getX()+warning.getWidth()-15,camera.getHeight()-warning.getHeight()-55+WarnText.getHeight(), resourcesManager.WarnFont,
				"  RAM isigaye: 100 MB,ikenewe: 128 MB ", vbom);	
		attachChild(RAMinfo);	
		RAMinfo.setScale(0);
		
		RAMinfo.setZIndex(4);
		WarnText.setZIndex(4);
		warning.setZIndex(4);
		
		background = new Sprite(0, 0, resourcesManager.build_background_region,vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		background.setScale(1.0f);
		background.setPosition(0, 0);
		background.setZIndex(0);

		this.canvas = new Sprite(0, 0, resourcesManager.canvas_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		canvas.setScale(0);
		canvas.setColor(0, 0.95f, 0.95f, 0.75f);
		canvas.setPosition(camera.getCenterX() - 250, camera.getCenterY() - 175);
		canvas.setZIndex(6);

		this.showmenu = new Sprite(0, 0, resourcesManager.showmenu_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (pSceneTouchEvent.isActionUp()) {
					if (!isDemoRunning) {
						showBuildMenu();
						SceneManager.getInstance()
						.playOnSubMenuButtonClickSound();
						SceneManager.getInstance().vibrate(200);
					} else {
						SceneManager.getInstance().vibrate(100);
					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}
				return true;
			};
		};
		showmenu.setScale(0.5f);
		showmenu.setAlpha(0.5f);
		showmenu.setPosition(camera.getWidth() - 80, camera.getHeight() - 80);
		showmenu.setZIndex(6);
		registerTouchArea(showmenu);

		finger = new Sprite(0, 0, resourcesManager.Finger_region, vbom) {
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

		};
		finger.setScale(1);
		finger.setPosition(0, camera.getHeight());
		finger.setZIndex(6);
		attachChild(finger);

		house1 = new Sprite(0, 0, resourcesManager.build_house1_region, vbom);
		house1.setPosition(200, 5);
		house1.setScale(0);
		house1.setAlpha(1);
		house1.setZIndex(1);

		roof = new Sprite(0, 0, resourcesManager.build_roof_region, vbom);
		roof.setZIndex(2);
		roof.setPosition(200, 5);
		roof.setScale(1);
		roof.setAlpha(1);

		buildSupernka = new AnimatedSprite(0, 0,
				resourcesManager.build_supernka_region, vbom);
		buildSupernka.setZIndex(4);
		buildSupernka.setScale(0);

		hangar0 = new Sprite(0, 0, resourcesManager.build_hangar0_region, vbom);
		hangar0.setPosition(260, 260);
		hangar0.setScale(1.2f);
		hangar0.setAlpha(1);
		hangar0.setZIndex(1);

		hangar1 = new Sprite(0, 0, resourcesManager.build_hangar1_region, vbom);
		hangar1.setPosition(367, 260);
		hangar1.setScale(1.2f);
		hangar1.setAlpha(1);
		hangar1.setZIndex(1);

		hangar2 = new Sprite(0, 0, resourcesManager.build_hangar2_region, vbom);
		hangar2.setPosition(467, 260);
		hangar2.setScale(1.2f);
		hangar2.setAlpha(1);
		hangar2.setZIndex(1);

		hangar3 = new Sprite(0, 0, resourcesManager.build_hangar3_region, vbom);
		hangar3.setPosition(260, 180);
		hangar3.setScale(1.2f);
		hangar3.setAlpha(1);
		hangar3.setZIndex(1);

		hangar4 = new Sprite(0, 0, resourcesManager.build_hangar4_region, vbom);
		hangar4.setPosition(367, 180);
		hangar4.setScale(1.2f);
		hangar4.setAlpha(1);
		hangar4.setZIndex(1);

		hangar5 = new Sprite(0, 0, resourcesManager.build_hangar5_region, vbom);
		hangar5.setPosition(467, 180);
		hangar5.setScale(1.2f);
		hangar5.setAlpha(1);
		hangar5.setZIndex(1);

		Letter_a = new Sprite(0, 0, resourcesManager.build_a_region, vbom);
		Letter_a.setPosition(InitX, InitLowY);
		Letter_a.setScale(0);
		Letter_a.setAlpha(1);
		Letter_a.setZIndex(3);

		// this is so important that it hit me for a while...
		setTouchAreaBindingOnActionMoveEnabled(true);
		setTouchAreaBindingOnActionDownEnabled(true);

		Letter_a1 = new Sprite(0, 0, resourcesManager.build_a1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_a_PartUp1 && avoidOffCameraDrag(Letter_a1)) {
					Letter_a1.setPosition(pSceneTouchEvent.getX()- Letter_a1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_a1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_a1.collidesWith(Letter_a)) {

						if (((Letter_a1.getX() + Letter_a1.getWidth()) >= (Letter_a.getX() + Letter_a.getWidth() - Tolerance))
								|| ((Letter_a1.getX() + Letter_a1.getWidth()) <= (Letter_a.getX() + Letter_a.getWidth() + Tolerance))) {

							if (((Letter_a1.getY() + Letter_a1.getHeight()) >= (Letter_a.getY() + Letter_a.getHeight() - Tolerance))
									|| ((Letter_a1.getY() + Letter_a1.getHeight()) <= (Letter_a.getY()+ Letter_a.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_a1.setPosition(Letter_a.getX(),Letter_a.getY());

								is_a_PartUp1 = true;
								checkLetterOnComplete(Letter_a);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_a1.setPosition(initLetterX1, initLetterY1);
		Letter_a1.setAlpha(1);
		Letter_a1.setZIndex(4);
		Letter_a1.setScale(0);

		registerTouchArea(Letter_a1);

		Letter_a2 = new Sprite(0, 0, resourcesManager.build_a2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_a_PartUp2 && avoidOffCameraDrag(Letter_a2)) {
					Letter_a2.setPosition(pSceneTouchEvent.getX()- Letter_a2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_a2.getHeight() / 2);
				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_a2.collidesWith(Letter_a)) {

						if (((Letter_a2.getX() + Letter_a2.getWidth()) >= (Letter_a.getX() - Tolerance))
								|| ((Letter_a2.getX() + Letter_a1.getWidth()) <= (Letter_a.getX() + Tolerance))) {

							if (((Letter_a2.getY() + Letter_a2.getHeight()) >= (Letter_a.getY() + Letter_a.getHeight() - Tolerance))
									|| ((Letter_a2.getY() + Letter_a2.getHeight()) <= (Letter_a.getY()+ Letter_a.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_a2.setPosition(Letter_a.getX()+Letter_a.getWidth()-Letter_a2.getWidth(),Letter_a.getY());

								is_a_PartUp2 = true;
								checkLetterOnComplete(Letter_a);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_a2.setPosition(initLetterX2, initLetterY2);
		Letter_a2.setAlpha(1);
		Letter_a2.setZIndex(4);
		Letter_a2.setScale(0);
		registerTouchArea(Letter_a2);

		Letter_a3 = new Sprite(0, 0, resourcesManager.build_a3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_a_PartUp3 && avoidOffCameraDrag(Letter_a3)) {

					Letter_a3
					.setPosition(
							pSceneTouchEvent.getX()
							- Letter_a3.getWidth() / 2,
							pSceneTouchEvent.getY()
							- Letter_a3.getHeight() / 2);

				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_a3.collidesWith(Letter_a)) {

						if (((Letter_a3.getX()) >= (Letter_a.getX() - Tolerance))
								|| ((Letter_a3.getX()) <= (Letter_a.getX() + Tolerance))) {

							if (((Letter_a3.getY()) >= (Letter_a.getY() - Tolerance))
									|| ((Letter_a3.getY()) <= (Letter_a.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_a3.setPosition(Letter_a.getX(),Letter_a.getY()+Letter_a.getHeight()-Letter_a3.getHeight());

								is_a_PartUp3 = true;
								checkLetterOnComplete(Letter_a);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_a3.setPosition(initLetterX3, initLetterY3);
		Letter_a3.setAlpha(1);
		Letter_a3.setZIndex(4);
		Letter_a3.setScale(0);

		registerTouchArea(Letter_a3);

		Letter_A = new Sprite(0, 0, resourcesManager.build_A_region, vbom);
		Letter_A.setPosition(InitX, InitCapY);
		Letter_A.setAlpha(1);
		Letter_A.setZIndex(3);

		Letter_A.setScale(0);

		Letter_A1 = new Sprite(0, 0, resourcesManager.build_A1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_A_PartUp1 && avoidOffCameraDrag(Letter_A1)) {
					Letter_A1.setPosition(pSceneTouchEvent.getX()- Letter_A1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_A1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_A1.collidesWith(Letter_A)) {

						if (((Letter_A1.getX()) >= (Letter_A.getX() - Tolerance))
								|| ((Letter_A1.getX()) <= (Letter_A.getX() + Tolerance))) {

							if (((Letter_A1.getY()) >= (Letter_A.getY() - Tolerance))
									|| ((Letter_A1.getY()) <= (Letter_A.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_A1.setPosition(Letter_A.getX()+18.5f,Letter_A.getY());
								is_A_PartUp1 = true;
								checkLetterOnComplete(Letter_A);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_A1.setPosition(InitCapX1, InitCapY1);
		Letter_A1.setAlpha(1);
		Letter_A1.setZIndex(4);
		Letter_A1.setScale(0);

		registerTouchArea(Letter_A1);

		Letter_A2 = new Sprite(0, 0, resourcesManager.build_A2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_A_PartUp2 && avoidOffCameraDrag(Letter_A2)) {

					Letter_A2.setPosition(pSceneTouchEvent.getX()- Letter_A2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_A2.getHeight() / 2);

				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_A2.collidesWith(Letter_A)) {

						if (((Letter_A2.getX() >= (Letter_A.getX() - Tolerance)) 
								|| (Letter_A2.getX() <= (Letter_A.getX() + Tolerance)))) {

							if (((Letter_A2.getY() + Letter_A2.getHeight()) >= (Letter_A.getY() + Letter_A.getHeight() - Tolerance))
									|| ((Letter_A2.getY() + Letter_A2.getHeight()) <= (Letter_A.getY()+ Letter_A.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_A2.setPosition(Letter_A.getX()+3,Letter_A.getY() + Letter_A1.getHeight());

								is_A_PartUp2 = true;
								checkLetterOnComplete(Letter_A);
							}
						}

					}
				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_A2.setPosition(InitCapX2, InitCapY2);
		Letter_A2.setAlpha(1);
		Letter_A2.setZIndex(4);

		Letter_A2.setScale(0);
		registerTouchArea(Letter_A2);

		Letter_A3 = new Sprite(0, 0, resourcesManager.build_A3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_A_PartUp3 && avoidOffCameraDrag(Letter_A3)) {
					Letter_A3.setPosition(pSceneTouchEvent.getX()- Letter_A3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_A3.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_A3.collidesWith(Letter_A)) {

						if (((Letter_A3.getX() + Letter_A3.getWidth()) >= (Letter_A.getX() + Letter_A.getWidth() - Tolerance))
								|| ((Letter_A3.getX() + Letter_A3.getWidth()) <= (Letter_A.getX() + Letter_A.getWidth() + Tolerance))) {

							if (((Letter_A3.getY() + Letter_A3.getHeight()) >= (Letter_A.getY() + Letter_A.getHeight() - Tolerance))
									|| ((Letter_A3.getY() + Letter_A3.getHeight()) <= (Letter_A.getY()+ Letter_A.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_A3.setPosition(Letter_A.getX(),Letter_A.getY() + Letter_A.getHeight()- Letter_A3.getHeight()-1.5f);

								is_A_PartUp3 = true;
								checkLetterOnComplete(Letter_A);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_A3.setPosition(InitCapX3, InitCapY3);
		Letter_A3.setAlpha(1);
		Letter_A3.setZIndex(4);
		Letter_A3.setScale(0);
		registerTouchArea(Letter_A3);

		Letter_b = new Sprite(0, 0, resourcesManager.build_b_region, vbom);
		Letter_b.setPosition(InitX - 10, InitLowY);
		Letter_b.setScale(0);
		Letter_b.setAlpha(0.95f);
		Letter_b.setZIndex(3);
		;

		Letter_b1 = new Sprite(0, 0, resourcesManager.build_b1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_b_PartUp1 && avoidOffCameraDrag(Letter_b1)) {
					Letter_b1.setPosition(pSceneTouchEvent.getX()- Letter_b1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_b1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_b1.collidesWith(Letter_b)) {

						if (((Letter_b1.getX()) >= (Letter_b.getX() - Tolerance))
								|| ((Letter_b1.getX()) <= (Letter_b.getX() + Tolerance))) {

							if (((Letter_b1.getY()) >= (Letter_b.getY() - Tolerance))
									|| ((Letter_b1.getY()) <= (Letter_b.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_b1.setPosition(Letter_b.getX()+2,Letter_b.getY());

								is_b_PartUp1 = true;
								checkLetterOnComplete(Letter_b);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_b1.setPosition(initLetterX1, initLetterY1);
		Letter_b1.setScale(0);
		Letter_b1.setAlpha(1);
		Letter_b1.setZIndex(4);

		registerTouchArea(Letter_b1);

		Letter_b2 = new Sprite(0, 0, resourcesManager.build_b2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_b_PartUp2 && avoidOffCameraDrag(Letter_b2)) {
					Letter_b2.setPosition(pSceneTouchEvent.getX()- Letter_b2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_b2.getHeight() / 2);
				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_b2.collidesWith(Letter_b)) {

						if (((Letter_b2.getX()) >= (Letter_b.getX() - Tolerance))
								|| ((Letter_b2.getX()) <= (Letter_b.getX() + Tolerance))) {

							if (((Letter_b2.getY() + Letter_b2.getHeight()) >= (Letter_b.getY() + Letter_b.getHeight() - Tolerance))
									|| ((Letter_b2.getY() + Letter_b2.getHeight()) <= (Letter_b.getY()+ Letter_b.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_b2.setPosition(Letter_b.getX(),Letter_b.getY() + Letter_b.getHeight()- Letter_b2.getHeight()-2);

								is_b_PartUp2 = true;
								checkLetterOnComplete(Letter_b);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_b2.setPosition(initLetterX2, initLetterY2);
		Letter_b2.setAlpha(1);
		Letter_b2.setZIndex(4);
		Letter_b2.setScale(0);

		registerTouchArea(Letter_b2);

		Letter_b3 = new Sprite(0, 0, resourcesManager.build_b3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_b_PartUp3 && avoidOffCameraDrag(Letter_b3)) {

					Letter_b3
					.setPosition(
							pSceneTouchEvent.getX()
							- Letter_b3.getWidth() / 2,
							pSceneTouchEvent.getY()
							- Letter_b3.getHeight() / 2);

				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_b3.collidesWith(Letter_b)) {

						if (((Letter_b3.getX() + Letter_b3.getWidth()) >= (Letter_b.getX() + Letter_b.getWidth() - Tolerance))
								|| ((Letter_b3.getX() + Letter_b3.getWidth()) <= (Letter_b.getX() + Letter_b.getWidth() + Tolerance))) {

							if (((Letter_b3.getY() + Letter_b3.getHeight()) >= (Letter_b.getY() + Letter_b.getHeight() - Tolerance))
									|| ((Letter_b3.getY() + Letter_b3.getHeight()) <= (Letter_b.getY()+ Letter_b.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_b3.setPosition(Letter_b.getX() + Letter_b.getWidth()- Letter_b3.getWidth(),Letter_b.getY() + Letter_b.getHeight()- Letter_b3.getHeight()-1);

								is_b_PartUp3 = true;
								checkLetterOnComplete(Letter_b);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_b3.setPosition(initLetterX3, initLetterY3);

		Letter_b3.setAlpha(1);
		Letter_b3.setZIndex(4);
		Letter_b3.setScale(0);

		registerTouchArea(Letter_b3);

		Letter_B = new Sprite(0, 0, resourcesManager.build_B_region, vbom);
		Letter_B.setPosition(InitX, InitCapY);
		Letter_B.setScale(0);
		Letter_B.setAlpha(0.95f);
		Letter_B.setZIndex(3);


		Letter_B1 = new Sprite(0, 0, resourcesManager.build_B1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_B_PartUp1 && avoidOffCameraDrag(Letter_B1)) {
					Letter_B1.setPosition(pSceneTouchEvent.getX()	- Letter_B1.getWidth() / 2,	pSceneTouchEvent.getY()- Letter_B1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_B1.collidesWith(Letter_B)) {

						if (((Letter_B1.getX()) >= (Letter_B.getX() - Tolerance))
								|| ((Letter_B1.getX()) <= (Letter_B.getX() + Tolerance))) {

							if (((Letter_B1.getY()) >= (Letter_B.getY() - Tolerance))
									|| ((Letter_B1.getY()) <= (Letter_B.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_B1.setPosition(Letter_B.getX(),Letter_B.getY() + Letter_B.getHeight()- Letter_B1.getHeight());
								is_B_PartUp1 = true;
								checkLetterOnComplete(Letter_B);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_B1.setPosition(InitCapX1, InitCapY1);
		Letter_B1.setAlpha(1);
		Letter_B1.setScale(0);
		Letter_B1.setZIndex(4);

		registerTouchArea(Letter_B1);

		Letter_B2 = new Sprite(0, 0, resourcesManager.build_B2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_B_PartUp2 && avoidOffCameraDrag(Letter_B2)) {
					Letter_B2	.setPosition(pSceneTouchEvent.getX()- Letter_B2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_B2.getHeight() / 2);

				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_B2.collidesWith(Letter_B)) {

						if (((Letter_B2.getX() >= (Letter_B.getX() - Tolerance))
								|| (Letter_B2.getX() <= (Letter_B.getX() + Tolerance)))) {

							if (((Letter_B2.getY() + Letter_B2.getHeight()) >= (Letter_B.getY() + Letter_B.getHeight() - Tolerance))
									|| ((Letter_B2.getY() + Letter_B2.getHeight()) <= (Letter_B.getY()+ Letter_B.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_B2.setPosition(Letter_B.getX()+10,Letter_B.getY() + Letter_B.getHeight()- Letter_B2.getHeight()-3);
								is_B_PartUp2 = true;
								checkLetterOnComplete(Letter_B);
							}
						}

					}
				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_B2.setPosition(InitCapX2, InitCapY2);
		Letter_B2.setAlpha(1);
		Letter_B2.setZIndex(4);
		Letter_B2.setScale(0);

		registerTouchArea(Letter_B2);

		Letter_B3 = new Sprite(0, 0, resourcesManager.build_B3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_B_PartUp3 && avoidOffCameraDrag(Letter_B3)) {
					Letter_B3.setPosition(	pSceneTouchEvent.getX()- Letter_B3.getWidth() / 2,	pSceneTouchEvent.getY()- Letter_B3.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_B3.collidesWith(Letter_B)) {

						if (((Letter_B3.getX() + Letter_B3.getWidth()) >= (Letter_B.getX() + Letter_B.getWidth() - Tolerance))
								|| ((Letter_B3.getX() + Letter_B3.getWidth()) <= (Letter_B.getX() + Letter_B.getWidth() + Tolerance))) {

							if (((Letter_B3.getY()) >= (Letter_B.getY() - Tolerance))
									|| ((Letter_B3.getY()) <= (Letter_B.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_B3.setPosition(Letter_B.getX()+2,Letter_B.getY());

								is_B_PartUp3 = true;
								checkLetterOnComplete(Letter_B);
							}
						}
					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_B3.setPosition(InitCapX3, InitCapY3);
		Letter_B3.setAlpha(1);
		Letter_B3.setZIndex(4);
		Letter_B3.setScale(0);

		registerTouchArea(Letter_B3);

		Letter_c = new Sprite(0, 0, resourcesManager.build_c_region, vbom);
		Letter_c.setPosition(InitX, InitLowY);
		Letter_c.setScale(0);
		Letter_c.setAlpha(1);
		Letter_c.setZIndex(3);


		Letter_c1 = new Sprite(0, 0, resourcesManager.build_c1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_c_PartUp1 && avoidOffCameraDrag(Letter_c1)) {
					Letter_c1.setPosition(pSceneTouchEvent.getX()- Letter_c1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_c1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_c1.collidesWith(Letter_c)) {

						if (((Letter_c1.getX() + Letter_c1.getWidth()) >= (Letter_c.getX() + Letter_c.getWidth() - Tolerance))
								|| ((Letter_c1.getX() + Letter_c1.getWidth()) <= (Letter_c.getX() + Letter_c.getWidth() + Tolerance))) {

							if (((Letter_c1.getY()) >= (Letter_c.getY() - Tolerance))
									|| ((Letter_c1.getY()) <= (Letter_c.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_c1.setPosition(Letter_c.getX()+1,Letter_c.getY()+Letter_c2.getHeight()/2-3.5f);

								is_c_PartUp1 = true;
								checkLetterOnComplete(Letter_c);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_c1.setPosition(initLetterX1, initLetterY1);
		Letter_c1.setAlpha(1);
		Letter_c1.setZIndex(4);
		Letter_c1.setScale(0);

		registerTouchArea(Letter_c1);

		Letter_c2 = new Sprite(0, 0, resourcesManager.build_c2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_c_PartUp2 && avoidOffCameraDrag(Letter_c2)) {
					Letter_c2.setPosition(pSceneTouchEvent.getX()	- Letter_c2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_c2.getHeight() / 2);
				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_c2.collidesWith(Letter_c)) {

						if (((Letter_c2.getX() + Letter_c2.getWidth()) >= (Letter_c.getX() + Letter_c.getWidth() - Tolerance))
								|| ((Letter_c2.getX() + Letter_c1.getWidth()) <= (Letter_c.getX() + Letter_c.getWidth() + Tolerance))) {

							if (((Letter_c2.getY() + Letter_c2.getHeight()) >= (Letter_c.getY() + Letter_c.getHeight() - Tolerance))
									|| ((Letter_c2.getY() + Letter_c2.getHeight()) <= (Letter_c.getY()+ Letter_c.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_c2.setPosition(Letter_c.getX()+2,Letter_c.getY() );

								is_c_PartUp2 = true;
								checkLetterOnComplete(Letter_c);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_c2.setPosition(initLetterX2, initLetterY2);
		Letter_c2.setAlpha(1);
		Letter_c2.setZIndex(4);

		Letter_c2.setScale(0);
		registerTouchArea(Letter_c2);

		Letter_c3 = new Sprite(0, 0, resourcesManager.build_c3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_c_PartUp3 && avoidOffCameraDrag(Letter_c3)) {

					Letter_c3.setPosition(pSceneTouchEvent.getX()- Letter_c3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_c3.getHeight() / 2);

				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_c3.collidesWith(Letter_c)) {

						if (((Letter_c3.getX()) >= (Letter_c.getX() - Tolerance))
								|| ((Letter_c3.getX()) <= (Letter_c.getX() + Tolerance))) {

							if (((Letter_c3.getY()) >= (Letter_c.getY() - Tolerance))
									|| ((Letter_c3.getY()) <= (Letter_c.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_c3.setPosition(Letter_c.getX()+Letter_c.getWidth()-Letter_c3.getWidth(),Letter_c.getY()+Letter_c.getHeight()-Letter_c3.getHeight()-2);

								is_c_PartUp3 = true;
								checkLetterOnComplete(Letter_c);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_c3.setPosition(initLetterX3, initLetterY3);
		Letter_c3.setAlpha(1);
		Letter_c3.setZIndex(4);
		Letter_c3.setScale(0);

		registerTouchArea(Letter_c3);

		Letter_C = new Sprite(0, 0, resourcesManager.build_C_region, vbom);
		Letter_C.setPosition(InitX, InitCapY);
		Letter_C.setAlpha(1);
		Letter_C.setScale(0);
		Letter_C.setZIndex(3);
		;

		Letter_C1 = new Sprite(0, 0, resourcesManager.build_C1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_C_PartUp1 && avoidOffCameraDrag(Letter_C1)) {
					Letter_C1
					.setPosition(
							pSceneTouchEvent.getX()
							- Letter_C1.getWidth() / 2,
							pSceneTouchEvent.getY()
							- Letter_C1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_C1.collidesWith(Letter_C)) {

						if (((Letter_C1.getX() + Letter_C1.getWidth()) >= (Letter_C.getX() + Letter_C.getWidth() - Tolerance))
								|| ((Letter_C1.getX() + Letter_C1.getWidth()) <= (Letter_C.getX() + Letter_C.getWidth() + Tolerance))) {

							if (((Letter_C1.getY()) >= (Letter_C.getY() - Tolerance))
									|| ((Letter_C1.getY()) <= (Letter_C.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_C1.setPosition(Letter_C.getX()+2 ,Letter_C.getY());
								is_C_PartUp1 = true;
								checkLetterOnComplete(Letter_C);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_C1.setPosition(InitCapX1, InitCapY1);
		Letter_C1.setAlpha(1);
		Letter_C1.setZIndex(4);

		Letter_C1.setScale(0);
		registerTouchArea(Letter_C1);

		Letter_C2 = new Sprite(0, 0, resourcesManager.build_C2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,
					float Y) {
				if (!is_C_PartUp2 && avoidOffCameraDrag(Letter_C2)) {
					Letter_C2.setPosition(pSceneTouchEvent.getX()- Letter_C2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_C2.getHeight() / 2);

				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_C2.collidesWith(Letter_C)) {

						if (((Letter_C2.getX() >= (Letter_C.getX() - Tolerance)) || (Letter_C1.getX() <= (Letter_C.getX() + Tolerance)))) {

							if (((Letter_C2.getY() + Letter_C2.getHeight()) >= (Letter_C.getY() + Letter_C.getHeight() - Tolerance))
									|| ((Letter_C2.getY() + Letter_C2.getHeight()) <= (Letter_C.getY()+ Letter_C.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_C2.setPosition(Letter_C.getX()+2,Letter_C.getY()+Letter_C1.getHeight()/2+7.5f);

								is_C_PartUp2 = true;
								checkLetterOnComplete(Letter_C);
							}
						}

					}
				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_C2.setPosition(InitCapX2, InitCapY2);
		Letter_C2.setAlpha(1);
		Letter_C2.setZIndex(4);
		Letter_C2.setScale(0);
		registerTouchArea(Letter_C2);

		Letter_C3 = new Sprite(0, 0, resourcesManager.build_C3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_C_PartUp3 && avoidOffCameraDrag(Letter_C3)) {
					Letter_C3.setPosition(pSceneTouchEvent.getX()- Letter_C3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_C3.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_C3.collidesWith(Letter_C)) {

						if (((Letter_C3.getX() + Letter_C3.getWidth()) >= (Letter_C.getX() + Letter_C.getWidth() - Tolerance))
								|| ((Letter_C3.getX() + Letter_C3.getWidth()) <= (Letter_C.getX() + Letter_C.getWidth() + Tolerance))) {

							if (((Letter_C3.getY() + Letter_C3.getHeight()) >= (Letter_C.getY() + Letter_C.getHeight() - Tolerance))
									|| ((Letter_C3.getY() + Letter_C3.getHeight()) <= (Letter_C.getY()+ Letter_C.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_C3.setPosition(Letter_C.getX() + Letter_C.getWidth()- Letter_C3.getWidth()-2,Letter_C.getY() + Letter_C.getHeight()- Letter_C3.getHeight());

								is_C_PartUp3 = true;
								checkLetterOnComplete(Letter_C);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_C3.setPosition(InitCapX3, InitCapY3);
		Letter_C3.setAlpha(1);
		Letter_C3.setZIndex(4);
		Letter_C3.setScale(0);

		registerTouchArea(Letter_C3);

		Letter_d = new Sprite(0, 0, resourcesManager.build_d_region, vbom);
		Letter_d.setPosition(InitX, InitLowY);
		Letter_d.setScale(0);
		Letter_d.setAlpha(1);
		Letter_d.setZIndex(3);


		Letter_d1 = new Sprite(0, 0, resourcesManager.build_d1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_d_PartUp1) {
					Letter_d1.setPosition(pSceneTouchEvent.getX()- Letter_d1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_d1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_d1.collidesWith(Letter_d)) {

						if (((Letter_d1.getX()) >= (Letter_d.getX() - Tolerance))
								|| ((Letter_d1.getX()) <= (Letter_d.getX() + Tolerance))) {

							if (((Letter_d1.getY() + Letter_d1.getHeight()) >= (Letter_d.getY() + Letter_d.getHeight() - Tolerance))
									|| ((Letter_d1.getY() + Letter_d1.getHeight()) <= (Letter_d.getY()+ Letter_d.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_d1.setPosition(Letter_d.getX(),Letter_d.getY());

								is_d_PartUp1 = true;
								checkLetterOnComplete(Letter_d);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_d1.setPosition(initLetterX1, initLetterY1);
		Letter_d1.setAlpha(1);
		Letter_d1.setZIndex(4);
		Letter_d1.setScale(0);

		registerTouchArea(Letter_d1);

		Letter_d2 = new Sprite(0, 0, resourcesManager.build_d2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_d_PartUp2) {
					Letter_d2.setPosition(pSceneTouchEvent.getX()- Letter_d2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_d2.getHeight() / 2);
				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_d2.collidesWith(Letter_d)) {

						if (((Letter_d2.getX() + Letter_d2.getWidth()) >= (Letter_d.getX() + Letter_d.getWidth() - Tolerance))
								|| ((Letter_d2.getX() + Letter_d2.getWidth()) <= (Letter_d.getX() + Letter_d.getWidth() + Tolerance))) {

							if (((Letter_d2.getY() + Letter_d2.getHeight()) >= (Letter_d.getY() + Letter_d.getHeight() - Tolerance))
									|| ((Letter_d2.getY() + Letter_d2.getHeight()) <= (Letter_d.getY()+ Letter_d.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_d2.setPosition(Letter_d.getX(),Letter_d.getY() + Letter_d.getHeight()- Letter_d2.getHeight() - 5);

								is_d_PartUp2 = true;
								checkLetterOnComplete(Letter_d);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_d2.setPosition(initLetterX2, initLetterY2);
		Letter_d2.setAlpha(1);
		Letter_d2.setZIndex(4);
		Letter_d2.setScale(0);
		registerTouchArea(Letter_d2);

		Letter_d3 = new Sprite(0, 0, resourcesManager.build_d3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_d_PartUp3) {

					Letter_d3.setPosition(pSceneTouchEvent.getX()- Letter_d3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_d3.getHeight() / 2);

				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_d3.collidesWith(Letter_d)) {

						if (((Letter_d3.getX() + Letter_d3.getWidth()) >= (Letter_d.getX() + Letter_d.getWidth() - Tolerance))
								|| ((Letter_d3.getX() + Letter_d3.getWidth()) <= (Letter_d.getX() + Letter_d.getWidth() + Tolerance))) {

							if (((Letter_d3.getY()) >= (Letter_d.getY() - Tolerance))
									|| ((Letter_d3.getY()) <= (Letter_d.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_d3.setPosition(Letter_d.getX() + Letter_d.getWidth()- Letter_d3.getWidth()-3,Letter_d.getY()+Letter_d.getHeight()-Letter_d3.getHeight()-3);

								is_d_PartUp3 = true;
								checkLetterOnComplete(Letter_d);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_d3.setPosition(initLetterX3, initLetterY3);
		Letter_d3.setAlpha(1);
		Letter_d3.setZIndex(4);
		Letter_d3.setScale(0);

		registerTouchArea(Letter_d3);

		Letter_D = new Sprite(0, 0, resourcesManager.build_D_region, vbom);
		Letter_D.setPosition(InitX, InitCapY);
		Letter_D.setAlpha(1);
		Letter_D.setScale(0);
		Letter_D.setZIndex(3);


		Letter_D1 = new Sprite(0, 0, resourcesManager.build_D1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_D_PartUp1) {
					Letter_D1.setPosition(pSceneTouchEvent.getX()- Letter_D1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_D1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_D1.collidesWith(Letter_D)) {

						if (((Letter_D1.getX()) >= (Letter_D.getX() - Tolerance))
								|| ((Letter_D1.getX()) <= (Letter_D.getX() + Tolerance))) {

							if (((Letter_D1.getY()) >= (Letter_D.getY() - Tolerance))
									|| ((Letter_D1.getY()) <= (Letter_D.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_D1.setPosition(Letter_D.getX(),Letter_D.getY());
								is_D_PartUp1 = true;
								checkLetterOnComplete(Letter_D);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_D1.setPosition(InitCapX1, InitCapY1);
		Letter_D1.setAlpha(1);
		Letter_D1.setZIndex(4);

		Letter_D1.setScale(0);
		registerTouchArea(Letter_D1);

		Letter_D2 = new Sprite(0, 0, resourcesManager.build_D2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_D_PartUp2) {
					Letter_D2.setPosition(pSceneTouchEvent.getX()- Letter_D2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_D2.getHeight() / 2);

				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_D2.collidesWith(Letter_D)) {

						if (((Letter_D2.getX() + Letter_D2.getWidth() >= (Letter_D.getX() + Letter_D.getWidth() - Tolerance))
								|| (Letter_D2.getX() + Letter_D2.getWidth() <= (Letter_D.getX() + Letter_D.getWidth() + Tolerance)))) {

							if (((Letter_D2.getY()) >= (Letter_D.getY() - Tolerance))
									|| ((Letter_D2.getY()) <= (Letter_D.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_D2.setPosition(Letter_D.getX(),Letter_D.getY()+Letter_D.getHeight()-Letter_D2.getHeight());

								is_D_PartUp2 = true;
								checkLetterOnComplete(Letter_D);
							}
						}

					}
				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_D2.setPosition(InitCapX2, InitCapY2);
		Letter_D2.setAlpha(1);
		Letter_D2.setZIndex(4);
		Letter_D2.setScale(0);

		registerTouchArea(Letter_D2);

		Letter_D3 = new Sprite(0, 0, resourcesManager.build_D3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_D_PartUp3) {
					Letter_D3.setPosition(pSceneTouchEvent.getX()- Letter_D3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_D3.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_D3.collidesWith(Letter_D)) {

						if (((Letter_D3.getX() + Letter_D3.getWidth()) >= (Letter_D.getX() + Letter_D.getWidth() - Tolerance))
								|| ((Letter_D3.getX() + Letter_D3.getWidth()) <= (Letter_D.getX() + Letter_D.getWidth() + Tolerance))) {

							if (((Letter_D3.getY() + Letter_D3.getHeight()) >= (Letter_D.getY() + Letter_D.getHeight() - Tolerance))
									|| ((Letter_D3.getY() + Letter_D3.getHeight()) <= (Letter_D.getY()+ Letter_D.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_D3.setPosition(Letter_D.getX() + Letter_D.getWidth()- Letter_D3.getWidth(),Letter_D.getY() + Letter_D.getHeight()- Letter_D3.getHeight());

								is_D_PartUp3 = true;
								checkLetterOnComplete(Letter_D);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_D3.setPosition(InitCapX3, InitCapY3);
		Letter_D3.setAlpha(1);
		Letter_D3.setZIndex(4);
		Letter_D3.setScale(0);

		registerTouchArea(Letter_D3);

		Letter_e = new Sprite(0, 0, resourcesManager.build_e_region, vbom);
		Letter_e.setPosition(InitX, InitLowY);
		Letter_e.setScale(0);
		Letter_e.setAlpha(1);
		Letter_e.setZIndex(3);

		Letter_e1 = new Sprite(0, 0, resourcesManager.build_e1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_e_PartUp1) {
					Letter_e1.setPosition(pSceneTouchEvent.getX()- Letter_e1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_e1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_e1.collidesWith(Letter_e)) {

						if (((Letter_e1.getX() + Letter_e1.getWidth()) >= (Letter_e.getX() + Letter_e.getWidth() - Tolerance))
								|| ((Letter_e1.getX() + Letter_e1.getWidth()) <= (Letter_e.getX() + Letter_e.getWidth() + Tolerance))) {

							if (((Letter_e1.getY()) >= (Letter_e.getY() - Tolerance))
									|| ((Letter_e1.getY()) <= (Letter_e.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_e1.setPosition(Letter_e.getX()+2,Letter_e.getY()+2);

								is_e_PartUp1 = true;
								checkLetterOnComplete(Letter_e);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_e1.setPosition(initLetterX1, initLetterY1);
		Letter_e1.setAlpha(1);
		Letter_e1.setZIndex(4);
		Letter_e1.setScale(0);

		registerTouchArea(Letter_e1);

		Letter_e2 = new Sprite(0, 0, resourcesManager.build_e2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_e_PartUp2) {
					Letter_e2.setPosition(pSceneTouchEvent.getX()- Letter_e2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_e2.getHeight() / 2);
				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_e2.collidesWith(Letter_e)) {

						if (((Letter_e2.getX()) >= (Letter_e.getX() - Tolerance))
								|| ((Letter_e2.getX()) <= (Letter_e.getX() + Tolerance))) {

							if (((Letter_e2.getY() + Letter_e2.getHeight()) >= (Letter_e.getY() + Letter_e.getHeight() - Tolerance))
									|| ((Letter_e2.getY() + Letter_e2.getHeight()) <= (Letter_e.getY()+ Letter_e.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_e2.setPosition(Letter_e.getX(),Letter_e.getY()+Letter_e.getHeight()-Letter_e2.getHeight()-9.5f);

								is_e_PartUp2 = true;
								checkLetterOnComplete(Letter_e);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_e2.setPosition(initLetterX2, initLetterY2);
		Letter_e2.setAlpha(1);
		Letter_e2.setZIndex(4);

		Letter_e2.setScale(0);
		registerTouchArea(Letter_e2);

		Letter_e3 = new Sprite(0, 0, resourcesManager.build_e3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_e_PartUp3) {

					Letter_e3.setPosition(pSceneTouchEvent.getX()- Letter_e3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_e3.getHeight() / 2);

				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_e3.collidesWith(Letter_e)) {

						if (((Letter_e3.getX() + Letter_e3.getWidth()) >= (Letter_e.getX() + Letter_e.getWidth() - Tolerance))
								|| ((Letter_e3.getX() + Letter_e3.getWidth()) <= (Letter_e.getX() + Letter_e.getWidth() + Tolerance))) {

							if (((Letter_e3.getY() + Letter_e3.getHeight()) >= (Letter_e.getY() + Letter_e.getHeight() - Tolerance))
									|| ((Letter_e3.getY() + Letter_e3.getHeight()) <= (Letter_e.getY()+ Letter_e.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_e3.setPosition(Letter_e.getX() + Letter_e.getWidth()- Letter_e3.getWidth()-2,Letter_e.getY() + Letter_e.getHeight()-Letter_e3.getHeight());

								is_e_PartUp3 = true;
								checkLetterOnComplete(Letter_e);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_e3.setPosition(initLetterX3, initLetterY3);
		Letter_e3.setAlpha(1);
		Letter_e3.setZIndex(4);
		Letter_e3.setScale(0);
		registerTouchArea(Letter_e3);

		Letter_E = new Sprite(0, 0, resourcesManager.build_E_region, vbom);
		Letter_E.setPosition(InitX, InitCapY);
		Letter_E.setAlpha(1);
		Letter_E.setScale(0);
		Letter_E.setZIndex(3);


		Letter_E1 = new Sprite(0, 0, resourcesManager.build_E1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_E_PartUp1) {
					Letter_E1.setPosition(pSceneTouchEvent.getX()- Letter_E1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_E1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_E1.collidesWith(Letter_E)) {

						if (((Letter_E1.getX()) >= (Letter_E.getX() - Tolerance))
								|| ((Letter_E1.getX()) <= (Letter_E.getX() + Tolerance))) {

							if (((Letter_E1.getY()) >= (Letter_E.getY() - Tolerance))
									|| ((Letter_E1.getY()) <= (Letter_E.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_E1.setPosition(Letter_E.getX()+2,Letter_E.getY()+2);
								is_E_PartUp1 = true;
								checkLetterOnComplete(Letter_E);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_E1.setPosition(InitCapX1, InitCapY1);
		Letter_E1.setAlpha(1);
		Letter_E1.setZIndex(4);
		Letter_E1.setScale(0);
		registerTouchArea(Letter_E1);

		Letter_E2 = new Sprite(0, 0, resourcesManager.build_E2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_E_PartUp2) {
					Letter_E2.setPosition(pSceneTouchEvent.getX()- Letter_E2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_E2.getHeight() / 2);

				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_E2.collidesWith(Letter_E)) {

						if (((Letter_E2.getX() >= (Letter_E.getX() - Tolerance)) || (Letter_E1.getX() <= (Letter_E.getX() + Tolerance)))) {

							if (((Letter_E2.getY() + Letter_E2.getHeight()) >= (Letter_E.getY() + Letter_E.getHeight() / 2 - Tolerance))
									|| ((Letter_E2.getY() + Letter_E2.getHeight()) <= (Letter_E.getY()+ Letter_E.getHeight() / 2 + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_E2.setPosition(Letter_E.getX(),Letter_E.getY()+ Letter_E.getHeight()- Letter_E2.getHeight()-5);

								is_E_PartUp2 = true;
								checkLetterOnComplete(Letter_E);
							}
						}

					}
				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_E2.setPosition(InitCapX2, InitCapY2);
		Letter_E2.setAlpha(1);
		Letter_E2.setZIndex(4);
		Letter_E2.setScale(0);

		registerTouchArea(Letter_E2);

		Letter_E3 = new Sprite(0, 0, resourcesManager.build_E3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_E_PartUp3) {
					Letter_E3.setPosition(pSceneTouchEvent.getX()- Letter_E3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_E3.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_E3.collidesWith(Letter_E)) {

						if (((Letter_E3.getX()) >= (Letter_E.getX() - Tolerance))
								|| ((Letter_E3.getX()) <= (Letter_E.getX() + Tolerance))) {

							if (((Letter_E3.getY() + Letter_E3.getHeight()) >= (Letter_E.getY() + Letter_E.getHeight() - Tolerance))
									|| ((Letter_E3.getY() + Letter_E3.getHeight()) <= (Letter_E.getY()+ Letter_E.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_E3.setPosition(Letter_E.getX(),Letter_E.getY() + Letter_E.getHeight()- Letter_E3.getHeight());

								is_E_PartUp3 = true;
								checkLetterOnComplete(Letter_E);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_E3.setPosition(InitCapX3, InitCapY3);
		Letter_E3.setAlpha(1);
		Letter_E3.setZIndex(4);
		Letter_E3.setScale(0);
		registerTouchArea(Letter_E3);

		Letter_f = new Sprite(0, 0, resourcesManager.build_f_region, vbom);
		Letter_f.setPosition(InitX, InitLowY);
		Letter_f.setScale(0);
		Letter_f.setAlpha(1);
		Letter_f.setZIndex(3);

		Letter_f1 = new Sprite(0, 0, resourcesManager.build_f1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_f_PartUp1) {
					Letter_f1.setPosition(pSceneTouchEvent.getX()- Letter_f1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_f1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_f1.collidesWith(Letter_f)) {

						if (((Letter_f1.getX()) >= (Letter_f.getX() - Tolerance))
								|| ((Letter_f1.getX()) <= (Letter_f.getX() + Tolerance))) {

							if (((Letter_f1.getY()) >= (Letter_f.getY() - Tolerance))
									|| ((Letter_f1.getY()) <= (Letter_f.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_f1.setPosition(Letter_f.getX()+2,Letter_f.getY()+2);

								is_f_PartUp1 = true;
								checkLetterOnComplete(Letter_f);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_f1.setPosition(initLetterX1, initLetterY1);
		Letter_f1.setAlpha(1);
		Letter_f1.setZIndex(4);
		Letter_f1.setScale(0);

		registerTouchArea(Letter_f1);

		Letter_f2 = new Sprite(0, 0, resourcesManager.build_f2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,
					float Y) {
				if (!is_f_PartUp2) {
					Letter_f2.setPosition(	pSceneTouchEvent.getX()- Letter_f2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_f2.getHeight() / 2);
				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_f2.collidesWith(Letter_f)) {

						if (((Letter_f2.getX() + Letter_f2.getWidth()) >= (Letter_f.getX() + Letter_f.getWidth() - Tolerance))
								|| ((Letter_f1.getX() + Letter_f1.getWidth()) <= (Letter_f.getX() + Letter_f.getWidth() + Tolerance))) {

							if (((Letter_f2.getY()) >= (Letter_f.getY() + 40 - Tolerance))
									|| ((Letter_f2.getY()) <= (Letter_f.getY() + 40 + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_f2.setPosition(Letter_f.getX()+10,Letter_f.getY()+Letter_f.getHeight()-Letter_f3.getHeight()-Letter_f2.getHeight()+2);

								is_f_PartUp2 = true;
								checkLetterOnComplete(Letter_f);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_f2.setPosition(initLetterX2, initLetterY2);
		Letter_f2.setAlpha(1);
		Letter_f2.setZIndex(4);

		Letter_f2.setScale(0);
		registerTouchArea(Letter_f2);

		Letter_f3 = new Sprite(0, 0, resourcesManager.build_f3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_f_PartUp3) {

					Letter_f3.setPosition(pSceneTouchEvent.getX()- Letter_f3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_f3.getHeight() / 2);

				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_f3.collidesWith(Letter_f)) {

						if (((Letter_f3.getX()) >= (Letter_f.getX() - Tolerance))
								|| ((Letter_f3.getX()) <= (Letter_f.getX() + Tolerance))) {

							if (((Letter_f3.getY() + Letter_f3.getHeight()) >= (Letter_f.getY() + Letter_f.getHeight() - Tolerance))
									|| ((Letter_f3.getY() + Letter_f3.getHeight()) <= (Letter_f.getY()+ Letter_f.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_f3.setPosition(Letter_f.getX() + 10,Letter_f.getY() + Letter_f.getHeight()- Letter_f3.getHeight());

								is_f_PartUp3 = true;
								checkLetterOnComplete(Letter_f);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_f3.setPosition(initLetterX3, initLetterY3);
		Letter_f3.setAlpha(1);
		Letter_f3.setZIndex(4);
		Letter_f3.setScale(0);

		registerTouchArea(Letter_f3);

		Letter_F = new Sprite(0, 0, resourcesManager.build_F_region, vbom);
		Letter_F.setPosition(InitX, InitCapY);
		Letter_F.setAlpha(1);
		Letter_F.setScale(0);
		Letter_F.setZIndex(3);


		Letter_F1 = new Sprite(0, 0, resourcesManager.build_F1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_F_PartUp1) {Letter_F1.setPosition(pSceneTouchEvent.getX()- Letter_F1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_F1.getHeight() / 2);

				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_F1.collidesWith(Letter_F)) {

						if (((Letter_F1.getX()) >= (Letter_F.getX() - Tolerance))
								|| ((Letter_F1.getX()) <= (Letter_F.getX() + Tolerance))) {

							if (((Letter_F1.getY()) >= (Letter_F.getY() - Tolerance))
									|| ((Letter_F1.getY()) <= (Letter_F.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_F1.setPosition(Letter_F.getX()+2,Letter_F.getY());
								is_F_PartUp1 = true;
								checkLetterOnComplete(Letter_F);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_F1.setPosition(InitCapX1, InitCapY1);
		Letter_F1.setAlpha(1);
		Letter_F1.setZIndex(4);

		Letter_F1.setScale(0);
		registerTouchArea(Letter_F1);

		Letter_F2 = new Sprite(0, 0, resourcesManager.build_F2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_F_PartUp2) {
					Letter_F2.setPosition(pSceneTouchEvent.getX()- Letter_F2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_F2.getHeight() / 2);

				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_F2.collidesWith(Letter_F)) {

						if (((Letter_F2.getX() >= (Letter_F.getX() - Tolerance)) || (Letter_F1.getX() <= (Letter_F.getX() + Tolerance)))) {

							if (((Letter_F2.getY() + Letter_F2.getHeight()) >= (Letter_F.getY() + Letter_F.getHeight() / 2 - Tolerance))
									|| ((Letter_F2.getY() + Letter_F2.getHeight()) <= (Letter_F.getY()+ Letter_F.getHeight() / 2 + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_F2.setPosition(Letter_F.getX(),Letter_F.getY()+Letter_F.getHeight()- Letter_F3.getHeight()-Letter_F2.getHeight()+2);

								is_F_PartUp2 = true;
								checkLetterOnComplete(Letter_F);
							}
						}

					}
				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_F2.setPosition(InitCapX2, InitCapY2);
		Letter_F2.setAlpha(1);
		Letter_F2.setZIndex(4);
		Letter_F2.setScale(0);

		registerTouchArea(Letter_F2);

		Letter_F3 = new Sprite(0, 0, resourcesManager.build_F3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_F_PartUp3) {Letter_F3.setPosition(pSceneTouchEvent.getX()- Letter_F3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_F3.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_F3.collidesWith(Letter_F)) {

						if (((Letter_F3.getX()) >= (Letter_F.getX() - Tolerance))
								|| ((Letter_F3.getX()) <= (Letter_F.getX() + Tolerance))) {

							if (((Letter_F3.getY() + Letter_F3.getHeight()) >= (Letter_F.getY() + Letter_F.getHeight() - Tolerance))
									|| ((Letter_F3.getY() + Letter_F3.getHeight()) <= (Letter_F.getY()+ Letter_F.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_F3.setPosition(Letter_F.getX(),Letter_F.getY() + Letter_F.getHeight()- Letter_F3.getHeight());

								is_F_PartUp3 = true;
								checkLetterOnComplete(Letter_F);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_F3.setPosition(InitCapX3, InitCapY3);
		Letter_F3.setAlpha(1);
		Letter_F3.setZIndex(4);
		Letter_F3.setScale(0);

		registerTouchArea(Letter_F3);

		Letter_g = new Sprite(0, 0, resourcesManager.build_g_region, vbom);
		Letter_g.setPosition(InitX, InitLowY);
		Letter_g.setScale(0);
		Letter_g.setAlpha(1);
		Letter_g.setZIndex(3);


		Letter_g1 = new Sprite(0, 0, resourcesManager.build_g1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_g_PartUp1) {
					Letter_g1.setPosition(pSceneTouchEvent.getX()- Letter_g1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_g1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_g1.collidesWith(Letter_g)) {

						if (((Letter_g1.getX()) >= (Letter_g.getX() - Tolerance))
								|| ((Letter_g1.getX()) <= (Letter_g.getX() + Tolerance))) {

							if (((Letter_g1.getY()) >= (Letter_g.getY() - Tolerance))
									|| ((Letter_g1.getY()) <= (Letter_g.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_g1.setPosition(Letter_g.getX()+Letter_g.getWidth()-Letter_g1.getWidth()-2,Letter_g.getY()+4);

								is_g_PartUp1 = true;
								checkLetterOnComplete(Letter_g);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_g1.setPosition(initLetterX1, initLetterY1);
		Letter_g1.setAlpha(1);
		Letter_g1.setZIndex(4);
		Letter_g1.setScale(0);

		registerTouchArea(Letter_g1);

		Letter_g2 = new Sprite(0, 0, resourcesManager.build_g2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_g_PartUp2) {
					Letter_g2.setPosition(pSceneTouchEvent.getX()- Letter_g2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_g2.getHeight() / 2);
				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_g2.collidesWith(Letter_g)) {

						if (((Letter_g2.getX()) >= (Letter_g.getX() - Tolerance))
								|| ((Letter_g2.getX()) <= (Letter_g.getX() + Tolerance))) {

							if (((Letter_g2.getY()) >= (Letter_g.getY() + 40 - Tolerance))
									|| ((Letter_g2.getY()) <= (Letter_g.getY() + 40 + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_g2.setPosition(Letter_g.getX(),Letter_g.getY());

								is_g_PartUp2 = true;
								checkLetterOnComplete(Letter_g);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_g2.setPosition(initLetterX2, initLetterY2);
		Letter_g2.setAlpha(1);
		Letter_g2.setZIndex(4);

		Letter_g2.setScale(0);
		registerTouchArea(Letter_g2);

		Letter_g3 = new Sprite(0, 0, resourcesManager.build_g3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_g_PartUp3) {

					Letter_g3.setPosition(pSceneTouchEvent.getX()- Letter_g3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_g3.getHeight() / 2);

				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_g3.collidesWith(Letter_g)) {

						if (((Letter_g3.getX()) >= (Letter_g.getX() - Tolerance))
								|| ((Letter_g3.getX()) <= (Letter_g.getX() + Tolerance))) {

							if (((Letter_g3.getY() + Letter_g3.getHeight()) >= (Letter_g.getY() + Letter_g.getHeight() - Tolerance))
									|| ((Letter_g3.getY() + Letter_g3.getHeight()) <= (Letter_g.getY()+ Letter_g.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_g3.setPosition(Letter_g.getX(),Letter_g.getY() + Letter_g.getHeight()- Letter_g3.getHeight()-2);
								is_g_PartUp3 = true;
								checkLetterOnComplete(Letter_g);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_g3.setPosition(initLetterX3, initLetterY3);
		Letter_g3.setAlpha(1);
		Letter_g3.setZIndex(4);
		Letter_g3.setScale(0);

		registerTouchArea(Letter_g3);

		Letter_G = new Sprite(0, 0, resourcesManager.build_G_region, vbom);
		Letter_G.setPosition(InitX, InitCapY);
		Letter_G.setAlpha(1);
		Letter_G.setScale(0);
		Letter_G.setZIndex(3);


		Letter_G1 = new Sprite(0, 0, resourcesManager.build_G1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_G_PartUp1) {
					Letter_G1.setPosition(pSceneTouchEvent.getX()- Letter_G1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_G1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_G1.collidesWith(Letter_G)) {

						if (((Letter_G1.getX()) >= (Letter_G.getX() - Tolerance))
								|| ((Letter_G1.getX()) <= (Letter_G.getX() + Tolerance))) {

							if (((Letter_G1.getY() + Letter_G1.getHeight()) >= (Letter_G.getY() + Letter_G.getHeight() - Tolerance))
									|| ((Letter_G1.getY() + Letter_G1.getHeight()) <= (Letter_G.getY()+ Letter_G.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_G1.setPosition(Letter_G.getX()+2,Letter_G.getY());
								is_G_PartUp1 = true;
								checkLetterOnComplete(Letter_G);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_G1.setPosition(InitCapX1, InitCapY1);
		Letter_G1.setAlpha(1);
		Letter_G1.setZIndex(4);
		Letter_G1.setScale(0);
		registerTouchArea(Letter_G1);

		Letter_G2 = new Sprite(0, 0, resourcesManager.build_G2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,
					float Y) {
				if (!is_G_PartUp2) {
					Letter_G2.setPosition(pSceneTouchEvent.getX()- Letter_G2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_G2.getHeight() / 2);

				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_G2.collidesWith(Letter_G)) {

						if (((Letter_G2.getX() + Letter_G2.getWidth()) >= (Letter_G.getX() + Letter_G.getWidth() - Tolerance))
								|| (Letter_G2.getX() + Letter_G2.getWidth() <= (Letter_G.getX() + Letter_G.getWidth() + Tolerance))) {

							if (((Letter_G2.getY()) >= (Letter_G.getY() - Tolerance))
									|| ((Letter_G2.getY()) <= (Letter_G.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_G2.setPosition(Letter_G.getX(),Letter_G.getY()+Letter_G1.getHeight()/2+5);

								is_G_PartUp2 = true;
								checkLetterOnComplete(Letter_G);
							}
						}

					}
				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_G2.setPosition(InitCapX2, InitCapY2);
		Letter_G2.setAlpha(1);
		Letter_G2.setZIndex(4);
		Letter_G2.setScale(0);

		registerTouchArea(Letter_G2);

		Letter_G3 = new Sprite(0, 0, resourcesManager.build_G3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_G_PartUp3) {
					Letter_G3.setPosition(pSceneTouchEvent.getX()- Letter_G3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_G3.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_G3.collidesWith(Letter_G)) {

						if (((Letter_G3.getX() + Letter_G3.getWidth()) >= (Letter_G.getX() + Letter_G.getWidth() - Tolerance))
								|| ((Letter_G3.getX() + Letter_G3.getWidth()) <= (Letter_G.getX() + Letter_G.getWidth() + Tolerance))) {

							if (((Letter_G3.getY() + Letter_G3.getHeight()) >= (Letter_G.getY() + Letter_G.getHeight() - Tolerance))
									|| ((Letter_G3.getY() + Letter_G3.getHeight()) <= (Letter_G.getY()+ Letter_G.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_G3.setPosition(Letter_G.getX() + Letter_G.getWidth()- Letter_G3.getWidth()-2,Letter_G.getY() + Letter_G.getHeight()- Letter_G3.getHeight());

								is_G_PartUp3 = true;
								checkLetterOnComplete(Letter_G);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_G3.setPosition(InitCapX3, InitCapY3);
		Letter_G3.setAlpha(1);
		Letter_G3.setZIndex(4);
		Letter_G3.setScale(0);

		registerTouchArea(Letter_G3);

		Letter_h = new Sprite(0, 0, resourcesManager.build_h_region, vbom);
		Letter_h.setPosition(InitX, InitLowY);
		Letter_h.setScale(0);
		Letter_h.setAlpha(1);
		Letter_h.setZIndex(3);


		Letter_h1 = new Sprite(0, 0, resourcesManager.build_h1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_h_PartUp1) {
					Letter_h1.setPosition(pSceneTouchEvent.getX()- Letter_h1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_h1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_h1.collidesWith(Letter_h)) {

						if (((Letter_h1.getX()) >= (Letter_h.getX() - Tolerance))
								|| ((Letter_h1.getX()) <= (Letter_h.getX() + Tolerance))) {

							if (((Letter_h1.getY()) >= (Letter_h.getY() - Tolerance))
									|| ((Letter_h1.getY()) <= (Letter_h.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();
								Letter_h1.setPosition(Letter_h.getX()+2,Letter_h.getY());

								is_h_PartUp1 = true;
								checkLetterOnComplete(Letter_h);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_h1.setPosition(initLetterX1, initLetterY1);
		Letter_h1.setAlpha(1);
		Letter_h1.setZIndex(4);
		Letter_h1.setScale(0);

		registerTouchArea(Letter_h1);

		Letter_h2 = new Sprite(0, 0, resourcesManager.build_h2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_h_PartUp2) {
					Letter_h2.setPosition(pSceneTouchEvent.getX()- Letter_h2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_h2.getHeight() / 2);
				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_h2.collidesWith(Letter_h)) {

						if (((Letter_h2.getX()) >= (Letter_h.getX() - Tolerance))
								|| ((Letter_h2.getX()) <= (Letter_h.getX() + Tolerance))) {

							if (((Letter_h2.getY() + Letter_h2.getHeight()) >= (Letter_h.getY() + Letter_h.getHeight() - Tolerance))
									|| ((Letter_h2.getY() + Letter_h2.getHeight()) <= (Letter_h.getY()+ Letter_h.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_h2.setPosition(Letter_h.getX()+2,Letter_h.getY() + Letter_h.getHeight()- Letter_h2.getHeight());

								is_h_PartUp2 = true;
								checkLetterOnComplete(Letter_h);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_h2.setPosition(initLetterX2, initLetterY2);
		Letter_h2.setAlpha(1);
		Letter_h2.setZIndex(4);

		Letter_h2.setScale(0);
		registerTouchArea(Letter_h2);

		Letter_h3 = new Sprite(0, 0, resourcesManager.build_h3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_h_PartUp3) {

					Letter_h3.setPosition(pSceneTouchEvent.getX()- Letter_h3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_h3.getHeight() / 2);

				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_h3.collidesWith(Letter_h)) {

						if (((Letter_h3.getX() + Letter_h3.getWidth()) >= (Letter_h.getX() + Letter_h.getWidth() - Tolerance))
								|| ((Letter_h3.getX() + Letter_h3.getWidth()) <= (Letter_h.getX() + Letter_h.getWidth() + Tolerance))) {

							if (((Letter_h3.getY() + Letter_h3.getHeight()) >= (Letter_h.getY() + Letter_h.getHeight() - Tolerance))
									|| ((Letter_h3.getY() + Letter_h3.getHeight()) <= (Letter_h.getY()+ Letter_h.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_h3.setPosition(Letter_h.getX() + Letter_h.getWidth()- Letter_h3.getWidth(),Letter_h.getY() + Letter_h.getHeight()- Letter_h3.getHeight());

								is_h_PartUp3 = true;
								checkLetterOnComplete(Letter_h);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_h3.setPosition(initLetterX3, initLetterY3);
		Letter_h3.setAlpha(1);
		Letter_h3.setZIndex(4);
		Letter_h3.setScale(0);

		registerTouchArea(Letter_h3);

		Letter_H = new Sprite(0, 0, resourcesManager.build_H_region, vbom);
		Letter_H.setPosition(InitX, InitCapY);
		Letter_H.setAlpha(1);
		Letter_H.setScale(0);
		Letter_H.setZIndex(3);

		Letter_H1 = new Sprite(0, 0, resourcesManager.build_H1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_H_PartUp1) {
					Letter_H1.setPosition(pSceneTouchEvent.getX()- Letter_H1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_H1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_H1.collidesWith(Letter_H)) {

						if (((Letter_H1.getX()) >= (Letter_H.getX() - Tolerance))
								|| ((Letter_H1.getX()) <= (Letter_H.getX() + Tolerance))) {

							if (((Letter_H1.getY()) >= (Letter_H.getY() - Tolerance))
									|| ((Letter_H1.getY()) <= (Letter_H.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_H1.setPosition(Letter_H.getX()+2,Letter_H.getY());
								is_H_PartUp1 = true;
								checkLetterOnComplete(Letter_H);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_H1.setPosition(InitCapX1, InitCapY1);
		Letter_H1.setAlpha(1);
		Letter_H1.setZIndex(4);

		Letter_H1.setScale(0);
		registerTouchArea(Letter_H1);

		Letter_H2 = new Sprite(0, 0, resourcesManager.build_H2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_H_PartUp2) {
					Letter_H2.setPosition(pSceneTouchEvent.getX()- Letter_H2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_H2.getHeight() / 2);

				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_H2.collidesWith(Letter_H)) {

						if (((Letter_H2.getX() >= (Letter_H.getX() - Tolerance))
								|| (Letter_H1.getX() <= (Letter_H.getX() + Tolerance)))) {

							if (((Letter_H2.getY() + Letter_H2.getHeight() / 2) >= (Letter_H.getY() + Letter_H.getHeight() / 2 - Tolerance))
									|| ((Letter_H2.getY() + Letter_H2.getHeight() / 2) <= (Letter_H.getY() + Letter_H.getHeight() / 2 + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_H2.setPosition(Letter_H.getX(),Letter_H.getY()+ Letter_H.getHeight()- Letter_H2.getHeight());

								is_H_PartUp2 = true;
								checkLetterOnComplete(Letter_H);
							}
						}

					}
				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_H2.setPosition(InitCapX2, InitCapY2);
		Letter_H2.setAlpha(1);
		Letter_H2.setZIndex(4);
		Letter_H2.setScale(0);

		registerTouchArea(Letter_H2);

		Letter_H3 = new Sprite(0, 0, resourcesManager.build_H3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_H_PartUp3) {
					Letter_H3.setPosition(pSceneTouchEvent.getX()- Letter_H3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_H3.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_H3.collidesWith(Letter_H)) {

						if (((Letter_H3.getX()) >= (Letter_H.getX() - Tolerance))
								|| ((Letter_H3.getX()) <= (Letter_H.getX() + Tolerance))) {

							if (((Letter_H3.getY() + Letter_H3.getHeight()) >= (Letter_H.getY() + Letter_H.getHeight() - Tolerance))
									|| ((Letter_H3.getY() + Letter_H3.getHeight()) <= (Letter_H.getY()+ Letter_H.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_H3.setPosition(Letter_H.getX()+Letter_H.getWidth()-Letter_H3.getWidth(),Letter_H.getY() + Letter_H.getHeight()- Letter_H3.getHeight());

								is_H_PartUp3 = true;
								checkLetterOnComplete(Letter_H);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_H3.setPosition(InitCapX3, InitCapY3);
		Letter_H3.setAlpha(1);
		Letter_H3.setZIndex(4);
		Letter_H3.setScale(0);

		registerTouchArea(Letter_H3);



		Letter_k = new Sprite(0, 0, resourcesManager.build_k_region, vbom);
		Letter_k.setPosition(InitX, InitLowY);
		Letter_k.setScale(0);
		Letter_k.setAlpha(1);
		Letter_k.setZIndex(3);

		Letter_k1 = new Sprite(0, 0, resourcesManager.build_k1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_k_PartUp1) {
					Letter_k1.setPosition(pSceneTouchEvent.getX()- Letter_k1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_k1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_k1.collidesWith(Letter_k)) {

						if (((Letter_k1.getX()) >= (Letter_k.getX() - Tolerance))
								|| ((Letter_k1.getX()) <= (Letter_k.getX() + Tolerance))) {

							if (((Letter_k1.getY()) >= (Letter_k.getY() - Tolerance))
									|| ((Letter_k1.getY()) <= (Letter_k.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_k1.setPosition(Letter_k.getX()+2,Letter_k.getY());

								is_k_PartUp1 = true;
								checkLetterOnComplete(Letter_k);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_k1.setPosition(initLetterX1, initLetterY1);
		Letter_k1.setAlpha(1);
		Letter_k1.setZIndex(4);
		Letter_k1.setScale(0);

		registerTouchArea(Letter_k1);

		Letter_k2 = new Sprite(0, 0, resourcesManager.build_k2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_k_PartUp2) {
					Letter_k2.setPosition(pSceneTouchEvent.getX()- Letter_k2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_k2.getHeight() / 2);
				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_k2.collidesWith(Letter_k)) {

						if (((Letter_k2.getX()) >= (Letter_k.getX() - Tolerance))
								|| ((Letter_k1.getX()) <= (Letter_k.getX() + Tolerance))) {

							if (((Letter_k2.getY()) >= (Letter_k.getY() - Tolerance))
									|| ((Letter_k2.getY()) <= (Letter_k.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_k2.setPosition(Letter_k.getX(),Letter_k.getY()+Letter_k.getHeight()-Letter_k2.getHeight());

								is_k_PartUp2 = true;
								checkLetterOnComplete(Letter_k);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_k2.setPosition(initLetterX2, initLetterY2);
		Letter_k2.setAlpha(1);
		Letter_k2.setZIndex(4);

		Letter_k2.setScale(0);
		registerTouchArea(Letter_k2);

		Letter_k3 = new Sprite(0, 0, resourcesManager.build_k3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_k_PartUp3) {

					Letter_k3.setPosition(pSceneTouchEvent.getX()- Letter_k3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_k3.getHeight() / 2);

				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_k3.collidesWith(Letter_k)) {

						if (((Letter_k3.getX() + Letter_k3.getWidth()) >= (Letter_k.getX() + Letter_k.getWidth() - Tolerance))
								|| ((Letter_k3.getX() + Letter_k3.getWidth()) <= (Letter_k.getX() + Letter_k.getWidth() + Tolerance))) {

							if (((Letter_k3.getY() + Letter_k3.getHeight()) >= (Letter_k.getY() + Letter_k.getHeight() - Tolerance))
									|| ((Letter_k3.getY() + Letter_k3.getHeight()) <= (Letter_k.getY()+ Letter_k.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_k3.setPosition(Letter_k.getX() + Letter_k.getWidth()- Letter_k3.getWidth()-2,Letter_k.getY() + Letter_k.getHeight()- Letter_k3.getHeight());

								is_k_PartUp3 = true;
								checkLetterOnComplete(Letter_k);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_k3.setPosition(initLetterX3, initLetterY3);
		Letter_k3.setAlpha(1);
		Letter_k3.setZIndex(4);
		Letter_k3.setScale(0);

		registerTouchArea(Letter_k3);

		Letter_K = new Sprite(0, 0, resourcesManager.build_K_region, vbom);
		Letter_K.setPosition(InitX, InitCapY);
		Letter_K.setAlpha(1);
		Letter_K.setScale(0);
		Letter_K.setZIndex(3);


		Letter_K1 = new Sprite(0, 0, resourcesManager.build_K1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_K_PartUp1) {
					Letter_K1.setPosition(pSceneTouchEvent.getX()- Letter_K1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_K1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_K1.collidesWith(Letter_K)) {

						if (((Letter_K1.getX()) >= (Letter_K.getX() - Tolerance))
								|| ((Letter_K1.getX()) <= (Letter_K.getX() + Tolerance))) {

							if (((Letter_K1.getY()) >= (Letter_K.getY() - Tolerance))
									|| ((Letter_K1.getY()) <= (Letter_K.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_K1.setPosition(Letter_K.getX(),Letter_K.getY());
								is_K_PartUp1 = true;
								checkLetterOnComplete(Letter_K);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_K1.setPosition(InitCapX1, InitCapY1);
		Letter_K1.setAlpha(1);
		Letter_K1.setZIndex(4);

		Letter_K1.setScale(0);
		registerTouchArea(Letter_K1);

		Letter_K2 = new Sprite(0, 0, resourcesManager.build_K2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_K_PartUp2) {
					Letter_K2.setPosition(pSceneTouchEvent.getX()- Letter_K2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_K2.getHeight() / 2);

				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_K2.collidesWith(Letter_K)) {

						if (((Letter_K2.getX() >= (Letter_K.getX() - Tolerance)) 
								|| (Letter_K1.getX() <= (Letter_K.getX() + Tolerance)))) {

							if (((Letter_K2.getY() + Letter_K2.getHeight() / 2) >= (Letter_K.getY() + Letter_K.getHeight() / 2 - Tolerance))
									|| ((Letter_K2.getY() + Letter_K2.getHeight() / 2) <= (Letter_K.getY() + Letter_K.getHeight() / 2 + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_K2.setPosition(Letter_K.getX(),Letter_K.getY()+ (Letter_K.getHeight()- Letter_K2.getHeight()-2));

								is_K_PartUp2 = true;
								checkLetterOnComplete(Letter_K);
							}
						}

					}
				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_K2.setPosition(InitCapX2, InitCapY2);
		Letter_K2.setAlpha(1);
		Letter_K2.setZIndex(4);
		Letter_K2.setScale(0);

		registerTouchArea(Letter_K2);

		Letter_K3 = new Sprite(0, 0, resourcesManager.build_K3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_K_PartUp3) {
					Letter_K3.setPosition(pSceneTouchEvent.getX()- Letter_K3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_K3.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_K3.collidesWith(Letter_K)) {

						if (((Letter_K3.getX()) >= (Letter_K.getX() - Tolerance))
								|| ((Letter_K3.getX()) <= (Letter_K.getX() + Tolerance))) {

							if (((Letter_K3.getY() + Letter_K3.getHeight()) >= (Letter_K.getY() + Letter_K.getHeight() - Tolerance))
									|| ((Letter_K3.getY() + Letter_K3.getHeight()) <= (Letter_K.getY()+ Letter_K.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_K3.setPosition(Letter_K.getX()+Letter_K.getWidth()-Letter_K3.getWidth()-2,Letter_K.getY() + Letter_K.getHeight()- Letter_K3.getHeight());

								is_K_PartUp3 = true;
								checkLetterOnComplete(Letter_K);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_K3.setPosition(InitCapX3, InitCapY3);
		Letter_K3.setAlpha(1);
		Letter_K3.setZIndex(4);
		Letter_K3.setScale(0);

		registerTouchArea(Letter_K3);

		Letter_l = new Sprite(0, 0, resourcesManager.build_l_region, vbom);
		Letter_l.setPosition(InitX, InitLowY);
		Letter_l.setScale(0);
		Letter_l.setAlpha(1);
		Letter_l.setZIndex(3);


		Letter_l1 = new Sprite(0, 0, resourcesManager.build_l1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_l_PartUp1) {
					Letter_l1.setPosition(pSceneTouchEvent.getX()- Letter_l1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_l1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_l1.collidesWith(Letter_l)) {

						if (((Letter_l1.getX()) >= (Letter_l.getX() - Tolerance))
								|| ((Letter_l1.getX()) <= (Letter_l.getX() + Tolerance))) {

							if (((Letter_l1.getY()) >= (Letter_l.getY() - Tolerance))
									|| ((Letter_l1.getY()) <= (Letter_l.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_l1.setPosition(Letter_l.getX()+2,Letter_l.getY());

								is_l_PartUp1 = true;
								checkLetterOnComplete(Letter_l);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_l1.setPosition(initLetterX1, initLetterY1);
		Letter_l1.setAlpha(1);
		Letter_l1.setZIndex(4);
		Letter_l1.setScale(0);

		registerTouchArea(Letter_l1);

		Letter_l2 = new Sprite(0, 0, resourcesManager.build_l2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_l_PartUp2) {
					Letter_l2.setPosition(pSceneTouchEvent.getX()- Letter_l2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_l2.getHeight() / 2);
				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_l2.collidesWith(Letter_l)) {

						if (((Letter_l2.getX() + Letter_l2.getWidth()) >= (Letter_l.getX() + Letter_l.getWidth() - Tolerance))
								|| ((Letter_l1.getX() + Letter_l1.getWidth()) <= (Letter_l.getX() + Letter_l.getWidth() + Tolerance))) {

							if (((Letter_l2.getY() + Letter_l2.getHeight() / 2) >= (Letter_l.getY() + Letter_l.getHeight() / 2 - Tolerance))
									|| ((Letter_l2.getY() + Letter_l2.getHeight() / 2) <= (Letter_l.getY() + Letter_l.getHeight() / 2 + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_l2.setPosition(Letter_l.getX()+2,Letter_l.getY()+ Letter_l.getHeight()/ 2- Letter_l2.getHeight()/ 2+2);

								is_l_PartUp2 = true;
								checkLetterOnComplete(Letter_l);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_l2.setPosition(initLetterX2, initLetterY2);
		Letter_l2.setAlpha(1);
		Letter_l2.setZIndex(4);

		Letter_l2.setScale(0);
		registerTouchArea(Letter_l2);

		Letter_l3 = new Sprite(0, 0, resourcesManager.build_l3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_l_PartUp3) {

					Letter_l3.setPosition(pSceneTouchEvent.getX()- Letter_l3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_l3.getHeight() / 2);

				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_l3.collidesWith(Letter_l)) {

						if (((Letter_l3.getX()) >= (Letter_l.getX() - Tolerance))
								|| ((Letter_l3.getX()) <= (Letter_l.getX() + Tolerance))) {

							if (((Letter_l3.getY() + Letter_l3.getHeight()) >= (Letter_l.getY() + Letter_l.getHeight() - Tolerance))
									|| ((Letter_l3.getY() + Letter_l3.getHeight()) <= (Letter_l.getY()+ Letter_l.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_l3.setPosition(Letter_l.getX()+2,Letter_l.getY() + Letter_l.getHeight()- Letter_l3.getHeight());

								is_l_PartUp3 = true;
								checkLetterOnComplete(Letter_l);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_l3.setPosition(initLetterX3, initLetterY3);
		Letter_l3.setAlpha(1);
		Letter_l3.setZIndex(4);
		Letter_l3.setScale(0);

		registerTouchArea(Letter_l3);

		Letter_L = new Sprite(0, 0, resourcesManager.build_L_region, vbom);
		Letter_L.setPosition(InitX, InitCapY);
		Letter_L.setAlpha(1);
		Letter_L.setScale(0);
		Letter_L.setZIndex(3);


		Letter_L1 = new Sprite(0, 0, resourcesManager.build_L1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,
					float Y) {

				if (!is_L_PartUp1) {
					Letter_L1.setPosition(pSceneTouchEvent.getX()- Letter_L1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_L1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_L1.collidesWith(Letter_L)) {

						if (((Letter_L1.getX()) >= (Letter_L.getX() - Tolerance))
								|| ((Letter_L1.getX()) <= (Letter_L.getX() + Tolerance))) {

							if (((Letter_L1.getY()) >= (Letter_L.getY() - Tolerance))
									|| ((Letter_L1.getY()) <= (Letter_L.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_L1.setPosition(Letter_L.getX()+2,Letter_L.getY());

								is_L_PartUp1 = true;
								checkLetterOnComplete(Letter_L);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_L1.setPosition(InitCapX1, InitCapY1);
		Letter_L1.setAlpha(1);
		Letter_L1.setZIndex(4);

		Letter_L1.setScale(0);
		registerTouchArea(Letter_L1);

		Letter_L2 = new Sprite(0, 0, resourcesManager.build_L2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,
					float Y) {
				if (!is_L_PartUp2) {
					Letter_L2.setPosition(pSceneTouchEvent.getX()- Letter_L2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_L2.getHeight() / 2);

				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_L2.collidesWith(Letter_L)) {

						if (((Letter_L2.getX() >= (Letter_L.getX() - Tolerance)) || (Letter_L1.getX() <= (Letter_L.getX() + Tolerance)))) {

							if (((Letter_L2.getY() + Letter_L2.getHeight() / 2) >= (Letter_L.getY() + Letter_L.getHeight() / 2 - Tolerance))
									|| ((Letter_L2.getY() + Letter_L2.getHeight() / 2) <= (Letter_L.getY() + Letter_L.getHeight() / 2 + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_L2.setPosition(Letter_L.getX(),Letter_L.getY()+ (Letter_L.getHeight() / 2 - Letter_L2.getHeight() / 2+2));

								is_L_PartUp2 = true;
								checkLetterOnComplete(Letter_L);
							}
						}

					}
				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_L2.setPosition(InitCapX2, InitCapY2);
		Letter_L2.setAlpha(1);
		Letter_L2.setZIndex(4);
		Letter_L2.setScale(0);

		registerTouchArea(Letter_L2);

		Letter_L3 = new Sprite(0, 0, resourcesManager.build_L3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,
					float Y) {

				if (!is_L_PartUp3) {
					Letter_L3.setPosition(pSceneTouchEvent.getX()- Letter_L3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_L3.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_L3.collidesWith(Letter_L)) {

						if (((Letter_L3.getX()) >= (Letter_L.getX() - Tolerance))
								|| ((Letter_L3.getX()) <= (Letter_L.getX() + Tolerance))) {

							if (((Letter_L3.getY() + Letter_L3.getHeight()) >= (Letter_L.getY() + Letter_L.getHeight() - Tolerance))
									|| ((Letter_L3.getY() + Letter_L3.getHeight()) <= (Letter_L.getY()+ Letter_L.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_L3.setPosition(Letter_L.getX()+2,Letter_L.getY() + Letter_L.getHeight()- Letter_L3.getHeight()-2);

								is_L_PartUp3 = true;
								checkLetterOnComplete(Letter_L);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_L3.setPosition(InitCapX3, InitCapY3);
		Letter_L3.setAlpha(1);
		Letter_L3.setZIndex(4);
		Letter_L3.setScale(0);

		registerTouchArea(Letter_L3);

		Letter_m = new Sprite(0, 0, resourcesManager.build_m_region, vbom);
		Letter_m.setPosition(InitX, InitLowY);
		Letter_m.setScale(0);
		Letter_m.setAlpha(1);
		Letter_m.setZIndex(3);


		Letter_m1 = new Sprite(0, 0, resourcesManager.build_m1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_m_PartUp1) {
					Letter_m1.setPosition(pSceneTouchEvent.getX()- Letter_m1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_m1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_m1.collidesWith(Letter_m)) {

						if (((Letter_m1.getX()) >= (Letter_m.getX() - Tolerance))
								|| ((Letter_m1.getX()) <= (Letter_m.getX() + Tolerance))) {

							if (((Letter_m1.getY()) >= (Letter_m.getY() - Tolerance))
									|| ((Letter_m1.getY()) <= (Letter_m.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_m1.setPosition(Letter_m.getX()+2,Letter_m.getY()+Letter_m.getHeight()-Letter_m1.getHeight());

								is_m_PartUp1 = true;
								checkLetterOnComplete(Letter_m);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_m1.setPosition(initLetterX1, initLetterY1);
		Letter_m1.setAlpha(1);
		Letter_m1.setZIndex(4);
		Letter_m1.setScale(0);

		registerTouchArea(Letter_m1);

		Letter_m2 = new Sprite(0, 0, resourcesManager.build_m2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_m_PartUp2) {

					Letter_m2.setPosition(pSceneTouchEvent.getX()- Letter_m2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_m2.getHeight() / 2);
				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_m2.collidesWith(Letter_m)) {

						if (((Letter_m2.getX() + Letter_m2.getWidth() / 2) >= (Letter_m.getX() + Letter_m.getWidth() / 2 - Tolerance))
								|| ((Letter_m2.getX() + Letter_m2.getWidth() / 2) <= (Letter_m.getX() + Letter_m.getWidth() / 2 + Tolerance))) {

							if (((Letter_m2.getY()) >= (Letter_m.getY() - Tolerance))
									|| ((Letter_m2.getY()) <= (Letter_m.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_m2.setPosition(Letter_m.getX()+2,Letter_m.getY()+2);

								is_m_PartUp2 = true;
								checkLetterOnComplete(Letter_m);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_m2.setPosition(initLetterX2, initLetterY2);
		Letter_m2.setAlpha(1);
		Letter_m2.setZIndex(4);

		Letter_m2.setScale(0);
		registerTouchArea(Letter_m2);

		Letter_m3 = new Sprite(0, 0, resourcesManager.build_m3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_m_PartUp3) {

					Letter_m3.setPosition(pSceneTouchEvent.getX()- Letter_m3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_m3.getHeight() / 2);

				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_m3.collidesWith(Letter_m)) {

						if (((Letter_m3.getX() + Letter_m3.getWidth()) >= (Letter_m	.getX() + Letter_m.getWidth() - Tolerance))
								|| ((Letter_m3.getX() + Letter_m3.getWidth()) <= (Letter_m.getX() + Letter_m.getWidth() + Tolerance))) {

							if (((Letter_m3.getY()) >= (Letter_m.getY() - Tolerance))
									|| ((Letter_m3.getY()) <= (Letter_m.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_m3.setPosition(Letter_m.getX() + Letter_m.getWidth()- Letter_m3.getWidth(),Letter_m.getY()+Letter_m.getHeight()-Letter_m2.getHeight());

								is_m_PartUp3 = true;
								checkLetterOnComplete(Letter_m);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_m3.setPosition(initLetterX3, initLetterY3);
		Letter_m3.setAlpha(1);
		Letter_m3.setZIndex(4);
		Letter_m3.setScale(0);

		registerTouchArea(Letter_m3);

		Letter_M = new Sprite(0, 0, resourcesManager.build_M_region, vbom);
		Letter_M.setPosition(InitX, InitCapY);
		Letter_M.setAlpha(1);
		Letter_M.setScale(0);
		Letter_M.setZIndex(3);


		Letter_M1 = new Sprite(0, 0, resourcesManager.build_M1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_M_PartUp1) {
					Letter_M1.setPosition(pSceneTouchEvent.getX()- Letter_M1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_M1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_M1.collidesWith(Letter_M)) {

						if (((Letter_M1.getX()) >= (Letter_M.getX() - Tolerance))
								|| ((Letter_M1.getX()) <= (Letter_M.getX() + Tolerance))) {

							if (((Letter_M1.getY()) >= (Letter_M.getY() - Tolerance))
									|| ((Letter_M1.getY()) <= (Letter_M.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_M1.setPosition(Letter_M.getX(),Letter_M.getY());
								is_M_PartUp1 = true;
								checkLetterOnComplete(Letter_M);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_M1.setPosition(InitCapX1, InitCapY1);
		Letter_M1.setAlpha(1);
		Letter_M1.setZIndex(4);

		Letter_M1.setScale(0);
		registerTouchArea(Letter_M1);

		Letter_M2 = new Sprite(0, 0, resourcesManager.build_M2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_M_PartUp2) {
					Letter_M2.setPosition(pSceneTouchEvent.getX()- Letter_M2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_M2.getHeight() / 2);

				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_M2.collidesWith(Letter_M)) {

						if (((Letter_M2.getX() + Letter_M2.getWidth() / 2 >= (Letter_M.getX() + Letter_M.getWidth() / 2 - Tolerance))
								|| (Letter_M2.getX() + Letter_M2.getWidth() / 2 <= (Letter_M.getX() + Letter_M.getWidth() / 2 + Tolerance)))) {

							if (((Letter_M2.getY()) >= (Letter_M.getY() - Tolerance))
									|| ((Letter_M2.getY()) <= (Letter_M.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_M2.setPosition(Letter_M.getX() ,Letter_M.getY()+Letter_M.getHeight()-Letter_M2.getHeight());

								is_M_PartUp2 = true;
								checkLetterOnComplete(Letter_M);
							}
						}

					}
				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_M2.setPosition(InitCapX2, InitCapY2);
		Letter_M2.setAlpha(1);
		Letter_M2.setZIndex(4);
		Letter_M2.setScale(0);

		registerTouchArea(Letter_M2);

		Letter_M3 = new Sprite(0, 0, resourcesManager.build_M3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_M_PartUp3) {
					Letter_M3.setPosition(	pSceneTouchEvent.getX()- Letter_M3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_M3.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_M3.collidesWith(Letter_M)) {

						if (((Letter_M3.getX() + Letter_M3.getWidth()) >= (Letter_M.getX() + Letter_M.getWidth() - Tolerance))
								|| ((Letter_M3.getX() + Letter_M3.getWidth()) <= (Letter_M.getX() + Letter_M.getWidth() + Tolerance))) {

							if (((Letter_M3.getY()) >= (Letter_M.getY() - Tolerance))
									|| ((Letter_M3.getY()) <= (Letter_M.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_M3.setPosition(Letter_M.getX() + Letter_M.getWidth()- Letter_M3.getWidth(),Letter_M.getY()+Letter_M.getHeight()-Letter_M3.getHeight());

								is_M_PartUp3 = true;
								checkLetterOnComplete(Letter_M);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_M3.setPosition(InitCapX3, InitCapY3);
		Letter_M3.setAlpha(1);
		Letter_M3.setZIndex(4);
		Letter_M3.setScale(0);

		registerTouchArea(Letter_M3);



		Letter_n = new Sprite(0, 0, resourcesManager.build_n_region, vbom);
		Letter_n.setPosition(InitX, InitLowY);
		Letter_n.setScale(0);
		Letter_n.setAlpha(1);
		Letter_n.setZIndex(3);

		Letter_n1 = new Sprite(0, 0, resourcesManager.build_n1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_n_PartUp1) {
					Letter_n1.setPosition(pSceneTouchEvent.getX()- Letter_n1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_n1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_n1.collidesWith(Letter_n)) {

						if (((Letter_n1.getX()) >= (Letter_n.getX() - Tolerance))
								|| ((Letter_n1.getX()) <= (Letter_n.getX() + Tolerance))) {

							if (((Letter_n1.getY()) >= (Letter_n.getY() - Tolerance))
									|| ((Letter_n1.getY()) <= (Letter_n.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_n1.setPosition(Letter_n.getX()+3,Letter_n.getY()+2);

								is_n_PartUp1 = true;
								checkLetterOnComplete(Letter_n);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_n1.setPosition(initLetterX1, initLetterY1);
		Letter_n1.setAlpha(1);
		Letter_n1.setZIndex(4);
		Letter_n1.setScale(0);

		registerTouchArea(Letter_n1);

		Letter_n2 = new Sprite(0, 0, resourcesManager.build_n2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_n_PartUp2) {

					Letter_n2.setPosition(pSceneTouchEvent.getX()- Letter_n2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_n2.getHeight() / 2);
				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_n2.collidesWith(Letter_n)) {

						if (((Letter_n2.getX() + Letter_n2.getWidth() / 2) >= (Letter_n.getX() + Letter_n.getWidth() / 2 - Tolerance))
								|| ((Letter_n2.getX() + Letter_n2.getWidth() / 2) <= (Letter_n.getX() + Letter_n.getWidth() / 2 + Tolerance))) {

							if (((Letter_n2.getY()) >= (Letter_n.getY() - Tolerance))
									|| ((Letter_n2.getY()) <= (Letter_n.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_n2.setPosition(Letter_n.getX() + Letter_n.getWidth()- Letter_n2.getWidth()-2,Letter_n.getY()+Letter_n.getHeight()-Letter_n2.getHeight()-2);



								is_n_PartUp2 = true;
								checkLetterOnComplete(Letter_n);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_n2.setPosition(initLetterX2, initLetterY2);
		Letter_n2.setAlpha(1);
		Letter_n2.setZIndex(4);

		Letter_n2.setScale(0);
		registerTouchArea(Letter_n2);

		Letter_n3 = new Sprite(0, 0, resourcesManager.build_n3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_m_PartUp3) {

					Letter_n3.setPosition(pSceneTouchEvent.getX()- Letter_n3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_n3.getHeight() / 2);

				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_n3.collidesWith(Letter_n)) {

						if (((Letter_n3.getX() + Letter_n3.getWidth()) >= (Letter_n	.getX() + Letter_n.getWidth() - Tolerance))
								|| ((Letter_n3.getX() + Letter_n3.getWidth()) <= (Letter_n.getX() + Letter_n.getWidth() + Tolerance))) {

							if (((Letter_n3.getY()) >= (Letter_n.getY() - Tolerance))
									|| ((Letter_n3.getY()) <= (Letter_n.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_n3.setPosition(Letter_n.getX()+2,Letter_n.getY()+Letter_n.getHeight()-Letter_n3.getHeight()-4);

								is_n_PartUp3 = true;
								checkLetterOnComplete(Letter_n);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_n3.setPosition(initLetterX3, initLetterY3);
		Letter_n3.setAlpha(1);
		Letter_n3.setZIndex(4);
		Letter_n3.setScale(0);
		registerTouchArea(Letter_n3);

		Letter_N = new Sprite(0, 0, resourcesManager.build_N_region, vbom);
		Letter_N.setPosition(InitX, InitCapY);
		Letter_N.setAlpha(1);
		Letter_N.setScale(0);
		Letter_N.setZIndex(3);





		Letter_N1 = new Sprite(0, 0, resourcesManager.build_N1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_N_PartUp1) {
					Letter_N1.setPosition(pSceneTouchEvent.getX()- Letter_N1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_N1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_N1.collidesWith(Letter_N)) {

						if (((Letter_N1.getX()) >= (Letter_N.getX() - Tolerance))
								|| ((Letter_N1.getX()) <= (Letter_N.getX() + Tolerance))) {

							if (((Letter_N1.getY()) >= (Letter_N.getY() - Tolerance))
									|| ((Letter_N1.getY()) <= (Letter_N.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_N1.setPosition(Letter_N.getX()+5,Letter_N.getY());
								is_N_PartUp1 = true;
								checkLetterOnComplete(Letter_N);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_N1.setPosition(InitCapX1, InitCapY1);
		Letter_N1.setAlpha(1);
		Letter_N1.setZIndex(4);

		Letter_N1.setScale(0);
		registerTouchArea(Letter_N1);

		Letter_N2 = new Sprite(0, 0, resourcesManager.build_N2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_N_PartUp2) {
					Letter_N2.setPosition(pSceneTouchEvent.getX()- Letter_N2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_N2.getHeight() / 2);

				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_N2.collidesWith(Letter_N)) {

						if (((Letter_N2.getX() + Letter_N2.getWidth() / 2 >= (Letter_N.getX() + Letter_N.getWidth() / 2 - Tolerance))
								|| (Letter_N2.getX() + Letter_N2.getWidth() / 2 <= (Letter_N.getX() + Letter_N.getWidth() / 2 + Tolerance)))) {

							if (((Letter_N2.getY()) >= (Letter_N.getY() - Tolerance))
									|| ((Letter_N2.getY()) <= (Letter_N.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_N2.setPosition(Letter_N.getX()+3,Letter_N.getY()+Letter_N.getHeight()-Letter_N2.getHeight());

								is_N_PartUp2 = true;
								checkLetterOnComplete(Letter_N);
							}
						}

					}
				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_N2.setPosition(InitCapX2, InitCapY2);
		Letter_N2.setAlpha(1);
		Letter_N2.setZIndex(4);
		Letter_N2.setScale(0);

		registerTouchArea(Letter_N2);

		Letter_N3 = new Sprite(0, 0, resourcesManager.build_N3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_N_PartUp3) {
					Letter_N3.setPosition(	pSceneTouchEvent.getX()- Letter_N3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_N3.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_N3.collidesWith(Letter_N)) {

						if (((Letter_N3.getX() + Letter_N3.getWidth()) >= (Letter_N.getX() + Letter_N.getWidth() - Tolerance))
								|| ((Letter_N3.getX() + Letter_N3.getWidth()) <= (Letter_N.getX() + Letter_N.getWidth() + Tolerance))) {

							if (((Letter_N3.getY()) >= (Letter_N.getY() - Tolerance))
									|| ((Letter_N3.getY()) <= (Letter_N.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_N3.setPosition(Letter_N.getX() + Letter_N.getWidth()- Letter_N3.getWidth(),Letter_N.getY()+Letter_N.getHeight()-Letter_N3.getHeight());

								is_N_PartUp3 = true;
								checkLetterOnComplete(Letter_N);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_N3.setPosition(InitCapX3, InitCapY3);
		Letter_N3.setAlpha(1);
		Letter_N3.setZIndex(4);
		Letter_N3.setScale(0);

		registerTouchArea(Letter_N3);

		Letter_o = new Sprite(0, 0, resourcesManager.build_o_region, vbom);
		Letter_o.setPosition(InitX, InitLowY);
		Letter_o.setScale(0);
		Letter_o.setAlpha(1);
		Letter_o.setZIndex(3);


		Letter_o1 = new Sprite(0, 0, resourcesManager.build_o1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,
					float Y) {

				if (!is_o_PartUp1) {
					Letter_o1.setPosition(pSceneTouchEvent.getX()- Letter_o1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_o1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_o1.collidesWith(Letter_o)) {

						if (((Letter_o1.getX()) >= (Letter_o.getX() - Tolerance))
								|| ((Letter_o1.getX()) <= (Letter_o.getX() + Tolerance))) {

							if (((Letter_o1.getY()) >= (Letter_o.getY() - Tolerance))
									|| ((Letter_o1.getY()) <= (Letter_o.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_o1.setPosition(Letter_o.getX(),Letter_o.getY());

								is_o_PartUp1 = true;
								checkLetterOnComplete(Letter_o);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_o1.setPosition(initLetterX1, initLetterY1);
		Letter_o1.setAlpha(1);
		Letter_o1.setZIndex(4);
		Letter_o1.setScale(0);

		registerTouchArea(Letter_o1);

		Letter_o2 = new Sprite(0, 0, resourcesManager.build_o2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,
					float Y) {
				if (!is_o_PartUp2) {
					Letter_o2.setPosition(pSceneTouchEvent.getX()- Letter_o2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_o2.getHeight() / 2);
				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_o2.collidesWith(Letter_o)) {

						if (((Letter_o2.getX() + Letter_o2.getWidth()) >= (Letter_o.getX() + Letter_o.getWidth() - Tolerance))
								|| ((Letter_o1.getX() + Letter_o1.getWidth()) <= (Letter_o.getX() + Letter_o.getWidth() + Tolerance))) {

							if (((Letter_o2.getY()) >= (Letter_o.getY() - Tolerance))
									|| ((Letter_o2.getY()) <= (Letter_o.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_o2.setPosition(Letter_o.getX(),Letter_o.getY()+Letter_o.getHeight()-Letter_o2.getHeight());

								is_o_PartUp2 = true;
								checkLetterOnComplete(Letter_o);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_o2.setPosition(initLetterX2, initLetterY2);
		Letter_o2.setAlpha(1);
		Letter_o2.setZIndex(4);

		Letter_o2.setScale(0);
		registerTouchArea(Letter_o2);

		Letter_o3 = new Sprite(0, 0, resourcesManager.build_o3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_o_PartUp3) {

					Letter_o3.setPosition(pSceneTouchEvent.getX()- Letter_o3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_o3.getHeight() / 2);

				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_o3.collidesWith(Letter_o)) {

						if (((Letter_o3.getX()) >= (Letter_o.getX() - Tolerance))
								|| ((Letter_o3.getX()) <= (Letter_o.getX() + Tolerance))) {

							if (((Letter_o3.getY() + Letter_o3.getHeight()) >= (Letter_o.getY() + Letter_o.getHeight() - Tolerance))
									|| ((Letter_o3.getY() + Letter_o3.getHeight()) <= (Letter_o.getY()+ Letter_o.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_o3.setPosition(Letter_o.getX()+Letter_o.getWidth()-Letter_o3.getWidth(),Letter_o.getY() + Letter_o.getHeight()- Letter_o3.getHeight());

								is_o_PartUp3 = true;
								checkLetterOnComplete(Letter_o);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_o3.setPosition(initLetterX3, initLetterY3);
		Letter_o3.setAlpha(1);
		Letter_o3.setZIndex(4);
		Letter_o3.setScale(0);

		registerTouchArea(Letter_o3);

		Letter_O = new Sprite(0, 0, resourcesManager.build_O_region, vbom);
		Letter_O.setPosition(InitX, InitCapY);
		Letter_O.setAlpha(1);
		Letter_O.setScale(0);
		Letter_O.setZIndex(3);


		Letter_O1 = new Sprite(0, 0, resourcesManager.build_O1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_O_PartUp1) {
					Letter_O1.setPosition(pSceneTouchEvent.getX()- Letter_O1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_O1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_O1.collidesWith(Letter_O)) {

						if (((Letter_O1.getX()) >= (Letter_O.getX() - Tolerance))
								|| ((Letter_O1.getX()) <= (Letter_O.getX() + Tolerance))) {

							if (((Letter_O1.getY()) >= (Letter_O.getY() - Tolerance))
									|| ((Letter_O1.getY()) <= (Letter_O.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_O1.setPosition(Letter_O.getX(),Letter_O.getY());
								is_O_PartUp1 = true;
								checkLetterOnComplete(Letter_O);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_O1.setPosition(InitCapX1, InitCapY1);
		Letter_O1.setAlpha(1);
		Letter_O1.setZIndex(4);

		Letter_O1.setScale(0);
		registerTouchArea(Letter_O1);

		Letter_O2 = new Sprite(0, 0, resourcesManager.build_O2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_O_PartUp2) {
					Letter_O2.setPosition(pSceneTouchEvent.getX()- Letter_O2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_O2.getHeight() / 2);

				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_O2.collidesWith(Letter_O)) {

						if (((Letter_O2.getX() + Letter_O2.getWidth() >= (Letter_O.getX() + Letter_O.getWidth() - Tolerance))
								|| (Letter_O2.getX() + Letter_O2.getWidth() <= (Letter_O.getX() + Letter_O.getWidth() + Tolerance)))) {

							if (((Letter_O2.getY()) >= (Letter_O.getY() - Tolerance))
									|| ((Letter_O2.getY()) <= (Letter_O.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_O2.setPosition(Letter_O.getX() ,Letter_O.getY()+ Letter_O.getHeight()- Letter_O2.getHeight());

								is_O_PartUp2 = true;
								checkLetterOnComplete(Letter_O);
							}
						}

					}
				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_O2.setPosition(InitCapX2, InitCapY2);
		Letter_O2.setAlpha(1);
		Letter_O2.setZIndex(4);
		Letter_O2.setScale(0);

		registerTouchArea(Letter_O2);

		Letter_O3 = new Sprite(0, 0, resourcesManager.build_O3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_O_PartUp3) {
					Letter_O3.setPosition(	pSceneTouchEvent.getX()- Letter_O3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_O3.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_O3.collidesWith(Letter_O)) {

						if (((Letter_O3.getX() + Letter_O3.getWidth()) >= (Letter_O.getX() + Letter_O.getWidth() - Tolerance))
								|| ((Letter_O3.getX() + Letter_O3.getWidth()) <= (Letter_O.getX() + Letter_O.getWidth() + Tolerance))) {

							if (((Letter_O3.getY() + Letter_O3.getHeight()) >= (Letter_O.getY() + Letter_O.getHeight() - Tolerance))
									|| ((Letter_O3.getY() + Letter_O3.getHeight()) <= (Letter_O.getY()+ Letter_O.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_O3.setPosition(Letter_O.getX() + Letter_O.getWidth()- Letter_O3.getWidth(),Letter_O.getY() + Letter_O.getHeight()- Letter_O3.getHeight());

								is_O_PartUp3 = true;
								checkLetterOnComplete(Letter_O);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_O3.setPosition(InitCapX3, InitCapY3);
		Letter_O3.setAlpha(1);
		Letter_O3.setZIndex(4);
		Letter_O3.setScale(0);

		registerTouchArea(Letter_O3);


		Letter_r = new Sprite(0, 0, resourcesManager.build_r_region, vbom);
		Letter_r.setPosition(InitX, InitLowY);
		Letter_r.setScale(0);
		Letter_r.setAlpha(1);
		Letter_r.setZIndex(3);


		Letter_r1 = new Sprite(0, 0, resourcesManager.build_r1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,
					float Y) {

				if (!is_r_PartUp1) {
					Letter_r1.setPosition(pSceneTouchEvent.getX()- Letter_r1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_r1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_r1.collidesWith(Letter_r)) {

						if (((Letter_r1.getX()) >= (Letter_r.getX() - Tolerance))
								|| ((Letter_r1.getX()) <= (Letter_r.getX() + Tolerance))) {

							if (((Letter_r1.getY()) >= (Letter_r.getY() - Tolerance))
									|| ((Letter_r1.getY()) <= (Letter_r.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_r1.setPosition(Letter_r.getX()+2,Letter_r.getY());

								is_r_PartUp1 = true;
								checkLetterOnComplete(Letter_r);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_r1.setPosition(initLetterX1, initLetterY1);
		Letter_r1.setAlpha(1);
		Letter_r1.setZIndex(4);
		Letter_r1.setScale(0);

		registerTouchArea(Letter_r1);

		Letter_r2 = new Sprite(0, 0, resourcesManager.build_r2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_r_PartUp2) {

					Letter_r2.setPosition(pSceneTouchEvent.getX()- Letter_r2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_r2.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_r2.collidesWith(Letter_r)) {

						if (((Letter_r2.getX()) >= (Letter_r.getX() - Tolerance))
								|| ((Letter_r2.getX()) <= (Letter_r.getX() + Tolerance))) {

							if (((Letter_r2.getY()) >= (Letter_r.getY() - Tolerance))
									|| ((Letter_r2.getY()) <= (Letter_r.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_r2.setPosition(Letter_r.getX()+2,Letter_r.getY()+Letter_r.getHeight()-Letter_r2.getHeight()-14);

								is_r_PartUp2 = true;
								checkLetterOnComplete(Letter_r);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_r2.setPosition(initLetterX2, initLetterY2);
		Letter_r2.setAlpha(1);
		Letter_r2.setZIndex(4);

		Letter_r2.setScale(0);
		registerTouchArea(Letter_r2);

		Letter_r3 = new Sprite(0, 0, resourcesManager.build_r3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_r_PartUp3) {

					Letter_r3.setPosition(pSceneTouchEvent.getX()- Letter_r3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_r3.getHeight() / 2);

				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_r3.collidesWith(Letter_r)) {

						if (((Letter_r3.getX()) >= (Letter_r.getX() - Tolerance))
								|| ((Letter_r3.getX()) <= (Letter_r.getX() + Tolerance))) {

							if (((Letter_r3.getY() + Letter_r3.getHeight()) >= (Letter_r.getY() + Letter_r.getHeight() - Tolerance))
									|| ((Letter_r3.getY() + Letter_r3.getHeight()) <= (Letter_r.getY()+ Letter_r.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_r3.setPosition(Letter_r.getX(),Letter_r.getY() + Letter_r.getHeight()- Letter_r3.getHeight());

								is_r_PartUp3 = true;
								checkLetterOnComplete(Letter_r);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_r3.setPosition(initLetterX3, initLetterY3);
		Letter_r3.setAlpha(1);
		Letter_r3.setZIndex(4);
		Letter_r3.setScale(0);

		registerTouchArea(Letter_r3);

		Letter_R = new Sprite(0, 0, resourcesManager.build_R_region, vbom);
		Letter_R.setPosition(InitX, InitCapY);
		Letter_R.setAlpha(1);
		Letter_R.setScale(0);
		Letter_R.setZIndex(3);


		Letter_R1 = new Sprite(0, 0, resourcesManager.build_R1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_R_PartUp1) {
					Letter_R1.setPosition(pSceneTouchEvent.getX()- Letter_R1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_R1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_R1.collidesWith(Letter_R)) {

						if (((Letter_R1.getX()) >= (Letter_R.getX() - Tolerance))
								|| ((Letter_R1.getX()) <= (Letter_R.getX() + Tolerance))) {

							if (((Letter_R1.getY()) >= (Letter_R.getY() - Tolerance))
									|| ((Letter_R1.getY()) <= (Letter_R.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_R1.setPosition(Letter_R.getX()+3,Letter_R.getY());
								is_R_PartUp1 = true;
								checkLetterOnComplete(Letter_R);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_R1.setPosition(InitCapX1, InitCapY1);
		Letter_R1.setAlpha(1);
		Letter_R1.setZIndex(4);

		Letter_R1.setScale(0);
		registerTouchArea(Letter_R1);

		Letter_R2 = new Sprite(0, 0, resourcesManager.build_R2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_R_PartUp2) {
					Letter_R2.setPosition(pSceneTouchEvent.getX()- Letter_R2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_R2.getHeight() / 2);

				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_R2.collidesWith(Letter_R)) {

						if (((Letter_R2.getX() >= (Letter_R.getX() - Tolerance))
								|| (Letter_R2.getX() <= (Letter_R.getX() + Tolerance)))) {

							if (((Letter_R2.getY() + Letter_R2.getHeight()) >= (Letter_R.getY() + Letter_R.getHeight() - Tolerance))
									|| ((Letter_R2.getY() + Letter_R2.getHeight()) <= (Letter_R.getY()+ Letter_R.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_R2.setPosition(Letter_R.getX()+3,Letter_R.getY() + Letter_R.getHeight()- Letter_R2.getHeight());

								is_R_PartUp2 = true;
								checkLetterOnComplete(Letter_R);
							}
						}

					}
				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_R2.setPosition(InitCapX2, InitCapY2);
		Letter_R2.setAlpha(1);
		Letter_R2.setZIndex(4);
		Letter_R2.setScale(0);

		registerTouchArea(Letter_R2);

		Letter_R3 = new Sprite(0, 0, resourcesManager.build_R3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_R_PartUp3) {
					Letter_R3.setPosition(pSceneTouchEvent.getX()- Letter_R3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_R3.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_R3.collidesWith(Letter_R)) {

						if (((Letter_R3.getX() + Letter_R3.getWidth()) >= (Letter_R.getX() + Letter_R.getWidth() - Tolerance))
								|| ((Letter_R3.getX() + Letter_R3.getWidth()) <= (Letter_R.getX() + Letter_R.getWidth() + Tolerance))) {

							if (((Letter_R3.getY() + Letter_R3.getHeight()) >= (Letter_R.getY() + Letter_R.getHeight() - Tolerance))
									|| ((Letter_R3.getY() + Letter_R3.getHeight()) <= (Letter_R.getY()+ Letter_R.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_R3.setPosition(Letter_R.getX() + Letter_R.getWidth()- Letter_R3.getWidth()-3,Letter_R.getY() + Letter_R.getHeight()- Letter_R3.getHeight());

								is_R_PartUp3 = true;
								checkLetterOnComplete(Letter_R);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_R3.setPosition(InitCapX3, InitCapY3);
		Letter_R3.setAlpha(1);
		Letter_R3.setZIndex(4);
		Letter_R3.setScale(0);

		registerTouchArea(Letter_R3);

		Letter_s = new Sprite(0, 0, resourcesManager.build_s_region, vbom);
		Letter_s.setPosition(InitX, InitLowY);
		Letter_s.setScale(0);
		Letter_s.setAlpha(1);
		Letter_s.setZIndex(3);


		Letter_s1 = new Sprite(0, 0, resourcesManager.build_s1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_s_PartUp1) {
					Letter_s1.setPosition(pSceneTouchEvent.getX()- Letter_s1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_s1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_s1.collidesWith(Letter_s)) {

						if (((Letter_s1.getX()) >= (Letter_s.getX() - Tolerance))
								|| ((Letter_s1.getX()) <= (Letter_s.getX() + Tolerance))) {

							if (((Letter_s1.getY()) >= (Letter_s.getY() - Tolerance))
									|| ((Letter_s1.getY()) <= (Letter_s.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_s1.setPosition(Letter_s.getX(),Letter_s.getY());

								is_s_PartUp1 = true;
								checkLetterOnComplete(Letter_s);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_s1.setPosition(initLetterX1, initLetterY1);
		Letter_s1.setAlpha(1);
		Letter_s1.setZIndex(4);
		Letter_s1.setScale(0);

		registerTouchArea(Letter_s1);

		Letter_s2 = new Sprite(0, 0, resourcesManager.build_s2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,
					float Y) {
				if (!is_s_PartUp2) {
					Letter_s2.setPosition(pSceneTouchEvent.getX()- Letter_s2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_s2.getHeight() / 2);
				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_s2.collidesWith(Letter_s)) {

						if (((Letter_s2.getX() + Letter_s2.getWidth()) >= (Letter_s.getX() + Letter_s.getWidth() - Tolerance))
								|| ((Letter_s2.getX() + Letter_s2.getWidth()) <= (Letter_s.getX() + Letter_s.getWidth() + Tolerance))) {

							if (((Letter_s2.getY() + Letter_s2.getHeight() / 2) >= (Letter_s.getY() + Letter_s.getHeight() / 2 - Tolerance))
									|| ((Letter_s2.getY() + Letter_s2.getHeight() / 2) <= (Letter_s.getY() + Letter_s.getHeight() / 2 + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_s2.setPosition(Letter_s.getX()+Letter_s.getWidth()-Letter_s2.getWidth()-2,Letter_s.getY()+49);

								is_s_PartUp2 = true;
								checkLetterOnComplete(Letter_s);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_s2.setPosition(initLetterX2, initLetterY2);
		Letter_s2.setAlpha(1);
		Letter_s2.setZIndex(4);

		Letter_s2.setScale(0);
		registerTouchArea(Letter_s2);

		Letter_s3 = new Sprite(0, 0, resourcesManager.build_s3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_s_PartUp3) {

					Letter_s3.setPosition(pSceneTouchEvent.getX()- Letter_s3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_s3.getHeight() / 2);

				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_s3.collidesWith(Letter_s)) {

						if (((Letter_s3.getX()) >= (Letter_s.getX() - Tolerance))
								|| ((Letter_s3.getX()) <= (Letter_s.getX() + Tolerance))) {

							if (((Letter_s3.getY() + Letter_s3.getHeight()) >= (Letter_s.getY() + Letter_s.getHeight() - Tolerance))
									|| ((Letter_s3.getY() + Letter_s3.getHeight()) <= (Letter_s.getY()+ Letter_s.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_s3.setPosition(Letter_s.getX(),Letter_s.getY() + Letter_s.getHeight()- Letter_s3.getHeight());

								is_s_PartUp3 = true;
								checkLetterOnComplete(Letter_s);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_s3.setPosition(initLetterX3, initLetterY3);
		Letter_s3.setAlpha(1);
		Letter_s3.setZIndex(4);
		Letter_s3.setScale(0);

		registerTouchArea(Letter_s3);

		Letter_S = new Sprite(0, 0, resourcesManager.build_S_region, vbom);
		Letter_S.setPosition(InitX, InitCapY);
		Letter_S.setAlpha(1);
		Letter_S.setScale(0);
		Letter_S.setZIndex(3);


		Letter_S1 = new Sprite(0, 0, resourcesManager.build_S1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_S_PartUp1) {
					Letter_S1.setPosition(pSceneTouchEvent.getX()- Letter_S1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_S1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_S1.collidesWith(Letter_S)) {

						if (((Letter_S1.getX()) >= (Letter_S.getX() - Tolerance))
								|| ((Letter_S1.getX()) <= (Letter_S.getX() + Tolerance))) {

							if (((Letter_S1.getY()) >= (Letter_S.getY() - Tolerance))
									|| ((Letter_S1.getY()) <= (Letter_S.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_S1.setPosition(Letter_S.getX()+Letter_S.getWidth()-Letter_S1.getWidth()-5,Letter_S.getY()+2);
								is_S_PartUp1 = true;
								checkLetterOnComplete(Letter_S);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_S1.setPosition(InitCapX1, InitCapY1);
		Letter_S1.setAlpha(1);
		Letter_S1.setZIndex(4);

		Letter_S1.setScale(0);
		registerTouchArea(Letter_S1);

		Letter_S2 = new Sprite(0, 0, resourcesManager.build_S2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,
					float Y) {
				if (!is_S_PartUp2) {
					Letter_S2.setPosition(pSceneTouchEvent.getX()- Letter_S2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_S2.getHeight() / 2);

				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_S2.collidesWith(Letter_S)) {

						if (((Letter_S2.getX() >= (Letter_S.getX() - Tolerance)) 
								|| (Letter_S2.getX() <= (Letter_S.getX() + Tolerance)))) {

							if (((Letter_S2.getY() + Letter_S2.getHeight()) >= (Letter_S.getY() + Letter_S.getHeight() - Tolerance))
									|| ((Letter_S2.getY() + Letter_S2.getHeight()) <= (Letter_S.getY()+ Letter_S.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_S2.setPosition(Letter_S.getX(),Letter_S.getY());

								is_S_PartUp2 = true;
								checkLetterOnComplete(Letter_S);
							}
						}

					}
				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_S2.setPosition(InitCapX2, InitCapY2);
		Letter_S2.setAlpha(1);
		Letter_S2.setZIndex(4);
		Letter_S2.setScale(0);

		registerTouchArea(Letter_S2);

		Letter_S3 = new Sprite(0, 0, resourcesManager.build_S3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_S_PartUp3) {
					Letter_S3.setPosition(pSceneTouchEvent.getX()- Letter_S3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_S3.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_S3.collidesWith(Letter_S)) {

						if (((Letter_S3.getX()) >= (Letter_S.getX() - Tolerance))
								|| ((Letter_S3.getX()) <= (Letter_S.getX() + Tolerance))) {

							if (((Letter_S3.getY() + Letter_S3.getHeight()) >= (Letter_S.getY() + Letter_S.getHeight() - Tolerance))
									|| ((Letter_S3.getY() + Letter_S3.getHeight()) <= (Letter_S.getY()+ Letter_S.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();
								Letter_S3.setPosition(Letter_S.getX()+2,Letter_S.getY() + Letter_S.getHeight()- Letter_S3.getHeight()-2);

								is_S_PartUp3 = true;
								checkLetterOnComplete(Letter_S);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_S3.setPosition(InitCapX3, InitCapY3);
		Letter_S3.setAlpha(1);
		Letter_S3.setZIndex(4);
		Letter_S3.setScale(0);

		registerTouchArea(Letter_S3);


		Letter_u = new Sprite(0, 0, resourcesManager.build_u_region, vbom);
		Letter_u.setPosition(InitX, InitLowY);
		Letter_u.setScale(0);
		Letter_u.setAlpha(1);
		Letter_u.setZIndex(3);


		Letter_u1 = new Sprite(0, 0, resourcesManager.build_u1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_u_PartUp1) {
					Letter_u1.setPosition(pSceneTouchEvent.getX()- Letter_u1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_u1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_u1.collidesWith(Letter_u)) {

						if (((Letter_u1.getX()) >= (Letter_u.getX() - Tolerance))
								|| ((Letter_u1.getX()) <= (Letter_u.getX() + Tolerance))) {

							if (((Letter_u1.getY() + Letter_u1.getHeight()) >= (Letter_u.getY() + Letter_u.getHeight() - Tolerance))
									|| ((Letter_u1.getY() + Letter_u1.getHeight()) <= (Letter_u.getY()+ Letter_u.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_u1.setPosition(Letter_u.getX()+2,Letter_u.getY());

								is_u_PartUp1 = true;
								checkLetterOnComplete(Letter_u);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_u1.setPosition(initLetterX1, initLetterY1);
		Letter_u1.setAlpha(1);
		Letter_u1.setZIndex(4);
		Letter_u1.setScale(0);

		registerTouchArea(Letter_u1);

		Letter_u2 = new Sprite(0, 0, resourcesManager.build_u2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_u_PartUp2) {
					Letter_u2.setPosition(pSceneTouchEvent.getX()- Letter_u2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_u2.getHeight() / 2);
				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_u2.collidesWith(Letter_u)) {

						if (((Letter_u2.getX() + Letter_u2.getWidth()) >= (Letter_u.getX() + Letter_u.getWidth() - Tolerance))
								|| ((Letter_u2.getX() + Letter_u2.getWidth()) <= (Letter_u.getX() + Letter_u.getWidth() + Tolerance))) {

							if (((Letter_u2.getY()) >= (Letter_u.getY() - Tolerance))
									|| ((Letter_u2.getY()) <= (Letter_u.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_u2.setPosition(Letter_u.getX()+1,Letter_u.getY()+Letter_u.getHeight()-Letter_u2.getHeight()-2);

								is_u_PartUp2 = true;
								checkLetterOnComplete(Letter_u);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_u2.setPosition(initLetterX2, initLetterY2);
		Letter_u2.setAlpha(1);
		Letter_u2.setZIndex(4);

		Letter_u2.setScale(0);
		registerTouchArea(Letter_u2);

		Letter_u3 = new Sprite(0, 0, resourcesManager.build_u3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_u_PartUp3) {

					Letter_u3.setPosition(pSceneTouchEvent.getX()- Letter_u3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_u3.getHeight() / 2);

				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_u3.collidesWith(Letter_u)) {

						if (((Letter_u3.getX()) >= (Letter_u.getX() - Tolerance))
								|| ((Letter_u3.getX()) <= (Letter_u.getX() + Tolerance))) {

							if (((Letter_u3.getY()) >= (Letter_u.getY() - Tolerance))
									|| ((Letter_u3.getY()) <= (Letter_u.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_u3.setPosition(Letter_u.getX() + Letter_u.getWidth()- Letter_u3.getWidth()-2,Letter_u.getY()+Letter_u.getHeight()-Letter_u3.getHeight()-2);

								is_u_PartUp3 = true;
								checkLetterOnComplete(Letter_u);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_u3.setPosition(initLetterX3, initLetterY3);
		Letter_u3.setAlpha(1);
		Letter_u3.setZIndex(4);
		Letter_u3.setScale(0);

		registerTouchArea(Letter_u3);

		Letter_U = new Sprite(0, 0, resourcesManager.build_U_region, vbom);
		Letter_U.setPosition(InitX, InitCapY);
		Letter_U.setAlpha(1);
		Letter_U.setScale(0);
		Letter_U.setZIndex(3);


		Letter_U1 = new Sprite(0, 0, resourcesManager.build_U1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_U_PartUp1) {
					Letter_U1.setPosition(pSceneTouchEvent.getX()- Letter_U1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_U1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_U1.collidesWith(Letter_U)) {

						if (((Letter_U1.getX()) >= (Letter_U.getX() - Tolerance))
								|| ((Letter_U1.getX()) <= (Letter_U.getX() + Tolerance))) {

							if (((Letter_U1.getY() + Letter_U1.getHeight()) >= (Letter_U.getY() + Letter_U.getHeight() - Tolerance))
									|| ((Letter_U1.getY() + Letter_U1.getHeight()) <= (Letter_U.getY()+ Letter_U.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_U1.setPosition(Letter_U.getX()+2,Letter_U.getY());
								is_U_PartUp1 = true;
								checkLetterOnComplete(Letter_U);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_U1.setPosition(InitCapX1, InitCapY1);
		Letter_U1.setAlpha(1);
		Letter_U1.setZIndex(4);

		Letter_U1.setScale(0);
		registerTouchArea(Letter_U1);

		Letter_U2 = new Sprite(0, 0, resourcesManager.build_U2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_U_PartUp2) {
					Letter_U2.setPosition(pSceneTouchEvent.getX()- Letter_U2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_U2.getHeight() / 2);

				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_U2.collidesWith(Letter_U)) {

						if (((Letter_U2.getX() >= (Letter_U.getX() - Tolerance))
								|| (Letter_U2.getX() <= (Letter_U.getX() + Tolerance)))) {

							if (((Letter_U2.getY()) >= (Letter_U.getY() - Tolerance))
									|| ((Letter_U2.getY()) <= (Letter_U.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_U2.setPosition(Letter_U.getX()+2,Letter_U.getY()+Letter_U.getHeight()-Letter_U2.getHeight());

								is_U_PartUp2 = true;
								checkLetterOnComplete(Letter_U);
							}
						}

					}
				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_U2.setPosition(InitCapX2, InitCapY2);
		Letter_U2.setAlpha(1);
		Letter_U2.setZIndex(4);
		Letter_U2.setScale(0);
		registerTouchArea(Letter_U2);

		Letter_U3 = new Sprite(0, 0, resourcesManager.build_U3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_U_PartUp3) {
					Letter_U3.setPosition(pSceneTouchEvent.getX()- Letter_U3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_U3.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_U3.collidesWith(Letter_U)) {

						if (((Letter_U3.getX() + Letter_U3.getWidth()) >= (Letter_U.getX() + Letter_U.getWidth() - Tolerance))
								|| ((Letter_U3.getX() + Letter_U3.getWidth()) <= (Letter_U.getX() + Letter_U.getWidth() + Tolerance))) {

							if (((Letter_U3.getY()) >= (Letter_U.getY() - Tolerance))
									|| ((Letter_U3.getY()) <= (Letter_U.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_U3.setPosition(Letter_U.getX() + Letter_U.getWidth()- Letter_U3.getWidth()-2,Letter_U.getY()+Letter_U.getHeight()-Letter_U3.getHeight()-2);

								is_U_PartUp3 = true;
								checkLetterOnComplete(Letter_U);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_U3.setPosition(InitCapX3, InitCapY3);
		Letter_U3.setAlpha(1);
		Letter_U3.setZIndex(4);
		Letter_U3.setScale(0);

		registerTouchArea(Letter_U3);

		Letter_v = new Sprite(0, 0, resourcesManager.build_v_region, vbom);
		Letter_v.setPosition(InitX, InitLowY);
		Letter_v.setScale(0);
		Letter_v.setAlpha(1);
		Letter_v.setZIndex(3);


		Letter_v1 = new Sprite(0, 0, resourcesManager.build_v1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_v_PartUp1) {
					Letter_v1.setPosition(pSceneTouchEvent.getX()- Letter_v1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_v1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_v1.collidesWith(Letter_v)) {

						if (((Letter_v1.getX()) >= (Letter_v.getX() - Tolerance))
								|| ((Letter_v1.getX()) <= (Letter_v.getX() + Tolerance))) {

							if (((Letter_v1.getY() + Letter_v1.getHeight()) >= (Letter_v.getY() + Letter_v.getHeight() - Tolerance))
									|| ((Letter_v1.getY() + Letter_v1.getHeight()) <= (Letter_v.getY()+ Letter_v.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_v1.setPosition(Letter_v.getX() + 17,Letter_v.getY() + Letter_v.getHeight()- Letter_v1.getHeight());

								is_v_PartUp1 = true;
								checkLetterOnComplete(Letter_v);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_v1.setPosition(initLetterX1, initLetterY1);
		Letter_v1.setAlpha(1);
		Letter_v1.setZIndex(4);
		Letter_v1.setScale(0);

		registerTouchArea(Letter_v1);

		Letter_v2 = new Sprite(0, 0, resourcesManager.build_v2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,
					float Y) {
				if (!is_v_PartUp2) {
					Letter_v2.setPosition(pSceneTouchEvent.getX()- Letter_v2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_v2.getHeight() / 2);
				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_v2.collidesWith(Letter_v)) {

						if (((Letter_v2.getX()) >= (Letter_v.getX() - Tolerance))
								|| ((Letter_v2.getX()) <= (Letter_v.getX() + Tolerance))) {

							if (((Letter_v2.getY()) >= (Letter_v.getY() - Tolerance))
									|| ((Letter_v2.getY()) <= (Letter_v.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_v2.setPosition(Letter_v.getX()+3,Letter_v.getY());

								is_v_PartUp2 = true;
								checkLetterOnComplete(Letter_v);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_v2.setPosition(initLetterX2, initLetterY2);
		Letter_v2.setAlpha(1);
		Letter_v2.setZIndex(4);

		Letter_v2.setScale(0);
		registerTouchArea(Letter_v2);

		Letter_v3 = new Sprite(0, 0, resourcesManager.build_v3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_v_PartUp3) {

					Letter_v3.setPosition(pSceneTouchEvent.getX()- Letter_v3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_v3.getHeight() / 2);

				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_v3.collidesWith(Letter_v)) {

						if (((Letter_v3.getX() + Letter_v3.getWidth()) >= (Letter_v.getX() + Letter_v.getWidth() - Tolerance))
								|| ((Letter_v3.getX() + Letter_v3.getWidth()) <= (Letter_v.getX() + Letter_v.getWidth() + Tolerance))) {

							if (((Letter_v3.getY()) >= (Letter_v.getY() - Tolerance))
									|| ((Letter_v3.getY()) <= (Letter_v.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_v3.setPosition(Letter_v.getX() + Letter_v.getWidth()- Letter_v3.getWidth(),Letter_v.getY());

								is_v_PartUp3 = true;
								checkLetterOnComplete(Letter_v);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_v3.setPosition(initLetterX3, initLetterY3);
		Letter_v3.setAlpha(1);
		Letter_v3.setZIndex(4);
		Letter_v3.setScale(0);

		registerTouchArea(Letter_v3);

		Letter_V = new Sprite(0, 0, resourcesManager.build_V_region, vbom);
		Letter_V.setPosition(InitX, InitCapY);
		Letter_V.setAlpha(1);
		Letter_V.setScale(0);
		Letter_V.setZIndex(3);


		Letter_V1 = new Sprite(0, 0, resourcesManager.build_V1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,
					float Y) {

				if (!is_V_PartUp1) {
					Letter_V1.setPosition(pSceneTouchEvent.getX()- Letter_V1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_V1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_V1.collidesWith(Letter_V)) {

						if (((Letter_V1.getX()) >= (Letter_V.getX() - Tolerance))
								|| ((Letter_V1.getX()) <= (Letter_V.getX() + Tolerance))) {

							if (((Letter_V1.getY() + Letter_V1.getHeight()) >= (Letter_V.getY() + Letter_V.getHeight() - Tolerance))
									|| ((Letter_V1.getY() + Letter_V1.getHeight()) <= (Letter_V.getY()+ Letter_V.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_V1.setPosition(Letter_V.getX() + 20,Letter_V.getY() + Letter_V.getHeight()- Letter_V1.getHeight());
								is_V_PartUp1 = true;
								checkLetterOnComplete(Letter_V);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_V1.setPosition(InitCapX1, InitCapY1);
		Letter_V1.setAlpha(1);
		Letter_V1.setZIndex(4);

		Letter_V1.setScale(0);
		registerTouchArea(Letter_V1);

		Letter_V2 = new Sprite(0, 0, resourcesManager.build_V2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_V_PartUp2) {
					Letter_V2.setPosition(pSceneTouchEvent.getX()- Letter_V2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_V2.getHeight() / 2);

				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_V2.collidesWith(Letter_V)) {

						if (((Letter_V2.getX() + Letter_V2.getWidth() >= (Letter_V.getX() + Letter_V.getWidth() - Tolerance))
								|| (Letter_V2.getX() + Letter_V2.getWidth() <= (Letter_V.getX() + Letter_V.getWidth() + Tolerance)))) {

							if (((Letter_V2.getY()) >= (Letter_V.getY()))
									|| ((Letter_V2.getY()) <= (Letter_V.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_V2.setPosition(Letter_V.getX(),Letter_V.getY());

								is_V_PartUp2 = true;
								checkLetterOnComplete(Letter_V);
							}
						}

					}
				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_V2.setPosition(InitCapX2, InitCapY2);
		Letter_V2.setAlpha(1);
		Letter_V2.setZIndex(4);
		Letter_V2.setScale(0);

		registerTouchArea(Letter_V2);

		Letter_V3 = new Sprite(0, 0, resourcesManager.build_V3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_V_PartUp3) {
					Letter_V3.setPosition(pSceneTouchEvent.getX()- Letter_V3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_V3.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_V3.collidesWith(Letter_V)) {

						if (((Letter_V3.getX()) >= (Letter_V.getX() - Tolerance))
								|| ((Letter_V3.getX()) <= (Letter_V.getX() + Tolerance))) {

							if (((Letter_V3.getY()) >= (Letter_V.getY() - Tolerance))
									|| ((Letter_V3.getY()) <= (Letter_V.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_V3.setPosition(Letter_V.getX()+Letter_V.getWidth()-Letter_V3.getWidth(),Letter_V.getY());

								is_V_PartUp3 = true;
								checkLetterOnComplete(Letter_V);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_V3.setPosition(InitCapX3, InitCapY3);
		Letter_V3.setAlpha(1);
		Letter_V3.setZIndex(4);
		Letter_V3.setScale(0);

		registerTouchArea(Letter_V3);

		Letter_w = new Sprite(0, 0, resourcesManager.build_w_region, vbom);
		Letter_w.setPosition(InitX, InitLowY);
		Letter_w.setScale(0);
		Letter_w.setAlpha(1);
		Letter_w.setZIndex(3);


		Letter_w1 = new Sprite(0, 0, resourcesManager.build_w1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_w_PartUp1) {
					Letter_w1.setPosition(pSceneTouchEvent.getX()- Letter_w1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_w1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_w1.collidesWith(Letter_w)) {

						if (((Letter_w1.getX() + Letter_w1.getWidth() / 2) >= (Letter_w.getX() + Letter_w.getWidth() / 2 - Tolerance))
								|| ((Letter_w1.getX() + Letter_w1.getWidth() / 2) <= (Letter_w.getX() + Letter_w.getWidth() / 2 + Tolerance))) {

							if (((Letter_w1.getY()) >= (Letter_w.getY() - Tolerance))
									|| ((Letter_w1.getY()) <= (Letter_w.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_w1.setPosition(Letter_w.getX() + Letter_w.getWidth()/ 2 - Letter_w1.getWidth() / 2,Letter_w.getY());

								is_w_PartUp1 = true;
								checkLetterOnComplete(Letter_w);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_w1.setPosition(initLetterX1, initLetterY1);
		Letter_w1.setAlpha(1);
		Letter_w1.setZIndex(4);
		Letter_w1.setScale(0);

		registerTouchArea(Letter_w1);

		Letter_w2 = new Sprite(0, 0, resourcesManager.build_w2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_w_PartUp2) {
					Letter_w2.setPosition(pSceneTouchEvent.getX()- Letter_w2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_w2.getHeight() / 2);
				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_w2.collidesWith(Letter_w)) {

						if (((Letter_w2.getX()) >= (Letter_w.getX() - Tolerance))
								|| ((Letter_w2.getX()) <= (Letter_w.getX() + Tolerance))) {

							if (((Letter_w2.getY()) >= (Letter_w.getY() - Tolerance))
									|| ((Letter_w2.getY()) <= (Letter_w.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_w2.setPosition(Letter_w.getX()+2,Letter_w.getY()+2);

								is_w_PartUp2 = true;
								checkLetterOnComplete(Letter_w);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_w2.setPosition(initLetterX2, initLetterY2);
		Letter_w2.setAlpha(1);
		Letter_w2.setZIndex(4);

		Letter_w2.setScale(0);
		registerTouchArea(Letter_w2);

		Letter_w3 = new Sprite(0, 0, resourcesManager.build_w3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,
					float Y) {
				if (!is_w_PartUp3) {

					Letter_w3.setPosition(pSceneTouchEvent.getX()- Letter_w3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_w3.getHeight() / 2);

				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_w3.collidesWith(Letter_w)) {

						if (((Letter_w3.getX() + Letter_w3.getWidth()) >= (Letter_w.getX() + Letter_w.getWidth() - Tolerance))
								|| ((Letter_w3.getX() + Letter_w3.getWidth()) <= (Letter_w.getX() + Letter_w.getWidth() + Tolerance))) {

							if (((Letter_w3.getY()) >= (Letter_w.getY() - Tolerance))
									|| ((Letter_w3.getY()) <= (Letter_w.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_w3.setPosition(Letter_w.getX() + Letter_w.getWidth()- Letter_w3.getWidth()-2,Letter_w.getY()+3);

								is_w_PartUp3 = true;
								checkLetterOnComplete(Letter_w);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_w3.setPosition(initLetterX3, initLetterY3);
		Letter_w3.setAlpha(1);
		Letter_w3.setZIndex(4);
		Letter_w3.setScale(0);

		registerTouchArea(Letter_w3);

		Letter_W = new Sprite(0, 0, resourcesManager.build_W_region, vbom);
		Letter_W.setPosition(InitX, InitCapY);
		Letter_W.setAlpha(1);
		Letter_W.setScale(0);
		Letter_W.setZIndex(3);


		Letter_W1 = new Sprite(0, 0, resourcesManager.build_W1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_W_PartUp1) {
					Letter_W1.setPosition(pSceneTouchEvent.getX()- Letter_W1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_W1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_W1.collidesWith(Letter_W)) {

						if (((Letter_W1.getX() + Letter_W1.getWidth() / 2) >= (Letter_W.getX() + Letter_W.getWidth() / 2 - Tolerance))
								|| ((Letter_W1.getX() + Letter_W1.getWidth() / 2) <= (Letter_W.getX() + Letter_W.getWidth() / 2 + Tolerance))) {

							if (((Letter_W1.getY()) >= (Letter_W.getY() - Tolerance))
									|| ((Letter_W1.getY()) <= (Letter_W.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_W1.setPosition(Letter_W.getX()+2,Letter_W.getY());
								is_W_PartUp1 = true;
								checkLetterOnComplete(Letter_W);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_W1.setPosition(InitCapX1, InitCapY1);
		Letter_W1.setAlpha(1);
		Letter_W1.setZIndex(4);

		Letter_W1.setScale(0);
		registerTouchArea(Letter_W1);

		Letter_W2 = new Sprite(0, 0, resourcesManager.build_W2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,
					float Y) {
				if (!is_W_PartUp2) {
					Letter_W2.setPosition(pSceneTouchEvent.getX()- Letter_W2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_W2.getHeight() / 2);

				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_W2.collidesWith(Letter_W)) {

						if (((Letter_W2.getX() + Letter_W2.getWidth() >= (Letter_W.getX() + Letter_W.getWidth() - Tolerance))
								|| (Letter_W2.getX() + Letter_W2.getWidth() <= (Letter_W.getX() + Letter_W.getWidth() + Tolerance)))) {

							if (((Letter_W2.getY()) >= (Letter_W.getY() - Tolerance))
									|| ((Letter_W2.getY()) <= (Letter_W.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_W2.setPosition(Letter_W.getX()+5,Letter_W.getY()+Letter_W.getHeight()-Letter_W2.getHeight());

								is_W_PartUp2 = true;
								checkLetterOnComplete(Letter_W);
							}
						}

					}
				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_W2.setPosition(InitCapX2, InitCapY2);
		Letter_W2.setAlpha(1);
		Letter_W2.setZIndex(4);
		Letter_W2.setScale(0);

		registerTouchArea(Letter_W2);

		Letter_W3 = new Sprite(0, 0, resourcesManager.build_W3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,
					float Y) {

				if (!is_W_PartUp3) {
					Letter_W3.setPosition(pSceneTouchEvent.getX()- Letter_W3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_W3.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_W3.collidesWith(Letter_W)) {

						if (((Letter_W3.getX()) >= (Letter_W.getX() - Tolerance))
								|| ((Letter_W3.getX()) <= (Letter_W.getX() + Tolerance))) {

							if (((Letter_W3.getY()) >= (Letter_W.getY() - Tolerance))
									|| ((Letter_W3.getY()) <= (Letter_W.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_W3.setPosition(Letter_W.getX()+Letter_W.getWidth()-Letter_W3.getWidth()-5,Letter_W.getY()+Letter_W.getHeight()-Letter_W3.getHeight());

								is_W_PartUp3 = true;
								checkLetterOnComplete(Letter_W);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_W3.setPosition(InitCapX3, InitCapY3);
		Letter_W3.setAlpha(1);
		Letter_W3.setZIndex(4);
		Letter_W3.setScale(0);

		registerTouchArea(Letter_W3);

		Letter_z = new Sprite(0, 0, resourcesManager.build_z_region, vbom);
		Letter_z.setPosition(InitX, InitLowY);
		Letter_z.setScale(0);
		Letter_z.setAlpha(1);
		Letter_z.setZIndex(3);


		Letter_z1 = new Sprite(0, 0, resourcesManager.build_z1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_z_PartUp1) {
					Letter_z1.setPosition(pSceneTouchEvent.getX()- Letter_z1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_z1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_z1.collidesWith(Letter_z)) {

						if (((Letter_z1.getX()) >= (Letter_z.getX() - Tolerance))
								|| ((Letter_z1.getX()) <= (Letter_z.getX() + Tolerance))) {

							if (((Letter_z1.getY()) >= (Letter_z.getY() - Tolerance))
									|| ((Letter_z1.getY()) <= (Letter_z.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_z1.setPosition(Letter_z.getX()+5,Letter_z.getY());

								is_z_PartUp1 = true;
								checkLetterOnComplete(Letter_z);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_z1.setPosition(initLetterX1, initLetterY1);
		Letter_z1.setAlpha(1);
		Letter_z1.setZIndex(4);
		Letter_z1.setScale(0);

		registerTouchArea(Letter_z1);

		Letter_z2 = new Sprite(0, 0, resourcesManager.build_z2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_z_PartUp2) {
					Letter_z2.setPosition(pSceneTouchEvent.getX()- Letter_z2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_z2.getHeight() / 2);
				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_z2.collidesWith(Letter_z)) {

						if (((Letter_z2.getX() + Letter_z2.getWidth() / 2) >= (Letter_z.getX() + Letter_z.getWidth() / 2 - Tolerance))
								|| ((Letter_z2.getX() + Letter_z2.getWidth() / 2) <= (Letter_z.getX() + Letter_z.getWidth() / 2 + Tolerance))) {

							if (((Letter_z2.getY() + Letter_z2.getHeight() / 2) >= (Letter_z.getY() + Letter_z.getHeight() / 2 - Tolerance))
									|| ((Letter_z2.getY() + Letter_z2.getHeight() / 2) <= (Letter_z.getY() + Letter_z.getHeight() / 2 + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_z2.setPosition(Letter_z.getX(),Letter_z.getY()+30);

								is_z_PartUp2 = true;
								checkLetterOnComplete(Letter_z);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_z2.setPosition(initLetterX2, initLetterY2);
		Letter_z2.setAlpha(1);
		Letter_z2.setZIndex(4);

		Letter_z2.setScale(0);
		registerTouchArea(Letter_z2);

		Letter_z3 = new Sprite(0, 0, resourcesManager.build_z3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_z_PartUp3) {

					Letter_z3.setPosition(pSceneTouchEvent.getX()- Letter_z3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_z3.getHeight() / 2);

				}
				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_z3.collidesWith(Letter_z)) {

						if (((Letter_z3.getX()) >= (Letter_z.getX() - Tolerance))
								|| ((Letter_z3.getX()) <= (Letter_z.getX() + Tolerance))) {

							if (((Letter_z3.getY() + Letter_z3.getHeight()) >= (Letter_z.getY() + Letter_z.getHeight() - Tolerance))
									|| ((Letter_z3.getY() + Letter_z3.getHeight()) <= (Letter_z.getY()+ Letter_z.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_z3.setPosition(Letter_z.getX(),Letter_z.getY() + Letter_z.getHeight()- Letter_z3.getHeight());

								is_z_PartUp3 = true;
								checkLetterOnComplete(Letter_z);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_z3.setPosition(initLetterX3, initLetterY3);
		Letter_z3.setAlpha(1);
		Letter_z3.setZIndex(4);
		Letter_z3.setScale(0);

		registerTouchArea(Letter_z3);

		Letter_Z = new Sprite(0, 0, resourcesManager.build_Z_region, vbom);
		Letter_Z.setPosition(InitX, InitCapY);
		Letter_Z.setAlpha(1);
		Letter_Z.setScale(0);
		Letter_Z.setZIndex(3);


		Letter_Z1 = new Sprite(0, 0, resourcesManager.build_Z1_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,
					float Y) {

				if (!is_Z_PartUp1) {
					Letter_Z1.setPosition(pSceneTouchEvent.getX()- Letter_Z1.getWidth() / 2,pSceneTouchEvent.getY()- Letter_Z1.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_Z1.collidesWith(Letter_Z)) {

						if (((Letter_Z1.getX()) >= (Letter_Z.getX() - Tolerance))
								|| ((Letter_Z1.getX()) <= (Letter_Z.getX() + Tolerance))) {

							if (((Letter_Z1.getY()) >= (Letter_Z.getY() - Tolerance))
									|| ((Letter_Z1.getY()) <= (Letter_Z.getY() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_Z1.setPosition(Letter_Z.getX() + 5,Letter_Z.getY());
								is_Z_PartUp1 = true;
								checkLetterOnComplete(Letter_Z);

							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_Z1.setPosition(InitCapX1, InitCapY1);
		Letter_Z1.setAlpha(1);
		Letter_Z1.setZIndex(4);

		Letter_Z1.setScale(0);
		registerTouchArea(Letter_Z1);

		Letter_Z2 = new Sprite(0, 0, resourcesManager.build_Z2_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {
				if (!is_Z_PartUp2) {
					Letter_Z2.setPosition(pSceneTouchEvent.getX()- Letter_Z2.getWidth() / 2,pSceneTouchEvent.getY()- Letter_Z2.getHeight() / 2);

				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_Z2.collidesWith(Letter_Z)) {

						if (((Letter_Z2.getX() + Letter_Z2.getWidth() / 2 >= (Letter_Z.getX() + Letter_Z.getWidth() / 2 - Tolerance)) 
								|| (Letter_Z2.getX() + Letter_Z2.getWidth() / 2 <= (Letter_Z.getX() + Letter_Z.getWidth() / 2 + Tolerance)))) {

							if (((Letter_Z2.getY() + Letter_Z2.getHeight() / 2) >= (Letter_Z.getY() + Letter_Z.getHeight() / 2 - Tolerance))
									|| ((Letter_Z2.getY() + Letter_Z2.getHeight() / 2) <= (Letter_Z.getY() + Letter_Z.getHeight() / 2 + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_Z2.setPosition(Letter_Z.getX(),Letter_Z.getY()+ Letter_Z.getHeight()/ 2- Letter_Z2.getHeight()/2+10);

								is_Z_PartUp2 = true;
								checkLetterOnComplete(Letter_Z);
							}
						}

					}
				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_Z2.setPosition(InitCapX2, InitCapY2);
		Letter_Z2.setAlpha(1);
		Letter_Z2.setZIndex(4);
		Letter_Z2.setScale(0);

		registerTouchArea(Letter_Z2);

		Letter_Z3 = new Sprite(0, 0, resourcesManager.build_Z3_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (!is_Z_PartUp3) {
					Letter_Z3.setPosition(pSceneTouchEvent.getX()- Letter_Z3.getWidth() / 2,pSceneTouchEvent.getY()- Letter_Z3.getHeight() / 2);
				}

				if (pSceneTouchEvent.isActionUp()) {

					if (Letter_Z3.collidesWith(Letter_Z)) {

						if (((Letter_Z3.getX()) >= (Letter_Z.getX() - Tolerance))
								|| ((Letter_Z3.getX()) <= (Letter_Z.getX() + Tolerance))) {

							if (((Letter_Z3.getY() + Letter_Z3.getHeight()) >= (Letter_Z.getY() + Letter_Z.getHeight() - Tolerance))
									|| ((Letter_Z3.getY() + Letter_Z3.getHeight()) <= (Letter_Z.getY()+ Letter_Z.getHeight() + Tolerance))) {

								SceneManager.getInstance().playBuildOkSound();

								Letter_Z3.setPosition(Letter_Z.getX(),Letter_Z.getY() + Letter_Z.getHeight()- Letter_Z3.getHeight());

								is_Z_PartUp3 = true;
								checkLetterOnComplete(Letter_Z);
							}
						}

					}

				}

				if (pSceneTouchEvent.isActionDown()) {

				}

				return true;

			};

		};

		Letter_Z3.setPosition(InitCapX3, InitCapY3);
		Letter_Z3.setAlpha(1);
		Letter_Z3.setZIndex(4);
		Letter_Z3.setScale(0);
		registerTouchArea(Letter_Z3);

		initScene();
		sortChildren();

	}

	private void initScene() {

		Hanger0Part0Mod = new ParallelEntityModifier(new ScaleModifier(3, 1,0.2f), new MoveModifier(3, InitX, InitHangarX0, InitLowY,InitHangarRow1 + 30), new ColorModifier(3, 1, 1, 1, 0, 0.75f,0.5f));
		Hanger0Part1Mod = new ParallelEntityModifier(new ScaleModifier(3, 1,0.2f), new MoveModifier(3, InitX, InitHangarCapX0, InitCapY,InitHangarRow1+10), new ColorModifier(3, 1, 1, 1, 0.5f, 0.75f, 0.5f));

		Hanger1Part0Mod = new ParallelEntityModifier(new ScaleModifier(3, 1,0.2f), new MoveModifier(3, InitX, InitHangarX1, InitLowY,InitHangarRow1 + 30), new ColorModifier(3, 1, 1, 1, 0, 0.75f,0.5f));
		Hanger1Part1Mod = new ParallelEntityModifier(new ScaleModifier(3, 1,0.2f), new MoveModifier(3, InitX, InitHangarCapX1, InitCapY,InitHangarRow1+10), new ColorModifier(3, 1, 1, 1, 0.5f, 0.75f, 0.5f));

		Hanger2Part0Mod = new ParallelEntityModifier(new ScaleModifier(3, 1,0.2f), new MoveModifier(3, InitX, InitHangarX2, InitLowY,InitHangarRow1 + 30), new ColorModifier(3, 1, 1, 1, 0, 0.75f,0.5f));
		Hanger2Part1Mod = new ParallelEntityModifier(new ScaleModifier(3, 1,0.2f), new MoveModifier(3, InitX, InitHangarCapX2, InitCapY,InitHangarRow1+10), new ColorModifier(3, 1, 1, 1, 0.5f, 0.75f, 0.5f));

		Hanger3Part0Mod = new ParallelEntityModifier(new ScaleModifier(3, 1,0.2f), new MoveModifier(3, InitX, InitHangarX0, InitLowY,InitHangarRow2 + 30), new ColorModifier(3, 1, 1, 1, 0, 0.75f,0.5f));
		Hanger3Part1Mod = new ParallelEntityModifier(new ScaleModifier(3, 1,0.2f), new MoveModifier(3, InitX, InitHangarCapX0, InitCapY,InitHangarRow2+10), new ColorModifier(3, 1, 1, 1, 0.5f, 0.75f, 0.5f));

		Hanger4Part0Mod = new ParallelEntityModifier(new ScaleModifier(3, 1,0.2f), new MoveModifier(3, InitX, InitHangarX1, InitLowY,InitHangarRow2 + 30), new ColorModifier(3, 1, 1, 1, 0, 0.75f,0.5f));
		Hanger4Part1Mod = new ParallelEntityModifier(new ScaleModifier(3, 1,0.2f), new MoveModifier(3, InitX, InitHangarCapX1, InitCapY,InitHangarRow2+10), new ColorModifier(3, 1, 1, 1, 0.5f, 0.75f, 0.5f));

		Hanger5Part0Mod = new ParallelEntityModifier(new ScaleModifier(3, 1,0.2f), new MoveModifier(3, InitX, InitHangarX2, InitLowY,InitHangarRow2 + 30), new ColorModifier(3, 1, 1, 1, 0, 0.75f,0.5f));
		Hanger5Part1Mod = new ParallelEntityModifier(new ScaleModifier(3, 1,0.2f), new MoveModifier(3, InitX, InitHangarCapX2, InitCapY,InitHangarRow2+10), new ColorModifier(3, 1, 1, 1, 0.5f, 0.75f, 0.5f));

		attachChild(house1);
		attachChild(roof);
		attachChild(showmenu);
		attachChild(canvas);
		attachChild(background);
		attachChild(buildSupernka);
		attachChild(hangar0);
		attachChild(hangar1);
		attachChild(hangar2);
		attachChild(hangar3);
		attachChild(hangar4);
		attachChild(hangar5);
		attachChild(Letter_a);
		attachChild(Letter_a1);
		attachChild(Letter_a2);
		attachChild(Letter_a3);
		attachChild(Letter_A);
		attachChild(Letter_A1);
		attachChild(Letter_A2);
		attachChild(Letter_A3);
		attachChild(Letter_b);
		attachChild(Letter_b1);
		attachChild(Letter_b2);
		attachChild(Letter_b3);
		attachChild(Letter_B);
		attachChild(Letter_B1);
		attachChild(Letter_B2);
		attachChild(Letter_B3);
		attachChild(Letter_c);
		attachChild(Letter_c1);
		attachChild(Letter_c2);
		attachChild(Letter_c3);
		attachChild(Letter_C);
		attachChild(Letter_C1);
		attachChild(Letter_C2);
		attachChild(Letter_C3);
		attachChild(Letter_d);
		attachChild(Letter_d1);
		attachChild(Letter_d2);
		attachChild(Letter_d3);
		attachChild(Letter_D);
		attachChild(Letter_D1);
		attachChild(Letter_D2);
		attachChild(Letter_D3);
		attachChild(Letter_e);
		attachChild(Letter_e1);
		attachChild(Letter_e2);
		attachChild(Letter_e3);
		attachChild(Letter_E);
		attachChild(Letter_E1);
		attachChild(Letter_E2);
		attachChild(Letter_E3);
		attachChild(Letter_f);
		attachChild(Letter_f1);
		attachChild(Letter_f2);
		attachChild(Letter_f3);
		attachChild(Letter_F);
		attachChild(Letter_F1);
		attachChild(Letter_F2);
		attachChild(Letter_F3);
		attachChild(Letter_g);
		attachChild(Letter_g1);
		attachChild(Letter_g2);
		attachChild(Letter_g3);
		attachChild(Letter_G);
		attachChild(Letter_G1);
		attachChild(Letter_G2);
		attachChild(Letter_G3);
		attachChild(Letter_h);
		attachChild(Letter_h1);
		attachChild(Letter_h2);
		attachChild(Letter_h3);
		attachChild(Letter_H);
		attachChild(Letter_H1);
		attachChild(Letter_H2);
		attachChild(Letter_H3);
		attachChild(Letter_k);
		attachChild(Letter_k1);
		attachChild(Letter_k2);
		attachChild(Letter_k3);
		attachChild(Letter_K);
		attachChild(Letter_K1);
		attachChild(Letter_K2);
		attachChild(Letter_K3);
		attachChild(Letter_l);
		attachChild(Letter_l1);
		attachChild(Letter_l2);
		attachChild(Letter_l3);
		attachChild(Letter_L);
		attachChild(Letter_L1);
		attachChild(Letter_L2);
		attachChild(Letter_L3);
		attachChild(Letter_m);
		attachChild(Letter_m1);
		attachChild(Letter_m2);
		attachChild(Letter_m3);
		attachChild(Letter_M);
		attachChild(Letter_M1);
		attachChild(Letter_M2);
		attachChild(Letter_M3);
		attachChild(Letter_n);
		attachChild(Letter_n1);
		attachChild(Letter_n2);
		attachChild(Letter_n3);
		attachChild(Letter_N);
		attachChild(Letter_N1);
		attachChild(Letter_N2);
		attachChild(Letter_N3);
		attachChild(Letter_o);
		attachChild(Letter_o1);
		attachChild(Letter_o2);
		attachChild(Letter_o3);
		attachChild(Letter_O);
		attachChild(Letter_O1);
		attachChild(Letter_O2);
		attachChild(Letter_O3);
		attachChild(Letter_r);
		attachChild(Letter_r1);
		attachChild(Letter_r2);
		attachChild(Letter_r3);
		attachChild(Letter_R);
		attachChild(Letter_R1);
		attachChild(Letter_R2);
		attachChild(Letter_R3);
		attachChild(Letter_s);
		attachChild(Letter_s1);
		attachChild(Letter_s2);
		attachChild(Letter_s3);
		attachChild(Letter_S);
		attachChild(Letter_S1);
		attachChild(Letter_S2);
		attachChild(Letter_S3);

		attachChild(Letter_u);
		attachChild(Letter_u1);
		attachChild(Letter_u2);
		attachChild(Letter_u3);
		attachChild(Letter_U);
		attachChild(Letter_U1);
		attachChild(Letter_U2);
		attachChild(Letter_U3);
		attachChild(Letter_v);

		attachChild(Letter_v1);
		attachChild(Letter_v2);
		attachChild(Letter_v3);
		attachChild(Letter_V);
		attachChild(Letter_V1);
		attachChild(Letter_V2);
		attachChild(Letter_V3);
		attachChild(Letter_w);
		attachChild(Letter_w1);
		attachChild(Letter_w2);
		attachChild(Letter_w3);
		attachChild(Letter_W);
		attachChild(Letter_W1);

		attachChild(Letter_W2);
		attachChild(Letter_W3);
		attachChild(Letter_z);
		attachChild(Letter_z1);
		attachChild(Letter_z2);
		attachChild(Letter_z3);
		attachChild(Letter_Z);
		attachChild(Letter_Z1);
		attachChild(Letter_Z2);
		attachChild(Letter_Z3);

	}

	private void cleanScene() {

		this.roof.setScale(1);
		this.house1.setScale(0);
		this.buildSupernka.setScale(0);
		this.unregisterUpdateHandler(postLetter);
		this.isVictoryUp = false;
		isDemoRunning = false;

		this.is_a_PartUp1 = false;
		this.is_a_PartUp2 = false;
		this.is_a_PartUp3 = false;

		this.is_A_PartUp1 = false;
		this.is_A_PartUp2 = false;
		this.is_A_PartUp3 = false;

		this.is_b_PartUp1 = false;
		this.is_b_PartUp2 = false;
		this.is_b_PartUp3 = false;

		this.is_B_PartUp1 = false;
		this.is_B_PartUp2 = false;
		this.is_B_PartUp3 = false;

		this.is_c_PartUp1 = false;
		this.is_c_PartUp2 = false;
		this.is_c_PartUp3 = false;

		this.is_C_PartUp1 = false;
		this.is_C_PartUp2 = false;
		this.is_C_PartUp3 = false;

		this.is_d_PartUp1 = false;
		this.is_d_PartUp2 = false;
		this.is_d_PartUp3 = false;

		this.is_D_PartUp1 = false;
		this.is_D_PartUp2 = false;
		this.is_D_PartUp3 = false;

		this.is_e_PartUp1 = false;
		this.is_e_PartUp2 = false;
		this.is_e_PartUp3 = false;

		this.is_E_PartUp1 = false;
		this.is_E_PartUp2 = false;
		this.is_E_PartUp3 = false;

		this.is_f_PartUp1 = false;
		this.is_f_PartUp2 = false;
		this.is_f_PartUp3 = false;

		this.is_F_PartUp1 = false;
		this.is_F_PartUp2 = false;
		this.is_F_PartUp3 = false;

		this.is_g_PartUp1 = false;
		this.is_g_PartUp2 = false;
		this.is_g_PartUp3 = false;

		this.is_G_PartUp1 = false;
		this.is_G_PartUp2 = false;
		this.is_G_PartUp3 = false;

		this.is_h_PartUp1 = false;
		this.is_h_PartUp2 = false;
		this.is_h_PartUp3 = false;

		this.is_H_PartUp1 = false;
		this.is_H_PartUp2 = false;
		this.is_H_PartUp3 = false;

		this.is_k_PartUp1 = false;
		this.is_k_PartUp2 = false;
		this.is_k_PartUp3 = false;

		this.is_K_PartUp1 = false;
		this.is_K_PartUp2 = false;
		this.is_K_PartUp3 = false;

		this.is_l_PartUp1 = false;
		this.is_l_PartUp2 = false;
		this.is_l_PartUp3 = false;

		this.is_L_PartUp1 = false;
		this.is_L_PartUp2 = false;
		this.is_L_PartUp3 = false;

		this.is_m_PartUp1 = false;
		this.is_m_PartUp2 = false;
		this.is_m_PartUp3 = false;

		this.is_M_PartUp1 = false;
		this.is_M_PartUp2 = false;
		this.is_M_PartUp3 = false;

		this.is_n_PartUp1 = false;
		this.is_n_PartUp2 = false;
		this.is_n_PartUp3 = false;

		this.is_N_PartUp1 = false;
		this.is_N_PartUp2 = false;
		this.is_N_PartUp3 = false;

		this.is_o_PartUp1 = false;
		this.is_o_PartUp2 = false;
		this.is_o_PartUp3 = false;

		this.is_O_PartUp1 = false;
		this.is_O_PartUp2 = false;
		this.is_O_PartUp3 = false;


		this.is_r_PartUp1 = false;
		this.is_r_PartUp2 = false;
		this.is_r_PartUp3 = false;

		this.is_R_PartUp1 = false;
		this.is_R_PartUp2 = false;
		this.is_R_PartUp3 = false;

		this.is_s_PartUp1 = false;
		this.is_s_PartUp2 = false;
		this.is_s_PartUp3 = false;

		this.is_S_PartUp1 = false;
		this.is_S_PartUp2 = false;
		this.is_S_PartUp3 = false;


		this.is_u_PartUp1 = false;
		this.is_u_PartUp2 = false;
		this.is_u_PartUp3 = false;

		this.is_U_PartUp1 = false;
		this.is_U_PartUp2 = false;
		this.is_U_PartUp3 = false;

		this.is_v_PartUp1 = false;
		this.is_v_PartUp2 = false;
		this.is_v_PartUp3 = false;

		this.is_V_PartUp1 = false;
		this.is_V_PartUp2 = false;
		this.is_V_PartUp3 = false;

		this.is_w_PartUp1 = false;
		this.is_w_PartUp2 = false;
		this.is_w_PartUp3 = false;

		this.is_W_PartUp1 = false;
		this.is_W_PartUp2 = false;
		this.is_W_PartUp3 = false;

		this.is_z_PartUp1 = false;
		this.is_z_PartUp2 = false;
		this.is_z_PartUp3 = false;

		this.is_Z_PartUp1 = false;
		this.is_Z_PartUp2 = false;
		this.is_Z_PartUp3 = false;

		isHangarFree0 = true;
		isHangarFree1 = true;
		isHangarFree2 = true;
		isHangarFree3 = true;
		isHangarFree4 = true;
		isHangarFree5 = true;

		isHanger0Part0Used = false;
		isHanger0Part1Used = false;

		isHanger1Part0Used = false;
		isHanger1Part1Used = false;

		isHanger2Part0Used = false;
		isHanger2Part1Used = false;

		isHanger3Part0Used = false;
		isHanger3Part1Used = false;

		isHanger4Part0Used = false;
		isHanger4Part1Used = false;

		isHanger5Part0Used = false;
		isHanger5Part1Used = false;

		Hangar0Ack = false;
		Hangar1Ack = false;
		Hangar2Ack = false;
		Hangar3Ack = false;
		Hangar4Ack = false;
		Hangar5Ack = false;

		this.unregisterEntityModifier(Hanger0Part0Mod);
		this.unregisterEntityModifier(Hanger0Part0Mod);

		this.unregisterEntityModifier(Hanger2Part0Mod);
		this.unregisterEntityModifier(Hanger2Part0Mod);

		this.unregisterEntityModifier(Hanger3Part0Mod);
		this.unregisterEntityModifier(Hanger3Part0Mod);

		this.unregisterEntityModifier(Hanger4Part0Mod);
		this.unregisterEntityModifier(Hanger4Part0Mod);

		this.unregisterEntityModifier(Hanger5Part0Mod);
		this.unregisterEntityModifier(Hanger5Part1Mod);
		this.clearTouchAreas();

		detachChild(house1);
		detachChild(roof);
		detachChild(showmenu);
		detachChild(canvas);
		detachChild(background);
		detachChild(buildSupernka);

		detachChild(hangar0);
		detachChild(hangar1);
		detachChild(hangar2);
		detachChild(hangar3);
		detachChild(hangar4);
		detachChild(hangar5);

		detachChild(Letter_a);
		detachChild(Letter_a1);
		detachChild(Letter_a2);
		detachChild(Letter_a3);
		detachChild(Letter_A);
		detachChild(Letter_A1);
		detachChild(Letter_A2);
		detachChild(Letter_A3);

		detachChild(Letter_b);
		detachChild(Letter_b1);
		detachChild(Letter_b2);
		detachChild(Letter_b3);
		detachChild(Letter_B);
		detachChild(Letter_B1);
		detachChild(Letter_B2);
		detachChild(Letter_B3);

		detachChild(Letter_c);
		detachChild(Letter_c1);
		detachChild(Letter_c2);
		detachChild(Letter_c3);
		detachChild(Letter_C);
		detachChild(Letter_C1);
		detachChild(Letter_C2);
		detachChild(Letter_C3);

		detachChild(Letter_d);
		detachChild(Letter_d1);
		detachChild(Letter_d2);
		detachChild(Letter_d3);
		detachChild(Letter_D);
		detachChild(Letter_D1);
		detachChild(Letter_D2);
		detachChild(Letter_D3);

		detachChild(Letter_e);
		detachChild(Letter_e1);
		detachChild(Letter_e2);
		detachChild(Letter_e3);
		detachChild(Letter_E);
		detachChild(Letter_E1);
		detachChild(Letter_E2);
		detachChild(Letter_E3);

		detachChild(Letter_f);
		detachChild(Letter_f1);
		detachChild(Letter_f2);
		detachChild(Letter_f3);
		detachChild(Letter_F);
		detachChild(Letter_F1);
		detachChild(Letter_F2);
		detachChild(Letter_F3);

		detachChild(Letter_g);
		detachChild(Letter_g1);
		detachChild(Letter_g2);
		detachChild(Letter_g3);
		detachChild(Letter_G);
		detachChild(Letter_G1);
		detachChild(Letter_G2);
		detachChild(Letter_G3);

		detachChild(Letter_h);
		detachChild(Letter_h1);
		detachChild(Letter_h2);
		detachChild(Letter_h3);
		detachChild(Letter_H);
		detachChild(Letter_H1);
		detachChild(Letter_H2);
		detachChild(Letter_H3);

		detachChild(Letter_k);
		detachChild(Letter_k1);
		detachChild(Letter_k2);
		detachChild(Letter_k3);
		detachChild(Letter_K);
		detachChild(Letter_K1);
		detachChild(Letter_K2);
		detachChild(Letter_K3);

		detachChild(Letter_l);
		detachChild(Letter_l1);
		detachChild(Letter_l2);
		detachChild(Letter_l3);
		detachChild(Letter_L);
		detachChild(Letter_L1);
		detachChild(Letter_L2);
		detachChild(Letter_L3);

		detachChild(Letter_m);
		detachChild(Letter_m1);
		detachChild(Letter_m2);
		detachChild(Letter_m3);
		detachChild(Letter_M);
		detachChild(Letter_M1);
		detachChild(Letter_M2);
		detachChild(Letter_M3);

		detachChild(Letter_n);
		detachChild(Letter_n1);
		detachChild(Letter_n2);
		detachChild(Letter_n3);
		detachChild(Letter_N);
		detachChild(Letter_N1);
		detachChild(Letter_N2);
		detachChild(Letter_N3);

		detachChild(Letter_o);
		detachChild(Letter_o1);
		detachChild(Letter_o2);
		detachChild(Letter_o3);
		detachChild(Letter_O);
		detachChild(Letter_O1);
		detachChild(Letter_O2);
		detachChild(Letter_O3);

		detachChild(Letter_r);
		detachChild(Letter_r1);
		detachChild(Letter_r2);
		detachChild(Letter_r3);
		detachChild(Letter_R);
		detachChild(Letter_R1);
		detachChild(Letter_R2);
		detachChild(Letter_R3);

		detachChild(Letter_s);
		detachChild(Letter_s1);
		detachChild(Letter_s2);
		detachChild(Letter_s3);
		detachChild(Letter_S);
		detachChild(Letter_S1);
		detachChild(Letter_S2);
		detachChild(Letter_S3);

		detachChild(Letter_u);
		detachChild(Letter_u1);
		detachChild(Letter_u2);
		detachChild(Letter_u3);
		detachChild(Letter_U);
		detachChild(Letter_U1);
		detachChild(Letter_U2);
		detachChild(Letter_U3);

		detachChild(Letter_v);
		detachChild(Letter_v1);
		detachChild(Letter_v2);
		detachChild(Letter_v3);
		detachChild(Letter_V);
		detachChild(Letter_V1);
		detachChild(Letter_V2);
		detachChild(Letter_V3);

		detachChild(Letter_w);
		detachChild(Letter_w1);
		detachChild(Letter_w2);
		detachChild(Letter_w3);
		detachChild(Letter_W);
		detachChild(Letter_W1);
		detachChild(Letter_W2);
		detachChild(Letter_W3);

		detachChild(Letter_z);
		detachChild(Letter_z1);
		detachChild(Letter_z2);
		detachChild(Letter_z3);
		detachChild(Letter_Z);
		detachChild(Letter_Z1);
		detachChild(Letter_Z2);
		detachChild(Letter_Z3);
		this.unregisterUpdateHandler(waitForMenu);
		this.clearEntityModifiers();

	}

	private void checkLetterOnComplete(Sprite letter) {

		if (letter.equals(Letter_a)) {

			if (is_a_PartUp1 && is_a_PartUp2 && is_a_PartUp3) {

				Letter_a.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_a1.setScale(0);
				Letter_a2.setScale(0);
				Letter_a3.setScale(0);

				hangLetter(Letter_a, false);

			}

		}
		else if (letter.equals(Letter_A)) {

			if (is_A_PartUp1 && is_A_PartUp2 && is_A_PartUp3) {

				Letter_A.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_A1.setScale(0);
				Letter_A2.setScale(0);
				Letter_A3.setScale(0);

				hangLetter(Letter_A, true);
			}

		}
		else if (letter.equals(Letter_b)) {

			if (is_b_PartUp1 && is_b_PartUp2 && is_b_PartUp3) {

				Letter_b.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_b1.setScale(0);
				Letter_b2.setScale(0);
				Letter_b3.setScale(0);

				hangLetter(Letter_b, false);

			}

		}
		else if (letter.equals(Letter_B)) {

			if (is_B_PartUp1 && is_B_PartUp2 && is_B_PartUp3) {

				Letter_B.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_B1.setScale(0);
				Letter_B2.setScale(0);
				Letter_B3.setScale(0);

				hangLetter(Letter_B, true);
			}
		} 
		else if (letter.equals(Letter_c)) {

			if (is_c_PartUp1 && is_c_PartUp2 && is_c_PartUp3) {

				Letter_c.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_c1.setScale(0);
				Letter_c2.setScale(0);
				Letter_c3.setScale(0);

				hangLetter(Letter_c, false);

			}

		} 
		else if (letter.equals(Letter_C)) {

			if (is_C_PartUp1 && is_C_PartUp2 && is_C_PartUp3) {

				Letter_C.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_C1.setScale(0);
				Letter_C2.setScale(0);
				Letter_C3.setScale(0);

				hangLetter(Letter_C, true);
			}
		}
		else if (letter.equals(Letter_d)) {

			if (is_d_PartUp1 && is_d_PartUp2 && is_d_PartUp3) {

				Letter_d.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_d1.setScale(0);
				Letter_d2.setScale(0);
				Letter_d3.setScale(0);

				hangLetter(Letter_d, false);
			}
		} 
		else if (letter.equals(Letter_D)) {

			if (is_D_PartUp1 && is_D_PartUp2 && is_D_PartUp3) {

				Letter_D.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_D1.setScale(0);
				Letter_D2.setScale(0);
				Letter_D3.setScale(0);

				hangLetter(Letter_D, true);
			}
		}
		else if (letter.equals(Letter_e)) {

			if (is_e_PartUp1 && is_e_PartUp2 && is_e_PartUp3) {

				Letter_e.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_e1.setScale(0);
				Letter_e2.setScale(0);
				Letter_e3.setScale(0);

				hangLetter(Letter_e, false);
			}
		}
		else if (letter.equals(Letter_E)) {

			if (is_E_PartUp1 && is_E_PartUp2 && is_E_PartUp3) {

				Letter_E.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_E1.setScale(0);
				Letter_E2.setScale(0);
				Letter_E3.setScale(0);

				hangLetter(Letter_E, true);
			}
		} 
		else if (letter.equals(Letter_f)) {

			if (is_f_PartUp1 && is_f_PartUp2 && is_f_PartUp3) {

				Letter_f.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_f1.setScale(0);
				Letter_f2.setScale(0);
				Letter_f3.setScale(0);

				hangLetter(Letter_f, false);
			}
		} 
		else if (letter.equals(Letter_F)) {

			if (is_F_PartUp1 && is_F_PartUp2 && is_F_PartUp3) {

				Letter_F.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_F1.setScale(0);
				Letter_F2.setScale(0);
				Letter_F3.setScale(0);

				hangLetter(Letter_F, true);
			}
		}
		else if (letter.equals(Letter_g)) {

			if (is_g_PartUp1 && is_g_PartUp2 && is_g_PartUp3) {

				Letter_g.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_g1.setScale(0);
				Letter_g2.setScale(0);
				Letter_g3.setScale(0);

				hangLetter(Letter_g, false);
			}
		}
		else if (letter.equals(Letter_G)) {

			if (is_G_PartUp1 && is_G_PartUp2 && is_G_PartUp3) {

				Letter_G.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_G1.setScale(0);
				Letter_G2.setScale(0);
				Letter_G3.setScale(0);

				hangLetter(Letter_G, true);
			}
		} 
		else if (letter.equals(Letter_h)) {

			if (is_h_PartUp1 && is_h_PartUp2 && is_h_PartUp3) {

				Letter_h.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_h1.setScale(0);
				Letter_h2.setScale(0);
				Letter_h3.setScale(0);

				hangLetter(Letter_h, false);
			}
		} 
		else if (letter.equals(Letter_H)) {

			if (is_H_PartUp1 && is_H_PartUp2 && is_H_PartUp3) {

				Letter_H.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_H1.setScale(0);
				Letter_H2.setScale(0);
				Letter_H3.setScale(0);

				hangLetter(Letter_H, true);
			}
		}
		else if (letter.equals(Letter_k)) {

			if (is_k_PartUp1 && is_k_PartUp2 && is_k_PartUp3) {

				Letter_k.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_k1.setScale(0);
				Letter_k2.setScale(0);
				Letter_k3.setScale(0);

				hangLetter(Letter_k, false);
			}
		}
		else if (letter.equals(Letter_K)) {

			if (is_K_PartUp1 && is_K_PartUp2 && is_K_PartUp3) {

				Letter_K.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_K1.setScale(0);
				Letter_K2.setScale(0);
				Letter_K3.setScale(0);

				hangLetter(Letter_K, true);
			}
		}
		else if (letter.equals(Letter_l)) {

			if (is_l_PartUp1 && is_l_PartUp2 && is_l_PartUp3) {

				Letter_l.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_l1.setScale(0);
				Letter_l2.setScale(0);
				Letter_l3.setScale(0);

				hangLetter(Letter_l, false);
			}
		} 
		else if (letter.equals(Letter_L)) {

			if (is_L_PartUp1 && is_L_PartUp2 && is_L_PartUp3) {

				Letter_L.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_L1.setScale(0);
				Letter_L2.setScale(0);
				Letter_L3.setScale(0);

				hangLetter(Letter_L, true);
			}
		} else if (letter.equals(Letter_m)) {

			if (is_m_PartUp1 && is_m_PartUp2 && is_m_PartUp3) {

				Letter_m.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_m1.setScale(0);
				Letter_m2.setScale(0);
				Letter_m3.setScale(0);

				hangLetter(Letter_m, false);
			}
		}
		else if (letter.equals(Letter_M)) {

			if (is_M_PartUp1 && is_M_PartUp2 && is_M_PartUp3) {

				Letter_M.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_M1.setScale(0);
				Letter_M2.setScale(0);
				Letter_M3.setScale(0);

				hangLetter(Letter_M, true);
			}
		}

		else if (letter.equals(Letter_n)) {

			if (is_n_PartUp1 && is_n_PartUp2 && is_n_PartUp3) {

				Letter_n.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_n1.setScale(0);
				Letter_n2.setScale(0);
				Letter_n3.setScale(0);

				hangLetter(Letter_n, false);
			}
		}
		else if (letter.equals(Letter_N)) {

			if (is_N_PartUp1 && is_N_PartUp2 && is_N_PartUp3) {

				Letter_N.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_N1.setScale(0);
				Letter_N2.setScale(0);
				Letter_N3.setScale(0);

				hangLetter(Letter_N, true);
			}
		}

		else if (letter.equals(Letter_o)) {

			if (is_o_PartUp1 && is_o_PartUp2 && is_o_PartUp3) {

				Letter_o.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_o1.setScale(0);
				Letter_o2.setScale(0);
				Letter_o3.setScale(0);

				hangLetter(Letter_o, false);
			}
		}
		else if (letter.equals(Letter_O)) {

			if (is_O_PartUp1 && is_O_PartUp2 && is_O_PartUp3) {

				Letter_O.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_O1.setScale(0);
				Letter_O2.setScale(0);
				Letter_O3.setScale(0);

				hangLetter(Letter_O, true);
			}
		}
		else if (letter.equals(Letter_r)) {

			if (is_r_PartUp1 && is_r_PartUp2 && is_r_PartUp3) {

				Letter_r.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_r1.setScale(0);
				Letter_r2.setScale(0);
				Letter_r3.setScale(0);

				hangLetter(Letter_r, false);
			}
		} 
		else if (letter.equals(Letter_R)) {

			if (is_R_PartUp1 && is_R_PartUp2 && is_R_PartUp3) {

				Letter_R.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_R1.setScale(0);
				Letter_R2.setScale(0);
				Letter_R3.setScale(0);

				hangLetter(Letter_R, true);
			}
		} 
		else if (letter.equals(Letter_s)) {

			if (is_s_PartUp1 && is_s_PartUp2 && is_s_PartUp3) {

				Letter_s.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_s1.setScale(0);
				Letter_s2.setScale(0);
				Letter_s3.setScale(0);

				hangLetter(Letter_s, false);
			}
		} else if (letter.equals(Letter_S)) {

			if (is_S_PartUp1 && is_S_PartUp2 && is_S_PartUp3) {

				Letter_S.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_S1.setScale(0);
				Letter_S2.setScale(0);
				Letter_S3.setScale(0);

				hangLetter(Letter_S, true);
			}
		} 
		else if (letter.equals(Letter_u)) {

			if (is_u_PartUp1 && is_u_PartUp2 && is_u_PartUp3) {

				Letter_u.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_u1.setScale(0);
				Letter_u2.setScale(0);
				Letter_u3.setScale(0);

				hangLetter(Letter_u, false);
			}
		} 
		else if (letter.equals(Letter_U)) {

			if (is_U_PartUp1 && is_U_PartUp2 && is_U_PartUp3) {

				Letter_U.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_U1.setScale(0);
				Letter_U2.setScale(0);
				Letter_U3.setScale(0);

				hangLetter(Letter_U, true);
			}
		} 
		else if (letter.equals(Letter_v)) {

			if (is_v_PartUp1 && is_v_PartUp2 && is_v_PartUp3) {

				Letter_v.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_v1.setScale(0);
				Letter_v2.setScale(0);
				Letter_v3.setScale(0);

				hangLetter(Letter_v, false);
			}
		} 
		else if (letter.equals(Letter_V)) {

			if (is_V_PartUp1 && is_V_PartUp2 && is_V_PartUp3) {

				Letter_V.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_V1.setScale(0);
				Letter_V2.setScale(0);
				Letter_V3.setScale(0);

				hangLetter(Letter_V, true);
			}
		} 
		else if (letter.equals(Letter_w)) {

			if (is_w_PartUp1 && is_w_PartUp2 && is_w_PartUp3) {

				Letter_w.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_w1.setScale(0);
				Letter_w2.setScale(0);
				Letter_w3.setScale(0);

				hangLetter(Letter_w, false);
			}
		} else if (letter.equals(Letter_W)) {

			if (is_W_PartUp1 && is_W_PartUp2 && is_W_PartUp3) {

				Letter_W.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_W1.setScale(0);
				Letter_W2.setScale(0);
				Letter_W3.setScale(0);

				hangLetter(Letter_W, true);
			}
		} 
		else if (letter.equals(Letter_z)) {

			if (is_z_PartUp1 && is_z_PartUp2 && is_z_PartUp3) {

				Letter_z.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_z1.setScale(0);
				Letter_z2.setScale(0);
				Letter_z3.setScale(0);
				hangLetter(Letter_z, false);
			}
		} else if (letter.equals(Letter_Z)) {

			if (is_Z_PartUp1 && is_Z_PartUp2 && is_Z_PartUp3) {

				Letter_Z.setColor(1, 0.75f, 0.5f, 0.9f);

				Letter_Z1.setScale(0);
				Letter_Z2.setScale(0);
				Letter_Z3.setScale(0);
				hangLetter(Letter_Z, true);
			}
		}

		updateHouse();
	}

	private void updateHouse() {

		TimerHandler updateHouse = new TimerHandler(0.1f, true,
				new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
				if (Hanger0Part0Mod.isFinished()
						&& Hanger0Part1Mod.isFinished()) {
					hangar0.setColor(0.5f, 0.5f, 0.5f, 1);
					if (!Hangar0Ack) {
						SceneManager.getInstance().playAckSound();
						Hangar0Ack = true;
					}
				}

				if (Hanger1Part0Mod.isFinished()
						&& Hanger1Part1Mod.isFinished()) {
					hangar1.setColor(0.5f, 0.5f, 0.5f, 1);
					if (!Hangar1Ack) {
						SceneManager.getInstance().playAckSound();
						Hangar1Ack = true;
					}
				}

				if (Hanger2Part0Mod.isFinished()
						&& Hanger2Part1Mod.isFinished()) {
					hangar2.setColor(0.5f, 0.5f, 0.5f, 1);
					if (!Hangar2Ack) {
						SceneManager.getInstance().playAckSound();
						Hangar2Ack = true;
					}
				}

				if (Hanger3Part0Mod.isFinished()
						&& Hanger3Part1Mod.isFinished()) {
					hangar3.setColor(0.5f, 0.5f, 0.5f, 1);
					if (!Hangar3Ack) {
						SceneManager.getInstance().playAckSound();
						Hangar3Ack = true;
					}
				}
				if (Hanger4Part0Mod.isFinished()
						&& Hanger4Part1Mod.isFinished()) {
					hangar4.setColor(0.5f, 0.5f, 0.5f, 1);
					if (!Hangar4Ack) {
						SceneManager.getInstance().playAckSound();
						Hangar4Ack = true;
					}
				}
				if (Hanger5Part0Mod.isFinished()
						&& Hanger5Part1Mod.isFinished()) {
					hangar5.setColor(0.5f, 0.5f, 0.5f, 1);
					if (!Hangar5Ack) {
						SceneManager.getInstance().playAckSound();
						Hangar5Ack = true;
					}
				}
			}

		});
		registerUpdateHandler(updateHouse);
	}

	private void hangLetter(Sprite letter, boolean isCapital) {

		if (isHangarFree0) {

			if (!isCapital) {

				letter.registerEntityModifier(Hanger0Part0Mod);

				isHanger0Part0Used = true;
			} else {
				letter.registerEntityModifier(Hanger0Part1Mod);
				isHanger0Part1Used = true;
			}

			if (isHanger0Part0Used && isHanger0Part1Used) {
				isHangarFree0 = false;

				if (!isDemoRunning) // do not build next after demo is done
					buildLetter(randBuildLetterIndex1);
			}

		} else if (isHangarFree1) {

			if (!isCapital) {

				letter.registerEntityModifier(Hanger1Part0Mod);
				isHanger1Part0Used = true;
			} else {
				letter.registerEntityModifier(Hanger1Part1Mod);
				isHanger1Part1Used = true;
			}

			if (isHanger1Part0Used && isHanger1Part1Used) {
				isHangarFree1 = false;

				if (!isDemoRunning) // do not build next after demo is done
					buildLetter(randBuildLetterIndex2);
			}

		} else if (isHangarFree2) {

			if (!isCapital) {

				letter.registerEntityModifier(Hanger2Part0Mod);
				isHanger2Part0Used = true;
			} else {
				letter.registerEntityModifier(Hanger2Part1Mod);
				isHanger2Part1Used = true;
			}

			if (isHanger2Part0Used && isHanger2Part1Used) {
				isHangarFree2 = false;
				if (!isDemoRunning) // do not build next after demo is done
					buildLetter(randBuildLetterIndex3);
			}

		} else if (isHangarFree3) {
			if (!isCapital) {

				letter.registerEntityModifier(Hanger3Part0Mod);
				isHanger3Part0Used = true;
			} else {
				letter.registerEntityModifier(Hanger3Part1Mod);
				isHanger3Part1Used = true;
			}

			if (isHanger3Part0Used && isHanger3Part1Used) {
				isHangarFree3 = false;
				if (!isDemoRunning) // do not build next after demo is done
					buildLetter(randBuildLetterIndex4);
			}

		} else if (isHangarFree4) {
			if (!isCapital) {

				letter.registerEntityModifier(Hanger4Part0Mod);
				isHanger4Part0Used = true;
			} else {
				letter.registerEntityModifier(Hanger4Part1Mod);
				isHanger4Part1Used = true;
			}

			if (isHanger4Part0Used && isHanger4Part1Used) {
				isHangarFree4 = false;
				if (!isDemoRunning) // do not build next after demo is done
					buildLetter(randBuildLetterIndex5);
			}

		} else if (isHangarFree5) {
			if (!isCapital) {

				letter.registerEntityModifier(Hanger5Part0Mod);
				isHanger5Part0Used = true;
			} else {
				letter.registerEntityModifier(Hanger5Part1Mod);
				isHanger5Part1Used = true;
			}

			if (isHanger5Part0Used && isHanger5Part1Used) {
				StartVictorySequence(true);
			}
		}

	}

	private void buildLetter(final int nextLetter) {

		postLetter = new TimerHandler(1f, false, new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {

				switch (nextLetter) {
				case 0:
					Letter_a.setScale(1);
					Letter_a1.setScale(1);
					Letter_a2.setScale(1);
					Letter_a3.setScale(1);
					Letter_A.setScale(1);
					Letter_A1.setScale(1);
					Letter_A2.setScale(1);
					Letter_A3.setScale(1);
					SceneManager.getInstance().playLetterSound("a");
					break;
				case 1:
					Letter_b.setScale(1);
					Letter_b1.setScale(1);
					Letter_b2.setScale(1);
					Letter_b3.setScale(1);
					Letter_B.setScale(1);
					Letter_B1.setScale(1);
					Letter_B2.setScale(1);
					Letter_B3.setScale(1);
					SceneManager.getInstance().playLetterSound("b");
					break;
				case 2:
					Letter_c.setScale(1);
					Letter_c1.setScale(1);
					Letter_c2.setScale(1);
					Letter_c3.setScale(1);
					Letter_C.setScale(1);
					Letter_C1.setScale(1);
					Letter_C2.setScale(1);
					Letter_C3.setScale(1);
					SceneManager.getInstance().playLetterSound("c");
					break;

				case 3:
					Letter_d.setScale(1);
					Letter_d1.setScale(1);
					Letter_d2.setScale(1);
					Letter_d3.setScale(1);
					Letter_D.setScale(1);
					Letter_D1.setScale(1);
					Letter_D2.setScale(1);
					Letter_D3.setScale(1);
					SceneManager.getInstance().playLetterSound("d");

					break;
				case 4:
					Letter_e.setScale(1);
					Letter_e1.setScale(1);
					Letter_e2.setScale(1);
					Letter_e3.setScale(1);
					Letter_E.setScale(1);
					Letter_E1.setScale(1);
					Letter_E2.setScale(1);
					Letter_E3.setScale(1);
					SceneManager.getInstance().playLetterSound("e");

					break;
				case 5:
					Letter_f.setScale(1);
					Letter_f1.setScale(1);
					Letter_f2.setScale(1);
					Letter_f3.setScale(1);
					Letter_F.setScale(1);
					Letter_F1.setScale(1);
					Letter_F2.setScale(1);
					Letter_F3.setScale(1);
					SceneManager.getInstance().playLetterSound("f");

					break;
				case 6:
					Letter_g.setScale(1);
					Letter_g1.setScale(1);
					Letter_g2.setScale(1);
					Letter_g3.setScale(1);
					Letter_G.setScale(1);
					Letter_G1.setScale(1);
					Letter_G2.setScale(1);
					Letter_G3.setScale(1);
					SceneManager.getInstance().playLetterSound("g");

					break;
				case 7:
					Letter_h.setScale(1);
					Letter_h1.setScale(1);
					Letter_h2.setScale(1);
					Letter_h3.setScale(1);
					Letter_H.setScale(1);
					Letter_H1.setScale(1);
					Letter_H2.setScale(1);
					Letter_H3.setScale(1);
					SceneManager.getInstance().playLetterSound("h");

					break;

				case 8:
					Letter_k.setScale(1);
					Letter_k1.setScale(1);
					Letter_k2.setScale(1);
					Letter_k3.setScale(1);
					Letter_K.setScale(1);
					Letter_K1.setScale(1);
					Letter_K2.setScale(1);
					Letter_K3.setScale(1);
					SceneManager.getInstance().playLetterSound("k");

					break;
				case 9:
					Letter_l.setScale(1);
					Letter_l1.setScale(1);
					Letter_l2.setScale(1);
					Letter_l3.setScale(1);
					Letter_L.setScale(1);
					Letter_L1.setScale(1);
					Letter_L2.setScale(1);
					Letter_L3.setScale(1);
					SceneManager.getInstance().playLetterSound("l");

					break;
				case 10:
					Letter_m.setScale(1);
					Letter_m1.setScale(1);
					Letter_m2.setScale(1);
					Letter_m3.setScale(1);
					Letter_M.setScale(1);
					Letter_M1.setScale(1);
					Letter_M2.setScale(1);
					Letter_M3.setScale(1);
					SceneManager.getInstance().playLetterSound("m");

					break;
				case 11:
					Letter_n.setScale(1);
					Letter_n1.setScale(1);
					Letter_n2.setScale(1);
					Letter_n3.setScale(1);
					Letter_N.setScale(1);
					Letter_N1.setScale(1);
					Letter_N2.setScale(1);
					Letter_N3.setScale(1);
					SceneManager.getInstance().playLetterSound("n");

					break;
				case 12:
					Letter_o.setScale(1);
					Letter_o1.setScale(1);
					Letter_o2.setScale(1);
					Letter_o3.setScale(1);
					Letter_O.setScale(1);
					Letter_O1.setScale(1);
					Letter_O2.setScale(1);
					Letter_O3.setScale(1);
					SceneManager.getInstance().playLetterSound("o");

					break;

				case 13:
					Letter_r.setScale(1);
					Letter_r1.setScale(1);
					Letter_r2.setScale(1);
					Letter_r3.setScale(1);
					Letter_R.setScale(1);
					Letter_R1.setScale(1);
					Letter_R2.setScale(1);
					Letter_R3.setScale(1);
					SceneManager.getInstance().playLetterSound("r");

					break;
				case 14:
					Letter_s.setScale(1);
					Letter_s1.setScale(1);
					Letter_s2.setScale(1);
					Letter_s3.setScale(1);
					Letter_S.setScale(1);
					Letter_S1.setScale(1);
					Letter_S2.setScale(1);
					Letter_S3.setScale(1);
					SceneManager.getInstance().playLetterSound("s");

					break;

				case 15:

					Letter_u.setScale(1);
					Letter_u1.setScale(1);
					Letter_u2.setScale(1);
					Letter_u3.setScale(1);
					Letter_U.setScale(1);
					Letter_U1.setScale(1);
					Letter_U2.setScale(1);
					Letter_U3.setScale(1);
					SceneManager.getInstance().playLetterSound("u");

					break;
				case 16:
					Letter_v.setScale(1);
					Letter_v1.setScale(1);
					Letter_v2.setScale(1);
					Letter_v3.setScale(1);
					Letter_V.setScale(1);
					Letter_V1.setScale(1);
					Letter_V2.setScale(1);
					Letter_V3.setScale(1);
					SceneManager.getInstance().playLetterSound("v");

					break;

				case 17:
					Letter_w.setScale(1);
					Letter_w1.setScale(1);
					Letter_w2.setScale(1);
					Letter_w3.setScale(1);
					Letter_W.setScale(1);
					Letter_W1.setScale(1);
					Letter_W2.setScale(1);
					Letter_W3.setScale(1);
					SceneManager.getInstance().playLetterSound("w");

					break;

				case 18:
					Letter_z.setScale(1);
					Letter_z1.setScale(1);
					Letter_z2.setScale(1);
					Letter_z3.setScale(1);
					Letter_Z.setScale(1);
					Letter_Z1.setScale(1);
					Letter_Z2.setScale(1);
					Letter_Z3.setScale(1);
					SceneManager.getInstance().playLetterSound("z");

					break;

				default:

					Letter_a.setScale(0);
					Letter_A.setScale(0);
					Letter_b.setScale(0);
					Letter_B.setScale(0);
					Letter_c.setScale(0);
					Letter_C.setScale(0);
					Letter_d.setScale(0);
					Letter_D.setScale(0);
					Letter_e.setScale(0);
					Letter_E.setScale(0);
					Letter_f.setScale(0);
					Letter_F.setScale(0);
					Letter_g.setScale(0);
					Letter_G.setScale(0);
					Letter_h.setScale(0);
					Letter_H.setScale(0);
					Letter_k.setScale(0);
					Letter_K.setScale(0);
					Letter_l.setScale(0);
					Letter_L.setScale(0);
					Letter_m.setScale(0);
					Letter_M.setScale(0);
					Letter_n.setScale(0);
					Letter_N.setScale(0);
					Letter_o.setScale(0);
					Letter_O.setScale(0);
					Letter_r.setScale(0);
					Letter_R.setScale(0);
					Letter_s.setScale(0);
					Letter_S.setScale(0);
					Letter_u.setScale(0);
					Letter_U.setScale(0);
					Letter_v.setScale(0);
					Letter_V.setScale(0);
					Letter_w.setScale(0);
					Letter_W.setScale(0);	
					Letter_z.setScale(0);
					Letter_Z.setScale(0);
					break;
				}
			}

		});
		registerUpdateHandler(postLetter);

	}

	private void AnimateBuildSuperNka(int stage,final float posX, final float posY) {

		buildSupernka.setPosition(posX, posY);
		buildSupernka.setScale(1);
		if(stage==1){
			buildSupernka.animate(new long[] {100, 150, 250, 250, 250, 150, 200}, 0, 6, false);

		}
		else{
			buildSupernka.animate(new long[] { 100, 150, 250, 150, 250, 250, 200,350, 250,450}, 0, 9, false);

		}

		TimerHandler WaitForHummering = new TimerHandler(0.35f, false,new ITimerCallback() {

			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {

				SceneManager.getInstance().playHammerBuild();
			}

		});
		registerUpdateHandler(WaitForHummering);


	}


	private void StartVictorySequence(boolean action){

		if(action){
			hasVictorySequenceStarted=true;
			TimerHandler waitHammer0 = new TimerHandler(3.1f, false,new ITimerCallback() {
				@Override
				public void onTimePassed(TimerHandler pTimerHandler) {
					buildLetter(2000);
					buildSupernka.setScale(1.5f);
					AnimateBuildSuperNka(1,camera.getCenterX() - 60,camera.getHeight() - 250);
					isHangarFree5 = false;
					roof.setScale(0);
					hangar0.setScale(0);
					hangar1.setScale(0);
					hangar2.setScale(0);
					hangar3.setScale(0);
					hangar4.setScale(0);
					hangar5.setScale(0);
					house1.setScale(1);
					house1.registerEntityModifier(new AlphaModifier(3, 0, 1));

				}

			});
			registerUpdateHandler(waitHammer0);

			TimerHandler waitHammer1 = new TimerHandler(6f, false,new ITimerCallback() {
				@Override
				public void onTimePassed(TimerHandler pTimerHandler) {

					AnimateBuildSuperNka(2,camera.getCenterX()+110,camera.getHeight() - 260);
				}

			});
			registerUpdateHandler(waitHammer1);

			TimerHandler waitForVic = new TimerHandler(8f, false,new ITimerCallback() {
				@Override
				public void onTimePassed(TimerHandler pTimerHandler) {
					SceneManager.getInstance().playBuildVictorySound();
				}

			});
			registerUpdateHandler(waitForVic);

			TimerHandler waitForCongs = new TimerHandler(10f, false,new ITimerCallback() {
				@Override
				public void onTimePassed(TimerHandler pTimerHandler) {

					SceneManager.getInstance().playPaintCongratulorySound(true);
					isVictoryUp = true;
					hasVictorySequenceStarted=false;
					onVictory();

				}

			});
			registerUpdateHandler(waitForCongs);


		}
		else{

		}

	}
	private void onVictory() {

		waitForMenu = new TimerHandler(5f, true, new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
				if (isVictoryUp)
					showBuildMenu();
			}

		});
		registerUpdateHandler(waitForMenu);
	}

	private void delayOfficer(final int letterToBuild, final float delayTime) {

		TimerHandler delay = new TimerHandler(delayTime, false,
				new ITimerCallback() {

			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {

				buildLetter(letterToBuild);
			}

		});
		registerUpdateHandler(delay);
	}

	private void lockTouch() {
		this.unregisterTouchArea(Letter_a1);
		this.unregisterTouchArea(Letter_a2);
		this.unregisterTouchArea(Letter_a3);
		this.unregisterTouchArea(Letter_A1);
		this.unregisterTouchArea(Letter_A2);
		this.unregisterTouchArea(Letter_A3);

	}

	private void doDemo() {

		lockTouch();
		isDemoRunning = true;
		delayOfficer(0,3f);
		toPart0 = new MoveModifier(1, 0, this.InitCapX1, camera.getHeight(),this.InitCapY1);
		toPart1 = new MoveModifier(1, 0, this.InitCapX2, camera.getHeight(),this.InitCapY2);
		toPart2 = new MoveModifier(1, 0, this.InitCapX3, camera.getHeight(),this.InitCapY3);

		toPart3 = new MoveModifier(0.5f, 0, this.initLetterX1, camera.getHeight(),this.initLetterY1);
		toPart4 = new MoveModifier(0.5f, 0, this.initLetterX2, camera.getHeight(),this.initLetterY2);
		toPart5 = new MoveModifier(0.5f, 0, this.initLetterX3, camera.getHeight(),this.initLetterY3);

		toLetter0 = new MoveModifier(1, this.InitCapX1, this.InitX,this.InitCapY1, this.InitCapY);
		toLetter1 = new MoveModifier(1, this.InitCapX2, this.InitX+25,this.InitCapY2, this.InitCapY + 40);
		toLetter2 = new MoveModifier(1, this.InitCapX3, this.InitX + 20,this.InitCapY3, this.InitCapY + 100);

		toLetter3 = new MoveModifier(0.5f, this.initLetterX1, this.InitX+10 ,this.initLetterY1, this.InitLowY);
		toLetter4 = new MoveModifier(0.5f, this.initLetterX2, this.InitX+35,this.initLetterY2, this.InitLowY);
		toLetter5 = new MoveModifier(0.5f, this.initLetterX3, this.InitX+30,this.initLetterY3, this.InitLowY+30);

		DragPart0 = new MoveModifier(1, this.InitCapX1, this.InitX,this.InitCapY1, this.InitCapY);
		DragPart1 = new MoveModifier(1, this.InitCapX2, this.InitX+25,this.InitCapY2, this.InitCapY+40);
		DragPart2 = new MoveModifier(1, this.InitCapX3, this.InitX+20,this.InitCapY3, this.InitCapY+100);

		DragPart3 = new MoveModifier(0.5f, this.initLetterX1, this.InitX+10,this.initLetterY1, this.InitLowY);
		DragPart4 = new MoveModifier(0.5f, this.initLetterX2, this.InitX+35,this.initLetterY2, this.InitLowY );
		DragPart5 = new MoveModifier(0.5f, this.initLetterX3, this.InitX+30,this.initLetterY3, this.InitLowY+30);

		toPart0.setAutoUnregisterWhenFinished(true);
		toPart1.setAutoUnregisterWhenFinished(true);
		toPart2.setAutoUnregisterWhenFinished(true);
		toPart3.setAutoUnregisterWhenFinished(true);
		toPart4.setAutoUnregisterWhenFinished(true);
		toPart5.setAutoUnregisterWhenFinished(true);

		toLetter0.setAutoUnregisterWhenFinished(true);
		toLetter1.setAutoUnregisterWhenFinished(true);
		toLetter2.setAutoUnregisterWhenFinished(true);
		toLetter3.setAutoUnregisterWhenFinished(true);
		toLetter4.setAutoUnregisterWhenFinished(true);
		toLetter5.setAutoUnregisterWhenFinished(true);

		DragPart0.setAutoUnregisterWhenFinished(true);
		DragPart1.setAutoUnregisterWhenFinished(true);
		DragPart2.setAutoUnregisterWhenFinished(true);
		DragPart3.setAutoUnregisterWhenFinished(true);
		DragPart4.setAutoUnregisterWhenFinished(true);
		DragPart5.setAutoUnregisterWhenFinished(true);

		TimerHandler sniffer = new TimerHandler(5f, false,new ITimerCallback() {

			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {

				finger.setColor(1, 0.5f, 0.1f, 1);
				finger.registerEntityModifier(toPart0);

				TimerHandler moveToLetter0 = new TimerHandler(1.1f,false, new ITimerCallback() {

					@Override
					public void onTimePassed(TimerHandler pTimerHandler) {
						finger.registerEntityModifier(toLetter0);
						Letter_A1.registerEntityModifier(DragPart0);
					}

				});
				registerUpdateHandler(moveToLetter0);

				TimerHandler waitToColorPart0 = new TimerHandler(2.2f,false, new ITimerCallback() {
					@Override
					public void onTimePassed(TimerHandler pTimerHandler) {
						SceneManager.getInstance().playBuildOkSound();
						Letter_A1.setPosition(Letter_A.getX()+18.5f,Letter_A.getY());
						finger.registerEntityModifier(toPart1);
					}

				});
				registerUpdateHandler(waitToColorPart0);

				TimerHandler moveToLetter1 = new TimerHandler(3.1f,false, new ITimerCallback() {

					@Override
					public void onTimePassed(
							TimerHandler pTimerHandler) {
						finger.registerEntityModifier(toLetter1);
						Letter_A2.registerEntityModifier(DragPart1);
					}

				});
				registerUpdateHandler(moveToLetter1);

				TimerHandler waitToColorPart1 = new TimerHandler(4.2f,
						false, new ITimerCallback() {
					@Override
					public void onTimePassed(
							TimerHandler pTimerHandler) {
						SceneManager.getInstance().playBuildOkSound();
						Letter_A2.setPosition(Letter_A.getX()+3,Letter_A.getY() + Letter_A1.getHeight());
						finger.registerEntityModifier(toPart2);
					}

				});
				registerUpdateHandler(waitToColorPart1);

				TimerHandler moveToLetter2 = new TimerHandler(5.1f,
						false, new ITimerCallback() {

					@Override
					public void onTimePassed(
							TimerHandler pTimerHandler) {
						finger.registerEntityModifier(toLetter2);
						Letter_A3.registerEntityModifier(DragPart2);
					}

				});
				registerUpdateHandler(moveToLetter2);

				TimerHandler waitToColorPart2 = new TimerHandler(6.1f,
						false, new ITimerCallback() {
					@Override
					public void onTimePassed(
							TimerHandler pTimerHandler) {
						SceneManager.getInstance().playBuildOkSound();
						hangLetter(Letter_A, true);
						Letter_A1.setScale(0);
						Letter_A2.setScale(0);
						Letter_A3.setScale(0);
						Letter_A.setColor(1, 0.75f, 0.5f, 0.9f);
						finger.registerEntityModifier(toPart3);
					}

				});
				registerUpdateHandler(waitToColorPart2);

				TimerHandler moveToLetter3 = new TimerHandler(7.1f,false, new ITimerCallback() {

					@Override
					public void onTimePassed(
							TimerHandler pTimerHandler) {
						finger.registerEntityModifier(toLetter3);
						Letter_a1.registerEntityModifier(DragPart3);
					}

				});
				registerUpdateHandler(moveToLetter3);

				TimerHandler waitToColorPart3 = new TimerHandler(7.65f,false, new ITimerCallback() {
					@Override
					public void onTimePassed(TimerHandler pTimerHandler) {
						SceneManager.getInstance().playBuildOkSound();
						Letter_a1.setPosition(Letter_a.getX(),Letter_a.getY());
						finger.registerEntityModifier(toPart4);
					}

				});
				registerUpdateHandler(waitToColorPart3);

				TimerHandler moveToLetter4 = new TimerHandler(8.2f,false, new ITimerCallback() {

					@Override
					public void onTimePassed(
							TimerHandler pTimerHandler) {
						finger.registerEntityModifier(toLetter4);
						Letter_a2.registerEntityModifier(DragPart4);
					}

				});
				registerUpdateHandler(moveToLetter4);

				TimerHandler waitToColorPart4 = new TimerHandler(10.2f,false, new ITimerCallback() {
					@Override
					public void onTimePassed(TimerHandler pTimerHandler) {
					
						Letter_a2.setPosition(Letter_a.getX()+Letter_a.getWidth()-Letter_a2.getWidth(),Letter_a.getY());
						finger.registerEntityModifier(toPart5);
					}

				});
				registerUpdateHandler(waitToColorPart4);

				TimerHandler moveToLetter5 = new TimerHandler(8.75f,false, new ITimerCallback() {

					@Override
					public void onTimePassed(
							TimerHandler pTimerHandler) {
						finger.registerEntityModifier(toLetter5);
						Letter_a3.registerEntityModifier(DragPart5);
					}

				});
				registerUpdateHandler(moveToLetter5);

				TimerHandler waitToColorPart5 = new TimerHandler(9.3f,false, new ITimerCallback() {
					@Override
					public void onTimePassed(TimerHandler pTimerHandler) {
						finger.setScale(0);
						Letter_a1.setScale(0);
						Letter_a2.setScale(0);
						Letter_a3.setScale(0);
						Letter_a.setColor(1, 0.75f, 0.5f, 0.9f);
						hangLetter(Letter_a, false);
					}

				});
				registerUpdateHandler(waitToColorPart5);

				TimerHandler waitToColorBlock = new TimerHandler(12.3f,false, new ITimerCallback() {

					@Override
					public void onTimePassed(
							TimerHandler pTimerHandler) {

						hangar0.setColor(0.5f, 0.5f, 0.5f, 1);
						SceneManager.getInstance().playAckSound();
					}

				});
				registerUpdateHandler(waitToColorBlock);

				TimerHandler waitToReset = new TimerHandler(14f, false,new ITimerCallback() {
					@Override
					public void onTimePassed(TimerHandler pTimerHandler) {

						reloadBuildGame();

					}

				});
				registerUpdateHandler(waitToReset);

			}

		});
		registerUpdateHandler(sniffer);

	}

	private void postMemInfo(){


		WarnText.setScale(0.75f);
		RAMinfo.setScale(0.75f);
		warning.setScale(1.25f);

		TimerHandler waitForWarning = new TimerHandler(17f,false,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
							
					warning.setScale(0);
				    WarnText.setScale(0);
				    RAMinfo.setScale(0);
			}

		});
		registerUpdateHandler(waitForWarning);
	}

	private void updateMemoryInfo(){

		TimerHandler MemCheckDaemon = new TimerHandler(20f,true,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {

				MemoryInfo mi = new MemoryInfo();
				ActivityManager activityManager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
				if(mi!=null){
					activityManager.getMemoryInfo(mi);
					long availableMegs = mi.availMem / 1048576L;

					final String badmemory="  RAM isigaye: "+availableMegs+" MB,ikenewe: 128 MB";
					if(availableMegs<128L){	
						RAMinfo.setText(badmemory);
						postMemInfo();
					}

				}

			}

		});
		registerUpdateHandler(MemCheckDaemon);
	}

	protected void createMenuScene() {

		this.buildMenuScene = new MenuScene(camera);

		final SpriteMenuItem resetMenuItem = new SpriteMenuItem(MENU_RESET,
				resourcesManager.reset_region, vbom);
		resetMenuItem.setBlendFunction(GLES20.GL_SRC_ALPHA,
				GLES20.GL_ONE_MINUS_SRC_ALPHA);

		final SpriteMenuItem quitMenuItem = new SpriteMenuItem(MENU_QUIT,
				resourcesManager.back_region, vbom);
		quitMenuItem.setBlendFunction(GLES20.GL_SRC_ALPHA,
				GLES20.GL_ONE_MINUS_SRC_ALPHA);

		final SpriteMenuItem cancelMenuItem = new SpriteMenuItem(MENU_CANCEL,
				resourcesManager.cancel_region, vbom);
		quitMenuItem.setBlendFunction(GLES20.GL_SRC_ALPHA,
				GLES20.GL_ONE_MINUS_SRC_ALPHA);

		this.buildMenuScene.addMenuItem(resetMenuItem);
		this.buildMenuScene.addMenuItem(cancelMenuItem);
		this.buildMenuScene.addMenuItem(quitMenuItem);

		this.buildMenuScene.buildAnimations();

		this.buildMenuScene.setBackgroundEnabled(false);
		// this.buildMenuScene.setBackground(new Background(0.009804f, 0.06274f,
		// 0.08784f,0.5f));

		this.buildMenuScene.setOnMenuItemClickListener(this);
	}

	@Override
	public SceneType getSceneType() {
		return SceneType.BUILD_SCENE_GAME;
	}

	@Override
	public void disposeScene() {

		this.clearEntityModifiers();
		this.clearUpdateHandlers();
		this.clearTouchAreas();
		this.clearChildScene();
		this.detachChildren(); // detach all children
		this.detachSelf();
		this.dispose();
		System.gc();

	}

	@Override
	public boolean onMenuItemClicked(final MenuScene pMenuScene,
			final IMenuItem pMenuItem, final float pMenuItemLocalX,
			final float pMenuItemLocalY) {
		switch (pMenuItem.getID()) {
		case MENU_RESET:
			reloadBuildGame();
			return true;
		case MENU_CANCEL:
			cancelMenu();
			return true;
		case MENU_QUIT:
			// End game mode.
			exitBuildGame();
			return true;
		default:
			return false;
		}
	}

	private void showBuildMenu() {

		if (this.hasChildScene()) {
			// Remove the menu and reset it.
			canvas.setScale(0);
			showmenu.setScale(0.5f);
			this.buildMenuScene.back();
			SceneManager.getInstance().resumeGameModeBackMusic();
			GameManager.getInstance().setPaused(false);

		} else {

			// Attach the menu.
			canvas.setScale(1);
			showmenu.setScale(0);
			this.setChildScene(this.buildMenuScene, false, true, true);
			SceneManager.getInstance().pauseGameModeBackMusic();
			GameManager.getInstance().setPaused(true);

		}
	}

	private void reloadBuildGame() {

		// resume background music if paused
		if(!hasVictorySequenceStarted){
			SceneManager.getInstance().playPaintCongratulorySound(false);
			GameManager.getInstance().setPaused(false);
			SceneManager.getInstance().resumeGameModeBackMusic();

			// Remove the menu and reset it.
			this.clearChildScene();
			this.buildMenuScene.reset();
			canvas.setScale(0);
			finger.setScale(0);
			showmenu.setScale(0.5f);
			cleanScene();
			this.initLetterParts();
			delayOfficer(randBuildLetterIndex0, 1.5f);
		}
		else{
			SceneManager.getInstance().vibrate(200);	 
		}

	}

	private void exitBuildGame() {

		// return to Soma menu
		SceneManager.getInstance().stopGameModeBackMusic();
		SceneManager.getInstance().resumeMenuBackMusic();
		SceneManager.getInstance().loadMenuScene(engine);
	}

	@Override
	public void onMenuKeyPressed() {
		if (!isDemoRunning)
			showBuildMenu();
		else {
			SceneManager.getInstance().vibrate(100);
			Toast.makeText(activity, "Reka Soma irangize ku kwerekera...",
					Toast.LENGTH_SHORT).show();

		}

	}

	@Override
	public void onBackKeyPressed() {
		if (!isDemoRunning)
			showBuildMenu();
		else {
			SceneManager.getInstance().vibrate(100);
			Toast.makeText(activity, "Reka Soma irangize ku kwerekera...",
					Toast.LENGTH_SHORT).show();
		}
	}

	private void cancelMenu() {
		canvas.setScale(0);
		GameManager.getInstance().setPaused(false); 
		SceneManager.getInstance().resumeGameModeBackMusic();
		this.clearChildScene();
		this.buildMenuScene.reset();
		showmenu.setScale(0.5f);

	}

}
