
/**
 @company: Hehe Ltd
 @Project: Soma
 @Date: mm/dd/yyyy
 @Author: Amiri Mugarura and Sixbert Uwiringiyimana
 @Credits: thanks given to Richard Rusa and other artists for graphic designs ....

 @About this Class "EggLetterScene.java":
   ----------------------------------------
 This class implements the game mode of "HUZA" which engages the kid in matching 
 capital letters to smaller case letters.
 The game mode loads texturemap and audio sound/music factory from resourcesManager instance
 It uses timerHandler to display and to hide letters.
 ......./.....
 */


package com.hehe.soma.scene;
import java.util.LinkedList;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.modifier.ColorModifier;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.util.GLState;
import org.andengine.util.math.MathUtils;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.opengl.GLES20;

import com.hehe.soma.manager.GameManager;
import com.hehe.soma.manager.SceneManager;
import com.hehe.soma.manager.SceneManager.SceneType;

public class EggLetterScene extends ParentScene implements IOnMenuItemClickListener {



	protected MenuScene eggMenuScene;
	protected static final int MENU_RESET = 0;
	protected static final int MENU_CANCEL = 1;
	protected static final int MENU_QUIT = 2;


	private Sprite background,canvas,showmenu,mother,feedBaby,balloon,car0,car1;	
	private Sprite egg0,egg1,egg2,egg3,egg4,egg5,bird0,bird1,bird2,bird3,bird4,bird5;
	private AnimatedSprite balloonBird,flyingBird0,flyingBird1,flyingBird2,flyingBird3,flyingBird4,flyingBird5,crackEggSheet;
	private Text WarnText,RAMinfo;
	private Sprite warning;
	
	private float InitBirdX0,InitBirdX1,InitBirdX2,InitBirdX3,InitBirdX4,InitBirdX5;
	private float InitEggY, InitBirdY0,InitBirdY1,InitBirdY2,InitBirdY3,InitBirdY4,InitBirdY5;

	private static final float InitEggX0=120,InitEggX1=220,InitEggX2=320,InitEggX3=420,InitEggX4=520,InitEggX5=620;

	TimerHandler  ShowEggLetterTimerHandler,animateWaitTimer,layEggTimer,waitForGuidanceToFinish;
	TimerHandler waitForMenu,WaitToFeed,waitForLastEgg,waitForNackToFinish;


	private LinkedList<String>letterNguvi;

	private LinkedList<Text> CapitalLetterList;
	private LinkedList<Text>LowerCaseLetterList;
	private Text t,e0,g,e1,r,e2,z,a;
	private ParallelEntityModifier tS,e0S,gS,e1S,rS,e2S,zS,aS; 
	private ParallelEntityModifier layEgg0,layEgg1,layEgg2,layEgg3,layEgg4,layEgg5;

	private boolean isEgg0Hatched=false;
	private boolean isEgg1Hatched=false;
	private boolean isEgg2Hatched=false;
	private boolean isEgg3Hatched=false;
	private boolean isEgg4Hatched=false;
	private boolean isEgg5Hatched=false;
	private boolean isVictoryAchieved=false;
	private boolean was_last_a_trick=false;
	private boolean playAgain;


	int letterID0;
	int letterID1;
	int letterID2;
	int letterID3;
	int letterID4;
	int letterID5;
	private int remainingEggCount=6;
	private int randomColorID=0;



	@Override
	public void createScene()
	{   
		engine.enableVibrator(activity);
		createMenuScene() ;
		SceneManager.getInstance().playGameModeBackMusic();
		createBackground();	
		initEggLetterSprites();
		sortChildren();
		layScene();
		updateMemoryInfo();
	}





	private void createBackground()
	{
		this.background=new Sprite(0, 0, resourcesManager.egg_background_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		background.setScale(1.0f);
		background.setPosition(0,0);
		attachChild(background);

		this.canvas=new Sprite(0, 0, resourcesManager.canvas_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		canvas.setScale(0);
		canvas.setColor(0,0.95f, 0.95f,0.75f);
		canvas.setZIndex(10);
		canvas.setPosition(camera.getCenterX()-250,camera.getCenterY()-175);
		attachChild(canvas);


		this.showmenu=new Sprite(0, 0, resourcesManager.showmenu_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   

				if (pSceneTouchEvent.isActionUp())
				{   
					showEggMenu();
					SceneManager.getInstance().playOnSubMenuButtonClickSound();
					SceneManager.getInstance().vibrate(200);

				}

				if (pSceneTouchEvent.isActionDown())
				{  



				}
				return true;
			};
		};
		showmenu.setScale(0.5f);
		showmenu.setAlpha(0.5f);
		showmenu.setZIndex(4);
		showmenu.setPosition(camera.getWidth()-80,camera.getHeight()-80);
		attachChild(showmenu);
		registerTouchArea(showmenu);


		t=new Text(camera.getCenterX()-200,camera.getCenterY(), resourcesManager.Eggfont,".", vbom);
		e0=new Text(camera.getCenterX()-150,camera.getCenterY(), resourcesManager.Eggfont,".", vbom);
		g=new Text(camera.getCenterX()-100,camera.getCenterY(), resourcesManager.Eggfont,".", vbom);
		e1=new Text(camera.getCenterX()-50,camera.getCenterY(), resourcesManager.Eggfont,".", vbom);
		r=new Text(camera.getCenterX(),camera.getCenterY(), resourcesManager.Eggfont,".", vbom);
		e2=new Text(camera.getCenterX()+50,camera.getCenterY(), resourcesManager.Eggfont,".", vbom);
		z=new Text(camera.getCenterX()+100,camera.getCenterY(), resourcesManager.Eggfont,".", vbom);
		a=new Text(camera.getCenterX()+150,camera.getCenterY(), resourcesManager.Eggfont,".", vbom);
		t.setScale(0);
		e0.setScale(0);
		g.setScale(0);
		r.setScale(0);
		e1.setScale(0);
		r.setScale(0);
		e2.setScale(0);
		z.setScale(0);
		a.setScale(0);

		attachChild(t);
		attachChild(e0);
		attachChild(g);
		attachChild(e1);
		attachChild(r);
		attachChild(e2);
		attachChild(z);
		attachChild(a);
		
		warning=new Sprite(0, 0, resourcesManager.showwarn_region, vbom)
		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   

				if (pSceneTouchEvent.isActionUp())
				{   
					WarnText.setScale(0);
					RAMinfo.setScale(0);
					warning.setScale(0);
					
				

				}
				return true;
			};
		};
		warning.setPosition(100,camera.getHeight()-warning.getHeight()-25);
		attachChild(warning);
		registerTouchArea(warning);
		warning.setScale(0);
		

		WarnText=new Text(warning.getX()+warning.getWidth()-15,camera.getHeight()-warning.getHeight()-50, resourcesManager.WarnFont,
				"Telefone yawe ifite ibyangombwa bidahagije!\n Funga izindi apps udakeneye cyangwa\n ujye usubiramo SOMA nijya yifunga.", vbom);	
		attachChild(WarnText);	
		WarnText.setScale(0);
		
		RAMinfo=new Text(warning.getX()+warning.getWidth()-15,camera.getHeight()-warning.getHeight()-55+WarnText.getHeight(), resourcesManager.WarnFont,
				"  RAM isigaye: 100 MB,ikenewe: 128 MB ", vbom);	
		attachChild(RAMinfo);	
		RAMinfo.setScale(0);
		
		RAMinfo.setZIndex(4);
		WarnText.setZIndex(4);
		warning.setZIndex(4);

	}


	private void initEggLetterSprites() {

		letterID0=MathUtils.random(0,3);
		letterID1=MathUtils.random(5,7);
		letterID2=MathUtils.random(9,11);
		letterID3=MathUtils.random(13,15);
		letterID4=MathUtils.random(17,19);
		letterID5=MathUtils.random(21,22);
		remainingEggCount=6;
		playAgain=true;
		//randomize trickery a litte bit
		if(MathUtils.random(0,7)%2==0){
			was_last_a_trick=false;	  
			
		}
		else{
			was_last_a_trick=true;  
		}

		letterNguvi=new LinkedList<String>();

		InitBirdX0=camera.getCenterX()-250;
		InitBirdY0=camera.getHeight()-200;

		InitBirdX1=camera.getCenterX()-200;
		InitBirdY1=camera.getHeight()-150;

		InitBirdX2=camera.getCenterX()-100;
		InitBirdY2=camera.getHeight()-100;



		InitBirdX3=camera.getCenterX();
		InitBirdY3=camera.getHeight()-100;

		InitBirdX4=camera.getCenterX()+100;
		InitBirdY4=camera.getHeight()-100;

		InitBirdX5=camera.getCenterX()+200;
		InitBirdY5=camera.getHeight()-150;

		InitEggY=camera.getHeight()-100;



		letterNguvi.add("a");letterNguvi.add("b");letterNguvi.add("c");
		letterNguvi.add("d");letterNguvi.add("e");letterNguvi.add("f");
		letterNguvi.add("g");letterNguvi.add("h");letterNguvi.add("i");
		letterNguvi.add("j");letterNguvi.add("k");letterNguvi.add("l");
		letterNguvi.add("m");letterNguvi.add("n");letterNguvi.add("o");
		letterNguvi.add("p");letterNguvi.add("r");letterNguvi.add("s");
		letterNguvi.add("t");letterNguvi.add("u");letterNguvi.add("v");
		letterNguvi.add("w");letterNguvi.add("y");letterNguvi.add("z");




		this.balloonBird=new AnimatedSprite(0, 0, resourcesManager.birdfly_region, vbom);
		balloonBird.setPosition(50,5);
		balloonBird.setZIndex(4);
		balloonBird.setScale(0);

		this.flyingBird0=new AnimatedSprite(0, 0, resourcesManager.birdfly_region, vbom);
		flyingBird0.setPosition(InitBirdX0,InitBirdY0);
		flyingBird0.setScale(0);
		flyingBird0.setZIndex(10);

		this.flyingBird1=new AnimatedSprite(0, 0, resourcesManager.birdfly_region, vbom);
		flyingBird1.setPosition(InitBirdX1,InitBirdY1);
		flyingBird1.setScale(0);
		flyingBird1.setZIndex(10);

		this.flyingBird2=new AnimatedSprite(0, 0, resourcesManager.birdfly_region, vbom);
		flyingBird2.setPosition(InitBirdX2,InitBirdY2);
		flyingBird2.setScale(0);
		flyingBird2.setZIndex(10);

		this.flyingBird3=new AnimatedSprite(0, 0, resourcesManager.birdfly_region, vbom);
		flyingBird3.setPosition(InitBirdX3,InitBirdY3);
		flyingBird3.setScale(0);
		flyingBird3.setZIndex(10);

		this.flyingBird4=new AnimatedSprite(0, 0, resourcesManager.birdfly_region, vbom);
		flyingBird4.setPosition(InitBirdX4,InitBirdY4);
		flyingBird4.setScale(0);
		flyingBird4.setZIndex(10);

		this.flyingBird5=new AnimatedSprite(0, 0, resourcesManager.birdfly_region, vbom);
		flyingBird5.setPosition(InitBirdX5,InitBirdY5);
		flyingBird5.setScale(0);
		flyingBird5.setZIndex(10);

		this.crackEggSheet=new AnimatedSprite(0, 0, resourcesManager.crane_region, vbom);
		crackEggSheet.setPosition(20,camera.getHeight()-50);
		crackEggSheet.setScale(0);

		egg0=new Sprite(0, 0, resourcesManager.egg_region, vbom){ 

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   
				if (pSceneTouchEvent.isActionUp())
				{   
					if(GameManager.getInstance().getCurrentLowerCaseEggLetter(0).equalsIgnoreCase(GameManager.getInstance().getCurrentCapitalEggLetter())) {

						SceneManager.getInstance().vibrate(50);
						crackEgg(egg0);
						isEgg0Hatched=true;
						remainingEggCount--;
						LowerCaseLetterList.get(letterID0).setScale(0);
						SceneManager.getInstance().playEggFeedback(true);
						checkVictory();
					}
					else{

						if(playAgain){
							SceneManager.getInstance().playEggFeedback(false);
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}
				}

				if (pSceneTouchEvent.isActionDown())
				{  


				}
				return true;
			};


		};
		egg0.setPosition(0,InitEggY);
		egg0.setAlpha(1f);
		egg0.setZIndex(1);
		registerTouchArea(egg0);

		this.egg1=new Sprite(0, 0, resourcesManager.egg_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   
				if (pSceneTouchEvent.isActionUp())
				{   
					if(GameManager.getInstance().getCurrentLowerCaseEggLetter(1).equalsIgnoreCase(GameManager.getInstance().getCurrentCapitalEggLetter())) {

						SceneManager.getInstance().vibrate(50);
						crackEgg(egg1);
						isEgg1Hatched=true;
						remainingEggCount--;
						LowerCaseLetterList.get(letterID1).setScale(0);
						SceneManager.getInstance().playEggFeedback(true);

						checkVictory();
					}
					else{

						if(playAgain){
							SceneManager.getInstance().playEggFeedback(false);
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}

				}


				if (pSceneTouchEvent.isActionDown())
				{  


				}
				return true;
			};
		};
		egg1.setPosition(0,InitEggY);
		egg1.setAlpha(1f);
		egg1.setZIndex(1);
		registerTouchArea(egg1);

		this.egg2=new Sprite(0, 0, resourcesManager.egg_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   
				if (pSceneTouchEvent.isActionUp())
				{   
					if(GameManager.getInstance().getCurrentLowerCaseEggLetter(2).equalsIgnoreCase(GameManager.getInstance().getCurrentCapitalEggLetter())) {

						SceneManager.getInstance().vibrate(50);
						crackEgg(egg2);
						isEgg2Hatched=true;
						remainingEggCount--;
						LowerCaseLetterList.get(letterID2).setScale(0);	
						SceneManager.getInstance().playEggFeedback(true);
						checkVictory();

					}
					else{

						if(playAgain){
							SceneManager.getInstance().playEggFeedback(false);
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);	
					}

				}


				if (pSceneTouchEvent.isActionDown())
				{  


				}
				return true;
			};
		};
		egg2.setPosition(0,InitEggY);
		egg2.setAlpha(1f);
		egg2.setZIndex(1);
		registerTouchArea(egg2);

		this.egg3=new Sprite(0, 0, resourcesManager.egg_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   
				if (pSceneTouchEvent.isActionUp())
				{   
					if(GameManager.getInstance().getCurrentLowerCaseEggLetter(3).equalsIgnoreCase(GameManager.getInstance().getCurrentCapitalEggLetter())) {

						SceneManager.getInstance().vibrate(50);
						crackEgg(egg3);
						isEgg3Hatched=true;
						remainingEggCount--;
						LowerCaseLetterList.get(letterID3).setScale(0);	
						SceneManager.getInstance().playEggFeedback(true);
						checkVictory();
					}
					else{

						if(playAgain){
							SceneManager.getInstance().playEggFeedback(false);
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);	
					}

				}

				if (pSceneTouchEvent.isActionDown())
				{  


				}
				return true;
			};
		};
		egg3.setPosition(3,InitEggY);
		egg3.setAlpha(1f);
		egg3.setZIndex(1);
		registerTouchArea(egg3);

		this.egg4=new Sprite(0, 0, resourcesManager.egg_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   
				if (pSceneTouchEvent.isActionUp())
				{   
					if(GameManager.getInstance().getCurrentLowerCaseEggLetter(4).equalsIgnoreCase(GameManager.getInstance().getCurrentCapitalEggLetter())) {

						SceneManager.getInstance().vibrate(50);
						crackEgg(egg4);
						isEgg4Hatched=true;
						remainingEggCount--;
						LowerCaseLetterList.get(letterID4).setScale(0);	
						SceneManager.getInstance().playEggFeedback(true);
						checkVictory();
					}
					else{

						if(playAgain){
							SceneManager.getInstance().playEggFeedback(false);
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});
						this.registerUpdateHandler(waitForNackToFinish);
					}

				}

				if (pSceneTouchEvent.isActionDown())
				{  


				}
				return true;
			};
		};
		egg4.setPosition(0,InitEggY);
		egg4.setAlpha(1f);
		egg4.setZIndex(1);
		registerTouchArea(egg4);

		this.egg5=new Sprite(0, 0, resourcesManager.egg_region, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   
				if (pSceneTouchEvent.isActionUp())
				{   
					if(GameManager.getInstance().getCurrentLowerCaseEggLetter(5).equalsIgnoreCase(GameManager.getInstance().getCurrentCapitalEggLetter())) {

						SceneManager.getInstance().vibrate(50);
						crackEgg(egg5);
						isEgg5Hatched=true;
						remainingEggCount--;
						LowerCaseLetterList.get(letterID5).setScale(0);
						SceneManager.getInstance().playEggFeedback(true);
						checkVictory();
					}
					else{

						if(playAgain){
							SceneManager.getInstance().playEggFeedback(false);
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});

						this.registerUpdateHandler(waitForNackToFinish);
					}

				}

				if (pSceneTouchEvent.isActionDown())
				{  


				}
				return true;
			};
		};
		egg5.setPosition(0,InitEggY);
		egg5.setAlpha(1f);
		egg5.setZIndex(1);
		registerTouchArea(egg5);

		egg0.setScale(0);
		egg1.setScale(0);
		egg2.setScale(0);
		egg3.setScale(0);
		egg4.setScale(0);
		egg5.setScale(0);

		bird0=new Sprite(0, 0, resourcesManager.bird, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		bird0.setPosition(InitEggX0,InitEggY);
		bird0.setScale(0);

		bird1=new Sprite(0, 0, resourcesManager.bird, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		bird1.setPosition(InitEggX1,InitEggY);
		bird1.setScale(0);

		bird2=new Sprite(0, 0, resourcesManager.bird, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		bird2.setPosition(InitEggX2,InitEggY);
		bird2.setScale(0);


		bird3=new Sprite(0, 0, resourcesManager.bird, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		bird3.setPosition(InitEggX3,InitEggY);
		bird3.setScale(0);

		bird4=new Sprite(0, 0, resourcesManager.bird, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		bird4.setPosition(InitEggX4,InitEggY);
		bird4.setScale(0);

		bird5=new Sprite(0, 0, resourcesManager.bird, vbom) {

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		bird5.setPosition(InitEggX5,InitEggY);
		bird5.setScale(0);

		this.balloon=new Sprite(0, 0, resourcesManager.balloonEgg_region, vbom); 
		balloon.setPosition(0,50);


		balloon.setScale(0);
		balloon.setZIndex(1);

		this.mother=new Sprite(0, 0, resourcesManager.mother_region, vbom);
		mother.setPosition(20,camera.getHeight()-200);
		mother.setScale(1.12f);

		feedBaby=new Sprite(0, 0, resourcesManager.feed_region, vbom);
		feedBaby.setPosition(20,camera.getHeight()-200);
		feedBaby.setScale(0);


		this.car0=new Sprite(0, 0, resourcesManager.car0_region, vbom);
		car0.setPosition(camera.getCenterX()+30,10);
		car0.setScale(0.5f);
		car0.setZIndex(0);

		this.car1=new Sprite(0, 0, resourcesManager.car1_region, vbom);
		car1.setPosition(camera.getCenterX(),10);
		car1.setScale(0);
		car1.setZIndex(0);



		//init lowercase letters
		CapitalLetterList=new LinkedList<Text>();
		LowerCaseLetterList=new LinkedList<Text>();

		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"a", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"b", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"c", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"d", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"e", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"f", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"g", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"h", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"i", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"j", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"k", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"l", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"m", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"n", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"o", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"p", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"r", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"s", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"t", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"u", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"v", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"w", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"y", vbom));
		LowerCaseLetterList.add(new Text(110,camera.getHeight()-55, resourcesManager.Eggfont,"z", vbom));

		//init capital letters

		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"A", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"B", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"C", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"D", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"E", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"F", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"G", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"H", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"I", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"J", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"K", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"L", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"M", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"N", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"O", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"P", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"R", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"S", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"T", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"U", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"V", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"W", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"Y", vbom));
		CapitalLetterList.add(new Text(65,110, resourcesManager.Eggfont,"Z", vbom));


		this.attachLetters();
		this.attachSprites();


	}



	private void flyBirds() {

		bird0.setScale(0);
		bird1.setScale(0);
		bird2.setScale(0);
		bird3.setScale(0);
		bird4.setScale(0);
		bird5.setScale(0);
		mother.registerEntityModifier(new MoveXModifier(1f,mother.getX(),-100));
		feedBaby.setScale(0);

		flyingBird0.setFlippedHorizontal(true);
		flyingBird0.setScale(0.75f);
		flyingBird0.animate(new long[] { 100,100,100,200,200,200}, 0,5,true);

		flyingBird1.setFlippedHorizontal(true);
		flyingBird1.setScale(0.75f);
		flyingBird1.animate(new long[] {100,100,100,200,200,200}, 0,5,true);

		flyingBird2.setFlippedHorizontal(true);
		flyingBird2.setScale(0.75f);
		flyingBird2.animate(new long[] {100,100,100,200,200,200}, 0,5,true);

		flyingBird3.setScale(0.75f);
		flyingBird3.animate(new long[] {100,100,100,200,200,200}, 0,5,true);

		flyingBird4.setScale(0.75f);
		flyingBird4.animate(new long[] {100,100,100,200,200,200}, 0,5,true);


		flyingBird5.setScale(0.75f);
		flyingBird5.animate(new long[] {100,100,100,200,200,200}, 0,5,true);

		flyingBird0.registerEntityModifier(new MoveModifier(3f,InitBirdX0,-200,InitBirdY0,-20));
		flyingBird1.registerEntityModifier(new MoveModifier(3f,InitBirdX1,-200,InitBirdY1,-100));
		flyingBird2.registerEntityModifier(new MoveModifier(3f,InitBirdX2,-200,InitBirdY2,-200));

		flyingBird3.registerEntityModifier(new MoveModifier(3f,InitBirdX3,camera.getWidth(),InitBirdY3,20));
		flyingBird4.registerEntityModifier(new MoveModifier(3f,InitBirdX4,camera.getWidth(),InitBirdY4,100));
		flyingBird5.registerEntityModifier(new MoveModifier(3f,InitBirdX5,camera.getWidth(),InitBirdY5,200));




	}

	private void crackEgg(final Sprite egg) {

		mother.setScale(0);
		egg.setScale(0);

		crackEggSheet.setPosition(egg.getX()-43,camera.getCenterY()+110);
		crackEggSheet.setScale(1);
		crackEggSheet.animate(new long[] { 100,100,100,100,100,100,100,100,100,100}, 0,9,false);

		TimerHandler	WaitToHatch=new TimerHandler(1.1f,false,new ITimerCallback() {

			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {

				crackEggSheet.setScale(0);
				feedBaby.setPosition(egg.getX()-120,camera.getCenterY()+80);
				feedBaby.setScale(1);
				if(egg.equals(egg0)){
					bird0.setScale(1.5f);
				}
				else if(egg.equals(egg1)){
					bird1.setScale(1.5f);
				}

				else if(egg.equals(egg2)){
					bird2.setScale(1.5f);
				}
				else if(egg.equals(egg3)){
					bird3.setScale(1.5f);
				}
				else if(egg.equals(egg4)){
					bird4.setScale(1.5f);
				}
				else if(egg.equals(egg5)){
					bird5.setScale(1.5f);
				}


			}
		});

		this.registerUpdateHandler(WaitToHatch);

		WaitToFeed=new TimerHandler(2,false,new ITimerCallback() {

			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {

				mother.setScale(1);
				feedBaby.setScale(0);
			}
		});

		this.registerUpdateHandler(WaitToFeed);

	}









	private void recycleScene(){


		for(int index=0;index<CapitalLetterList.size();index++){
			if(CapitalLetterList.get(index).hasParent())
				detachChild(CapitalLetterList.get(index));
			if(LowerCaseLetterList.get(index).hasParent())
				detachChild(LowerCaseLetterList.get(index));
			detachChild(mother);
			detachChild(feedBaby);
			detachChild(car0);
			detachChild(car1);
			detachChild(balloon);
			detachChild(balloonBird);
			detachChild(this.flyingBird0);
			detachChild(this.flyingBird1);
			detachChild(this.flyingBird2);
			detachChild(this.flyingBird3);
			detachChild(this.flyingBird4);
			detachChild(this.flyingBird5);
			detachChild(crackEggSheet);
			detachChild(egg0);
			detachChild(egg1);
			detachChild(egg2);
			detachChild(egg3);
			detachChild(egg4);
			detachChild(egg5);
			detachChild(bird0);
			detachChild(bird1);
			detachChild(bird2);
			detachChild(bird3);
			detachChild(bird4);
			detachChild(bird5);


		}

		this.car1.setScale(0);
		isEgg0Hatched=false;
		isEgg1Hatched=false;
		isEgg2Hatched=false;
		isEgg3Hatched=false;
		isEgg4Hatched=false;
		isEgg5Hatched=false;
		isVictoryAchieved=false;
		this.clearEntityModifiers();
		this.unregisterUpdateHandler(ShowEggLetterTimerHandler);
		this.unregisterUpdateHandler(waitForMenu);
		this.unregisterUpdateHandler(WaitToFeed);


	}


	private void attachSprites(){

		attachChild(car0);
		attachChild(car1);
		attachChild(balloon);
		attachChild(balloonBird);
		attachChild(mother);
		attachChild(this.flyingBird0);
		attachChild(this.flyingBird1);
		attachChild(this.flyingBird2);
		attachChild(this.flyingBird3);
		attachChild(this.flyingBird4);
		attachChild(this.flyingBird5);
		attachChild(crackEggSheet);
		attachChild(egg0);
		attachChild(egg1);
		attachChild(egg2);
		attachChild(egg3);
		attachChild(egg4);
		attachChild(egg5);
		attachChild(bird0);
		attachChild(bird1);
		attachChild(bird2);
		attachChild(bird3);
		attachChild(bird4);
		attachChild(bird5);
		attachChild(feedBaby);
		sortChildren(true);
	}

	private void hideOnVictory(){

		SceneManager.getInstance().playPaintCongratulorySound(true);
		this.balloonBird.setScale(0);
		this.balloon.setScale(0);

		for(int index=0;index<CapitalLetterList.size();index++){

			CapitalLetterList.get(index).setScale(0);

			LowerCaseLetterList.get(index).setScale(0);
		}

	}

	private void attachLetters(){

		for(int index=0;index<CapitalLetterList.size();index++){
			attachChild(CapitalLetterList.get(index));
			CapitalLetterList.get(index).setScale(0);
			CapitalLetterList.get(index).setRotation(7);
			CapitalLetterList.get(index).setZIndex(6);

			attachChild(LowerCaseLetterList.get(index));
			LowerCaseLetterList.get(index).setScale(0);
			LowerCaseLetterList.get(index).setZIndex(3);

		}

	}

	//==============================================

	private int postCapital(){


		if(remainingEggCount>=5){
			if(was_last_a_trick){
				was_last_a_trick=false;
				switch(MathUtils.random(0,5)){
				case 0:
					if(!isEgg0Hatched)
						return letterID0;
					else					
						return letterID4;
				case 1:
					if(!isEgg1Hatched)
						return letterID1;
					else					
						return letterID5;
				case 2:
					if(!isEgg2Hatched)
						return letterID2;
					else					
						return letterID3;
				case 3:
					if(!isEgg3Hatched)
						return letterID3;
					else					
						return letterID2;
				case 4:
					if(!isEgg4Hatched)
						return letterID4;
					else					
						return letterID0;
				case 5:
					if(!isEgg5Hatched)
						return letterID5;
					else					
						return letterID1;
				default:
					return MathUtils.random(0,23);
				}
			}
			else{
				was_last_a_trick=true;
				switch(MathUtils.random(6,11)){
				case 6:
					return letterID0+1;
				case 7:
					return letterID1+1;
				case 8:
					return letterID2+1;
				case 9:
					return letterID3+1;
				case 10:
					return letterID4+1;
				case 11:
					return letterID5+1;
				default:
					return MathUtils.random(0,23);
				}
			}

		}
		else{
			if(MathUtils.random(0,1)==0){
				if(!isEgg0Hatched)
					return letterID0;
				else if(!isEgg1Hatched)
					return letterID1;
				else if(!isEgg2Hatched)
					return letterID2;
				else if(!isEgg3Hatched)
					return letterID3;
				else if(!isEgg4Hatched)
					return letterID4;
				else if(!isEgg5Hatched)
					return letterID5;
				else{
					return MathUtils.random(0,23);	
				}
			}
			else{
				if(!isEgg5Hatched)
					return letterID5;
				else if(!isEgg4Hatched)
					return letterID4;
				else if(!isEgg3Hatched)
					return letterID3;
				else if(!isEgg2Hatched)
					return letterID2;
				else if(!isEgg1Hatched)
					return letterID1;
				else if(!isEgg0Hatched)
					return letterID0;
				else{
					return MathUtils.random(0,23);	
				}   
			}

		}

	}

	private void showLowCases(int letter0,int letter1,int letter2,int letter3,int letter4,int letter5 ) {

		LowerCaseLetterList.get(letter0).setPosition(InitEggX0,InitEggY);
		LowerCaseLetterList.get(letter1).setPosition(InitEggX1,InitEggY);
		LowerCaseLetterList.get(letter2).setPosition(InitEggX2,InitEggY);
		LowerCaseLetterList.get(letter3).setPosition(InitEggX3,InitEggY);
		LowerCaseLetterList.get(letter4).setPosition(InitEggX4,InitEggY);
		LowerCaseLetterList.get(letter5).setPosition(InitEggX5,InitEggY);

		for(int i=0;i<LowerCaseLetterList.size();i++){
			LowerCaseLetterList.get(i).setScale(0);
		}

		if(!isEgg0Hatched){

			GameManager.getInstance().setCurrentLowerCaseEggLetter(letterNguvi.get(letter0),0);	

		}
		else{
			LowerCaseLetterList.get(letter0).setScale(0);
		}


		if(!isEgg1Hatched){

			GameManager.getInstance().setCurrentLowerCaseEggLetter(letterNguvi.get(letter1),1);	
		}
		else{
			LowerCaseLetterList.get(letter1).setScale(0);
		}

		if(!isEgg2Hatched){


			GameManager.getInstance().setCurrentLowerCaseEggLetter(letterNguvi.get(letter2),2);
		}

		else{
			LowerCaseLetterList.get(letter2).setScale(0);
		}

		if(!isEgg3Hatched){


			GameManager.getInstance().setCurrentLowerCaseEggLetter(letterNguvi.get(letter3),3);
		}
		else{
			LowerCaseLetterList.get(letter3).setScale(0);
		}

		if(!isEgg4Hatched){

			GameManager.getInstance().setCurrentLowerCaseEggLetter(letterNguvi.get(letter4),4);
		}
		else{
			LowerCaseLetterList.get(letter4).setScale(0);
		}

		if(!isEgg5Hatched){

			GameManager.getInstance().setCurrentLowerCaseEggLetter(letterNguvi.get(letter5),5);

		}
		else{
			LowerCaseLetterList.get(letter5).setScale(0);
		}



	}


	//mthod to fly capital letter with a bird and ballon
	private void moveCapital(int letterID,IEntityModifier LetterMover,IEntityModifier BalloonMover,IEntityModifier birdMover) {

		randomColorID=MathUtils.random(0,20);
		balloon.setScale(0.75f);
		balloonBird.setScale(0.75f);
		balloonBird.animate(new long[] { 50,100,100,50,100,50}, 0,5,true);
		balloon.setColor(GameManager.getInstance().getRandomColor("huza",randomColorID));
		balloon.setAlpha(0.75f);

		balloon.registerEntityModifier(BalloonMover);		
		balloonBird.registerEntityModifier(birdMover);

		if(!isVictoryAchieved)
			switch(letterID) {
			case 0:	
				CapitalLetterList.get(0).setScale(1);
				CapitalLetterList.get(0).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("a");
				SceneManager.getInstance().playLetterSound("a");

				break;
			case 1:
				CapitalLetterList.get(1).setScale(1);
				CapitalLetterList.get(1).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("b");
				SceneManager.getInstance().playLetterSound("b");

				break;
			case 2:
				CapitalLetterList.get(2).setScale(1);
				CapitalLetterList.get(2).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("c");
				SceneManager.getInstance().playLetterSound("c");


				break;
			case 3:
				CapitalLetterList.get(3).setScale(1);
				CapitalLetterList.get(3).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("d");
				SceneManager.getInstance().playLetterSound("d");


				break;
			case 4:
				CapitalLetterList.get(4).setScale(1);
				CapitalLetterList.get(4).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("e");
				SceneManager.getInstance().playLetterSound("e");


				break;
			case 5:
				CapitalLetterList.get(5).setScale(1);
				CapitalLetterList.get(5).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("f");
				SceneManager.getInstance().playLetterSound("f");


				break;
			case 6:
				CapitalLetterList.get(6).setScale(1);
				CapitalLetterList.get(6).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("g");
				SceneManager.getInstance().playLetterSound("g");


				break;
			case 7:
				CapitalLetterList.get(7).setScale(1);
				CapitalLetterList.get(7).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("h");
				SceneManager.getInstance().playLetterSound("h");


				break;
			case 8:
				CapitalLetterList.get(8).setScale(1);
				CapitalLetterList.get(8).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("i");	
				SceneManager.getInstance().playLetterSound("i");

				break;
			case 9:
				CapitalLetterList.get(9).setScale(1);
				CapitalLetterList.get(9).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("j");
				SceneManager.getInstance().playLetterSound("j");

				break;
			case 10:
				CapitalLetterList.get(10).setScale(1);
				CapitalLetterList.get(10).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("k");
				SceneManager.getInstance().playLetterSound("k");


				break;
			case 11:
				CapitalLetterList.get(11).setScale(1);
				CapitalLetterList.get(11).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("l");
				SceneManager.getInstance().playLetterSound("l");


				break;
			case 12:
				CapitalLetterList.get(12).setScale(1);
				CapitalLetterList.get(12).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("m");
				SceneManager.getInstance().playLetterSound("m");


				break;
			case 13:
				CapitalLetterList.get(13).setScale(1);
				CapitalLetterList.get(13).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("n");
				SceneManager.getInstance().playLetterSound("n");


				break;
			case 14:
				CapitalLetterList.get(14).setScale(1);
				CapitalLetterList.get(14).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("o");
				SceneManager.getInstance().playLetterSound("o");


				break;
			case 15:
				CapitalLetterList.get(15).setScale(1);
				CapitalLetterList.get(15).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("p");
				SceneManager.getInstance().playLetterSound("p");


				break;
			case 16:
				CapitalLetterList.get(16).setScale(1);
				CapitalLetterList.get(16).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("r");
				SceneManager.getInstance().playLetterSound("r");


				break;
			case 17:
				CapitalLetterList.get(17).setScale(1);
				CapitalLetterList.get(17).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("s");
				SceneManager.getInstance().playLetterSound("s");


				break;
			case 18:
				CapitalLetterList.get(18).setScale(1);
				CapitalLetterList.get(18).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("t");
				SceneManager.getInstance().playLetterSound("t");


				break;
			case 19:
				CapitalLetterList.get(19).setScale(1);
				CapitalLetterList.get(19).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("u");
				SceneManager.getInstance().playLetterSound("u");


				break;
			case 20:
				CapitalLetterList.get(20).setScale(1);
				CapitalLetterList.get(20).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("v");
				SceneManager.getInstance().playLetterSound("v");


				break;
			case 21:
				CapitalLetterList.get(21).setScale(1);
				CapitalLetterList.get(21).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("w");
				SceneManager.getInstance().playLetterSound("w");


				break;
			case 22:
				CapitalLetterList.get(22).setScale(1);
				CapitalLetterList.get(22).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("y");
				SceneManager.getInstance().playLetterSound("y");


				break;
			case 23:
				CapitalLetterList.get(23).setScale(1);
				CapitalLetterList.get(23).registerEntityModifier(LetterMover);
				GameManager.getInstance().setCurrentCapitalEggLetter("z");
				SceneManager.getInstance().playLetterSound("z");
				break;
			default:
				break;
			}
	}


	private void layScene(){

		SceneManager.getInstance().playEggGuidanceAudio();
		waitForGuidanceToFinish = new TimerHandler(5,false,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
				displayEggLetters();
				unregisterUpdateHandler(waitForGuidanceToFinish);
			}
		});
		this.registerUpdateHandler(waitForGuidanceToFinish);

	}

	private void displayEggLetters() {

		showLowCases(letterID0,letterID1,letterID2,letterID3,letterID4,letterID5);
		displayWaitMsg();

		TimerHandler waitForGuidance = new TimerHandler(5,false,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {

				SceneManager.getInstance().playEggInstruction();

			}
		});
		registerUpdateHandler(waitForGuidance);	

		ShowEggLetterTimerHandler = new TimerHandler(9,true,new ITimerCallback() {

			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
				if(!isVictoryAchieved){
					moveCapital(postCapital(),new MoveXModifier(8f,65,camera.getWidth()+45),new MoveXModifier(8f,20,camera.getWidth()),new MoveXModifier(8f,50,camera.getWidth()+30));
				}
				else{
					hideOnVictory();
				}
			}
		});
		registerUpdateHandler( ShowEggLetterTimerHandler);
	}




	/*after all eggs are hatched, call this method that flies birds
	moves cars, etc...*/

	private void checkVictory() {

		if(isEgg0Hatched&&isEgg1Hatched&&isEgg2Hatched&&isEgg3Hatched&&isEgg4Hatched&&isEgg5Hatched) {


			waitForLastEgg = new TimerHandler(2.5f,false,new ITimerCallback() {
				@Override
				public void onTimePassed(TimerHandler pTimerHandler) {		


					crackEggSheet.setScale(0);
					isVictoryAchieved=true;

					car0.setFlippedHorizontal(true);
					car0.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(2,0.5f,1),new MoveXModifier(2,camera.getCenterX(),camera.getWidth()+300)));

					TimerHandler driveIn = new TimerHandler(2.05f,false,new ITimerCallback() {
						@Override
						public void onTimePassed(TimerHandler pTimerHandler) {		
							hideOnVictory();
							car1.setScale(1);					
							car1.registerEntityModifier(new MoveXModifier(1,camera.getWidth(),camera.getCenterX()));
						}

					});
					registerUpdateHandler(driveIn);


					TimerHandler scareBirds = new TimerHandler(2.75f,false,new ITimerCallback() {
						@Override
						public void onTimePassed(TimerHandler pTimerHandler) {				
							flyBirds();	
							SceneManager.getInstance().eggHonk();
						}

					});
					registerUpdateHandler(scareBirds);


					waitForMenu = new TimerHandler(7f,true,new ITimerCallback() {
						@Override
						public void onTimePassed(TimerHandler pTimerHandler) {	
							if(isVictoryAchieved)
								showEggMenu();
							unregisterUpdateHandler(waitForLastEgg);
						}

					});
					registerUpdateHandler(waitForMenu);		

				}

			});
			registerUpdateHandler(waitForLastEgg);
		}
	}



	private void displayWaitMsg(){


		tS=new ParallelEntityModifier(new ScaleModifier(0.25f,0,2.15f),new ColorModifier(0.25f,0,0,0.5f,0.75f,1,0.25f));
		e0S=new ParallelEntityModifier(new ScaleModifier(0.25f,0,2.15f),new ColorModifier(0.25f,0,0,0.5f,0.75f,1,0.25f));
		gS=new ParallelEntityModifier(new ScaleModifier(0.25f,0,2.15f),new ColorModifier(0.25f,0,0,0.5f,0.75f,1,0.25f));
		e1S=new ParallelEntityModifier(new ScaleModifier(0.25f,0,2.15f),new ColorModifier(0.25f,0,0,0.5f,0.75f,1,0.25f));
		rS=new ParallelEntityModifier(new ScaleModifier(0.25f,0,2.15f),new ColorModifier(0.25f,0,0,0.5f,0.75f,1,0.25f));
		e2S=new ParallelEntityModifier(new ScaleModifier(0.25f,0,2.15f),new ColorModifier(0.25f,0,0,0.5f,0.75f,1,0.25f));
		zS=new ParallelEntityModifier(new ScaleModifier(0.25f,0,2.15f),new ColorModifier(0.25f,0,0,0.5f,0.75f,1,0.25f));
		aS=new ParallelEntityModifier(new ScaleModifier(0.25f,0,2.15f),new ColorModifier(0.25f,0,0,0.5f,0.75f,1,0.25f));

		layEgg0=new ParallelEntityModifier(new MoveXModifier(0.75f,60,InitEggX0),new ScaleModifier(0.5f,0,1.35f));
		layEgg1=new ParallelEntityModifier(new MoveXModifier(1f,60,InitEggX1),new ScaleModifier(0.5f,0,1.35f));
		layEgg2=new ParallelEntityModifier(new MoveXModifier(1f,60,InitEggX2),new ScaleModifier(0.5f,0,1.35f));
		layEgg3=new ParallelEntityModifier(new MoveXModifier(1f,60,InitEggX3),new ScaleModifier(0.5f,0,1.35f));
		layEgg4=new ParallelEntityModifier(new MoveXModifier(1f,60,InitEggX4),new ScaleModifier(0.5f,0,1.35f));
		layEgg5=new ParallelEntityModifier(new MoveXModifier(1f,60,InitEggX5),new ScaleModifier(0.5f,0,1.35f));

		egg0.setScale(0);
		egg1.setScale(0);
		egg2.setScale(0);
		egg3.setScale(0);
		egg4.setScale(0);
		egg5.setScale(0);

		layEggTimer = new TimerHandler(0.15f,true,new ITimerCallback() {

			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {

				egg5.registerEntityModifier(layEgg5);
				if(layEgg5.isFinished()){
					egg4.registerEntityModifier(layEgg4);
					LowerCaseLetterList.get(letterID5).registerEntityModifier(new ScaleModifier(1,0,1));

				}
				if(layEgg4.isFinished()){
					egg3.registerEntityModifier(layEgg3);
					LowerCaseLetterList.get(letterID4).registerEntityModifier(new ScaleModifier(1,0,1));

				}
				if(layEgg3.isFinished()){
					egg2.registerEntityModifier(layEgg2);	
					LowerCaseLetterList.get(letterID3).registerEntityModifier(new ScaleModifier(1,0,1));

				}
				if(layEgg2.isFinished()){
					egg1.registerEntityModifier(layEgg1);	
					LowerCaseLetterList.get(letterID2).registerEntityModifier(new ScaleModifier(1,0,1));

				}
				if(layEgg1.isFinished()){
					egg0.registerEntityModifier(layEgg0);
					LowerCaseLetterList.get(letterID1).registerEntityModifier(new ScaleModifier(1,0,1));
					LowerCaseLetterList.get(letterID0).registerEntityModifier(new ScaleModifier(2,0,1));
					animateWaitTimer = new TimerHandler(0.15f,true,new ITimerCallback() {

						@Override
						public void onTimePassed(TimerHandler pTimerHandler) {

							t.registerEntityModifier(tS);
							if(tS.isFinished()){
								e0.registerEntityModifier(e0S);	
							}
							if(e0S.isFinished()){
								g.registerEntityModifier(gS);	
							}
							if(gS.isFinished()){
								e1.registerEntityModifier(e1S);	
							}
							if(e1S.isFinished()){
								r.registerEntityModifier(rS);	
							}
							if(rS.isFinished()){
								e2.registerEntityModifier(e2S);	
							}
							if(e2S.isFinished()){
								z.registerEntityModifier(zS);	
							}
							if(zS.isFinished()){
								a.registerEntityModifier(aS);	
							}

						}

					});
					registerUpdateHandler(animateWaitTimer);

					TimerHandler	hideWaitMsg = new TimerHandler(3,false,new ITimerCallback() {

						@Override
						public void onTimePassed(TimerHandler pTimerHandler) {

							t.setScale(0);
							e0.setScale(0);
							g.setScale(0);
							r.setScale(0);
							e1.setScale(0);
							r.setScale(0);
							e2.setScale(0);
							z.setScale(0);
							a.setScale(0);

							unregisterUpdateHandler(animateWaitTimer);

							unregisterEntityModifier(aS);
							unregisterEntityModifier(e0S);
							unregisterEntityModifier(e1S);
							unregisterEntityModifier(e2S);
							unregisterEntityModifier(tS);
							unregisterEntityModifier(gS);
							unregisterEntityModifier(rS);
							unregisterEntityModifier(zS);


						}

					});
					registerUpdateHandler(hideWaitMsg);

				}


			}

		});
		registerUpdateHandler(layEggTimer);

		TimerHandler	showEggTimer = new TimerHandler(5f,false,new ITimerCallback() {

			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {

				unregisterUpdateHandler(layEggTimer);	
				unregisterEntityModifier(layEgg0);
				unregisterEntityModifier(layEgg1);
				unregisterEntityModifier(layEgg2);
				unregisterEntityModifier(layEgg3);
				unregisterEntityModifier(layEgg4);
				unregisterEntityModifier(layEgg5);
			}

		});
		registerUpdateHandler(showEggTimer);


	}

	private void postMemInfo(){


		WarnText.setScale(0.75f);
		RAMinfo.setScale(0.75f);
		warning.setScale(1.25f);

		TimerHandler waitForWarning = new TimerHandler(17f,false,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
							
					warning.setScale(0);
				    WarnText.setScale(0);
				    RAMinfo.setScale(0);
			}

		});
		registerUpdateHandler(waitForWarning);
	}

	private void updateMemoryInfo(){

		TimerHandler MemCheckDaemon = new TimerHandler(20f,true,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {

				MemoryInfo mi = new MemoryInfo();
				ActivityManager activityManager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
				if(mi!=null){
					activityManager.getMemoryInfo(mi);
					long availableMegs = mi.availMem / 1048576L;

					final String badmemory="  RAM isigaye: "+availableMegs+" MB,ikenewe: 128 MB";
					if(availableMegs<128L){	
						RAMinfo.setText(badmemory);
						postMemInfo();
					}

				}

			}

		});
		registerUpdateHandler(MemCheckDaemon);
	}

	
	protected void createMenuScene() {

		this.eggMenuScene = new MenuScene(camera);

		final SpriteMenuItem resetMenuItem = new SpriteMenuItem(MENU_RESET,resourcesManager.reset_region,vbom);
		resetMenuItem.setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

		final SpriteMenuItem quitMenuItem = new SpriteMenuItem(MENU_QUIT,resourcesManager.back_region, vbom);
		quitMenuItem.setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

		final SpriteMenuItem cancelMenuItem = new SpriteMenuItem(MENU_CANCEL,resourcesManager.cancel_region, vbom);
		quitMenuItem.setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);



		this.eggMenuScene.addMenuItem(resetMenuItem);
		this.eggMenuScene.addMenuItem(cancelMenuItem);
		this.eggMenuScene.addMenuItem(quitMenuItem);


		this.eggMenuScene.buildAnimations();

		this.eggMenuScene.setBackgroundEnabled(false);


		this.eggMenuScene.setOnMenuItemClickListener(this);
	}



	@Override
	public boolean onMenuItemClicked(final MenuScene pMenuScene, final IMenuItem pMenuItem, final float pMenuItemLocalX, final float pMenuItemLocalY) {
		switch(pMenuItem.getID()) {
		case MENU_RESET:				
			resetEggMenu();
			return true;
		case MENU_CANCEL:
			cancelMenu();
			return true;
		case MENU_QUIT:
			/* End game mode. */
			exitEggGame();
			return true;
		default:
			return false;
		}
	}


	@Override
	public void onBackKeyPressed()
	{    
		showEggMenu();
	}

	@Override
	public void onMenuKeyPressed() {

		showEggMenu();

	}


	private void showEggMenu() {

		if(this.hasChildScene()) {
			// Remove the menu and reset it. 
			canvas.setScale(0);
			showmenu.setScale(0.5f);
			this.eggMenuScene.back();
			GameManager.getInstance().setPaused(false);
			SceneManager.getInstance().resumeGameModeBackMusic();

		} 
		else {
			// Attach the menu. 
			canvas.setScale(1f);
			showmenu.setScale(0);
			this.setChildScene(this.eggMenuScene, false, true, true);
			SceneManager.getInstance().pauseGameModeBackMusic();
			GameManager.getInstance().setPaused(true);
		}


	}


	private void resetEggMenu() {

		GameManager.getInstance().setPaused(false);
		canvas.setScale(0);
		showmenu.setScale(0.5f);
		SceneManager.getInstance().playPaintCongratulorySound(false);
		//resume music 
		SceneManager.getInstance().resumeGameModeBackMusic();		
		/* Remove the menu and reset it. */
		this.clearChildScene();
		this.eggMenuScene.reset();
		this.recycleScene();
		this.initEggLetterSprites();
		this.displayEggLetters();

	}

	private void cancelMenu(){
		canvas.setScale(0);
		showmenu.setScale(0.5f);
		this.clearChildScene();
		this.eggMenuScene.reset();
		GameManager.getInstance().setPaused(false);
		SceneManager.getInstance().resumeGameModeBackMusic();
	}

	private void exitEggGame() {

		//kick back to home menu
		GameManager.getInstance().resetEggGame();
		SceneManager.getInstance().stopGameModeBackMusic();
		SceneManager.getInstance().resumeMenuBackMusic();
		SceneManager.getInstance().loadMenuScene(engine);

	}



	@Override
	public SceneType getSceneType()
	{
		return SceneType.BUILD_SCENE_GAME;
	}

	@Override
	public void disposeScene()
	{

		this.clearEntityModifiers();
		this.clearUpdateHandlers();
		this.clearTouchAreas();
		this.clearChildScene();
		this.detachChildren(); //detach all children
		this.detachSelf();
		this.dispose();
		System.gc();



	}

}
