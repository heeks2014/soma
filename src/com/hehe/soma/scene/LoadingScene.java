
/**
 @company: Hehe Ltd
 @Project: Soma
 @Date: mm/dd/yyyy
 @Author: Amiri Mugarura and Sixbert Uwiringiyimana
 @Credits: thanks given to Richard Rusa and other artists for graphic designs ....

 @About this Class "LoadingScene.java":
   ----------------------------------------
 
 */



package com.hehe.soma.scene;

import org.andengine.entity.scene.background.Background;
import org.andengine.entity.text.Text;
import org.andengine.util.color.Color;

import com.hehe.soma.manager.SceneManager.SceneType;
import com.hehe.soma.scene.ParentScene;




public class LoadingScene extends ParentScene
{

	
	@Override
	public void createScene()
	{   
		//this background should be replace by a texture made of
		//an ad commercial message
		setBackground(new Background(Color.BLACK));
		attachChild(new Text(150,camera.getHeight()/2-resourcesManager.font.getLineHeight()/2, resourcesManager.font, "Tegereza gato ...", vbom));	
	}



	@Override
	public void onBackKeyPressed()
	{
		return; //we will not listen to no button while we load  game mode
	}

	@Override
	public SceneType getSceneType()
	{
		return SceneType.SCENE_LOADING;
	}

	@Override
	public void disposeScene()
	{
		//we just need to override this

	}

	@Override
	public void onMenuKeyPressed() {
		// TODO Auto-generated method stub

	}
}