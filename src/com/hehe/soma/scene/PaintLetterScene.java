

/**
 @company: Hehe Ltd
 @Project: Soma
 @Date: mm/dd/yyyy
 @Author: Amiri Mugarura and Sixbert Uwiringiyimana
 @Credits: thanks given to Richard Rusa and other artists for graphic designs ....

 @About this Class "PaintLetterScene.java":
   ----------------------------------------

 */


package com.hehe.soma.scene;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.util.GLState;
import org.andengine.util.color.Color;
import org.andengine.util.math.MathUtils;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.opengl.GLES20;
import android.widget.Toast;

import com.hehe.soma.manager.GameManager;
import com.hehe.soma.manager.SceneManager;
import com.hehe.soma.manager.SceneManager.SceneType;

public class PaintLetterScene extends ParentScene implements IOnMenuItemClickListener 
{  

	protected MenuScene paintMenuScene;
	protected static final int MENU_RESET = 0;
	protected static final int MENU_CANCEL=1;
	protected static final int MENU_QUIT =2;

	//game mode background
	private Sprite background,canvas,showmenu,finger,Radio_guide_arrow,Basket_guide_arrow,Floater_guide_arrow;
	private Sprite RadioPart0; //main radio front side
	private Sprite RadioPart1; //left speaker
	private Sprite RadioPart2; //right speaker
	private Sprite RadioPart3; //dial
	private Sprite RadioPart4;  //tape drive
	private Sprite RadioPart5;  //top side
	private Sprite RadioPart6; //right side


	private Sprite FloaterPart0,FloaterPart1,FloaterPart2,FloaterPart3;
	private Sprite BasketPart0,BasketPart1,BasketPart2,BasketPart3,BasketPart4;

	private Sprite palette;

	private Sprite circle0,circle1,circle2,circle3,circle4,circle5,circle6;

	private AnimatedSprite paintsupernka;
	private Text letter0,letter1,letter2,letter3,letter4,letter5,letter6;
	private Text Radioletter0,Radioletter1,Radioletter2,Radioletter4,Radioletter5,Radioletter6;
	private Text Floaterletter0,Floaterletter1,Floaterletter2,Floaterletter3;
	private Text Basketletter0,Basketletter1,Basketletter2,Basketletter3,Basketletter4;
	
	private Text WarnText,RAMinfo;
	private Sprite warning;
	//color variables
	Color c0=Color.SIXBERT, c1=Color.RICHARD,c2=Color.CLARISSE, c3=Color.GAKUBA, c4=Color.HEHE, c5=Color.IVAN,c6=Color.MUGARURA;

	private boolean isDemoRunning; //never initialize.
	private boolean isVictoryAchieved=false;
	private boolean isRadioSelected=false;
	private boolean isFloaterSelected=false;

	private boolean isBasketSelected=false;
	private boolean isRadioFinished=false;
	private boolean isBasketFinished=false;
	private boolean isFloaterFinished=false;
	private boolean isMovementLocked=false;

	private boolean hasBasketBeenPointedAt;
	private boolean hasFloaterBeenPointedAt;
	private boolean hasRadioBeenPointedAt;

	private boolean radioPlayed;
	private boolean basketPlayed;
	private boolean floaterPlayed;

	private int objectid=0;

	private String ObjectToPaint;
	private String CircleLetter;
	//initial positions of circles
	private final float initXCircle=2.5f;

	private final float initYCircle0=0;
	private final float initYCircle1=65;
	private final float initYCircle2=125;
	private final float initYCircle3=185;
	private final float initYCircle4=250;
	private final float initYCircle5=310;
	private final float initYCircle6=375;

	private final float initXFloaterPart0=230;
	private final float initYFloaterPart0=230;

	private final float initXFloaterPart1=243;
	private final float initYFloaterPart1=223;

	private final float initXFloaterPart2=290;
	private final float initYFloaterPart2=230;

	private final float initXFloaterPart3=240;
	private final float initYFloaterPart3=263;

	//final position of flowers and pots
	private final float finalXFloaterPart0=323;
	private final float finalYFloaterPart0=210;

	private final float finalXFloaterPart1=408;
	private final float finalYFloaterPart1=147;

	private final float finalXFloaterPart2=533;
	private final float finalYFloaterPart2=205;

	private final float finalXFloaterPart3=405;
	private final float finalYFloaterPart3=300;


	//initial positions of letters
	private final float initXCircleLetter=20;

	/*position the flowers and their pots 
	 * */
	private final float initXFloaterLetter0=300;
	private final float initYFloaterLetter0=200;

	private final float initXFloaterLetter1=450;
	private final float initYFloaterLetter1=130;

	private final float initXFloaterLetter2=540;
	private final float initYFloaterLetter2=200;

	private final float initXFloaterLetter3=400;
	private final float initYFloaterLetter3=280;



	//initial position parts of the basket

	private final float initXBasket=100;
	private final float initYBasketPart0=200;
	private final float initYBasketPart1=352;
	private final float initYBasketPart2=386;
	private final float initYBasketPart3=416;
	private final float initYBasketPart4=450;

	//final position of basket after zoom and move

	private final float finalXBasket=400;
	private final float finalYBasketPart0=22;
	private final float finalYBasketPart1=220;
	private final float finalYBasketPart2=273;
	private final float finalYBasketPart3=320;
	private final float finalYBasketPart4=370;


	//initial positions of parts of the radio
	private final float initXRadioPart0=510;
	private final float initXRadioPart1=518;
	private final float initXRadioPart2=605;
	private final float initXRadioPart3=522;
	private final float initXRadioPart4=558;
	private final float initXRadioPart5=510;
	private final float initXRadioPart6=647;

	private final float initYRadioPart0=240;
	private final float initYRadioPart1=267;
	private final float initYRadioPart2=268;
	private final float initYRadioPart3=245;
	private final float initYRadioPart4=267;
	private final float initYRadioPart5=207;
	private final float initYRadioPart6=226;

	//final positions of parts of the radio

	private final float finalXRadioPart0=300; //front
	private final float finalXRadioPart1=242; //left speaker

	private final float finalXRadioPart2=461; //right speaker
	private final float finalXRadioPart3=313;  //dial

	private final float finalXRadioPart4=345; //tape drive
	private final float finalXRadioPart5=322; //top side
	private final float finalXRadioPart6=557; //right side

	private final float finalYRadioPart0=200; //front
	private final float finalYRadioPart1=235; //left speaker

	private final float finalYRadioPart2=246; //right speaker
	private final float finalYRadioPart3=150; //dial

	private final float finalYRadioPart4=222; //tape
	private final float finalYRadioPart5=75; //top
	private final float finalYRadioPart6=180; //right

	//basket letters
	private final float initXBasketLetter=460;

	//circles' colors
	int randomCircleColor0;
	int randomCircleColor1;
	int randomCircleColor2;
	int randomCircleColor3;
	int randomCircleColor4;
	int randomCircleColor5;
	int randomCircleColor6;

	//variables to hold circle random letters  
	private String randLetter0;
	private String randLetter1;
	private String randLetter2;
	private String randLetter3;
	private String randLetter4;
	private String randLetter5;
	private String randLetter6;
	//radio set parts letters
	private String radioLetter0;
	private String radioLetter1;
	private String radioLetter2;
	private String radioLetter3;
	private String radioLetter4;
	private String radioLetter5;
	private String radioLetter6;
	//the basket letters
	private String basketLetter0;
	private String basketLetter1;
	private String basketLetter2;
	private String basketLetter3;
	private String basketLetter4;


	//variables to hold letters displayed on Floater's parts
	private String floaterLetter0;
	private String floaterLetter1;

	private String floaterLetter2;
	private String floaterLetter3;

	//these variables will help to check for radio letters matching victory
	private boolean isMatchedRadioLetter0;
	private boolean isMatchedRadioLetter1;
	private boolean isMatchedRadioLetter2;
	private boolean isMatchedRadioLetter4;
	private boolean isMatchedRadioLetter5;
	private boolean isMatchedRadioLetter6;
	//victory check for agaseke
	private boolean isMatchedBasketLetter0;
	private boolean isMatchedBasketLetter1;
	private boolean isMatchedBasketLetter2;
	private boolean isMatchedBasketLetter3;
	private boolean isMatchedBasketLetter4;

	//for Floater
	private boolean isMatchedFloaterLetter0;
	private boolean isMatchedFloaterLetter1;

	private boolean isMatchedFloaterLetter2;
	private boolean isMatchedFloaterLetter3;

	private boolean playAgain;



	//get radio letters from a GameManager's getter
	private String Radio_child0;
	private String Radio_child1;
	private String Radio_child2;
	private String Radio_child4;
	private String Radio_child5;
	private String Radio_child6;

	// then  basket letters 
	private String Basket_child0;
	private String Basket_child1;
	private String Basket_child2;
	private String Basket_child3;
	private String Basket_child4;


	// Floater parts letters in later turn

	private String Floater_child0;
	private String Floater_child1;
	private String Floater_child2;
	private String Floater_child3;

	ParallelEntityModifier CombR0,CombR1,CombR2,CombR3,CombR4,CombR5,CombR6;
	ParallelEntityModifier CombF0,CombV0,CombF1,CombV1;
	ParallelEntityModifier CombB0,CombB1,CombB2,CombB3,CombB4;

	TimerHandler updateR;
	TimerHandler updateF;
	TimerHandler updateB;
	TimerHandler waitForNackToFinish;

	//variables for demo
	MoveModifier toRadio;	
	MoveModifier RadiotoCircle;
	MoveModifier BacktoRadio;
	AlphaModifier ShowArrow;


	@Override
	public void createScene()
	{   
		engine.enableVibrator(activity);
		createMenuScene();
		SceneManager.getInstance().playGameModeBackMusic();
		createStaticSprites();

		SceneManager.getInstance().playPaintGuidanceAudio();
		loadSprites();
		attachSprites();
		doDemo();
		updateMemoryInfo();
	}

	@Override
	public void disposeScene()
	{   	
		/*this is to release memory resources and
		 *  */
		this.clearEntityModifiers();
		this.clearUpdateHandlers();
		this.clearTouchAreas();
		this.clearChildScene();
		this.detachChildren(); //detach all children
		this.detachSelf();
		this.dispose();
		System.gc();

	}



	@Override
	public SceneType getSceneType()
	{
		return SceneType.PAINT_SCENE_GAME;
	}



	private void createStaticSprites()
	{
		
		
		background=new Sprite(0, 0, resourcesManager.paint_background_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		background.setZIndex(0);
		background.setScale(1.0f);
		background.setPosition(0,0);
		attachChild(background);
		
		warning=new Sprite(0, 0, resourcesManager.showwarn_region, vbom)
		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   

				if (pSceneTouchEvent.isActionUp())
				{   
					WarnText.setScale(0);
					RAMinfo.setScale(0);
					warning.setScale(0);
				

				}
				return true;
			};
		};
		warning.setPosition(200,camera.getHeight()-warning.getHeight()-25);
		attachChild(warning);
		registerTouchArea(warning);
		warning.setScale(0);

		WarnText=new Text(warning.getX()+warning.getWidth()-15,camera.getHeight()-warning.getHeight()-50, resourcesManager.WarnFont,
				"Telefone yawe ifite ibyangombwa bidahagije!\n Funga izindi apps udakeneye cyangwa\n ujye usubiramo SOMA nijya yifunga.", vbom);	
		attachChild(WarnText);	
		WarnText.setScale(0);

		RAMinfo=new Text(warning.getX()+warning.getWidth()-15,camera.getHeight()-warning.getHeight()-55+WarnText.getHeight(), resourcesManager.WarnFont,
				"  RAM isigaye: 100 MB,ikenewe: 128 MB ", vbom);	
		attachChild(RAMinfo);	
		RAMinfo.setScale(0);
		
		RAMinfo.setZIndex(4);
		WarnText.setZIndex(4);
		warning.setZIndex(4);

		this.canvas=new Sprite(0, 0, resourcesManager.canvas_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		canvas.setScale(0);
		canvas.setColor(0,0.95f, 0.95f,0.75f);
		canvas.setZIndex(4);
		canvas.setPosition(camera.getCenterX()-250,camera.getCenterY()-175);
		attachChild(canvas);

		finger=new Sprite(0,0, resourcesManager.Finger_region, vbom){
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

		};
		finger.setScale(1);
		finger.setPosition(camera.getWidth(),camera.getHeight());
		finger.setZIndex(7);
		attachChild(finger);

		Radio_guide_arrow=new Sprite(0,0, resourcesManager.paint_arrow_region, vbom){
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

		};
		Radio_guide_arrow.setPosition(camera.getWidth()-200,camera.getHeight()-150);
		Radio_guide_arrow.setZIndex(4);
		attachChild(Radio_guide_arrow);

		Floater_guide_arrow=new Sprite(0,0, resourcesManager.paint_arrow_region, vbom){
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

		};
		Floater_guide_arrow.setPosition(200,180);
		Floater_guide_arrow.setZIndex(4);
		Floater_guide_arrow.setRotation(150);
		attachChild(Floater_guide_arrow);

		Basket_guide_arrow=new Sprite(0,0, resourcesManager.paint_arrow_region, vbom){
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

		};
		Basket_guide_arrow.setPosition(240,camera.getHeight()-100);
		Basket_guide_arrow.setZIndex(4);
		Basket_guide_arrow.setRotation(270);
		attachChild(Basket_guide_arrow);

		this.showmenu=new Sprite(0, 0, resourcesManager.showmenu_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   

				if (pSceneTouchEvent.isActionUp())
				{   
					if(!isDemoRunning){
						showPaintMenu();
						SceneManager.getInstance().playOnSubMenuButtonClickSound();
						SceneManager.getInstance().vibrate(200);
					}
					else{

						SceneManager.getInstance().vibrate(100);
					}

				}

				if (pSceneTouchEvent.isActionDown())
				{  



				}
				return true;
			};
		};
		showmenu.setScale(0.5f);
		showmenu.setAlpha(0.5f);
		showmenu.setZIndex(4);
		showmenu.setPosition(camera.getWidth()-80,camera.getHeight()-80);
		attachChild(showmenu);
		registerTouchArea(showmenu);


		//color palette sprite initialization
		palette=new Sprite(0,0, resourcesManager.palette_region, vbom){
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

		};
		palette.setScale(0);
		palette.setPosition(0,0);
		palette.setZIndex(1);
		attachChild(palette);

		//init supercow

		paintsupernka=new AnimatedSprite(0, 0, resourcesManager.SupaNka_region, vbom){
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		paintsupernka.setPosition(camera.getCenterX()-100,camera.getCenterY()-120);
		attachChild(paintsupernka);
		paintsupernka.setScale(1);

	}


	private void loadSprites(){

		setTouchAreaBindingOnActionMoveEnabled(true);
		setTouchAreaBindingOnActionDownEnabled(true);

		final int ListID_CircleLetter0=MathUtils.random(0,14);
		final int ListID_CircleLetter1=MathUtils.random(110,220);
		final int ListID_CircleLetter2=MathUtils.random(30,100);
		final int ListID_CircleLetter3=MathUtils.random(70,140);
		final int ListID_CircleLetter4=MathUtils.random(80,170);
		final int ListID_CircleLetter5=MathUtils.random(50,90);
		final int ListID_CircleLetter6=MathUtils.random(90,120);

		final int LetterS_id=MathUtils.random(0,1);
		final int LetterP_id=MathUtils.random(2,3);
		final int LetterO_id=MathUtils.random(4,5);
		final int LetterY_id=MathUtils.random(6,7);
		final int LetterW_id=MathUtils.random(8,9);
		final int LetterC_id=MathUtils.random(10,11);
		final int LetterZ_id=MathUtils.random(12,13);
		final int LetterK_id=MathUtils.random(14,15);
		final int LetterV_id=MathUtils.random(16,17);

		final int randomCircleLetter0=MathUtils.random(0,4);
		final int randomCircleLetter1=MathUtils.random(5,9);
		final int randomCircleLetter2=MathUtils.random(10,13);
		final int randomCircleLetter3=MathUtils.random(14,17);
		final int randomCircleLetter4=MathUtils.random(18,21);
		final int randomCircleLetter5=MathUtils.random(22,25);
		final int randomCircleLetter6=MathUtils.random(26,29);

		radioPlayed=false;
		basketPlayed=false;
		floaterPlayed=false;
		objectid=0;
		playAgain=true;
		final int colorOrder=MathUtils.random(0,1);

		if(colorOrder==0){
			randomCircleColor0=0;
			randomCircleColor1=1;
			randomCircleColor2=2;
			randomCircleColor3=3;
			randomCircleColor4=4;
			randomCircleColor5=5;
			randomCircleColor6=MathUtils.random(6,7);
		}
		else{
			randomCircleColor0=MathUtils.random(0,1);
			randomCircleColor1=2;
			randomCircleColor2=3;
			randomCircleColor3=4;
			randomCircleColor4=5;
			randomCircleColor5=6;
			randomCircleColor6=7;	 
		}

		final int randomObjectLetter0=MathUtils.random(0,1);
		final int randomObjectLetter1=MathUtils.random(2,3);
		final int randomObjectLetter2=MathUtils.random(4,5);
		final int randomObjectLetter3=MathUtils.random(5,6);
		final int randomObjectLetter4=MathUtils.random(0,3);
		final int randomObjectLetter5=MathUtils.random(2,5);
		final int randomObjectLetter6=MathUtils.random(4,6);

		//start by randomly loading letter characters with specified font
		if(ListID_CircleLetter0%2==0){
			randLetter0=GameManager.getInstance().getRandomCircleLetter(0,randomCircleLetter0);
		}
		else{
			randLetter0=GameManager.getInstance().getRandomCircleLetter(1,LetterV_id);	
		}

		GameManager.getInstance().setCircleLetter(0,randLetter0);
		letter0=new Text(initXCircleLetter,initYCircle0, resourcesManager.CirclepaintFont,randLetter0, vbom)
		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (pSceneTouchEvent.isActionUp()) {

				}

				if (pSceneTouchEvent.isActionDown()) {
					zoomCircle(0,1.75f);
				}
				return true;
			};
		};

		if(ListID_CircleLetter1%2==0){
			randLetter1=GameManager.getInstance().getRandomCircleLetter(0,randomCircleLetter1);
		}
		else{
			randLetter1=GameManager.getInstance().getRandomCircleLetter(1,LetterP_id);	
		}

		GameManager.getInstance().setCircleLetter(1,randLetter1);
		letter1=new Text(initXCircleLetter,initYCircle1, resourcesManager.CirclepaintFont,randLetter1, vbom){
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (pSceneTouchEvent.isActionUp()) {

				}

				if (pSceneTouchEvent.isActionDown()) {
					zoomCircle(0,1.75f);
				}
				return true;
			};
		};
		if(ListID_CircleLetter2%2==0){
			randLetter2=GameManager.getInstance().getRandomCircleLetter(0,randomCircleLetter2);
		}
		else{
			randLetter2=GameManager.getInstance().getRandomCircleLetter(1,LetterS_id);	
		}
		GameManager.getInstance().setCircleLetter(2,randLetter2);
		letter2=new Text(initXCircleLetter,initYCircle2, resourcesManager.CirclepaintFont,randLetter2, vbom){
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (pSceneTouchEvent.isActionUp()) {

				}

				if (pSceneTouchEvent.isActionDown()) {
					zoomCircle(0,1.75f);
				}
				return true;
			};
		};
		if(ListID_CircleLetter3%2==0){
			randLetter3=GameManager.getInstance().getRandomCircleLetter(0,randomCircleLetter3);
		}
		else{
			randLetter3=GameManager.getInstance().getRandomCircleLetter(1,LetterY_id);	
		}
		GameManager.getInstance().setCircleLetter(3,randLetter3);
		letter3=new Text(initXCircleLetter,initYCircle3, resourcesManager.CirclepaintFont,randLetter3, vbom){
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (pSceneTouchEvent.isActionUp()) {

				}

				if (pSceneTouchEvent.isActionDown()) {
					zoomCircle(0,1.75f);
				}
				return true;
			};
		};
		if(ListID_CircleLetter4%2==0){
			randLetter4=GameManager.getInstance().getRandomCircleLetter(0,randomCircleLetter4);
		}
		else{
			randLetter4=GameManager.getInstance().getRandomCircleLetter(1,LetterK_id);	
		}

		GameManager.getInstance().setCircleLetter(4,randLetter4);
		letter4=new Text(initXCircleLetter,initYCircle4, resourcesManager.CirclepaintFont,randLetter4, vbom){
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (pSceneTouchEvent.isActionUp()) {

				}

				if (pSceneTouchEvent.isActionDown()) {
					zoomCircle(0,1.75f);
				}
				return true;
			};
		};
		if(ListID_CircleLetter5%2==0){
			randLetter5=GameManager.getInstance().getRandomCircleLetter(0,randomCircleLetter5);
		}
		else if(ListID_CircleLetter5%3==0){
			randLetter5=GameManager.getInstance().getRandomCircleLetter(0,LetterZ_id);	
		}
		else{
			randLetter5=GameManager.getInstance().getRandomCircleLetter(0,LetterC_id);	

		}
		GameManager.getInstance().setCircleLetter(5,randLetter5);
		letter5=new Text(initXCircleLetter,initYCircle5, resourcesManager.CirclepaintFont,randLetter5, vbom){
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (pSceneTouchEvent.isActionUp()) {

				}

				if (pSceneTouchEvent.isActionDown()) {
					zoomCircle(0,1.75f);
				}
				return true;
			};
		};   
		if(ListID_CircleLetter6%2==0){
			randLetter6=GameManager.getInstance().getRandomCircleLetter(0,randomCircleLetter6);
		}
		else if(ListID_CircleLetter6%2==0){
			randLetter6=GameManager.getInstance().getRandomCircleLetter(1,LetterW_id);	
		}
		else{
			randLetter6=GameManager.getInstance().getRandomCircleLetter(1,LetterO_id);	
		}
		GameManager.getInstance().setCircleLetter(6,randLetter6);
		letter6=new Text(initXCircleLetter,initYCircle6, resourcesManager.CirclepaintFont,randLetter6, vbom){
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X,float Y) {

				if (pSceneTouchEvent.isActionUp()) {

				}

				if (pSceneTouchEvent.isActionDown()) {
					zoomCircle(0,1.75f);
				}
				return true;
			};
		};

		//then our objects' letters
		//N.B: capitalized variable is a Text type while lower case is a just a string
		radioLetter0=GameManager.getInstance().getRandomObjectLetter(randomObjectLetter0);
		GameManager.getInstance().setObjectPartLetter("radio",0,radioLetter0);
		Radioletter0=new Text(360,290, resourcesManager.ObjectpaintFont,radioLetter0, vbom);

		radioLetter1=GameManager.getInstance().getRandomObjectLetter(randomObjectLetter1);		
		GameManager.getInstance().setObjectPartLetter("radio",1,radioLetter1);
		Radioletter1=new Text(250,222, resourcesManager.ObjectpaintFont,radioLetter1, vbom);

		radioLetter2=GameManager.getInstance().getRandomObjectLetter(randomObjectLetter3);	
		GameManager.getInstance().setObjectPartLetter("radio",2,radioLetter2);
		Radioletter2=new Text(475,230, resourcesManager.ObjectpaintFont,radioLetter2, vbom);

		radioLetter3=GameManager.getInstance().getRandomObjectLetter(randomObjectLetter6);
		GameManager.getInstance().setObjectPartLetter("radio",3,radioLetter3);

		radioLetter4=GameManager.getInstance().getRandomObjectLetter(randomObjectLetter2);	
		GameManager.getInstance().setObjectPartLetter("radio",4,radioLetter4);
		Radioletter4=new Text(350,220, resourcesManager.ObjectpaintFont,radioLetter4, vbom);	

		radioLetter5=GameManager.getInstance().getRandomObjectLetter(randomObjectLetter4);	
		GameManager.getInstance().setObjectPartLetter("radio",5,radioLetter5);
		Radioletter5=new Text(380,75, resourcesManager.ObjectpaintFont,radioLetter5, vbom);

		radioLetter6=GameManager.getInstance().getRandomObjectLetter(randomObjectLetter5);
		GameManager.getInstance().setObjectPartLetter("radio",6,radioLetter6);
		Radioletter6=new Text(560,230, resourcesManager.ObjectpaintFont,radioLetter6, vbom);

		//then basket letter
		basketLetter0=GameManager.getInstance().getRandomObjectLetter(randomObjectLetter0);
		GameManager.getInstance().setObjectPartLetter("agaseke",0,basketLetter0);
		Basketletter0=new Text(initXBasketLetter,finalYBasketPart0+90, resourcesManager.ObjectpaintFont,basketLetter0, vbom);

		basketLetter1=GameManager.getInstance().getRandomObjectLetter(randomObjectLetter5);
		GameManager.getInstance().setObjectPartLetter("agaseke",1,basketLetter1);
		Basketletter1=new Text(initXBasketLetter+20,finalYBasketPart1-20, resourcesManager.ObjectpaintFont,basketLetter1, vbom);

		basketLetter2=GameManager.getInstance().getRandomObjectLetter(randomObjectLetter3);
		GameManager.getInstance().setObjectPartLetter("agaseke",2,basketLetter2);
		Basketletter2=new Text(initXBasketLetter-10,finalYBasketPart2-10, resourcesManager.ObjectpaintFont,basketLetter2, vbom);

		basketLetter3=GameManager.getInstance().getRandomObjectLetter(randomObjectLetter2);
		GameManager.getInstance().setObjectPartLetter("agaseke",3,basketLetter3);
		Basketletter3=new Text(initXBasketLetter+20,finalYBasketPart3-10, resourcesManager.ObjectpaintFont,basketLetter3, vbom);

		basketLetter4=GameManager.getInstance().getRandomObjectLetter(randomObjectLetter4);
		GameManager.getInstance().setObjectPartLetter("agaseke",4,basketLetter4);
		Basketletter4=new Text(initXBasketLetter-15,finalYBasketPart4-20, resourcesManager.ObjectpaintFont,basketLetter4, vbom);


		floaterLetter0=GameManager.getInstance().getRandomObjectLetter(randomObjectLetter6);
		GameManager.getInstance().setObjectPartLetter("agapuliso",0,floaterLetter0);		
		Floaterletter0=new Text(initXFloaterLetter0+10,initYFloaterLetter0, resourcesManager.ObjectpaintFont,floaterLetter0, vbom);

		floaterLetter1=GameManager.getInstance().getRandomObjectLetter(randomObjectLetter3);
		GameManager.getInstance().setObjectPartLetter("agapuliso",1,floaterLetter1);	
		Floaterletter1=new Text(initXFloaterLetter1,initYFloaterLetter1-10, resourcesManager.ObjectpaintFont,floaterLetter1, vbom);

		floaterLetter2=GameManager.getInstance().getRandomObjectLetter(randomObjectLetter1);
		GameManager.getInstance().setObjectPartLetter("agapuliso",2,floaterLetter2);		
		Floaterletter2=new Text(initXFloaterLetter2,initYFloaterLetter2, resourcesManager.ObjectpaintFont,floaterLetter2, vbom);

		floaterLetter3=GameManager.getInstance().getRandomObjectLetter(randomObjectLetter0);
		GameManager.getInstance().setObjectPartLetter("agapuliso",3,floaterLetter3);
		Floaterletter3=new Text(initXFloaterLetter3,initYFloaterLetter3+10, resourcesManager.ObjectpaintFont,floaterLetter3, vbom);




		//retrieve letters from game manager object
		Radio_child0=GameManager.getInstance().getObjectPartLetter("radio",0);
		Radio_child1=GameManager.getInstance().getObjectPartLetter("radio",1);
		Radio_child2=GameManager.getInstance().getObjectPartLetter("radio",2);
		Radio_child4=GameManager.getInstance().getObjectPartLetter("radio",4);
		Radio_child5=GameManager.getInstance().getObjectPartLetter("radio",5);
		Radio_child6=GameManager.getInstance().getObjectPartLetter("radio",6);

		// then  basket letters 
		Basket_child0=GameManager.getInstance().getObjectPartLetter("agaseke",0);
		Basket_child1=GameManager.getInstance().getObjectPartLetter("agaseke",1);
		Basket_child2=GameManager.getInstance().getObjectPartLetter("agaseke",2);
		Basket_child3=GameManager.getInstance().getObjectPartLetter("agaseke",3);
		Basket_child4=GameManager.getInstance().getObjectPartLetter("agaseke",4);

		// Flowers' letters in later turn

		Floater_child0=GameManager.getInstance().getObjectPartLetter("agapuliso",0);
		Floater_child1=GameManager.getInstance().getObjectPartLetter("agapuliso",1);
		Floater_child2=GameManager.getInstance().getObjectPartLetter("agapuliso",2);
		Floater_child3=GameManager.getInstance().getObjectPartLetter("agapuliso",3);



		/*declare, initialize, and register color circle sprites by adding the to colors linkedlist/array(well this turned out to
        be dysfunctional...going the rookie way of declaring each circle as a separate sprite variable)*/



		circle0=new Sprite(0, 0, resourcesManager.circle_region, vbom){
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   
				circle0.setPosition(pSceneTouchEvent.getX() - circle0.getWidth() / 2, pSceneTouchEvent.getY() - circle0.getHeight()/ 2);
				letter0.setPosition(pSceneTouchEvent.getX() - letter0.getWidth() / 2, pSceneTouchEvent.getY() - letter0.getHeight()/ 2);
				if (pSceneTouchEvent.isActionUp())
				{   

					CircleLetter=GameManager.getInstance().getPaintCircleLetter(0);
					ObjectToPaint=GameManager.getInstance().getObject2Paint();

					if("radio".equalsIgnoreCase(ObjectToPaint)) {
						if(circle0.collidesWith(RadioPart0)&&CircleLetter.equals(Radio_child0)&&!isMatchedRadioLetter0) {
							RadioPart0.setColor(GameManager.getInstance().getPaintCircleColor(0));
							zoomRadioLetters(0,0);
							RadioPart0.setAlpha(1);
							isMatchedRadioLetter0=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle0.collidesWith(RadioPart1)&&CircleLetter.equals(Radio_child1)&&!isMatchedRadioLetter1) {
							RadioPart1.setColor(GameManager.getInstance().getPaintCircleColor(0));
							zoomRadioLetters(0,1);
							RadioPart1.setAlpha(1);
							isMatchedRadioLetter1=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle0.collidesWith(RadioPart2)&&CircleLetter.equals(Radio_child2)&&!isMatchedRadioLetter2) {
							RadioPart2.setColor(GameManager.getInstance().getPaintCircleColor(0));
							zoomRadioLetters(0,2);RadioPart2.setAlpha(1);
							isMatchedRadioLetter2=true;

							SceneManager.getInstance().playPaintOkSound();
						}

						else if(circle0.collidesWith(RadioPart4)&&CircleLetter.equals(Radio_child4)&&!isMatchedRadioLetter4) {
							RadioPart4.setColor(GameManager.getInstance().getPaintCircleColor(0));
							zoomRadioLetters(0,4);RadioPart4.setAlpha(1);
							isMatchedRadioLetter4=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle0.collidesWith(RadioPart5)&&CircleLetter.equals(Radio_child5)&&!isMatchedRadioLetter5) {
							RadioPart5.setColor(GameManager.getInstance().getPaintCircleColor(0));
							zoomRadioLetters(0,5);RadioPart5.setAlpha(1);
							isMatchedRadioLetter5=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else  if(circle0.collidesWith(RadioPart6)&&CircleLetter.equals(Radio_child6)&&!isMatchedRadioLetter6) {
							RadioPart6.setColor(GameManager.getInstance().getPaintCircleColor(0));
							zoomRadioLetters(0,6);
							RadioPart6.setAlpha(1);
							isMatchedRadioLetter6=true;

							SceneManager.getInstance().playPaintOkSound();
						}

						else {
							SceneManager.getInstance().vibrate(100);
							if(playAgain){
								SceneManager.getInstance().playNacksound();
							}
							playAgain=false;	

							waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

								@Override
								public void onTimePassed(TimerHandler pTimerHandler) {
									playAgain=true;
								}
							});

							this.registerUpdateHandler(waitForNackToFinish);
						}
					}//endif radio
					//--------------------------------------------------
					else if("agaseke".equalsIgnoreCase(ObjectToPaint)){

						if(circle0.collidesWith(BasketPart0)&&CircleLetter.equals(Basket_child0)&&!isMatchedBasketLetter0) {
							BasketPart0.setColor(GameManager.getInstance().getPaintCircleColor(0));
							zoomBasketLetters(0,0);BasketPart0.setAlpha(1);
							isMatchedBasketLetter0=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle0.collidesWith(BasketPart1)&&CircleLetter.equals(Basket_child1)&&!isMatchedBasketLetter1) {
							BasketPart1.setColor(GameManager.getInstance().getPaintCircleColor(0));
							zoomBasketLetters(0,1);BasketPart1.setAlpha(1);
							isMatchedBasketLetter1=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle0.collidesWith(BasketPart2)&&CircleLetter.equals(Basket_child2)&&!isMatchedBasketLetter2) {
							BasketPart2.setColor(GameManager.getInstance().getPaintCircleColor(0));
							zoomBasketLetters(0,2);BasketPart2.setAlpha(1);
							isMatchedBasketLetter2=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle0.collidesWith(BasketPart3)&&CircleLetter.equals(Basket_child3)&&!isMatchedBasketLetter3) {
							BasketPart3.setColor(GameManager.getInstance().getPaintCircleColor(0));
							zoomBasketLetters(0,3);BasketPart3.setAlpha(1);
							isMatchedBasketLetter3=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle0.collidesWith(BasketPart4)&&CircleLetter.equals(Basket_child4)&&!isMatchedBasketLetter4) {
							BasketPart4.setColor(GameManager.getInstance().getPaintCircleColor(0));
							zoomBasketLetters(0,4);
							BasketPart4.setAlpha(1);
							isMatchedBasketLetter4=true;

							SceneManager.getInstance().playPaintOkSound();
						}


						else {SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});

						this.registerUpdateHandler(waitForNackToFinish);
						}

					}
					else if("agapuliso".equalsIgnoreCase(ObjectToPaint)){

						if(circle0.collidesWith(FloaterPart0)&&CircleLetter.equals(Floater_child0)&&!isMatchedFloaterLetter0) {
							FloaterPart0.setColor(GameManager.getInstance().getPaintCircleColor(0));
							zoomFloaterLetters(0,0);
							FloaterPart0.setAlpha(1);
							isMatchedFloaterLetter0=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle0.collidesWith(FloaterPart1)&&CircleLetter.equals(Floater_child1)&&!isMatchedFloaterLetter1) {
							FloaterPart1.setColor(GameManager.getInstance().getPaintCircleColor(0));
							zoomFloaterLetters(0,1);
							FloaterPart1.setAlpha(1);
							isMatchedFloaterLetter1=true;

							SceneManager.getInstance().playPaintOkSound();
						}

						else if(circle0.collidesWith(FloaterPart2)&&CircleLetter.equals(Floater_child2)&&!isMatchedFloaterLetter2) {
							FloaterPart2.setColor(GameManager.getInstance().getPaintCircleColor(0));
							zoomFloaterLetters(0,2);
							FloaterPart2.setAlpha(1);	
							isMatchedFloaterLetter2=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle0.collidesWith(FloaterPart3)&&CircleLetter.equals(Floater_child3)&&!isMatchedFloaterLetter3) {
							FloaterPart3.setColor(GameManager.getInstance().getPaintCircleColor(0));
							zoomFloaterLetters(0,3);
							FloaterPart3.setAlpha(1);
							isMatchedFloaterLetter3=true;

							SceneManager.getInstance().playPaintOkSound();
						}


						else {SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});

						this.registerUpdateHandler(waitForNackToFinish);
						}

					}
					else {
						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});

						this.registerUpdateHandler(waitForNackToFinish);
					}

					circle0.setPosition(initXCircle,initYCircle0);
					letter0.setPosition(initXCircleLetter,initYCircle0);
					//always check for victory 
					checkVictory();
				}


				if (pSceneTouchEvent.isActionDown())
				{  

					zoomCircle(0,1.75f);
				}
				return true;
			};
		};
		circle0.setPosition(this.initXCircle,this.initYCircle0);

		//---------------------------------------------------------------------
		circle1=new Sprite(0, 0, resourcesManager.circle_region, vbom){
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   
				circle1.setPosition(pSceneTouchEvent.getX() - circle1.getWidth() / 2, pSceneTouchEvent.getY() - circle1.getHeight()/ 2);
				letter1.setPosition(pSceneTouchEvent.getX() - letter1.getWidth() / 2, pSceneTouchEvent.getY() - letter1.getHeight()/ 2);

				if (pSceneTouchEvent.isActionUp())
				{   

					CircleLetter=GameManager.getInstance().getPaintCircleLetter(1);
					ObjectToPaint=GameManager.getInstance().getObject2Paint();

					if("radio".equalsIgnoreCase(ObjectToPaint)) {
						if(circle1.collidesWith(RadioPart0)&&CircleLetter.equals(Radio_child0)&&!isMatchedRadioLetter0) {
							RadioPart0.setColor(GameManager.getInstance().getPaintCircleColor(1));
							zoomRadioLetters(0,0);RadioPart0.setAlpha(1);
							isMatchedRadioLetter0=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle1.collidesWith(RadioPart1)&&CircleLetter.equals(Radio_child1)&&!isMatchedRadioLetter1) {
							RadioPart1.setColor(GameManager.getInstance().getPaintCircleColor(1));
							zoomRadioLetters(0,1);RadioPart1.setAlpha(1);
							isMatchedRadioLetter1=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle1.collidesWith(RadioPart2)&&CircleLetter.equals(Radio_child2)&&!isMatchedRadioLetter2) {
							RadioPart2.setColor(GameManager.getInstance().getPaintCircleColor(1));
							zoomRadioLetters(0,2);RadioPart2.setAlpha(1);
							isMatchedRadioLetter2=true;

							SceneManager.getInstance().playPaintOkSound();
						}

						else if(circle1.collidesWith(RadioPart4)&&CircleLetter.equals(Radio_child4)&&!isMatchedRadioLetter4) {
							RadioPart4.setColor(GameManager.getInstance().getPaintCircleColor(1));
							zoomRadioLetters(0,4);
							RadioPart4.setAlpha(1);
							isMatchedRadioLetter4=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle1.collidesWith(RadioPart5)&&CircleLetter.equals(Radio_child5)&&!isMatchedRadioLetter5) {
							RadioPart5.setColor(GameManager.getInstance().getPaintCircleColor(1));
							zoomRadioLetters(0,5);RadioPart5.setAlpha(1);
							isMatchedRadioLetter5=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else  if(circle1.collidesWith(RadioPart6)&&CircleLetter.equals(Radio_child6)&&!isMatchedRadioLetter6) {
							RadioPart6.setColor(GameManager.getInstance().getPaintCircleColor(1));
							zoomRadioLetters(0,6);RadioPart6.setAlpha(1);
							isMatchedRadioLetter6=true;

							SceneManager.getInstance().playPaintOkSound();
						}

						else {
							SceneManager.getInstance().vibrate(100);
							if(playAgain){
								SceneManager.getInstance().playNacksound();
							}
							playAgain=false;	

							waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

								@Override
								public void onTimePassed(TimerHandler pTimerHandler) {
									playAgain=true;
								}
							});

							this.registerUpdateHandler(waitForNackToFinish);
						}
					}//endif radio
					//--------------------------------------------------
					else if("agaseke".equalsIgnoreCase(ObjectToPaint)){
						if(circle1.collidesWith(BasketPart0)&&CircleLetter.equals(Basket_child0)&&!isMatchedBasketLetter0) {
							BasketPart0.setColor(GameManager.getInstance().getPaintCircleColor(1));
							zoomBasketLetters(0,0);
							BasketPart0.setAlpha(1);
							isMatchedBasketLetter0=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle1.collidesWith(BasketPart1)&&CircleLetter.equals(Basket_child1)&&!isMatchedBasketLetter1) {
							BasketPart1.setColor(GameManager.getInstance().getPaintCircleColor(1));
							zoomBasketLetters(0,1);BasketPart1.setAlpha(1);
							isMatchedBasketLetter1=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle1.collidesWith(BasketPart2)&&CircleLetter.equals(Basket_child2)&&!isMatchedBasketLetter2) {
							BasketPart2.setColor(GameManager.getInstance().getPaintCircleColor(1));
							zoomBasketLetters(0,2);
							BasketPart2.setAlpha(1);
							isMatchedBasketLetter2=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle1.collidesWith(BasketPart3)&&CircleLetter.equals(Basket_child3)&&!isMatchedBasketLetter3) {
							BasketPart3.setColor(GameManager.getInstance().getPaintCircleColor(1));
							zoomBasketLetters(0,3);BasketPart3.setAlpha(1);
							isMatchedBasketLetter3=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle1.collidesWith(BasketPart4)&&CircleLetter.equals(Basket_child4)&&!isMatchedBasketLetter4) {
							BasketPart4.setColor(GameManager.getInstance().getPaintCircleColor(1));
							zoomBasketLetters(0,4);
							BasketPart4.setAlpha(1);
							isMatchedBasketLetter4=true;

							SceneManager.getInstance().playPaintOkSound();
						}


						else {
							SceneManager.getInstance().vibrate(100);
							if(playAgain){
								SceneManager.getInstance().playNacksound();
							}
							playAgain=false;	

							waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

								@Override
								public void onTimePassed(TimerHandler pTimerHandler) {
									playAgain=true;
								}
							});

							this.registerUpdateHandler(waitForNackToFinish);
						}

					}
					else if("agapuliso".equalsIgnoreCase(ObjectToPaint)){

						if(circle1.collidesWith(FloaterPart0)&&CircleLetter.equals(Floater_child0)&&!isMatchedFloaterLetter0) {
							FloaterPart0.setColor(GameManager.getInstance().getPaintCircleColor(1));
							zoomFloaterLetters(0,0);
							FloaterPart0.setAlpha(1);
							isMatchedFloaterLetter0=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle1.collidesWith(FloaterPart1)&&CircleLetter.equals(Floater_child1)&&!isMatchedFloaterLetter1) {
							FloaterPart1.setColor(GameManager.getInstance().getPaintCircleColor(1));
							zoomFloaterLetters(0,1);
							FloaterPart1.setAlpha(1);
							isMatchedFloaterLetter1=true;

							SceneManager.getInstance().playPaintOkSound();
						}

						else if(circle1.collidesWith(FloaterPart2)&&CircleLetter.equals(Floater_child2)&&!isMatchedFloaterLetter2) {
							FloaterPart2.setColor(GameManager.getInstance().getPaintCircleColor(1));
							zoomFloaterLetters(0,2);
							FloaterPart2.setAlpha(1);	
							isMatchedFloaterLetter2=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle1.collidesWith(FloaterPart3)&&CircleLetter.equals(Floater_child3)&&!isMatchedFloaterLetter3) {
							FloaterPart3.setColor(GameManager.getInstance().getPaintCircleColor(1));
							zoomFloaterLetters(0,3);
							FloaterPart3.setAlpha(1);
							isMatchedFloaterLetter3=true;

							SceneManager.getInstance().playPaintOkSound();
						}


						else {

							SceneManager.getInstance().vibrate(100);
							if(playAgain){
								SceneManager.getInstance().playNacksound();
							}
							playAgain=false;	

							waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

								@Override
								public void onTimePassed(TimerHandler pTimerHandler) {
									playAgain=true;
								}
							});

							this.registerUpdateHandler(waitForNackToFinish);

						}

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});

						this.registerUpdateHandler(waitForNackToFinish);
					}

					circle1.setPosition(initXCircle,initYCircle1);
					letter1.setPosition(initXCircleLetter,initYCircle1);
					//always check for victory 
					checkVictory();
				}




				if (pSceneTouchEvent.isActionDown())
				{
					zoomCircle(1,1.75f);
				}
				return true;
			};
		};

		circle1.setPosition(this.initXCircle,this.initYCircle1);

		//--------------------------------------------------------------------
		circle2=new Sprite(0, 0, resourcesManager.circle_region, vbom){
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   
				circle2.setPosition(pSceneTouchEvent.getX() - circle2.getWidth() / 2, pSceneTouchEvent.getY() - circle2.getHeight()/ 2);
				letter2.setPosition(pSceneTouchEvent.getX() - letter2.getWidth() / 2, pSceneTouchEvent.getY() - letter2.getHeight()/ 2);
				if (pSceneTouchEvent.isActionUp())
				{   

					CircleLetter=GameManager.getInstance().getPaintCircleLetter(2);
					ObjectToPaint=GameManager.getInstance().getObject2Paint();

					if("radio".equalsIgnoreCase(ObjectToPaint)) {
						if(circle2.collidesWith(RadioPart0)&&CircleLetter.equals(Radio_child0)&&!isMatchedRadioLetter0) {
							RadioPart0.setColor(GameManager.getInstance().getPaintCircleColor(2));
							zoomRadioLetters(0,0);RadioPart0.setAlpha(1);
							isMatchedRadioLetter0=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle2.collidesWith(RadioPart1)&&CircleLetter.equals(Radio_child1)&&!isMatchedRadioLetter1) {
							RadioPart1.setColor(GameManager.getInstance().getPaintCircleColor(2));
							zoomRadioLetters(0,1);RadioPart1.setAlpha(1);
							isMatchedRadioLetter1=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle2.collidesWith(RadioPart2)&&CircleLetter.equals(Radio_child2)&&!isMatchedRadioLetter2) {
							RadioPart2.setColor(GameManager.getInstance().getPaintCircleColor(2));
							zoomRadioLetters(0,2);RadioPart2.setAlpha(1);
							isMatchedRadioLetter2=true;

							SceneManager.getInstance().playPaintOkSound();
						}

						else if(circle2.collidesWith(RadioPart4)&&CircleLetter.equals(Radio_child4)&&!isMatchedRadioLetter4) {
							RadioPart4.setColor(GameManager.getInstance().getPaintCircleColor(2));
							zoomRadioLetters(0,4);RadioPart4.setAlpha(1);
							isMatchedRadioLetter4=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle2.collidesWith(RadioPart5)&&CircleLetter.equals(Radio_child5)&&!isMatchedRadioLetter5) {
							RadioPart5.setColor(GameManager.getInstance().getPaintCircleColor(2));
							zoomRadioLetters(0,5);RadioPart5.setAlpha(1);
							isMatchedRadioLetter5=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else  if(circle2.collidesWith(RadioPart6)&&CircleLetter.equals(Radio_child6)&&!isMatchedRadioLetter6) {
							RadioPart6.setColor(GameManager.getInstance().getPaintCircleColor(2));
							zoomRadioLetters(0,6);RadioPart6.setAlpha(1);
							isMatchedRadioLetter6=true;

							SceneManager.getInstance().playPaintOkSound();
						}

						else {
							SceneManager.getInstance().vibrate(100);
							if(playAgain){
								SceneManager.getInstance().playNacksound();
							}
							playAgain=false;	

							waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

								@Override
								public void onTimePassed(TimerHandler pTimerHandler) {
									playAgain=true;
								}
							});

							this.registerUpdateHandler(waitForNackToFinish);
						}
					}//endif radio
					//--------------------------------------------------
					else if("agaseke".equalsIgnoreCase(ObjectToPaint)){
						if(circle2.collidesWith(BasketPart0)&&CircleLetter.equals(Basket_child0)&&!isMatchedBasketLetter0) {
							BasketPart0.setColor(GameManager.getInstance().getPaintCircleColor(2));
							zoomBasketLetters(0,0);BasketPart0.setAlpha(1);
							isMatchedBasketLetter0=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle2.collidesWith(BasketPart1)&&CircleLetter.equals(Basket_child1)&&!isMatchedBasketLetter1) {
							BasketPart1.setColor(GameManager.getInstance().getPaintCircleColor(2));
							zoomBasketLetters(0,1);BasketPart1.setAlpha(1);
							isMatchedBasketLetter1=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle2.collidesWith(BasketPart2)&&CircleLetter.equals(Basket_child2)&&!isMatchedBasketLetter2) {
							BasketPart2.setColor(GameManager.getInstance().getPaintCircleColor(2));
							zoomBasketLetters(0,2);BasketPart2.setAlpha(1);
							isMatchedBasketLetter2=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle2.collidesWith(BasketPart3)&&CircleLetter.equals(Basket_child3)&&!isMatchedBasketLetter3) {
							BasketPart3.setColor(GameManager.getInstance().getPaintCircleColor(2));
							zoomBasketLetters(0,3);BasketPart3.setAlpha(1);
							isMatchedBasketLetter3=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle2.collidesWith(BasketPart4)&&CircleLetter.equals(Basket_child4)&&!isMatchedBasketLetter4) {
							BasketPart4.setColor(GameManager.getInstance().getPaintCircleColor(2));
							zoomBasketLetters(0,4);
							BasketPart4.setAlpha(1);
							isMatchedBasketLetter4=true;

							SceneManager.getInstance().playPaintOkSound();
						}


						else {
							SceneManager.getInstance().vibrate(100);
							if(playAgain){
								SceneManager.getInstance().playNacksound();
							}
							playAgain=false;	

							waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

								@Override
								public void onTimePassed(TimerHandler pTimerHandler) {
									playAgain=true;
								}
							});

							this.registerUpdateHandler(waitForNackToFinish);
						}

					}
					else if("agapuliso".equalsIgnoreCase(ObjectToPaint)){

						if(circle2.collidesWith(FloaterPart0)&&CircleLetter.equals(Floater_child0)&&!isMatchedFloaterLetter0) {
							FloaterPart0.setColor(GameManager.getInstance().getPaintCircleColor(2));
							zoomFloaterLetters(0,0);
							FloaterPart0.setAlpha(1);
							isMatchedFloaterLetter0=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle2.collidesWith(FloaterPart1)&&CircleLetter.equals(Floater_child1)&&!isMatchedFloaterLetter1) {
							FloaterPart1.setColor(GameManager.getInstance().getPaintCircleColor(2));
							zoomFloaterLetters(0,1);
							FloaterPart1.setAlpha(1);
							isMatchedFloaterLetter1=true;

							SceneManager.getInstance().playPaintOkSound();
						}

						else if(circle2.collidesWith(FloaterPart2)&&CircleLetter.equals(Floater_child2)&&!isMatchedFloaterLetter2) {
							FloaterPart2.setColor(GameManager.getInstance().getPaintCircleColor(2));
							zoomFloaterLetters(0,2);
							FloaterPart2.setAlpha(1);	
							isMatchedFloaterLetter2=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle2.collidesWith(FloaterPart3)&&CircleLetter.equals(Floater_child3)&&!isMatchedFloaterLetter3) {
							FloaterPart3.setColor(GameManager.getInstance().getPaintCircleColor(2));
							zoomFloaterLetters(0,3);
							FloaterPart3.setAlpha(1);
							isMatchedFloaterLetter3=true;

							SceneManager.getInstance().playPaintOkSound();
						}


						else {

							SceneManager.getInstance().vibrate(100);
							if(playAgain){
								SceneManager.getInstance().playNacksound();
							}
							playAgain=false;	

							waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

								@Override
								public void onTimePassed(TimerHandler pTimerHandler) {
									playAgain=true;
								}
							});

							this.registerUpdateHandler(waitForNackToFinish);

						}

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});

						this.registerUpdateHandler(waitForNackToFinish);
					}

					circle2.setPosition(initXCircle,initYCircle2);
					letter2.setPosition(initXCircleLetter,initYCircle2);
					//always check for victory 
					checkVictory();
				}




				if (pSceneTouchEvent.isActionDown())
				{
					zoomCircle(2,1.75f);
				}
				return true;
			};
		};

		circle2.setPosition(this.initXCircle,this.initYCircle2);

		//-----------------------------------------------------------------

		circle3=new Sprite(0, 0, resourcesManager.circle_region, vbom){
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{  
				circle3.setPosition(pSceneTouchEvent.getX() - circle3.getWidth() / 2, pSceneTouchEvent.getY() - circle3.getHeight()/ 2);
				letter3.setPosition(pSceneTouchEvent.getX() - letter3.getWidth() / 2, pSceneTouchEvent.getY() - letter3.getHeight()/ 2);
				if (pSceneTouchEvent.isActionUp())
				{   

					CircleLetter=GameManager.getInstance().getPaintCircleLetter(3);
					ObjectToPaint=GameManager.getInstance().getObject2Paint();

					if("radio".equalsIgnoreCase(ObjectToPaint)) {
						if(circle3.collidesWith(RadioPart0)&&CircleLetter.equals(Radio_child0)&&!isMatchedRadioLetter0) {
							RadioPart0.setColor(GameManager.getInstance().getPaintCircleColor(3));
							RadioPart0.setAlpha(1);
							zoomRadioLetters(0,0);
							isMatchedRadioLetter0=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle3.collidesWith(RadioPart1)&&CircleLetter.equals(Radio_child1)&&!isMatchedRadioLetter1) {
							RadioPart1.setColor(GameManager.getInstance().getPaintCircleColor(3));
							zoomRadioLetters(0,1);
							RadioPart1.setAlpha(1);
							isMatchedRadioLetter1=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle3.collidesWith(RadioPart2)&&CircleLetter.equals(Radio_child2)&&!isMatchedRadioLetter2) {
							RadioPart2.setColor(GameManager.getInstance().getPaintCircleColor(3));
							RadioPart2.setAlpha(1);
							zoomRadioLetters(0,2);
							isMatchedRadioLetter2=true;

							SceneManager.getInstance().playPaintOkSound();
						}

						else if(circle3.collidesWith(RadioPart4)&&CircleLetter.equals(Radio_child4)&&!isMatchedRadioLetter4) {
							RadioPart4.setColor(GameManager.getInstance().getPaintCircleColor(3));
							RadioPart4.setAlpha(1);
							zoomRadioLetters(0,4);
							isMatchedRadioLetter4=true;


							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle3.collidesWith(RadioPart5)&&CircleLetter.equals(Radio_child5)&&!isMatchedRadioLetter5) {
							RadioPart5.setColor(GameManager.getInstance().getPaintCircleColor(3));
							RadioPart5.setAlpha(1);
							zoomRadioLetters(0,5);
							isMatchedRadioLetter5=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else  if(circle3.collidesWith(RadioPart6)&&CircleLetter.equals(Radio_child6)&&!isMatchedRadioLetter6) {
							RadioPart6.setColor(GameManager.getInstance().getPaintCircleColor(3));
							RadioPart6.setAlpha(1);
							zoomRadioLetters(0,6);
							isMatchedRadioLetter6=true;

							SceneManager.getInstance().playPaintOkSound();
						}

						else {
							SceneManager.getInstance().vibrate(100);
							if(playAgain){
								SceneManager.getInstance().playNacksound();
							}
							playAgain=false;	

							waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

								@Override
								public void onTimePassed(TimerHandler pTimerHandler) {
									playAgain=true;
								}
							});

							this.registerUpdateHandler(waitForNackToFinish);
						}
					}//endif radio
					//--------------------------------------------------
					else if("agaseke".equalsIgnoreCase(ObjectToPaint)){

						if(circle3.collidesWith(BasketPart0)&&CircleLetter.equals(Basket_child0)&&!isMatchedBasketLetter0) {
							BasketPart0.setColor(GameManager.getInstance().getPaintCircleColor(3));
							zoomBasketLetters(0,0);BasketPart0.setAlpha(1);
							isMatchedBasketLetter0=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle3.collidesWith(BasketPart1)&&CircleLetter.equals(Basket_child1)&&!isMatchedBasketLetter1) {
							BasketPart1.setColor(GameManager.getInstance().getPaintCircleColor(3));
							zoomBasketLetters(0,1);BasketPart1.setAlpha(1);
							isMatchedBasketLetter1=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle3.collidesWith(BasketPart2)&&CircleLetter.equals(Basket_child2)&&!isMatchedBasketLetter2) {
							BasketPart2.setColor(GameManager.getInstance().getPaintCircleColor(3));
							zoomBasketLetters(0,2);BasketPart2.setAlpha(1);
							isMatchedBasketLetter2=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle3.collidesWith(BasketPart3)&&CircleLetter.equals(Basket_child3)&&!isMatchedBasketLetter3) {
							BasketPart3.setColor(GameManager.getInstance().getPaintCircleColor(3));
							zoomBasketLetters(0,3);BasketPart3.setAlpha(1);
							isMatchedBasketLetter3=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle3.collidesWith(BasketPart4)&&CircleLetter.equals(Basket_child4)&&!isMatchedBasketLetter4) {
							BasketPart4.setColor(GameManager.getInstance().getPaintCircleColor(3));
							zoomBasketLetters(0,4);
							BasketPart4.setAlpha(1);
							isMatchedBasketLetter4=true;

							SceneManager.getInstance().playPaintOkSound();
						}


						else {
							SceneManager.getInstance().vibrate(100);
							if(playAgain){
								SceneManager.getInstance().playNacksound();
							}
							playAgain=false;	

							waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

								@Override
								public void onTimePassed(TimerHandler pTimerHandler) {
									playAgain=true;
								}
							});

							this.registerUpdateHandler(waitForNackToFinish);
						}

					}
					else if("agapuliso".equalsIgnoreCase(ObjectToPaint)){

						if(circle3.collidesWith(FloaterPart0)&&CircleLetter.equals(Floater_child0)&&!isMatchedFloaterLetter0) {
							FloaterPart0.setColor(GameManager.getInstance().getPaintCircleColor(3));
							zoomFloaterLetters(0,0);
							FloaterPart0.setAlpha(1);
							isMatchedFloaterLetter0=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle3.collidesWith(FloaterPart1)&&CircleLetter.equals(Floater_child1)&&!isMatchedFloaterLetter1) {
							FloaterPart1.setColor(GameManager.getInstance().getPaintCircleColor(3));
							zoomFloaterLetters(0,1);
							FloaterPart1.setAlpha(1);
							isMatchedFloaterLetter1=true;

							SceneManager.getInstance().playPaintOkSound();
						}

						else if(circle3.collidesWith(FloaterPart2)&&CircleLetter.equals(Floater_child2)&&!isMatchedFloaterLetter2) {
							FloaterPart2.setColor(GameManager.getInstance().getPaintCircleColor(3));
							zoomFloaterLetters(0,2);
							FloaterPart2.setAlpha(1);	
							isMatchedFloaterLetter2=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle3.collidesWith(FloaterPart3)&&CircleLetter.equals(Floater_child3)&&!isMatchedFloaterLetter3) {
							FloaterPart3.setColor(GameManager.getInstance().getPaintCircleColor(3));
							zoomFloaterLetters(0,3);
							FloaterPart3.setAlpha(1);
							isMatchedFloaterLetter3=true;

							SceneManager.getInstance().playPaintOkSound();
						}


						else {
							SceneManager.getInstance().vibrate(100);
							if(playAgain){
								SceneManager.getInstance().playNacksound();
							}
							playAgain=false;	

							waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

								@Override
								public void onTimePassed(TimerHandler pTimerHandler) {
									playAgain=true;
								}
							});

							this.registerUpdateHandler(waitForNackToFinish);

						}

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});

						this.registerUpdateHandler(waitForNackToFinish);
					}

					circle3.setPosition(initXCircle,initYCircle3);
					letter3.setPosition(initXCircleLetter,initYCircle3);
					//always check for victory 
					checkVictory();
				}



				if (pSceneTouchEvent.isActionDown())
				{
					zoomCircle(3,1.75f);
				}
				return true;
			};
		};

		circle3.setPosition(this.initXCircle,this.initYCircle3);

		//-----------------------------------------------------------------

		circle4=new Sprite(0, 0, resourcesManager.circle_region, vbom){
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{  
				circle4.setPosition(pSceneTouchEvent.getX() - circle4.getWidth() / 2, pSceneTouchEvent.getY() - circle4.getHeight()/ 2);
				letter4.setPosition(pSceneTouchEvent.getX() - letter4.getWidth() / 2, pSceneTouchEvent.getY() - letter4.getHeight()/ 2);
				if (pSceneTouchEvent.isActionUp())
				{   

					CircleLetter=GameManager.getInstance().getPaintCircleLetter(4);
					ObjectToPaint=GameManager.getInstance().getObject2Paint();

					if("radio".equalsIgnoreCase(ObjectToPaint)) {
						if(circle4.collidesWith(RadioPart0)&&CircleLetter.equals(Radio_child0)&&!isMatchedRadioLetter0) {
							RadioPart0.setColor(GameManager.getInstance().getPaintCircleColor(4));
							zoomRadioLetters(0,0);
							RadioPart0.setAlpha(1);
							isMatchedRadioLetter0=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle4.collidesWith(RadioPart1)&&CircleLetter.equals(Radio_child1)&&!isMatchedRadioLetter1) {
							RadioPart1.setColor(GameManager.getInstance().getPaintCircleColor(4));
							zoomRadioLetters(0,1);
							RadioPart1.setAlpha(1);
							isMatchedRadioLetter1=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle4.collidesWith(RadioPart2)&&CircleLetter.equals(Radio_child2)&&!isMatchedRadioLetter2) {
							RadioPart2.setColor(GameManager.getInstance().getPaintCircleColor(4));
							zoomRadioLetters(0,2);
							RadioPart2.setAlpha(1);
							isMatchedRadioLetter2=true;

							SceneManager.getInstance().playPaintOkSound();
						}

						else if(circle4.collidesWith(RadioPart4)&&CircleLetter.equals(Radio_child4)&&!isMatchedRadioLetter4) {
							RadioPart4.setColor(GameManager.getInstance().getPaintCircleColor(4));
							zoomRadioLetters(0,4);
							RadioPart4.setAlpha(1);
							isMatchedRadioLetter4=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle4.collidesWith(RadioPart5)&&CircleLetter.equals(Radio_child5)&&!isMatchedRadioLetter5) {
							RadioPart5.setColor(GameManager.getInstance().getPaintCircleColor(4));
							zoomRadioLetters(0,5);
							RadioPart5.setAlpha(1);
							isMatchedRadioLetter5=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else  if(circle4.collidesWith(RadioPart6)&&CircleLetter.equals(Radio_child6)&&!isMatchedRadioLetter6) {
							RadioPart6.setColor(GameManager.getInstance().getPaintCircleColor(4));
							zoomRadioLetters(0,6);
							RadioPart6.setAlpha(1);
							isMatchedRadioLetter6=true;

							SceneManager.getInstance().playPaintOkSound();
						}

						else {
							SceneManager.getInstance().vibrate(100);
							if(playAgain){
								SceneManager.getInstance().playNacksound();
							}
							playAgain=false;	

							waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

								@Override
								public void onTimePassed(TimerHandler pTimerHandler) {
									playAgain=true;
								}
							});

							this.registerUpdateHandler(waitForNackToFinish);
						}
					}//endif radio
					//--------------------------------------------------
					else if("agaseke".equalsIgnoreCase(ObjectToPaint)){

						if(circle4.collidesWith(BasketPart0)&&CircleLetter.equals(Basket_child0)&&!isMatchedBasketLetter0) {
							BasketPart0.setColor(GameManager.getInstance().getPaintCircleColor(4));
							zoomBasketLetters(0,0);BasketPart0.setAlpha(1);
							isMatchedBasketLetter0=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle4.collidesWith(BasketPart1)&&CircleLetter.equals(Basket_child1)&&!isMatchedBasketLetter1) {
							BasketPart1.setColor(GameManager.getInstance().getPaintCircleColor(4));
							zoomBasketLetters(0,1);BasketPart1.setAlpha(1);
							isMatchedBasketLetter1=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle4.collidesWith(BasketPart2)&&CircleLetter.equals(Basket_child2)&&!isMatchedBasketLetter2) {
							BasketPart2.setColor(GameManager.getInstance().getPaintCircleColor(4));
							zoomBasketLetters(0,2);BasketPart2.setAlpha(1);
							isMatchedBasketLetter2=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle4.collidesWith(BasketPart3)&&CircleLetter.equals(Basket_child3)&&!isMatchedBasketLetter3) {
							BasketPart3.setColor(GameManager.getInstance().getPaintCircleColor(4));
							zoomBasketLetters(0,3);
							BasketPart3.setAlpha(1);
							isMatchedBasketLetter3=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle4.collidesWith(BasketPart4)&&CircleLetter.equals(Basket_child4)&&!isMatchedBasketLetter4) {
							BasketPart4.setColor(GameManager.getInstance().getPaintCircleColor(4));
							zoomBasketLetters(0,4);
							BasketPart4.setAlpha(1);
							isMatchedBasketLetter4=true;

							SceneManager.getInstance().playPaintOkSound();
						}


						else {
							SceneManager.getInstance().vibrate(100);
							if(playAgain){
								SceneManager.getInstance().playNacksound();
							}
							playAgain=false;	

							waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

								@Override
								public void onTimePassed(TimerHandler pTimerHandler) {
									playAgain=true;
								}
							});

							this.registerUpdateHandler(waitForNackToFinish);
						}
					}
					else if("agapuliso".equalsIgnoreCase(ObjectToPaint)){

						if(circle4.collidesWith(FloaterPart0)&&CircleLetter.equals(Floater_child0)&&!isMatchedFloaterLetter0) {
							FloaterPart0.setColor(GameManager.getInstance().getPaintCircleColor(4));
							zoomFloaterLetters(0,0);
							FloaterPart0.setAlpha(1);
							isMatchedFloaterLetter0=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle4.collidesWith(FloaterPart1)&&CircleLetter.equals(Floater_child1)&&!isMatchedFloaterLetter1) {
							FloaterPart1.setColor(GameManager.getInstance().getPaintCircleColor(4));
							zoomFloaterLetters(0,1);
							FloaterPart1.setAlpha(1);
							isMatchedFloaterLetter1=true;

							SceneManager.getInstance().playPaintOkSound();
						}

						else if(circle4.collidesWith(FloaterPart2)&&CircleLetter.equals(Floater_child2)&&!isMatchedFloaterLetter2) {
							FloaterPart2.setColor(GameManager.getInstance().getPaintCircleColor(4));
							zoomFloaterLetters(0,2);
							FloaterPart2.setAlpha(1);	
							isMatchedFloaterLetter2=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle4.collidesWith(FloaterPart3)&&CircleLetter.equals(Floater_child3)&&!isMatchedFloaterLetter3) {
							FloaterPart3.setColor(GameManager.getInstance().getPaintCircleColor(4));
							zoomFloaterLetters(0,3);
							FloaterPart3.setAlpha(1);
							isMatchedFloaterLetter3=true;

							SceneManager.getInstance().playPaintOkSound();
						}


						else {

							SceneManager.getInstance().vibrate(100);
							if(playAgain){
								SceneManager.getInstance().playNacksound();
							}
							playAgain=false;	

							waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

								@Override
								public void onTimePassed(TimerHandler pTimerHandler) {
									playAgain=true;
								}
							});

							this.registerUpdateHandler(waitForNackToFinish);

						}

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});

						this.registerUpdateHandler(waitForNackToFinish);
					}

					circle4.setPosition(initXCircle,initYCircle4);
					letter4.setPosition(initXCircleLetter,initYCircle4);
					//always check for victory 
					checkVictory();
				}



				if (pSceneTouchEvent.isActionDown())
				{   
					zoomCircle(4,1.75f);
				}
				return true;
			};
		};

		circle4.setPosition(this.initXCircle,this.initYCircle4);

		//-----------------------------------------------------------------

		circle5=new Sprite(0, 0, resourcesManager.circle_region, vbom){
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{  
				circle5.setPosition(pSceneTouchEvent.getX() - circle5.getWidth() / 2, pSceneTouchEvent.getY() - circle5.getHeight()/ 2);
				letter5.setPosition(pSceneTouchEvent.getX() - letter5.getWidth() / 2, pSceneTouchEvent.getY() - letter5.getHeight()/ 2);
				if (pSceneTouchEvent.isActionUp())
				{   

					CircleLetter=GameManager.getInstance().getPaintCircleLetter(5);
					ObjectToPaint=GameManager.getInstance().getObject2Paint();

					if("radio".equalsIgnoreCase(ObjectToPaint)) {
						if(circle5.collidesWith(RadioPart0)&&CircleLetter.equals(Radio_child0)&&!isMatchedRadioLetter0) {
							RadioPart0.setColor(GameManager.getInstance().getPaintCircleColor(5));
							zoomRadioLetters(0,0);
							RadioPart0.setAlpha(1);
							isMatchedRadioLetter0=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle5.collidesWith(RadioPart1)&&CircleLetter.equals(Radio_child1)&&!isMatchedRadioLetter1) {
							RadioPart1.setColor(GameManager.getInstance().getPaintCircleColor(5));
							zoomRadioLetters(0,1);
							RadioPart1.setAlpha(1);
							isMatchedRadioLetter1=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle5.collidesWith(RadioPart2)&&CircleLetter.equals(Radio_child2)&&!isMatchedRadioLetter2) {
							RadioPart2.setColor(GameManager.getInstance().getPaintCircleColor(5));
							zoomRadioLetters(0,2);
							RadioPart2.setAlpha(1);
							isMatchedRadioLetter2=true;

							SceneManager.getInstance().playPaintOkSound();
						}

						else if(circle5.collidesWith(RadioPart4)&&CircleLetter.equals(Radio_child4)&&!isMatchedRadioLetter4) {
							RadioPart4.setColor(GameManager.getInstance().getPaintCircleColor(5));
							zoomRadioLetters(0,4);
							RadioPart4.setAlpha(1);
							isMatchedRadioLetter4=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle5.collidesWith(RadioPart5)&&CircleLetter.equals(Radio_child5)&&!isMatchedRadioLetter5) {
							RadioPart5.setColor(GameManager.getInstance().getPaintCircleColor(5));
							zoomRadioLetters(0,5);
							RadioPart5.setAlpha(1);
							isMatchedRadioLetter5=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else  if(circle5.collidesWith(RadioPart6)&&CircleLetter.equals(Radio_child6)&&!isMatchedRadioLetter6) {
							RadioPart6.setColor(GameManager.getInstance().getPaintCircleColor(5));
							zoomRadioLetters(0,6);
							RadioPart6.setAlpha(1);
							isMatchedRadioLetter6=true;

							SceneManager.getInstance().playPaintOkSound();
						}

						else {
							SceneManager.getInstance().vibrate(100);
							if(playAgain){
								SceneManager.getInstance().playNacksound();
							}
							playAgain=false;	

							waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

								@Override
								public void onTimePassed(TimerHandler pTimerHandler) {
									playAgain=true;
								}
							});

							this.registerUpdateHandler(waitForNackToFinish);
						}
					}//endif radio
					//--------------------------------------------------
					else if("agaseke".equalsIgnoreCase(ObjectToPaint)){

						if(circle5.collidesWith(BasketPart0)&&CircleLetter.equals(Basket_child0)&&!isMatchedBasketLetter0) {
							BasketPart0.setColor(GameManager.getInstance().getPaintCircleColor(5));
							zoomBasketLetters(0,0);BasketPart0.setAlpha(1);
							isMatchedBasketLetter0=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle5.collidesWith(BasketPart1)&&CircleLetter.equals(Basket_child1)&&!isMatchedBasketLetter1) {
							BasketPart1.setColor(GameManager.getInstance().getPaintCircleColor(5));
							zoomBasketLetters(0,1);BasketPart1.setAlpha(1);
							isMatchedBasketLetter1=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle5.collidesWith(BasketPart2)&&CircleLetter.equals(Basket_child2)&&!isMatchedBasketLetter2) {
							BasketPart2.setColor(GameManager.getInstance().getPaintCircleColor(5));
							zoomBasketLetters(0,2);BasketPart2.setAlpha(1);
							isMatchedBasketLetter2=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle5.collidesWith(BasketPart3)&&CircleLetter.equals(Basket_child3)&&!isMatchedBasketLetter3) {
							BasketPart3.setColor(GameManager.getInstance().getPaintCircleColor(5));
							zoomBasketLetters(0,3);BasketPart3.setAlpha(1);
							isMatchedBasketLetter3=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle5.collidesWith(BasketPart4)&&CircleLetter.equals(Basket_child4)&&!isMatchedBasketLetter4) {
							BasketPart4.setColor(GameManager.getInstance().getPaintCircleColor(5));
							zoomBasketLetters(0,4);
							BasketPart4.setAlpha(1);
							isMatchedBasketLetter4=true;

							SceneManager.getInstance().playPaintOkSound();
						}


						else {
							SceneManager.getInstance().vibrate(100);
							if(playAgain){
								SceneManager.getInstance().playNacksound();
							}
							playAgain=false;	

							waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

								@Override
								public void onTimePassed(TimerHandler pTimerHandler) {
									playAgain=true;
								}
							});

							this.registerUpdateHandler(waitForNackToFinish);
						}
					}
					else if("agapuliso".equalsIgnoreCase(ObjectToPaint)){

						if(circle5.collidesWith(FloaterPart0)&&CircleLetter.equals(Floater_child0)&&!isMatchedFloaterLetter0) {
							FloaterPart0.setColor(GameManager.getInstance().getPaintCircleColor(5));
							zoomFloaterLetters(0,0);
							FloaterPart0.setAlpha(1);
							isMatchedFloaterLetter0=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle5.collidesWith(FloaterPart1)&&CircleLetter.equals(Floater_child1)&&!isMatchedFloaterLetter1) {
							FloaterPart1.setColor(GameManager.getInstance().getPaintCircleColor(5));
							zoomFloaterLetters(0,1);
							FloaterPart1.setAlpha(1);
							isMatchedFloaterLetter1=true;

							SceneManager.getInstance().playPaintOkSound();
						}

						else if(circle5.collidesWith(FloaterPart2)&&CircleLetter.equals(Floater_child2)&&!isMatchedFloaterLetter2) {
							FloaterPart2.setColor(GameManager.getInstance().getPaintCircleColor(5));
							zoomFloaterLetters(0,2);
							FloaterPart2.setAlpha(1);	
							isMatchedFloaterLetter2=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle5.collidesWith(FloaterPart3)&&CircleLetter.equals(Floater_child3)&&!isMatchedFloaterLetter3) {
							FloaterPart3.setColor(GameManager.getInstance().getPaintCircleColor(5));
							zoomFloaterLetters(0,3);
							FloaterPart3.setAlpha(1);
							isMatchedFloaterLetter3=true;

							SceneManager.getInstance().playPaintOkSound();
						}


						else {

							SceneManager.getInstance().vibrate(100);
							if(playAgain){
								SceneManager.getInstance().playNacksound();
							}
							playAgain=false;	

							waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

								@Override
								public void onTimePassed(TimerHandler pTimerHandler) {
									playAgain=true;
								}
							});

							this.registerUpdateHandler(waitForNackToFinish);

						}

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});

						this.registerUpdateHandler(waitForNackToFinish);
					}

					circle5.setPosition(initXCircle,initYCircle5);
					letter5.setPosition(initXCircleLetter,initYCircle5);

					//always check for victory 
					checkVictory();
				}



				if (pSceneTouchEvent.isActionDown())
				{   
					zoomCircle(5,1.75f);
				}
				return true;
			};
		};

		circle5.setPosition(this.initXCircle,this.initYCircle5);

		//-------------------------------------------------------------
		circle6=new Sprite(0, 0, resourcesManager.circle_region, vbom){
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{  
				circle6.setPosition(pSceneTouchEvent.getX() - circle6.getWidth() / 2, pSceneTouchEvent.getY() - circle6.getHeight()/ 2);
				letter6.setPosition(pSceneTouchEvent.getX() - letter6.getWidth() / 2, pSceneTouchEvent.getY() - letter6.getHeight()/ 2);
				if (pSceneTouchEvent.isActionUp())
				{   

					CircleLetter=GameManager.getInstance().getPaintCircleLetter(6);
					ObjectToPaint=GameManager.getInstance().getObject2Paint();

					if("radio".equalsIgnoreCase(ObjectToPaint)) {
						if(circle6.collidesWith(RadioPart0)&&CircleLetter.equals(Radio_child0)&&!isMatchedRadioLetter0) {
							RadioPart0.setColor(GameManager.getInstance().getPaintCircleColor(6));
							zoomRadioLetters(0,0);
							RadioPart0.setAlpha(1);
							isMatchedRadioLetter0=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle6.collidesWith(RadioPart1)&&CircleLetter.equals(Radio_child1)&&!isMatchedRadioLetter1) {
							RadioPart1.setColor(GameManager.getInstance().getPaintCircleColor(6));
							zoomRadioLetters(0,1);
							RadioPart1.setAlpha(1);
							isMatchedRadioLetter1=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle6.collidesWith(RadioPart2)&&CircleLetter.equals(Radio_child2)&&!isMatchedRadioLetter2) {
							RadioPart2.setColor(GameManager.getInstance().getPaintCircleColor(6));
							zoomRadioLetters(0,2);
							RadioPart2.setAlpha(1);
							isMatchedRadioLetter2=true;

							SceneManager.getInstance().playPaintOkSound();
						}

						else if(circle6.collidesWith(RadioPart4)&&CircleLetter.equals(Radio_child4)&&!isMatchedRadioLetter4) {
							RadioPart4.setColor(GameManager.getInstance().getPaintCircleColor(6));
							zoomRadioLetters(0,4);
							RadioPart4.setAlpha(1);
							isMatchedRadioLetter4=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle6.collidesWith(RadioPart5)&&CircleLetter.equals(Radio_child5)&&!isMatchedRadioLetter5) {
							RadioPart5.setColor(GameManager.getInstance().getPaintCircleColor(6));
							zoomRadioLetters(0,5);
							RadioPart5.setAlpha(1);
							isMatchedRadioLetter5=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else  if(circle6.collidesWith(RadioPart6)&&CircleLetter.equals(Radio_child6)&&!isMatchedRadioLetter6) {
							RadioPart6.setColor(GameManager.getInstance().getPaintCircleColor(6));
							zoomRadioLetters(0,6);
							RadioPart6.setAlpha(1);
							isMatchedRadioLetter6=true;

							SceneManager.getInstance().playPaintOkSound();
						}

						else {
							SceneManager.getInstance().vibrate(100);
							if(playAgain){
								SceneManager.getInstance().playNacksound();
							}
							playAgain=false;	

							waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

								@Override
								public void onTimePassed(TimerHandler pTimerHandler) {
									playAgain=true;
								}
							});

							this.registerUpdateHandler(waitForNackToFinish);
						}
					}//endif radio
					//--------------------------------------------------
					else if("agaseke".equalsIgnoreCase(ObjectToPaint)){
						if(circle6.collidesWith(BasketPart0)&&CircleLetter.equals(Basket_child0)&&!isMatchedBasketLetter0) {
							BasketPart0.setColor(GameManager.getInstance().getPaintCircleColor(6));
							zoomBasketLetters(0,0);BasketPart0.setAlpha(1);
							isMatchedBasketLetter0=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle6.collidesWith(BasketPart1)&&CircleLetter.equals(Basket_child1)&&!isMatchedBasketLetter1) {
							BasketPart1.setColor(GameManager.getInstance().getPaintCircleColor(6));
							zoomBasketLetters(0,1);BasketPart1.setAlpha(1);
							isMatchedBasketLetter1=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle6.collidesWith(BasketPart2)&&CircleLetter.equals(Basket_child2)&&!isMatchedBasketLetter2) {
							BasketPart2.setColor(GameManager.getInstance().getPaintCircleColor(6));
							zoomBasketLetters(0,2);BasketPart2.setAlpha(1);
							isMatchedBasketLetter2=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle6.collidesWith(BasketPart3)&&CircleLetter.equals(Basket_child3)&&!isMatchedBasketLetter3) {
							BasketPart3.setColor(GameManager.getInstance().getPaintCircleColor(6));
							zoomBasketLetters(0,3);BasketPart3.setAlpha(1);
							isMatchedBasketLetter3=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle6.collidesWith(BasketPart4)&&CircleLetter.equals(Basket_child4)&&!isMatchedBasketLetter4) {
							BasketPart4.setColor(GameManager.getInstance().getPaintCircleColor(6));
							zoomBasketLetters(0,4);
							BasketPart4.setAlpha(1);
							isMatchedBasketLetter4=true;

							SceneManager.getInstance().playPaintOkSound();
						}


						else {
							SceneManager.getInstance().vibrate(100);
							if(playAgain){
								SceneManager.getInstance().playNacksound();
							}
							playAgain=false;	

							waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

								@Override
								public void onTimePassed(TimerHandler pTimerHandler) {
									playAgain=true;
								}
							});

							this.registerUpdateHandler(waitForNackToFinish);
						}

					}
					else if("agapuliso".equalsIgnoreCase(ObjectToPaint)){

						if(circle6.collidesWith(FloaterPart0)&&CircleLetter.equals(Floater_child0)&&!isMatchedFloaterLetter0) {
							FloaterPart0.setColor(GameManager.getInstance().getPaintCircleColor(6));
							zoomFloaterLetters(0,0);
							FloaterPart0.setAlpha(1);
							isMatchedFloaterLetter0=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle6.collidesWith(FloaterPart1)&&CircleLetter.equals(Floater_child1)&&!isMatchedFloaterLetter1) {
							FloaterPart1.setColor(GameManager.getInstance().getPaintCircleColor(6));
							zoomFloaterLetters(0,1);
							FloaterPart1.setAlpha(1);
							isMatchedFloaterLetter1=true;

							SceneManager.getInstance().playPaintOkSound();
						}

						else if(circle6.collidesWith(FloaterPart2)&&CircleLetter.equals(Floater_child2)&&!isMatchedFloaterLetter2) {
							FloaterPart2.setColor(GameManager.getInstance().getPaintCircleColor(6));
							zoomFloaterLetters(0,2);
							FloaterPart2.setAlpha(1);	
							isMatchedFloaterLetter2=true;

							SceneManager.getInstance().playPaintOkSound();
						}
						else if(circle6.collidesWith(FloaterPart3)&&CircleLetter.equals(Floater_child3)&&!isMatchedFloaterLetter3) {
							FloaterPart3.setColor(GameManager.getInstance().getPaintCircleColor(6));
							zoomFloaterLetters(0,3);
							FloaterPart3.setAlpha(1);
							isMatchedFloaterLetter3=true;

							SceneManager.getInstance().playPaintOkSound();
						}


						else {

							SceneManager.getInstance().vibrate(100);
							if(playAgain){
								SceneManager.getInstance().playNacksound();
							}
							playAgain=false;	

							waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

								@Override
								public void onTimePassed(TimerHandler pTimerHandler) {
									playAgain=true;
								}
							});

							this.registerUpdateHandler(waitForNackToFinish);

						}

					}
					else {

						SceneManager.getInstance().vibrate(100);
						if(playAgain){
							SceneManager.getInstance().playNacksound();
						}
						playAgain=false;	

						waitForNackToFinish=new TimerHandler(3,false,new ITimerCallback() {

							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								playAgain=true;
							}
						});

						this.registerUpdateHandler(waitForNackToFinish);
					}

					circle6.setPosition(initXCircle,initYCircle6);
					letter6.setPosition(initXCircleLetter,initYCircle6);
					//always check for victory 
					checkVictory();
				}



				if (pSceneTouchEvent.isActionDown())
				{   

					zoomCircle(6,1.75f);
				}
				return true;
			};

		};
		circle6.setPosition(this.initXCircle,this.initYCircle6);

		//register whether color circles has been touched============



		//======================start by hidding circles and letters===================         
		zoomCircle(10,0);


		/*color our circles by requesting random colors or using predefined colors in case there is a null return*/

		if(GameManager.getInstance().getRandomColor("siga",randomCircleColor0)!=null) {
			c0=GameManager.getInstance().getRandomColor("siga",randomCircleColor0);
			circle0.setColor(c0);
			GameManager.getInstance().setPaintCircle(c0,0);
		}
		else {
			circle0.setColor(c0);
			GameManager.getInstance().setPaintCircle(c0,0);
		}


		if(GameManager.getInstance().getRandomColor("siga",randomCircleColor1)!=null) {
			c1=GameManager.getInstance().getRandomColor("siga",randomCircleColor1);
			circle1.setColor(c1);
			GameManager.getInstance().setPaintCircle(c1,1);
		}
		else {
			circle1.setColor(c1);
			GameManager.getInstance().setPaintCircle(c1,1);
		}

		if(GameManager.getInstance().getRandomColor("siga",randomCircleColor2)!=null) {
			c2=GameManager.getInstance().getRandomColor("siga",randomCircleColor2);
			circle2.setColor(c2);
			GameManager.getInstance().setPaintCircle(c2,2);
		}
		else {
			circle2.setColor(c2);
			GameManager.getInstance().setPaintCircle(c2,2);
		}

		if(GameManager.getInstance().getRandomColor("siga",randomCircleColor3)!=null) {
			c3=GameManager.getInstance().getRandomColor("siga",randomCircleColor3);
			circle3.setColor(c3);
			GameManager.getInstance().setPaintCircle(c3,3);
		}
		else {
			circle3.setColor(c3);
			GameManager.getInstance().setPaintCircle(c3,3);
		}

		if(GameManager.getInstance().getRandomColor("siga",randomCircleColor4)!=null) {
			c4=GameManager.getInstance().getRandomColor("siga",randomCircleColor4);
			circle4.setColor(c4);
			GameManager.getInstance().setPaintCircle(c4,4);
		}
		else {
			circle4.setColor(c4);
			GameManager.getInstance().setPaintCircle(c4,4);
		}

		if(GameManager.getInstance().getRandomColor("siga",randomCircleColor5)!=null) {
			c5=GameManager.getInstance().getRandomColor("siga",randomCircleColor5);
			circle5.setColor(c5);
			GameManager.getInstance().setPaintCircle(c5,5);
		}
		else {
			circle5.setColor(c5);
			GameManager.getInstance().setPaintCircle(c5,5);
		}  

		if(GameManager.getInstance().getRandomColor("siga",randomCircleColor6)!=null) {
			c6=GameManager.getInstance().getRandomColor("siga",randomCircleColor6);
			circle6.setColor(c6);
			GameManager.getInstance().setPaintCircle(c6,6);
		}
		else {
			circle6.setColor(c6);
			GameManager.getInstance().setPaintCircle(c6,6);
		}

		registerTouchArea(circle0);
		registerTouchArea(circle1);
		registerTouchArea(circle2);
		registerTouchArea(circle3);
		registerTouchArea(circle4);
		registerTouchArea(circle5);
		registerTouchArea(circle6);
		registerTouchArea(letter0);
		registerTouchArea(letter1);
		registerTouchArea(letter2);
		registerTouchArea(letter3);
		registerTouchArea(letter4);
		registerTouchArea(letter5);
		registerTouchArea(letter6);




		//the following is the region in which we define radio parts region's sprites.
		//---------------------------------------------------------------------------------------		
		RadioPart0=new Sprite(0, 0, resourcesManager.RadioPart0_region, vbom){

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{    //first we will detach previous child letters
				if (pSceneTouchEvent.isActionUp())
				{   
					if(!isRadioSelected){
						if(!isMovementLocked){
							zoomCircle(10,1);
							repositionObject(1);
							repositionObject(2);

							moveObjectToPaint(0);

							isRadioSelected=true;
							isFloaterSelected=false;
							isBasketSelected=false;
							GameManager.getInstance().setObject2Paint("radio");

						}
						else{

							SceneManager.getInstance().vibrate(100);
						}
					}
					else {

						if(!isMovementLocked){
							zoomCircle(10,0);
							repositionObject(1);
							repositionObject(2);


							isRadioSelected=false;
							isFloaterSelected=false;
							isBasketSelected=false;
						}
						else{
							SceneManager.getInstance().vibrate(300);
						}

					}

				}

				return true;
			};
		};
		registerTouchArea(RadioPart0);


		//-------------------------------------------------------------------------------------
		RadioPart1=new Sprite(0, 0, resourcesManager.RadioPart1_region, vbom){

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{    //first we will detach previous child letters

				if (pSceneTouchEvent.isActionUp())
				{   
					if(!isRadioSelected){	
						if(!isMovementLocked){
							zoomCircle(10,1);
							repositionObject(1);
							repositionObject(2);

							moveObjectToPaint(0);

							isRadioSelected=true;
							isFloaterSelected=false;
							isBasketSelected=false;
							GameManager.getInstance().setObject2Paint("radio");

						}
						else{

							SceneManager.getInstance().vibrate(100);
						}

					}
					else{

						if(!isMovementLocked){
							zoomCircle(10,0);
							repositionObject(1);
							repositionObject(2);


							isRadioSelected=false;
							isFloaterSelected=false;
							isBasketSelected=false;
						}
						else{
							SceneManager.getInstance().vibrate(300);
						}

					}

				}

				return true;
			};
		};
		registerTouchArea(RadioPart1);

		//-----------------------------------------------------------------------------------
		RadioPart2=new Sprite(0, 0, resourcesManager.RadioPart2_region, vbom){

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{    //first we will detach previous child letters

				if (pSceneTouchEvent.isActionUp())
				{   
					if(!isRadioSelected){	
						if(!isMovementLocked){
							zoomCircle(10,1);
							repositionObject(1);
							repositionObject(2);

							moveObjectToPaint(0);

							isRadioSelected=true;
							isFloaterSelected=false;
							isBasketSelected=false;
							GameManager.getInstance().setObject2Paint("radio");

						}
						else{

							SceneManager.getInstance().vibrate(100);
						}
					}
					else {
						if(!isMovementLocked){
							zoomCircle(10,0);
							repositionObject(1);
							repositionObject(2);


							isRadioSelected=false;
							isFloaterSelected=false;
							isBasketSelected=false;
						}
						else{
							SceneManager.getInstance().vibrate(300);
						}

					}

				}

				return true;
			};
		};
		registerTouchArea(RadioPart2);


		//----------------------------------------------------------------------------------
		RadioPart3=new Sprite(0, 0, resourcesManager.RadioPart3_region, vbom){

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   
				//first we will detach previous child letters
				if (pSceneTouchEvent.isActionUp())
				{   
					if(!isRadioSelected){	
						if(!isMovementLocked){
							zoomCircle(10,1);
							repositionObject(1);
							repositionObject(2);

							moveObjectToPaint(0);

							isRadioSelected=true;
							isFloaterSelected=false;
							isBasketSelected=false;
							GameManager.getInstance().setObject2Paint("radio");

						}
						else{

							SceneManager.getInstance().vibrate(100);
						}

					}
					else {

						if(!isMovementLocked){
							zoomCircle(10,0);
							repositionObject(1);
							repositionObject(2);


							isRadioSelected=false;
							isFloaterSelected=false;
							isBasketSelected=false;
						}
						else{
							SceneManager.getInstance().vibrate(300);
						}

					}

				}

				return true;
			};
		};
		registerTouchArea(RadioPart3);



		RadioPart4=new Sprite(0, 0, resourcesManager.RadioPart4_region, vbom){

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   
				//first we will detach previous child letters
				if (pSceneTouchEvent.isActionUp())
				{   
					if(!isRadioSelected){	
						if(!isMovementLocked){
							zoomCircle(10,1);
							repositionObject(1);
							repositionObject(2);

							moveObjectToPaint(0);

							isRadioSelected=true;
							isFloaterSelected=false;
							isBasketSelected=false;
							GameManager.getInstance().setObject2Paint("radio");

						}
						else{

							SceneManager.getInstance().vibrate(100);
						}

					}
					else {

						if(!isMovementLocked){
							zoomCircle(10,0);

							repositionObject(1);
							repositionObject(2);


							isRadioSelected=false;
							isFloaterSelected=false;
							isBasketSelected=false;
						}
						else{
							SceneManager.getInstance().vibrate(300);
						}

					}

				}

				return true;
			};
		};
		registerTouchArea(RadioPart4);


		RadioPart5=new Sprite(0, 0, resourcesManager.RadioPart5_region, vbom){

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   
				//first we will detach previous child letters
				if (pSceneTouchEvent.isActionUp())
				{   
					if(!isRadioSelected){	
						if(!isMovementLocked){
							zoomCircle(10,1);
							repositionObject(1);
							repositionObject(2);

							moveObjectToPaint(0);

							isRadioSelected=true;
							isFloaterSelected=false;
							isBasketSelected=false;
							GameManager.getInstance().setObject2Paint("radio");

						}
						else{

							SceneManager.getInstance().vibrate(100);
						}
					}
					else {

						if(!isMovementLocked){
							zoomCircle(10,0);

							repositionObject(1);
							repositionObject(2);


							isRadioSelected=false;
							isFloaterSelected=false;
							isBasketSelected=false;
						}
						else{
							SceneManager.getInstance().vibrate(300);
						}

					}

				}

				return true;
			};
		};
		registerTouchArea(RadioPart5);


		RadioPart6=new Sprite(0, 0, resourcesManager.RadioPart6_region, vbom){

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   
				//first we will detach previous child letters
				if (pSceneTouchEvent.isActionUp())
				{   
					if(!isRadioSelected){	
						if(!isMovementLocked){
							zoomCircle(10,1);
							repositionObject(1);
							repositionObject(2);

							moveObjectToPaint(0);

							isRadioSelected=true;
							isFloaterSelected=false;
							isBasketSelected=false;
							GameManager.getInstance().setObject2Paint("radio");

						}
						else{

							SceneManager.getInstance().vibrate(100);
						}

					}
					else {

						if(!isMovementLocked){
							zoomCircle(10,0);
							repositionObject(1);
							repositionObject(2);


							isRadioSelected=false;
							isFloaterSelected=false;
							isBasketSelected=false;
						}
						else{
							SceneManager.getInstance().vibrate(300);
						}

					}

				}

				return true;
			};
		};
		registerTouchArea(RadioPart6);


		/*
		 * ============================================================================================================
		 * ============================================================================================================
		 * ============================================================================================================
		 * ============================================================================================================
		 */		


		/* the following region is where we define and register our Flowers    */
		FloaterPart0=new Sprite(0, 0, resourcesManager.floater0_region, vbom){

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{
				if (pSceneTouchEvent.isActionUp())
				{   
					if(!isFloaterSelected){	
						if(!isMovementLocked){
							zoomCircle(10,1);

							moveObjectToPaint(2);

							repositionObject(0);
							repositionObject(1);
							hideRadio();
							isRadioSelected=false;
							isFloaterSelected=true;
							isBasketSelected=false;
							GameManager.getInstance().setObject2Paint("agapuliso");
						}
						else{

							SceneManager.getInstance().vibrate(100);
						}

					}
					else {

						if(!isMovementLocked){
							zoomCircle(10,0);

							repositionObject(0);
							repositionObject(1);


							isRadioSelected=false;
							isFloaterSelected=false;
							isBasketSelected=false;
						}
						else{
							SceneManager.getInstance().vibrate(300);
						}

					}

				}

				return true;
			};
		};

		registerTouchArea(FloaterPart0);


		FloaterPart1=new Sprite(0, 0, resourcesManager.floater1_region, vbom){

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}


			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{
				if (pSceneTouchEvent.isActionUp())
				{   
					if(!isFloaterSelected){	
						if(!isMovementLocked){
							zoomCircle(10,1);
							moveObjectToPaint(2);
							repositionObject(0);
							repositionObject(1);
							hideRadio();
							isRadioSelected=false;
							isFloaterSelected=true;
							isBasketSelected=false;
							GameManager.getInstance().setObject2Paint("agapuliso");
						}
						else{

							SceneManager.getInstance().vibrate(100);
						}

					}
					else {
						if(!isMovementLocked){
							zoomCircle(10,0);
							repositionObject(0);
							repositionObject(1);


							isRadioSelected=false;
							isFloaterSelected=false;
							isBasketSelected=false;
						}
						else{
							SceneManager.getInstance().vibrate(300);
						}

					}

				}

				return true;
			};
		};

		registerTouchArea(FloaterPart1);

		FloaterPart2=new Sprite(0, 0, resourcesManager.floater2_region, vbom){

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}


			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{
				if (pSceneTouchEvent.isActionUp())
				{   
					if(!isFloaterSelected){	
						if(!isMovementLocked){
							zoomCircle(10,1);
							moveObjectToPaint(2);
							repositionObject(0);
							repositionObject(1);
							hideRadio();
							isRadioSelected=false;
							isFloaterSelected=true;
							isBasketSelected=false;
							GameManager.getInstance().setObject2Paint("agapuliso");
						}
						else{

							SceneManager.getInstance().vibrate(100);
						}

					}
					else {

						if(!isMovementLocked){
							zoomCircle(10,0);
							repositionObject(0);
							repositionObject(1);
							hideRadio();

							isRadioSelected=false;
							isFloaterSelected=false;
							isBasketSelected=false;
						}
						else{
							SceneManager.getInstance().vibrate(300);
						}

					}

				}

				return true;
			};
		};

		registerTouchArea(FloaterPart2);





		FloaterPart3=new Sprite(0, 0, resourcesManager.floater3_region, vbom){

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}


			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{
				if (pSceneTouchEvent.isActionUp())
				{   
					if(!isFloaterSelected){	
						if(!isMovementLocked){
							zoomCircle(10,1);
							moveObjectToPaint(2);
							repositionObject(0);
							repositionObject(1);
							hideRadio();
							isRadioSelected=false;
							isFloaterSelected=true;
							isBasketSelected=false;
							GameManager.getInstance().setObject2Paint("agapuliso");
						}
						else{

							SceneManager.getInstance().vibrate(100);
						}

					}
					else {
						if(!isMovementLocked){
							zoomCircle(10,0);
							repositionObject(0);
							repositionObject(1);


							isRadioSelected=false;
							isFloaterSelected=false;
							isBasketSelected=false;
						}
						else{
							SceneManager.getInstance().vibrate(300);
						}

					}

				}

				return true;
			};
		};

		registerTouchArea(FloaterPart3);

		/*
		 * ============================================================================================================
		 * ============================================================================================================
		 * ============================================================================================================
		 * ============================================================================================================
		 */

		BasketPart0=new Sprite(0, 0, resourcesManager.basket0_region, vbom){

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{
				if (pSceneTouchEvent.isActionUp())
				{  
					if(!isBasketSelected){

						if(!isMovementLocked){
							zoomCircle(10,1);

							moveObjectToPaint(1);
							repositionObject(0);
							repositionObject(2);		
							hideRadio();
							isRadioSelected=false;
							isFloaterSelected=false;
							isBasketSelected=true;
							GameManager.getInstance().setObject2Paint("agaseke");
						}
						else{


							SceneManager.getInstance().vibrate(100);

						}

					}
					else{

						if(!isMovementLocked){
							zoomCircle(10,0);
							repositionObject(0);
							repositionObject(2);
							hideRadio();

							isRadioSelected=false;
							isFloaterSelected=false;
							isBasketSelected=false;
						}
						else{
							SceneManager.getInstance().vibrate(300);
						}

					}

				}

				return true;
			};
		};

		registerTouchArea(BasketPart0);

		BasketPart1=new Sprite(0, 0, resourcesManager.basket1_region, vbom){

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{
				if (pSceneTouchEvent.isActionUp())
				{  
					if(!isBasketSelected){
						if(!isMovementLocked){
							zoomCircle(10,1);

							moveObjectToPaint(1);
							repositionObject(0);
							repositionObject(2);		
							hideRadio();
							isRadioSelected=false;
							isFloaterSelected=false;
							isBasketSelected=true;
							GameManager.getInstance().setObject2Paint("agaseke");
						}
						else{


							SceneManager.getInstance().vibrate(100);

						}

					}
					else{

						if(!isMovementLocked){
							zoomCircle(10,0);
							repositionObject(0);
							repositionObject(2);
							hideRadio();
							isRadioSelected=false;
							isFloaterSelected=false;
							isBasketSelected=false;
						}
						else{
							SceneManager.getInstance().vibrate(300);
						}

					}

				}

				return true;
			};
		};

		registerTouchArea(BasketPart1);

		BasketPart2=new Sprite(0, 0, resourcesManager.basket2_region, vbom){

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{
				if (pSceneTouchEvent.isActionUp())
				{  
					if(!isBasketSelected){
						if(!isMovementLocked){
							zoomCircle(10,1);

							moveObjectToPaint(1);
							repositionObject(0);
							repositionObject(2);		
							hideRadio();
							isRadioSelected=false;
							isFloaterSelected=false;
							isBasketSelected=true;
							GameManager.getInstance().setObject2Paint("agaseke");
						}
						else{


							SceneManager.getInstance().vibrate(100);

						}

					}
					else{
						if(!isMovementLocked){
							zoomCircle(10,0);
							repositionObject(0);
							repositionObject(2);
							hideRadio();

							isRadioSelected=false;
							isFloaterSelected=false;
							isBasketSelected=false;
						}
						else{
							SceneManager.getInstance().vibrate(300);
						}

					}

				}

				return true;
			};
		};

		registerTouchArea(BasketPart2);

		BasketPart3=new Sprite(0, 0, resourcesManager.basket3_region, vbom){

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{
				if (pSceneTouchEvent.isActionUp())
				{  
					if(!isBasketSelected){
						if(!isMovementLocked){
							zoomCircle(10,1);

							moveObjectToPaint(1);
							repositionObject(0);
							repositionObject(2);		
							hideRadio();
							isRadioSelected=false;
							isFloaterSelected=false;
							isBasketSelected=true;
							GameManager.getInstance().setObject2Paint("agaseke");
						}
						else{


							SceneManager.getInstance().vibrate(100);

						}

					}
					else {

						if(!isMovementLocked){
							zoomCircle(10,0);
							repositionObject(0);
							repositionObject(2);
							hideRadio();

							isRadioSelected=false;
							isFloaterSelected=false;
							isBasketSelected=false;
						}
						else{
							SceneManager.getInstance().vibrate(300);
						}

					}

				}

				return true;
			};
		};

		registerTouchArea(BasketPart3);

		BasketPart4=new Sprite(0, 0, resourcesManager.basket4_region, vbom){

			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{
				if (pSceneTouchEvent.isActionUp())
				{  
					if(!isBasketSelected){
						if(!isMovementLocked){
							zoomCircle(10,1);

							moveObjectToPaint(1);
							repositionObject(0);
							repositionObject(2);		
							hideRadio();
							isRadioSelected=false;
							isFloaterSelected=false;
							isBasketSelected=true;
							GameManager.getInstance().setObject2Paint("agaseke");
						}
						else{


							SceneManager.getInstance().vibrate(100);

						}

					}
					else{

						if(!isMovementLocked){
							zoomCircle(10,0);

							repositionObject(0);
							repositionObject(2);
							hideRadio();
							isRadioSelected=false;
							isFloaterSelected=false;
							isBasketSelected=false;
						}
						else{
							SceneManager.getInstance().vibrate(300);
						}

					}

				}

				return true;
			};
		};

		registerTouchArea(BasketPart4);

		repositionObject(10);	
		pointToObject2Paint(10,"hide");

	}

	private void attachSprites(){

		//prepare objects to mount

		//attach parts of objects to the scene	

		//circles
		attachChild(circle0);
		attachChild(circle1);
		attachChild(circle2);
		attachChild(circle3);
		attachChild(circle4);
		attachChild(circle5);
		attachChild(circle6);

		circle0.setZIndex(4);
		circle1.setZIndex(4);
		circle2.setZIndex(4);
		circle3.setZIndex(4);
		circle4.setZIndex(4);
		circle5.setZIndex(4);
		circle6.setZIndex(4);

		circle0.setAlpha(1);
		circle1.setAlpha(1);
		circle2.setAlpha(1);
		circle3.setAlpha(1);
		circle4.setAlpha(1);
		circle5.setAlpha(1);
		circle6.setAlpha(1);

		//prepare color letters and mount 'em 
		letter0.setZIndex(5);
		attachChild(letter0);
		letter1.setZIndex(5);
		attachChild(letter1);
		letter2.setZIndex(5);
		attachChild(letter2);
		letter3.setZIndex(5);
		attachChild(letter3);
		letter4.setZIndex(5);
		attachChild(letter4);
		letter5.setZIndex(5);
		attachChild(letter5);
		letter6.setZIndex(5);
		attachChild(letter6);

		//radio

		RadioPart0.setZIndex(2);
		attachChild(RadioPart0);

		RadioPart1.setZIndex(2);
		attachChild(RadioPart1);

		RadioPart2.setZIndex(2);
		attachChild(RadioPart2);

		RadioPart3.setZIndex(2);
		attachChild(RadioPart3);

		RadioPart4.setZIndex(2);
		attachChild(RadioPart4);

		RadioPart5.setZIndex(2);
		attachChild(RadioPart5);

		RadioPart6.setZIndex(2);
		attachChild(RadioPart6);

		//from part 0 to part 6 start by hiding radio letters until radio is touched
		attachChild(Radioletter0);
		attachChild(Radioletter1);
		attachChild(Radioletter2);
		attachChild(Radioletter4);
		attachChild(Radioletter5);
		attachChild(Radioletter6);


		Radioletter0.setZIndex(3);
		Radioletter1.setZIndex(3);
		Radioletter2.setZIndex(3);
		Radioletter4.setZIndex(3);
		Radioletter5.setZIndex(3);
		Radioletter6.setZIndex(3);



		//floater

		attachChild(FloaterPart0);
		attachChild(FloaterPart1);
		attachChild(FloaterPart2);	
		attachChild(FloaterPart3);

		FloaterPart0.setZIndex(1);
		FloaterPart2.setZIndex(1);
		FloaterPart1.setZIndex(1);
		FloaterPart3.setZIndex(1);

		attachChild(Floaterletter0);
		attachChild(Floaterletter1);
		attachChild(Floaterletter2);
		attachChild(Floaterletter3);

		Floaterletter0.setZIndex(3);
		Floaterletter1.setZIndex(3);
		Floaterletter2.setZIndex(3);
		Floaterletter3.setZIndex(3);

		//basket
		attachChild(BasketPart0);
		attachChild(BasketPart1);
		attachChild(BasketPart2);
		attachChild(BasketPart3);
		attachChild(BasketPart4);


		BasketPart0.setZIndex(1);
		BasketPart0.setScale(1);

		BasketPart1.setZIndex(1);
		BasketPart1.setScale(1);

		BasketPart2.setZIndex(1);
		BasketPart2.setScale(1);

		BasketPart3.setZIndex(1);
		BasketPart3.setScale(1);

		BasketPart4.setZIndex(1);
		BasketPart4.setScale(1);

		attachChild(Basketletter0);
		Basketletter0.setZIndex(3);

		attachChild(Basketletter1);
		Basketletter1.setZIndex(3);

		attachChild(Basketletter2);
		Basketletter2.setZIndex(3);

		attachChild(Basketletter3);
		Basketletter3.setZIndex(3);

		attachChild(Basketletter4);
		Basketletter4.setZIndex(3);

		zoomBasketLetters(0,10);
		zoomRadioLetters(0,10);
		zoomFloaterLetters(0,10);

		//sort children by z index
		sortChildren();

	}


	private void lockTouch(int objectID){
		switch(objectID){
		case 0:
			this.unregisterTouchArea(RadioPart0);
			this.unregisterTouchArea(RadioPart1);
			this.unregisterTouchArea(RadioPart2);
			this.unregisterTouchArea(RadioPart3);
			this.unregisterTouchArea(RadioPart4);
			this.unregisterTouchArea(RadioPart5);
			this.unregisterTouchArea(RadioPart6);

			break;
		case 1:
			this.unregisterTouchArea(BasketPart0);
			this.unregisterTouchArea(BasketPart1);
			this.unregisterTouchArea(BasketPart2);
			this.unregisterTouchArea(BasketPart3);
			this.unregisterTouchArea(BasketPart4);

			break;
		case 2:
			this.unregisterTouchArea(FloaterPart0);
			this.unregisterTouchArea(FloaterPart2);
			this.unregisterTouchArea(FloaterPart1);
			this.unregisterTouchArea(FloaterPart3);

			break;

		default:
			this.unregisterTouchArea(FloaterPart0);
			this.unregisterTouchArea(FloaterPart2);
			this.unregisterTouchArea(FloaterPart1);
			this.unregisterTouchArea(FloaterPart3);
			this.unregisterTouchArea(BasketPart0);
			this.unregisterTouchArea(BasketPart1);
			this.unregisterTouchArea(BasketPart2);
			this.unregisterTouchArea(BasketPart3);
			this.unregisterTouchArea(BasketPart4);
			this.unregisterTouchArea(RadioPart0);
			this.unregisterTouchArea(RadioPart1);
			this.unregisterTouchArea(RadioPart2);
			this.unregisterTouchArea(RadioPart3);
			this.unregisterTouchArea(RadioPart4);
			this.unregisterTouchArea(RadioPart5);
			this.unregisterTouchArea(RadioPart6);
			this.unregisterTouchArea(circle0);
			this.unregisterTouchArea(circle1);
			this.unregisterTouchArea(circle2);
			this.unregisterTouchArea(circle3);
			this.unregisterTouchArea(circle4);
			this.unregisterTouchArea(circle5);
			this.unregisterTouchArea(circle6);
			this.unregisterTouchArea(letter0);
			this.unregisterTouchArea(letter1);
			this.unregisterTouchArea(letter2);
			this.unregisterTouchArea(letter3);
			this.unregisterTouchArea(letter4);
			this.unregisterTouchArea(letter5);
			this.unregisterTouchArea(letter6);

			break;

		}
	}

	private void repositionObject(int objectID){



		switch(objectID){
		case 0:
			isRadioSelected=false;
			RadioPart0.setPosition(initXRadioPart0,initYRadioPart0);
			RadioPart1.setPosition(initXRadioPart1,initYRadioPart1);
			RadioPart2.setPosition(initXRadioPart2,initYRadioPart2);
			RadioPart3.setPosition(initXRadioPart3,initYRadioPart3);
			RadioPart4.setPosition(initXRadioPart4,initYRadioPart4);
			RadioPart5.setPosition(initXRadioPart5,initYRadioPart5);
			RadioPart6.setPosition(initXRadioPart6,initYRadioPart6);
			zoomRadio(0.99f);
			zoomRadioLetters(0,10);
			paintsupernka.setAlpha(0.3f);
			background.setAlpha(0.3f);
			break;

		case 1:
			isBasketSelected=false;
			BasketPart0.setPosition(initXBasket-3,initYBasketPart0);
			BasketPart1.setPosition(initXBasket,initYBasketPart1);
			BasketPart2.setPosition(initXBasket,initYBasketPart2);
			BasketPart3.setPosition(initXBasket,initYBasketPart3);
			BasketPart4.setPosition(initXBasket,initYBasketPart4);
			zoomBasket(0.99f);
			zoomBasketLetters(0,10);
			paintsupernka.setAlpha(0.3f);
			background.setAlpha(0.3f);
			break;
		case 2:

			isFloaterSelected=false;
			FloaterPart0.setPosition(initXFloaterPart0,initYFloaterPart0);
			FloaterPart1.setPosition(initXFloaterPart1,initYFloaterPart1);
			FloaterPart2.setPosition(initXFloaterPart2,initYFloaterPart2);
			FloaterPart3.setPosition(initXFloaterPart3,initYFloaterPart3);
			zoomFloater(0.99f);
			zoomFloaterLetters(0,10);
			paintsupernka.setAlpha(0.3f);
			background.setAlpha(0.3f);

			break;

		default:

			isRadioSelected=false;
			isBasketSelected=false;
			isFloaterSelected=false;

			RadioPart0.setPosition(initXRadioPart0,initYRadioPart0);
			RadioPart1.setPosition(initXRadioPart1,initYRadioPart1);
			RadioPart2.setPosition(initXRadioPart2,initYRadioPart2);
			RadioPart3.setPosition(initXRadioPart3,initYRadioPart3);
			RadioPart4.setPosition(initXRadioPart4,initYRadioPart4);
			RadioPart5.setPosition(initXRadioPart5,initYRadioPart5);
			RadioPart6.setPosition(initXRadioPart6,initYRadioPart6);

			FloaterPart0.setPosition(initXFloaterPart0,initYFloaterPart0);
			FloaterPart2.setPosition(initXFloaterPart2,initYFloaterPart2);
			FloaterPart1.setPosition(initXFloaterPart1,initYFloaterPart1);
			FloaterPart3.setPosition(initXFloaterPart3,initYFloaterPart3);

			BasketPart0.setPosition(initXBasket-3,initYBasketPart0);
			BasketPart1.setPosition(initXBasket,initYBasketPart1);
			BasketPart2.setPosition(initXBasket,initYBasketPart2);
			BasketPart3.setPosition(initXBasket,initYBasketPart3);
			BasketPart4.setPosition(initXBasket,initYBasketPart4);

			zoomBasketLetters(0,10);
			zoomFloaterLetters(0,10);
			zoomRadioLetters(0,10);
			zoomRadio(1);
			zoomFloater(1);
			zoomBasket(1);
			paintsupernka.setAlpha(1);
			background.setAlpha(1);
			break;
		}



	}

	private void moveObjectToPaint(int objectID){


		isMovementLocked=true;
		pointToObject2Paint(10,"hide");

		switch(objectID){

		case 0:

			CombR0=new ParallelEntityModifier(new ScaleModifier(1,1,2.5f),new MoveModifier(1,initXRadioPart0,finalXRadioPart0,initYRadioPart0,finalYRadioPart0));
			CombR0.setAutoUnregisterWhenFinished(true);

			CombR1=new ParallelEntityModifier(new ScaleModifier(1,1,2.5f),new MoveModifier(1,initXRadioPart1,finalXRadioPart1,initYRadioPart1,finalYRadioPart1));
			CombR1.setAutoUnregisterWhenFinished(true);

			CombR2=new ParallelEntityModifier(new ScaleModifier(1,1,2.5f),new MoveModifier(1,initXRadioPart2,finalXRadioPart2,initYRadioPart2,finalYRadioPart2));
			CombR2.setAutoUnregisterWhenFinished(true);

			CombR3=new ParallelEntityModifier(new ScaleModifier(1,1,2.5f),new MoveModifier(1,initXRadioPart3,finalXRadioPart3,initYRadioPart3,finalYRadioPart3));
			CombR3.setAutoUnregisterWhenFinished(true);

			CombR4=new ParallelEntityModifier(new ScaleModifier(1,1,2.5f),new MoveModifier(1,initXRadioPart4,finalXRadioPart4,initYRadioPart4,finalYRadioPart4));
			CombR4.setAutoUnregisterWhenFinished(true);

			CombR5=new ParallelEntityModifier(new ScaleModifier(1,1,2.5f),new MoveModifier(1,initXRadioPart5,finalXRadioPart5,initYRadioPart5,finalYRadioPart5));
			CombR5.setAutoUnregisterWhenFinished(true);

			CombR6=new ParallelEntityModifier(new ScaleModifier(1,1,2.5f),new MoveModifier(1,initXRadioPart6,finalXRadioPart6,initYRadioPart6,finalYRadioPart6));
			CombR6.setAutoUnregisterWhenFinished(true);

			updateR = new TimerHandler(0.2f,true,new ITimerCallback() {
				@Override
				public void onTimePassed(TimerHandler pTimerHandler) {



					RadioPart0.setAlpha(0.2f);
					RadioPart0.registerEntityModifier(CombR0);

					RadioPart1.setAlpha(0.2f);
					RadioPart1.registerEntityModifier(CombR1);

					RadioPart2.setAlpha(0.2f);
					RadioPart2.registerEntityModifier(CombR2);

					RadioPart3.setAlpha(0.2f);
					RadioPart3.registerEntityModifier(CombR3);

					RadioPart4.setAlpha(0.2f);
					RadioPart4.registerEntityModifier(CombR4);

					RadioPart5.setAlpha(0.2f);
					RadioPart5.registerEntityModifier(CombR5);

					RadioPart6.setAlpha(0.2f);
					RadioPart6.registerEntityModifier(CombR6);


					if(CombR6.isFinished()){

						zoomRadioLetters(1.5f,10);
						RadioPart0.setAlpha(1);
						RadioPart1.setAlpha(1);
						RadioPart2.setAlpha(1);
						RadioPart3.setAlpha(1);
						RadioPart4.setAlpha(1);
						RadioPart5.setAlpha(1);
						RadioPart6.setAlpha(1);
						FloaterPart2.setAlpha(0);
						FloaterPart3.setAlpha(0);

						updateR.setAutoReset(false);
						isMovementLocked=false;						

						unregisterUpdateHandler(updateR);
						unregisterEntityModifier(CombR0);
						unregisterEntityModifier(CombR1);			
						unregisterEntityModifier(CombR2);
						unregisterEntityModifier(CombR3);
						unregisterEntityModifier(CombR4);
						unregisterEntityModifier(CombR5);
						unregisterEntityModifier(CombR6);

					}

				}

			});
			registerUpdateHandler(updateR);

			break;

		case 1:



			CombB0=new ParallelEntityModifier(new ScaleModifier(1,1,1.5f),new MoveModifier(1,initXBasket-3,finalXBasket-4.5f,initYBasketPart0,finalYBasketPart0));
			CombB0.setAutoUnregisterWhenFinished(true);

			CombB1=new ParallelEntityModifier(new ScaleModifier(1,1,1.5f),new MoveModifier(1,initXBasket,finalXBasket,initYBasketPart1,finalYBasketPart1));
			CombB1.setAutoUnregisterWhenFinished(true);

			CombB2=new ParallelEntityModifier(new ScaleModifier(1,1,1.5f),new MoveModifier(1,initXBasket,finalXBasket,initYBasketPart2,finalYBasketPart2));
			CombB2.setAutoUnregisterWhenFinished(true);

			CombB3=new ParallelEntityModifier(new ScaleModifier(1,1,1.5f),new MoveModifier(1,initXBasket,finalXBasket,initYBasketPart3,finalYBasketPart3));
			CombB3.setAutoUnregisterWhenFinished(true);

			CombB4=new ParallelEntityModifier(new ScaleModifier(1,1,1.5f),new MoveModifier(1,initXBasket,finalXBasket,initYBasketPart4,finalYBasketPart4));
			CombB4.setAutoUnregisterWhenFinished(true);

			updateB = new TimerHandler(0.2f,true,new ITimerCallback() {
				@Override
				public void onTimePassed(TimerHandler pTimerHandler) {

					BasketPart0.setAlpha(0.2f);
					BasketPart0.registerEntityModifier(CombB0);

					BasketPart1.setAlpha(0.2f);
					BasketPart1.registerEntityModifier(CombB1);

					BasketPart2.setAlpha(0.2f);
					BasketPart2.registerEntityModifier(CombB2);

					BasketPart3.setAlpha(0.2f);
					BasketPart3.registerEntityModifier(CombB3);

					BasketPart4.setAlpha(0.2f);
					BasketPart4.registerEntityModifier(CombB4);

					if(CombB4.isFinished()){
						zoomBasketLetters(1,10);
						BasketPart0.setAlpha(1);
						BasketPart1.setAlpha(1);
						BasketPart2.setAlpha(1);
						BasketPart3.setAlpha(1);
						BasketPart4.setAlpha(1);
						BasketPart4.setAlpha(1);
						updateB.setAutoReset(false);
						isMovementLocked=false;

						unregisterUpdateHandler(updateB);

						unregisterEntityModifier(CombB0);
						unregisterEntityModifier(CombB1);			
						unregisterEntityModifier(CombB2);
						unregisterEntityModifier(CombB3);
						unregisterEntityModifier(CombB4);
					}
				}
			});
			registerUpdateHandler(updateB);
			break;

		case 2:


			CombF0=new ParallelEntityModifier(new ScaleModifier(1,1,3.5f),new MoveModifier(1,initXFloaterPart0,finalXFloaterPart0,initYFloaterPart0,finalYFloaterPart0));
			CombF0.setAutoUnregisterWhenFinished(true);

			CombV0=new ParallelEntityModifier(new ScaleModifier(1,1,3.5f),new MoveModifier(1,initXFloaterPart1,finalXFloaterPart1,initYFloaterPart1,finalYFloaterPart1));
			CombV0.setAutoUnregisterWhenFinished(true);

			CombF1=new ParallelEntityModifier(new ScaleModifier(1,1,3.5f),new MoveModifier(1,initXFloaterPart2,finalXFloaterPart2,initYFloaterPart2,finalYFloaterPart2));
			CombF1.setAutoUnregisterWhenFinished(true);

			CombV1=new ParallelEntityModifier(new ScaleModifier(1,1,3.5f),new MoveModifier(1,initXFloaterPart3,finalXFloaterPart3,initYFloaterPart3,finalYFloaterPart3));
			CombV1.setAutoUnregisterWhenFinished(true);

			updateF = new TimerHandler(0.2f,true,new ITimerCallback() {
				@Override
				public void onTimePassed(TimerHandler pTimerHandler) {

					FloaterPart0.setAlpha(0.2f);
					FloaterPart0.registerEntityModifier(CombF0);

					FloaterPart1.setAlpha(0.2f);
					FloaterPart1.registerEntityModifier(CombV0);

					FloaterPart2.setAlpha(0.2f);
					FloaterPart2.registerEntityModifier(CombF1);



					FloaterPart3.setAlpha(0.2f);
					FloaterPart3.registerEntityModifier(CombV1);


					if(CombV1.isFinished()){
						zoomFloaterLetters(1.25f,10);
						FloaterPart0.setAlpha(1);
						FloaterPart1.setAlpha(1);

						FloaterPart2.setAlpha(1);
						FloaterPart3.setAlpha(1);

						updateF.setAutoReset(false);
						isMovementLocked=false;							
						unregisterUpdateHandler(updateF);				
						unregisterEntityModifier(CombF0);
						unregisterEntityModifier(CombF1);			
						unregisterEntityModifier(CombV0);
						unregisterEntityModifier(CombV1);

					}
				}
			});
			registerUpdateHandler(updateF);



			break;
		default:

			break;

		}

	}

	private void detachSceneCharacters(){

		detachChild(circle0);
		detachChild(circle1);
		detachChild(circle2);
		detachChild(circle3);
		detachChild(circle4);
		detachChild(circle5);
		detachChild(circle6);

		detachChild(letter0);
		detachChild(letter1);
		detachChild(letter2);
		detachChild(letter3);
		detachChild(letter4);
		detachChild(letter5);
		detachChild(letter6);

		detachChild(RadioPart0);
		detachChild(RadioPart1);
		detachChild(RadioPart2);
		detachChild(RadioPart3);
		detachChild(RadioPart4);
		detachChild(RadioPart5);
		detachChild(RadioPart6);

		detachChild(Radioletter0);
		detachChild(Radioletter1);
		detachChild(Radioletter2);
		detachChild(Radioletter4);
		detachChild(Radioletter5);
		detachChild(Radioletter6);

		detachChild(FloaterPart0);
		detachChild(FloaterPart2);

		detachChild(FloaterPart1);
		detachChild(FloaterPart3);

		detachChild(Floaterletter0);
		detachChild(Floaterletter1);

		detachChild(Floaterletter2);
		detachChild(Floaterletter3);

		detachChild(BasketPart0);
		detachChild(BasketPart1);
		detachChild(BasketPart2);
		detachChild(BasketPart3);
		detachChild(BasketPart4);

		detachChild(Basketletter0);
		detachChild(Basketletter1);
		detachChild(Basketletter2);
		detachChild(Basketletter3);
		detachChild(Basketletter4);

		palette.setScale(0);

		isMatchedRadioLetter0=false;
		isMatchedRadioLetter1=false;
		isMatchedRadioLetter2=false;
		isMatchedRadioLetter4=false;
		isMatchedRadioLetter5=false;
		isMatchedRadioLetter6=false;

		isMatchedBasketLetter0=false;
		isMatchedBasketLetter1=false;
		isMatchedBasketLetter2=false;
		isMatchedBasketLetter3=false;
		isMatchedBasketLetter4=false;


		isMatchedFloaterLetter0=false;
		isMatchedFloaterLetter1=false;

		isMatchedFloaterLetter2=false;
		isMatchedFloaterLetter3=false;

		isRadioSelected=false;
		isFloaterSelected=false;
		isBasketSelected=false;

		isRadioFinished=false;
		isBasketFinished=false;
		isFloaterFinished=false;


	}


	private void zoomCircle(int circleID,float zoom){

		//first scale down all circles
		circle0.setScale(0.75f);
		circle1.setScale(0.75f);
		circle2.setScale(0.75f);
		circle3.setScale(0.75f);
		circle4.setScale(0.75f); 
		circle5.setScale(0.75f);
		circle6.setScale(0.75f);

		letter0.setScale(0.5f);
		letter1.setScale(0.5f);
		letter2.setScale(0.5f);
		letter3.setScale(0.5f);
		letter4.setScale(0.5f);
		letter5.setScale(0.5f);
		letter6.setScale(0.5f);

		//then scale up the one singled out
		switch(circleID) {
		case 0:
			circle0.setScale(zoom);
			letter0.setScale(1);
			break;
		case 1:
			circle1.setScale(zoom);
			letter1.setScale(1);
			break;
		case 2:
			circle2.setScale(zoom);
			letter2.setScale(1);
			break;
		case 3:
			circle3.setScale(zoom);
			letter3.setScale(1);
			break;
		case 4:
			circle4.setScale(zoom);
			letter4.setScale(1);
			break;
		case 5:
			circle5.setScale(zoom);
			letter5.setScale(1);
			break;
		case 6:
			circle6.setScale(zoom);
			letter6.setScale(1);
			break;
		default:
			palette.setScale(zoom);
			circle0.setScale(zoom);
			circle1.setScale(zoom);
			circle2.setScale(zoom);
			circle3.setScale(zoom);
			circle4.setScale(zoom); 
			circle5.setScale(zoom);
			circle6.setScale(zoom);
			if(zoom<0.5f){
				letter0.setScale(0);
				letter1.setScale(0);
				letter2.setScale(0);
				letter3.setScale(0);
				letter4.setScale(0);
				letter5.setScale(0);
				letter6.setScale(0);
			}


			break;
		}
	}

	private void zoomRadio(float zoomScale) {

		if(zoomScale>=1f) {


			RadioPart0.setAlpha(1f);
			RadioPart1.setAlpha(1f);
			RadioPart2.setAlpha(1f);
			RadioPart3.setAlpha(1f);
			RadioPart4.setAlpha(1f);
			RadioPart5.setAlpha(1f);
			RadioPart6.setAlpha(1f);

			RadioPart0.setScale(zoomScale);
			RadioPart1.setScale(zoomScale);
			RadioPart2.setScale(zoomScale);
			RadioPart3.setScale(zoomScale);
			RadioPart4.setScale(zoomScale);
			RadioPart5.setScale(zoomScale);
			RadioPart6.setScale(zoomScale);


		}
		else if(zoomScale<0){

			RadioPart0.setAlpha(0.4f);
			RadioPart1.setAlpha(0.4f);
			RadioPart2.setAlpha(0.4f);
			RadioPart3.setAlpha(0.4f);
			RadioPart4.setAlpha(0.4f);
			RadioPart5.setAlpha(0.4f);
			RadioPart6.setAlpha(0.4f);
		}
		else {
			RadioPart0.setAlpha(0.4f);
			RadioPart1.setAlpha(0.4f);
			RadioPart2.setAlpha(0.4f);
			RadioPart3.setAlpha(0.4f);
			RadioPart4.setAlpha(0.4f);
			RadioPart5.setAlpha(0.4f);
			RadioPart6.setAlpha(0.4f);

			RadioPart0.setScale(1);
			RadioPart1.setScale(1);
			RadioPart2.setScale(1);
			RadioPart3.setScale(1);
			RadioPart4.setScale(1);
			RadioPart5.setScale(1);
			RadioPart6.setScale(1);

		}

	}

	private void hideRadio() {

		RadioPart0.setAlpha(0.2f);
		RadioPart1.setAlpha(0.2f);
		RadioPart2.setAlpha(0.2f);
		RadioPart3.setAlpha(0.2f);
		RadioPart4.setAlpha(0.2f);
		RadioPart5.setAlpha(0.2f);
		RadioPart6.setAlpha(0.2f);
	}

	private void zoomBasket(float zoomScale) {

		if(zoomScale>=1) {

			BasketPart0.setScale(zoomScale);
			BasketPart1.setScale(zoomScale);
			BasketPart2.setScale(zoomScale);
			BasketPart3.setScale(zoomScale);
			BasketPart4.setScale(zoomScale);

			BasketPart0.setAlpha(1f);
			BasketPart1.setAlpha(1f);
			BasketPart2.setAlpha(1f);
			BasketPart3.setAlpha(1f);
			BasketPart4.setAlpha(1f);

		}

		else if(zoomScale<0){

			BasketPart0.setAlpha(0.4f);
			BasketPart1.setAlpha(0.4f);
			BasketPart2.setAlpha(0.4f);
			BasketPart3.setAlpha(0.4f);
			BasketPart4.setAlpha(0.4f);	
		}
		else {
			BasketPart0.setAlpha(0.4f);
			BasketPart1.setAlpha(0.4f);
			BasketPart2.setAlpha(0.4f);
			BasketPart3.setAlpha(0.4f);
			BasketPart4.setAlpha(0.4f);	
			BasketPart0.setScale(1);
			BasketPart1.setScale(1);
			BasketPart2.setScale(1);
			BasketPart3.setScale(1);
			BasketPart4.setScale(1);
		}


	}

	private void zoomFloater(float zoom) {

		if(zoom>=1){

			FloaterPart0.setScale(zoom);
			FloaterPart1.setScale(zoom);
			FloaterPart2.setScale(zoom);
			FloaterPart3.setScale(zoom);

			FloaterPart0.setAlpha(1);
			FloaterPart1.setAlpha(1);
			FloaterPart2.setAlpha(1);
			FloaterPart3.setAlpha(1);

		}

		else if(zoom<0){

			FloaterPart0.setAlpha(0.4f);
			FloaterPart1.setAlpha(0.4f);
			FloaterPart2.setAlpha(0.1f);
			FloaterPart3.setAlpha(0.4f);
		}
		else{
			FloaterPart0.setAlpha(0.4f);
			FloaterPart1.setAlpha(0.4f);
			FloaterPart2.setAlpha(0.4f);
			FloaterPart3.setAlpha(0.4f);

			FloaterPart0.setScale(1);
			FloaterPart1.setScale(1);
			FloaterPart2.setScale(1);
			FloaterPart3.setScale(1);
		}
	}


	private void zoomRadioLetters(float zoom,int letterID) {


		switch(letterID) {
		case 0:
			if(!isMatchedRadioLetter0)
				Radioletter0.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			else
				zoomFloaterLetters(0,0);
			break;
		case 1:
			if(!isMatchedRadioLetter1)
				Radioletter1.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			else
				Radioletter1.setScale(0);
			break;
		case 2:
			if(!isMatchedRadioLetter2)
				Radioletter2.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			else
				Radioletter2.setScale(0);
			break;
		case 3:

			break;
		case 4:
			if(!isMatchedRadioLetter4)
				Radioletter4.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			else
				Radioletter4.setScale(0);
			break;
		case 5:
			if(!isMatchedRadioLetter5)
				Radioletter5.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			else
				Radioletter5.setScale(0);
			break;
		case 6:
			if(!isMatchedRadioLetter6)
				Radioletter6.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			else
				Radioletter6.setScale(0);
			break;
		default:
			if(!isMatchedRadioLetter0)
				Radioletter0.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			if(!isMatchedRadioLetter1)
				Radioletter1.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			if(!isMatchedRadioLetter2)
				Radioletter2.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			if(!isMatchedRadioLetter4)
				Radioletter4.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			if(!isMatchedRadioLetter5)
				Radioletter5.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			if(!isMatchedRadioLetter6)
				Radioletter6.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			break;
		}

	}

	private void zoomBasketLetters(float zoom,int letterID) {



		switch(letterID) {
		case 0:
			if(!isMatchedBasketLetter0)
				Basketletter0.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			else
				Basketletter0.setScale(0);
			break;
		case 1:
			if(!isMatchedBasketLetter1)
				Basketletter1.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			else
				Basketletter1.setScale(0);
			break;
		case 2:
			if(!isMatchedBasketLetter2)
				Basketletter2.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			else
				Basketletter2.setScale(0);
			break;
		case 3:
			if(!isMatchedBasketLetter3)
				Basketletter3.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			else
				Basketletter3.setScale(0);
			break;
		case 4:
			if(!isMatchedBasketLetter4)
				Basketletter4.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			else
				Basketletter4.setScale(0);
			break;
		default:
			if(!isMatchedBasketLetter0)
				Basketletter0.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			if(!isMatchedBasketLetter1)
				Basketletter1.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			if(!isMatchedBasketLetter2)
				Basketletter2.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			if(!isMatchedBasketLetter3)
				Basketletter3.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			if(!isMatchedBasketLetter4)
				Basketletter4.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			break;
		}


	}

	private void zoomFloaterLetters(float zoom,int letterID) {


		switch(letterID) {
		case 0:
			if(!isMatchedFloaterLetter0)
				Floaterletter0.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			else
				Floaterletter0.setScale(0);
			break;
		case 1:
			if(!isMatchedFloaterLetter1)
				Floaterletter1.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			else
				Floaterletter1.setScale(0);
			break;
		case 2:
			if(!isMatchedFloaterLetter2)
				Floaterletter2.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			else
				Floaterletter2.setScale(0);
			break;
		case 3:
			if(!isMatchedFloaterLetter3)
				Floaterletter3.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			else
				Floaterletter3.setScale(0);
			break;
		default:
			if(!isMatchedFloaterLetter0)
				Floaterletter0.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			if(!isMatchedFloaterLetter1)
				Floaterletter1.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			if(!isMatchedFloaterLetter2)
				Floaterletter2.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			if(!isMatchedFloaterLetter3)
				Floaterletter3.registerEntityModifier(new ParallelEntityModifier(new ScaleModifier(1,0,zoom),new AlphaModifier(1,0.5f,1)));
			break;
		}


	}


	/*this method checks for victory in case all objects' parts are painted
	 * it also allow us to point to next object in case the previous one is done*/
	private void checkVictory() {



		if(isMatchedRadioLetter0&&isMatchedRadioLetter1&&isMatchedRadioLetter2&&isMatchedRadioLetter4&&isMatchedRadioLetter5&&isMatchedRadioLetter6) {
			isRadioFinished=true;
			if(!radioPlayed){
				radioPlayed=true;
				SceneManager.getInstance().playAfterFinishedObjectSound(objectid);
				objectid++;
			}

			repositionObject(0);


			if(!hasBasketBeenPointedAt&&!isBasketFinished&&!(isRadioSelected||isFloaterSelected)){
				pointToObject2Paint(1,"show");
				hasBasketBeenPointedAt=true;
			}
			else if(!hasFloaterBeenPointedAt&&!isFloaterFinished&&!(isRadioSelected||isBasketSelected)){
				pointToObject2Paint(2,"show");
				hasFloaterBeenPointedAt=true;	
			}

			lockTouch(0);

			if(!isBasketFinished&&!isBasketSelected){
				zoomBasket(-1);
				hasBasketBeenPointedAt=false;
			}
			if(!isFloaterFinished&&!isFloaterSelected){
				zoomFloater(-1);
				hasFloaterBeenPointedAt=false;
			}


		}
		if(isMatchedBasketLetter0&&isMatchedBasketLetter1&&isMatchedBasketLetter2&&isMatchedBasketLetter3&&isMatchedBasketLetter4){
			isBasketFinished=true;
			if(!basketPlayed){
				basketPlayed=true;
				SceneManager.getInstance().playAfterFinishedObjectSound(objectid);
				objectid++;
			}
			repositionObject(1);
			lockTouch(1);

			if(!hasFloaterBeenPointedAt&&!isFloaterFinished&&!(isRadioSelected||isBasketSelected)){
				pointToObject2Paint(2,"show");
				hasFloaterBeenPointedAt=true;
			}
			else if(!hasRadioBeenPointedAt&&!isRadioFinished&&!(isFloaterSelected||isBasketSelected)){
				pointToObject2Paint(0,"show");
				hasRadioBeenPointedAt=true;
			}

			if(!isFloaterFinished&&!isFloaterSelected){
				zoomFloater(-1);
				hasFloaterBeenPointedAt=false;
			}
			if(!isRadioFinished&&!isRadioSelected){
				zoomRadio(-1);
				hasRadioBeenPointedAt=false;
			}

		}
		else{
			hasBasketBeenPointedAt=false;
		}


		if(isMatchedFloaterLetter0&&isMatchedFloaterLetter1&&isMatchedFloaterLetter2&&isMatchedFloaterLetter3) {

			isFloaterFinished=true;
			if(!floaterPlayed){
				floaterPlayed=true;
				SceneManager.getInstance().playAfterFinishedObjectSound(objectid);
				objectid++;
			}

			repositionObject(2);
			lockTouch(2);

			if(!hasBasketBeenPointedAt&&!isBasketFinished&&!(isRadioSelected||isFloaterSelected)){
				pointToObject2Paint(1,"show");
				hasBasketBeenPointedAt=true;
			}
			else if(!hasRadioBeenPointedAt&&!isRadioFinished&&!(isFloaterSelected||isBasketSelected)){
				pointToObject2Paint(0,"show");
				hasRadioBeenPointedAt=true;
			}

			if(!isBasketFinished&&!isBasketSelected){
				zoomBasket(-1);
				hasBasketBeenPointedAt=false;
			}

			if(!isRadioFinished&&!isRadioSelected){
				zoomRadio(-1);
				hasRadioBeenPointedAt=false;
			}

		}
		else{
			hasFloaterBeenPointedAt=false;	
		}

		if(isRadioFinished&&isBasketFinished&&isFloaterFinished) {

			isVictoryAchieved=true;
			pointToObject2Paint(10,"hide");
			animatePaintSuperCow();
			SceneManager.getInstance().playPaintCongratulorySound(true);
			repositionObject(10);
			lockTouch(10);

			TimerHandler wait = new TimerHandler(8f,true,new ITimerCallback() {
				@Override
				public void onTimePassed(TimerHandler pTimerHandler) {
					if(isVictoryAchieved)
						showPaintMenu();
				}

			});
			registerUpdateHandler(wait);		

		}


	}

	//method to animate the super cow

	private void animatePaintSuperCow() {

		paintsupernka.setAlpha(1);
		paintsupernka.animate(new long[] { 400,300,300,400,500,300}, 0,5,false);

	}

	//method to be called when the user requests demo
	private void doDemo(){

		lockTouch(10);
		isDemoRunning=true;
		toRadio=new MoveModifier(1,camera.getWidth(),camera.getWidth()-250,camera.getHeight(),camera.getHeight()/2);
		RadiotoCircle=new MoveModifier(1,camera.getWidth()-250,20,camera.getHeight()/2,25);
		BacktoRadio=new MoveModifier(1,20,camera.getWidth()-250,25,(camera.getHeight()/2)-50);
		toRadio.setAutoUnregisterWhenFinished(true);
		RadiotoCircle.setAutoUnregisterWhenFinished(true);
		BacktoRadio.setAutoUnregisterWhenFinished(true);


		TimerHandler sniffer= new TimerHandler(2f,false,new ITimerCallback() {

			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {

				Radioletter6.setText(randLetter0);
				finger.setColor(1, 0.5f,0.1f,0.7f);
				finger.registerEntityModifier(toRadio) ; 

				TimerHandler waitForZoom = new TimerHandler(1.2f, false,new ITimerCallback() {

					@Override
					public void onTimePassed(TimerHandler pTimerHandler) {
						moveObjectToPaint(0);	
						zoomCircle(10,1f);
					}

				});
				registerUpdateHandler(waitForZoom);

				TimerHandler waitToMoveFinger = new TimerHandler(1.5f, false,new ITimerCallback() {
					@Override
					public void onTimePassed(TimerHandler pTimerHandler) {

						finger.registerEntityModifier(RadiotoCircle);

					}

				});
				registerUpdateHandler(waitToMoveFinger);  		


				TimerHandler waitToZoomCircle = new TimerHandler(2f, false,new ITimerCallback() {
					@Override
					public void onTimePassed(TimerHandler pTimerHandler) {

						finger.registerEntityModifier(RadiotoCircle);
						if(RadiotoCircle.isFinished()){
							Radioletter6.setColor(0.2f, 1, 0.2f, 1);
						}	

					}

				});
				registerUpdateHandler(waitToZoomCircle);  		



				TimerHandler waitForPaint = new TimerHandler(3.5f, false,new ITimerCallback() {

					@Override
					public void onTimePassed(TimerHandler pTimerHandler) {

						finger.registerEntityModifier(BacktoRadio) ;
						circle0.setScale(1.75f);
						letter0.setScale(1.15f);
						circle0.registerEntityModifier(new MoveModifier(1,20,camera.getWidth()-250,20,(camera.getHeight()/2)-50));
						letter0.registerEntityModifier(new MoveModifier(1,20,camera.getWidth()-250,20,(camera.getHeight()/2)-50));
					}

				});
				registerUpdateHandler(waitForPaint );

				TimerHandler waitForColor = new TimerHandler(4.7f, false,new ITimerCallback() {
					@Override
					public void onTimePassed(TimerHandler pTimerHandler) {

						if(BacktoRadio.isFinished()){
							Radioletter6.setScale(0);
							RadioPart6.setColor(GameManager.getInstance().getPaintCircleColor(0));
							RadioPart6.setAlpha(1);
							zoomCircle(10,0);
							SceneManager.getInstance().playEndPaintDemo();
						}

					}

				});
				registerUpdateHandler(waitForColor);  


				TimerHandler waitToReset = new TimerHandler(6.5f, false,new ITimerCallback() {
					@Override
					public void onTimePassed(TimerHandler pTimerHandler) {

						reloadPaintGame();	

					}

				});
				registerUpdateHandler(waitToReset);  


			}

		});
		registerUpdateHandler(sniffer);
	}

	//function to point to object to paint
	private void pointToObject2Paint(int object,String action){


		if("show".equalsIgnoreCase(action)){

			ShowArrow=new AlphaModifier(0.5f,0,1); 
			switch(object){
			case 0: //radio
				Floater_guide_arrow.setScale(0);
				Basket_guide_arrow.setScale(0);
				if(!isRadioSelected)
					Radio_guide_arrow.setScale(1);

				Radio_guide_arrow.registerEntityModifier(new LoopEntityModifier(ShowArrow));
				break;
			case 1:
				Floater_guide_arrow.setScale(0);
				Radio_guide_arrow.setScale(0);
				if(!isBasketSelected)
					Basket_guide_arrow.setScale(1);

				Basket_guide_arrow.registerEntityModifier(new LoopEntityModifier(ShowArrow));
				break;
			case 2:

				Basket_guide_arrow.setScale(0);
				Radio_guide_arrow.setScale(0);
				if(!isFloaterSelected) 
					Floater_guide_arrow.setScale(1);

				Floater_guide_arrow.registerEntityModifier(new LoopEntityModifier(ShowArrow));
				break;
			default:
				break;

			}

		}
		else{
			unregisterEntityModifier(ShowArrow);
			Floater_guide_arrow.setScale(0);
			Basket_guide_arrow.setScale(0);
			Radio_guide_arrow.setScale(0);
		}

	}




	//method to create paint menu 
	protected void createMenuScene() {

		this.paintMenuScene = new MenuScene(camera);

		final SpriteMenuItem resetMenuItem = new SpriteMenuItem(MENU_RESET,resourcesManager.reset_region,vbom);
		resetMenuItem.setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

		final SpriteMenuItem quitMenuItem = new SpriteMenuItem(MENU_QUIT,resourcesManager.back_region, vbom);
		quitMenuItem.setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);


		final SpriteMenuItem cancelMenuItem = new SpriteMenuItem(MENU_CANCEL,resourcesManager.cancel_region, vbom);
		quitMenuItem.setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

		this.paintMenuScene.addMenuItem(resetMenuItem);
		this.paintMenuScene.addMenuItem(cancelMenuItem);
		this.paintMenuScene.addMenuItem(quitMenuItem);


		this.paintMenuScene.buildAnimations();

		this.paintMenuScene.setBackgroundEnabled(false);

		this.paintMenuScene.setOnMenuItemClickListener(this);
	}


	@Override
	public boolean onMenuItemClicked(final MenuScene pMenuScene, final IMenuItem pMenuItem, final float pMenuItemLocalX, final float pMenuItemLocalY) {
		switch(pMenuItem.getID()) {
		case MENU_RESET:				
			reloadPaintGame();
			return true;
		case MENU_CANCEL:
			cancelMenu();
			return true;
		case MENU_QUIT:
			/* End game mode. */
			exitPaintGame();
			return true;
		default:
			return false;
		}
	}

	
	private void postMemInfo(){


		WarnText.setScale(0.75f);
		RAMinfo.setScale(0.75f);
		warning.setScale(1.25f);

		TimerHandler waitForWarning = new TimerHandler(17f,false,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
							
					warning.setScale(0);
				    WarnText.setScale(0);
				    RAMinfo.setScale(0);
			}

		});
		registerUpdateHandler(waitForWarning);
	}

	private void updateMemoryInfo(){

		TimerHandler MemCheckDaemon = new TimerHandler(20f,true,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {

				MemoryInfo mi = new MemoryInfo();
				ActivityManager activityManager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
				if(mi!=null){
					activityManager.getMemoryInfo(mi);
					long availableMegs = mi.availMem / 1048576L;

					final String badmemory="  RAM isigaye: "+availableMegs+" MB,ikenewe: 128 MB";
					if(availableMegs<128L){	
						RAMinfo.setText(badmemory);
						postMemInfo();
					}

				}

			}

		});
		registerUpdateHandler(MemCheckDaemon);
	}
	//override methods 

	@Override
	public void onBackKeyPressed()
	{   
		if(!isDemoRunning){ 
			showPaintMenu();
		}
		else{
			Toast.makeText(activity,"Reka Soma irangize ku kwerekera...", Toast.LENGTH_SHORT).show();
			SceneManager.getInstance().vibrate(100);
		}
	}

	@Override
	public void onMenuKeyPressed() {

		if(!isDemoRunning){ 
			showPaintMenu();
		}
		else{
			Toast.makeText(activity,"Reka Soma irangize ku kwerekera...", Toast.LENGTH_SHORT).show();
			SceneManager.getInstance().vibrate(100);
		}	
	}

	//method to be called when submenu is requested
	private void showPaintMenu(){

		if(this.hasChildScene()) {
			// Remove the menu and reset it. 
			this.paintMenuScene.back();
			GameManager.getInstance().setPaused(false);
			SceneManager.getInstance().resumeGameModeBackMusic();

			canvas.setScale(0);
			showmenu.setScale(0.5f);
		} 
		else {
			// Attach the menu. 
			this.setChildScene(this.paintMenuScene, false, true, true);
			SceneManager.getInstance().pauseGameModeBackMusic();
			GameManager.getInstance().setPaused(true);
			canvas.setScale(1);
			showmenu.setScale(0);
		}
	}


	private void exitPaintGame() {

		SceneManager.getInstance().stopGameModeBackMusic();
		SceneManager.getInstance().resumeMenuBackMusic();
		//clear game variables from game manager class
		GameManager.getInstance().resetPaintGame();
		//kick back to menu scene
		SceneManager.getInstance().loadMenuScene(engine);
	}


	// method to allow user to replay 
	private void reloadPaintGame() {
		isDemoRunning=false;
		isVictoryAchieved=false;
		hasBasketBeenPointedAt=false;
		hasFloaterBeenPointedAt=false;
		hasRadioBeenPointedAt=false;
		SceneManager.getInstance().playPaintCongratulorySound(false);
		GameManager.getInstance().setPaused(false);
		/* Remove the menu and reset it. */
		this.clearChildScene();
		this.paintMenuScene.reset();
		GameManager.getInstance().resetPaintGame();
		canvas.setScale(0);
		finger.setScale(0);
		showmenu.setScale(0.5f);
		/*please never miss with this...It can cost you a trip to Heavens*/
		clearTouchAreas();
		clearEntityModifiers();
		detachSceneCharacters();
		loadSprites();
		attachSprites();
		registerTouchArea(showmenu);
		pointToObject2Paint(MathUtils.random(0,2),"show");
		SceneManager.getInstance().resumeGameModeBackMusic();
	}

	//hide submenu without action
	private void cancelMenu(){
		GameManager.getInstance().setPaused(false);
		SceneManager.getInstance().resumeGameModeBackMusic();
		this.clearChildScene();
		this.paintMenuScene.reset();
		canvas.setScale(0);
		showmenu.setScale(0.5f);
	}
}