
/**
 @company: Hehe Ltd
 @Project: Soma
 @Date: mm/dd/yyyy
 @Author: Amiri Mugarura and Sixbert Uwiringiyimana
 @Credits: thanks given to Richard Rusa and other artists for graphic designs ....

 @About this Class "AboutScene.java":
   ----------------------------------------
 
 */



package com.hehe.soma.scene;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.scene.menu.item.decorator.ScaleMenuItemDecorator;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.opengl.util.GLState;

import com.hehe.soma.manager.SceneManager;
import com.hehe.soma.manager.SceneManager.SceneType;

public class AboutScene extends ParentScene implements IOnMenuItemClickListener {

	private MenuScene aboutChildScene;
	private Sprite background,HeHeLogo,THL_logo;
	private Text version,init,credit,enFooterText,frFooterText,kinFooterText,EnText,FrText,KinText;
	// 3 buttons for about 
	private final int KIN = 0;
	private final int ENG = 1;
	private final int FR =2;
	private final String initMsg="Kanda Ururimi wifuza gukoresha.\nPress the language of your choice.\nAppuyez sur la langue de votre choix.";
	private final String enAbout="SOMA is an engaging application for 2 to 8 year\nold children," +
			" teaching them early phonics by\nreinforcing letter recognition in Kinyarwanda.\n" +
			"Children can use SOMA with little to no supervision.\n";

	
	private final String frAbout="SOMA est une application ludique et interactive\npour enfants entre 2 et 8 ans," +
			" qui se focalise sur\n la reconnaissance des lettres \nde l’alphabet en Kinyarwanda.\n" +
			"L’utilisation de l’application est possible \navec ou sans supervision parentale.\n";

	private final String kinAbout="SOMA ni umukino wa telefone ugenewe\nabana hagati y’Imyaka 2 n’8," +
			" ukaba \nufasha umwana kwimenyereza inyuguti\nz’ibanze mu Kinyarwanda.\n" +
			"Umwana ashobora kuyikoresha ari kumwe\ncyangwa se Atari kumwe n’umubyeyi.\n";

	private final  String kinFooter="SOMA ni umutungo wa “A Thousand Hills Literacy”\nna HeHe Labs.\n©A Thousand Hills Literacy /HeHe Labs.";
	private final String enFooter="SOMA is a trademark of “A Thousand Hills Literacy”\nand HeHe labs.\n©A Thousand Hills Literacy /HeHe Labs. All Rights Reserved.";
	private final String frFooter="SOMA est une marque déposée de\n«A Thousand Hills Literacy» et HeHe Labs. \n©A Thousand Hills Literacy /HeHe Labs.Tous droits réservés.";

	private final String credits="Credits:\n"+
			"        -Chantal Uwiragiye: audio materials.\n"+
			"        -Nicolas Gramlich: author of Andengine, used by SOMA.\n"+
		    "        -Seif Bizimana: graphical illustrations.\n" +
		    "        -freesfx.co.uk,http://www.freesfx.co.uk: sound effects.";
	
	private final String SomaVersion="Soma v1.1.2";
	@Override
	public void createScene() {

		//register our messages with our system message management


		createBackground();

		init=new Text(100,camera.getCenterY()-80, resourcesManager.aboutFont,initMsg, vbom);

		EnText=new Text(60,120, resourcesManager.aboutFont,enAbout, vbom);
		FrText=new Text(60,120, resourcesManager.aboutFont,frAbout, vbom);
		KinText=new Text(60,120, resourcesManager.aboutFont,kinAbout, vbom);

		enFooterText=new Text(60,camera.getHeight()-200, resourcesManager.aboutFont,enFooter, vbom);
		frFooterText=new Text(60,camera.getHeight()-200, resourcesManager.aboutFont,frFooter, vbom);
		kinFooterText=new Text(60,camera.getHeight()-200, resourcesManager.aboutFont,kinFooter, vbom);
		
		credit=new Text(100,camera.getHeight()-110, resourcesManager.aboutFont,credits, vbom);

		version=new Text(camera.getCenterX()+200,camera.getHeight()-25, resourcesManager.aboutFont,SomaVersion, vbom);

		attachChild(init);
		credit.setScale(0.65f);
		version.setScale(0.65f);
		enFooterText.setScale(0);
		frFooterText.setScale(0);
		kinFooterText.setScale(0);

		EnText.setScale(0);
		FrText.setScale(0);
		KinText.setScale(0);
		

		attachChild(enFooterText);
		attachChild(frFooterText);
		attachChild(kinFooterText);

		attachChild(EnText);
		attachChild(FrText);
		attachChild(KinText);

		attachChild(credit);
		attachChild(version);
		createAboutChildScene(); 

	}

	@Override
	public void onBackKeyPressed() {
	
		SceneManager.getInstance().loadMenuScene(engine);	
	}

	@Override
	public void onMenuKeyPressed() {
		
		SceneManager.getInstance().loadMenuScene(engine);	
	}

	@Override
	public SceneType getSceneType() {

		return SceneType.SCENE_ABOUT;
	}

	@Override
	public void disposeScene() {

		
		this.detachChildren(); 
		this.detachSelf();
		this.dispose();
		System.gc();

	}

	private void createAboutChildScene()
	{
		aboutChildScene = new MenuScene(camera);
		aboutChildScene.setPosition(0, 0);

		final IMenuItem En_Item = new ScaleMenuItemDecorator(new SpriteMenuItem(ENG, resourcesManager.en_button_region, vbom),0.9f, 1);
		final IMenuItem Fr_Item = new ScaleMenuItemDecorator(new SpriteMenuItem(FR, resourcesManager.fr_button_region, vbom), 0.9f, 1);
		final IMenuItem Kin_Item = new ScaleMenuItemDecorator(new SpriteMenuItem(KIN, resourcesManager.kin_button_region, vbom), 0.9f, 1);

		aboutChildScene.addMenuItem(En_Item);
		aboutChildScene.addMenuItem(Fr_Item);
		aboutChildScene.addMenuItem(Kin_Item);

		aboutChildScene.buildAnimations();
		aboutChildScene.setBackgroundEnabled(false);


		Kin_Item.setPosition((camera.getWidth()/2)-320,20);
		En_Item.setPosition((camera.getWidth()/2)-100,20);
		Fr_Item.setPosition((camera.getWidth()/2)+120,20); 

		aboutChildScene.setOnMenuItemClickListener(this);

		setChildScene(aboutChildScene);
	}

	private void createBackground()
	{

		background=new Sprite(0, 0, resourcesManager.about_back_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		background.setScale(1.0f);
		background.setPosition(0,0);
		attachChild(background);

		HeHeLogo=new Sprite(0, 0, resourcesManager.HeHe_Logo_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		HeHeLogo.setScale(0.5f);
		HeHeLogo.setPosition(camera.getCenterX()+260,camera.getHeight()-90);
		attachChild(HeHeLogo);
		
		
		THL_logo=new Sprite(0, 0, resourcesManager._100Hills_Logo_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		THL_logo.setScale(0.4f);
		THL_logo.setPosition(0,camera.getHeight()-125);
		attachChild(THL_logo);
		
	}

	@Override
	public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem, float pMenuItemLocalX, float pMenuItemLocalY)
	{      


		switch(pMenuItem.getID())
		{
		case ENG:
			init.setScale(0);

			EnText.setScale(0.85f);
			FrText.setScale(0);
			KinText.setScale(0);

			enFooterText.setScale(0.85f);
			frFooterText.setScale(0);
			kinFooterText.setScale(0);
			return true;
		case FR:
			init.setScale(0);

			EnText.setScale(0);
			FrText.setScale(0.85f);
			KinText.setScale(0);

			enFooterText.setScale(0);
			frFooterText.setScale(0.85f);
			kinFooterText.setScale(0);

			return true;

		case KIN:
			init.setScale(0);

			EnText.setScale(0);
			FrText.setScale(0);
			KinText.setScale(0.85f);

			enFooterText.setScale(0);
			frFooterText.setScale(0);
			kinFooterText.setScale(0.85f);

			return true;
		default:
			return false;

		}


	}

}
