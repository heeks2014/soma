package com.hehe.soma.scene;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import android.app.Activity;
import android.content.Context;
import android.view.KeyEvent;

import com.hehe.soma.manager.ResourcesManager;
import com.hehe.soma.manager.SceneManager.SceneType;

/*
This is the mother(parent) class of any scene used by SOMA
Children classes will inherit abstract methods necessary for scenes 
flow and management.
 */
public abstract class ParentScene extends Scene
{
	//---------------------------------------------
	// VARIABLES
	//---------------------------------------------

	protected Engine engine;
	protected Activity activity;
	protected Context context;
	protected ResourcesManager resourcesManager;
	protected VertexBufferObjectManager vbom;
	protected Camera camera;

	//---------------------------------------------
	// CONSTRUCTOR
	//---------------------------------------------

	public ParentScene()
	{
		resourcesManager = ResourcesManager.getInstance();
		engine = resourcesManager.engine;
		activity = resourcesManager.activity;
		context=resourcesManager.context;
		vbom = resourcesManager.vbom;
		camera = resourcesManager.camera;
		createScene();
	}

	//---------------------------------------------
	// ABSTRACT METHODS to be overridden
	//---------------------------------------------

	public abstract void createScene();

	public abstract void onBackKeyPressed();

	public abstract void onMenuKeyPressed();

	public abstract SceneType getSceneType();

	public abstract void disposeScene();

	public boolean onKeyDown(int pKeyCode, KeyEvent pEvent) {
		// TODO Auto-generated method stub
		return false;
	}
}