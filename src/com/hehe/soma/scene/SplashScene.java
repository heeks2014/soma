/**
 @company: Hehe Ltd
 @Project: Soma
 @Date: mm/dd/yyyy
 @Author: Amiri Mugarura and Sixbert Uwiringiyimana
 @Credits: thanks given to Richard Rusa and other artists for graphic designs ....
 
 @About this Class "SplashScene.java"
   ----------------------------------
   
This class is the initial screen shown to the user while the game is loading resources in background.
 */
package com.hehe.soma.scene;

import javax.microedition.khronos.opengles.GL10;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.ColorModifier;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.util.GLState;

import com.hehe.soma.manager.SceneManager.SceneType;


public class SplashScene extends ParentScene
{
	private Sprite splash;
	
    @Override
    public void createScene()
    {
    	splash = new Sprite(0,0, resourcesManager.splash_region, vbom)
    	{
    	    @Override
    	    protected void preDraw(GLState pGLState, Camera pCamera) 
    	    {
    	       super.preDraw(pGLState, pCamera);
    	       pGLState.enableDither();
    	    }
    	};
    	        
    	splash.setScale(1.f);
    	splash.setPosition(0,0);//set initial position of splash screen to the screen' start
    	attachChild(splash);
    	splash.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
         IEntityModifier fader = new AlphaModifier(5,1,0);
         splash.registerEntityModifier(new ParallelEntityModifier(fader,new ColorModifier(3f,1,1,1,0.75f,0.65f,0)));
    	
    }

    @Override
    public void onBackKeyPressed()
    {
      //never listen to back button while loading resources
    }

    @Override
    public SceneType getSceneType()
    {
    	return SceneType.SCENE_SPLASH;
    }

    /*free memory and other resources associated with splash*/
    
    @Override
    public void disposeScene()
    {       
    	    clearEntityModifiers();
    	    splash.detachSelf();
    	    splash.dispose();
    	    detachSelf();
    	    dispose();
    	    
    }

	@Override
	public void onMenuKeyPressed() {
		
		// we will be kidding if we listen to this
		
	}
    
    
}