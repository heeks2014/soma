package com.hehe.soma.scene;

import java.util.ArrayList;
import java.util.List;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.scene.menu.item.decorator.ScaleMenuItemDecorator;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.util.GLState;
import org.andengine.util.color.Color;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.hehe.soma.manager.GameManager;
import com.hehe.soma.manager.SceneManager;
import com.hehe.soma.manager.SceneManager.SceneType;


public class OptionScene extends ParentScene implements IOnMenuItemClickListener
{
	private MenuScene optionChildScene;
	GLState pGLState;
	private Sprite background,canvas,RateStar0,RateStar1,RateStar2,RateStar3,RateStar4;
	private Text text;
	
	private Text WarnText,RAMinfo;
	private Sprite warning;
	
	//buttons for options 
	private final int MUSIC = 0;
	private final int SOUND = 1;
	private final int VIBRATION =2;
	private final int RATE =3;
	//private final int LEVEL =4;
	//private int currentGameLevel;
	private String deviceID=" ",Origin=" ";
	private String rateDegree;
	private boolean isRateScreenOn=false;
    private boolean hasRateBeenGiven=false;


	IMenuItem Music_Item ;
	IMenuItem No_Music_Item;
	IMenuItem Sound_Item;
	IMenuItem No_Sound_Item;
	IMenuItem Vibration_Item;
	IMenuItem No_Vibration_Item;
	IMenuItem Rate_Item;

	private String postRes;


	@Override
	public SceneType getSceneType()
	{
		return SceneType.SCENE_OPTION;
	}

	@Override
	public void createScene()
	{ 
		createBackground();
		createOptionsChildScene();
		initRateSprites();
		sortChildren();
		updateMemoryInfo();

	}

	private void postMemInfo(){


		WarnText.setScale(0.75f);
		RAMinfo.setScale(0.75f);
		warning.setScale(1.25f);

		TimerHandler waitForWarning = new TimerHandler(17f,false,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
							
					warning.setScale(0);
				    WarnText.setScale(0);
				    RAMinfo.setScale(0);
			}

		});
		registerUpdateHandler(waitForWarning);
	}

	private void updateMemoryInfo(){

		TimerHandler MemCheckDaemon = new TimerHandler(20f,true,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {

				MemoryInfo mi = new MemoryInfo();
				ActivityManager activityManager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
				if(mi!=null){
					activityManager.getMemoryInfo(mi);
					long availableMegs = mi.availMem / 1048576L;

					final String badmemory="  RAM isigaye: "+availableMegs+" MB,ikenewe: 128 MB";
					if(availableMegs<128L){	
						RAMinfo.setText(badmemory);
						postMemInfo();
					}

				}

			}

		});
		registerUpdateHandler(MemCheckDaemon);
	}


	private void createOptionsChildScene()
	{
		optionChildScene = new MenuScene(camera);
		optionChildScene.setPosition(0, 0);

		Music_Item = new ScaleMenuItemDecorator(new SpriteMenuItem(MUSIC, resourcesManager.music_button_region, vbom),0.9f, 1);	
		No_Music_Item = new ScaleMenuItemDecorator(new SpriteMenuItem(MUSIC, resourcesManager.nomusic_button_region, vbom),0.9f, 1);
		Sound_Item = new ScaleMenuItemDecorator(new SpriteMenuItem(SOUND, resourcesManager.sound_button_region, vbom), 0.9f, 1);
		No_Sound_Item = new ScaleMenuItemDecorator(new SpriteMenuItem(SOUND, resourcesManager.nosound_button_region, vbom),0.9f, 1);		

		//final IMenuItem Level_Item = new ScaleMenuItemDecorator(new SpriteMenuItem(LEVEL, resourcesManager.level_button_region, vbom), 0.9f, 1);
		Vibration_Item = new ScaleMenuItemDecorator(new SpriteMenuItem(VIBRATION, resourcesManager.vibration_button_region, vbom),0.9f, 1);
		No_Vibration_Item = new ScaleMenuItemDecorator(new SpriteMenuItem(VIBRATION, resourcesManager.novibration_button_region, vbom),0.9f, 1);
		Rate_Item = new ScaleMenuItemDecorator(new SpriteMenuItem(RATE, resourcesManager.rate_button_region, vbom), 0.9f, 1);

		if(GameManager.getInstance().isMusicEnabled()) {
			Music_Item.setScale(1);
			No_Music_Item.setScale(0);
		}
		else {
			No_Music_Item.setScale(1);
			Music_Item.setScale(0);
		}

		if(GameManager.getInstance().isSoundEnabled()) {
			Sound_Item.setScale(1);
			No_Sound_Item.setScale(0);
		}
		else {
			Sound_Item.setScale(0);
			No_Sound_Item.setScale(1);
		}

		if(GameManager.getInstance().isVibrationEnabled()) {		
			No_Vibration_Item.setScale(0);
			Vibration_Item.setScale(1);
		}
		else {
			No_Vibration_Item.setScale(1);
			Vibration_Item.setScale(0);
		}



		No_Sound_Item.setZIndex(1);
		No_Music_Item.setZIndex(1);
		No_Vibration_Item.setZIndex(1);

		Sound_Item.setZIndex(2);
		Music_Item.setZIndex(2);
		Vibration_Item.setZIndex(2);



		optionChildScene.addMenuItem(Sound_Item);
		optionChildScene.addMenuItem(Music_Item);
		optionChildScene.addMenuItem(Vibration_Item);

		optionChildScene.addMenuItem(No_Sound_Item);
		optionChildScene.addMenuItem(No_Music_Item);
		optionChildScene.addMenuItem(No_Vibration_Item);

		optionChildScene.addMenuItem(Rate_Item);
		//optionChildScene.addMenuItem(Level_Item);


		optionChildScene.buildAnimations();
		optionChildScene.setBackgroundEnabled(false);



		Sound_Item.setPosition(296,50);
		Music_Item.setPosition(296,100);
		Vibration_Item.setPosition(296,160); 
		Rate_Item.setPosition(296,210); 

		No_Sound_Item.setPosition(296,50);
		No_Music_Item.setPosition(296,100);
		No_Vibration_Item.setPosition(296,160); 

		//Level_Item.setPosition(Level_Item.getX(), Level_Item.getY() );


		optionChildScene.setOnMenuItemClickListener(this);


		setChildScene(optionChildScene);
	}

	//create option menu background

	private void createBackground()
	{
		

		background=new Sprite(0, 0, resourcesManager.option_back_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		background.setScale(1.0f);
		background.setPosition(0,0);
		attachChild(background);
	}

	private void initRateSprites(){
		
		text=new Text(0,0, resourcesManager.optionFont, "Murakoze kugera SOMA. Muhitemo utunyenyeri mwifuza kuyiha!", vbom);
		if(!text.hasParent()){
		  attachChild(text);
		}
		text.setScale(0);
		text.setZIndex(5);

		this.canvas=new Sprite(0, 0, resourcesManager.canvas_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		};
		canvas.setScale(0);
		canvas.setAlpha(0.5f);
		canvas.setZIndex(4);
		canvas.setPosition(camera.getCenterX()-200,camera.getCenterY()-200);
		attachChild(canvas);

		RateStar0=new Sprite(0, 0, resourcesManager.rate_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   

				if (pSceneTouchEvent.isActionUp())
				{   
					rateDegree="1";	
					hasRateBeenGiven=true;

				}

				if (pSceneTouchEvent.isActionDown())
				{  



				}
				return true;
			};
		};

		RateStar0.setScale(0);
		RateStar0.setZIndex(4);
		RateStar0.setPosition(camera.getCenterX()-200,camera.getCenterY()-125);
		attachChild(RateStar0);
		registerTouchArea(RateStar0);


		RateStar1=new Sprite(0, 0, resourcesManager.rate_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   

				if (pSceneTouchEvent.isActionUp())
				{ 
					rateDegree="2";	
					hasRateBeenGiven=true;

				}

				if (pSceneTouchEvent.isActionDown())
				{  



				}
				checkRating();
				return true;
			};
		};
		RateStar1.setScale(0);
		RateStar1.setZIndex(4);
		RateStar1.setPosition(camera.getCenterX()-100,camera.getCenterY()-125);
		attachChild(RateStar1);
		registerTouchArea(RateStar1);


		RateStar2=new Sprite(0, 0, resourcesManager.rate_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   

				if (pSceneTouchEvent.isActionUp())
				{   
					rateDegree="3";	
					hasRateBeenGiven=true;

				}

				if (pSceneTouchEvent.isActionDown())
				{  



				}
				checkRating();
				return true;
			};
		};

		RateStar2.setScale(0);
		RateStar2.setZIndex(4);
		RateStar2.setPosition(camera.getCenterX(),camera.getCenterY()-125);
		attachChild(RateStar2);
		registerTouchArea(RateStar2);

		RateStar3=new Sprite(0, 0, resourcesManager.rate_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   

				if (pSceneTouchEvent.isActionUp())
				{   
					rateDegree="4";	
					hasRateBeenGiven=true;

				}

				if (pSceneTouchEvent.isActionDown())
				{  



				}
				checkRating();
				return true;
			};
		};
		RateStar3.setScale(0);
		RateStar3.setZIndex(4);
		RateStar3.setPosition(camera.getCenterX()+100,camera.getCenterY()-125);
		attachChild(RateStar3);
		registerTouchArea(RateStar3);

		RateStar4=new Sprite(0, 0, resourcesManager.rate_region, vbom)

		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   

				if (pSceneTouchEvent.isActionUp())
				{   
					rateDegree="5";	
					hasRateBeenGiven=true;

				}

				if (pSceneTouchEvent.isActionDown())
				{  



				}
				checkRating();
				return true;
			};
		};
		RateStar4.setScale(0);
		RateStar4.setZIndex(4);
		RateStar4.setPosition(camera.getCenterX()+200,camera.getCenterY()-125);
		attachChild(RateStar4);
		registerTouchArea(RateStar4);

		RateStar0.setAlpha(1);
		RateStar1.setAlpha(1);
		RateStar2.setAlpha(1);
		RateStar3.setAlpha(1);
		RateStar4.setAlpha(1);

		warning=new Sprite(0, 0, resourcesManager.showwarn_region, vbom)
		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float X, float Y) 
			{   

				if (pSceneTouchEvent.isActionUp())
				{   
					WarnText.setScale(0);
					RAMinfo.setScale(0);
					warning.setScale(0);
				

				}
				return true;
			};
		};
		warning.setPosition(100,camera.getHeight()-warning.getHeight()-25);
		attachChild(warning);
		registerTouchArea(warning);
		warning.setScale(0);

		WarnText=new Text(warning.getX()+warning.getWidth()-15,camera.getHeight()-warning.getHeight()-50, resourcesManager.WarnFont,
				"Telefone yawe ifite ibyangombwa bidahagije!\n Funga izindi apps udakeneye cyangwa\n ujye usubiramo SOMA nijya yifunga.", vbom);	
		attachChild(WarnText);	
		WarnText.setScale(0);

		RAMinfo=new Text(warning.getX()+warning.getWidth()-15,camera.getHeight()-warning.getHeight()-55+WarnText.getHeight(), resourcesManager.WarnFont,
				"  RAM isigaye: 100 MB,ikenewe: 128 MB ", vbom);	
		attachChild(RAMinfo);	
		RAMinfo.setScale(0);
		
		RAMinfo.setZIndex(4);
		WarnText.setZIndex(4);
		warning.setZIndex(4);
		
	}

	@Override
	public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem, float pMenuItemLocalX, float pMenuItemLocalY)
	{    

		switch(pMenuItem.getID())
		{
		case MUSIC:
			if(GameManager.getInstance().isMusicEnabled())
			{   
				GameManager.getInstance().setMusic(false);
				Music_Item.setScale(0);
				No_Music_Item.setScale(1);
			}
			else{
				Music_Item.setScale(1);
				No_Music_Item.setScale(0);			

				GameManager.getInstance().setMusic(true);
			}
			applyScale(pGLState);
			return true;
		case SOUND:
			if(GameManager.getInstance().isSoundEnabled())
			{
				GameManager.getInstance().setSound(false);

				Sound_Item.setScale(0);
				No_Sound_Item.setScale(1);


			}
			else{
				GameManager.getInstance().setSound(true);

				Sound_Item.setScale(1);
				No_Sound_Item.setScale(0);

			}
			applyScale(pGLState);
			return true;

		case VIBRATION:
			if(GameManager.getInstance().isVibrationEnabled())
			{
				GameManager.getInstance().setVibration(false);
				Vibration_Item.setScale(0);
				No_Vibration_Item.setScale(1);
			}
			else{
				GameManager.getInstance().setVibration(true);

				Vibration_Item.setScale(1);
				No_Vibration_Item.setScale(0);
			}
			applyScale(pGLState);
			return true;
		case RATE:
			if(!isRateScreenOn){
				showRateScreen();
				isRateScreenOn=true;
			}
			else{		
				this.postRate();
				isRateScreenOn=false;
			}
			applyScale(pGLState);
			return true;
		default:
			return false;

		}
     
	}



	private void showRateScreen(){

		text.setScale(0.55f);
		text.setPosition(camera.getCenterX()-375,camera.getCenterY()-200);
		this.canvas.setScale(1);
		this.RateStar0.setScale(1);
		this.RateStar1.setScale(1);
		this.RateStar2.setScale(1);
		this.RateStar3.setScale(1);
		this.RateStar4.setScale(1);


		Music_Item.setScale(0);
		No_Music_Item.setScale(0);
		Sound_Item.setScale(0);
		No_Sound_Item.setScale(0);
		Vibration_Item.setScale(0);
		No_Vibration_Item.setScale(0);


	}

	private void postRate(){

		text.setScale(0);
		this.canvas.setScale(0);
		this.RateStar0.setScale(0);
		this.RateStar1.setScale(0);
		this.RateStar2.setScale(0);
		this.RateStar3.setScale(0);
		this.RateStar4.setScale(0);
		this.RateStar0.setColor(Color.WHITE);	 
		this.RateStar1.setColor(Color.WHITE);
		this.RateStar2.setColor(Color.WHITE);
		this.RateStar3.setColor(Color.WHITE);
		this.RateStar4.setColor(Color.WHITE);

		if(!GameManager.getInstance().isMusicEnabled())
		{   

			Music_Item.setScale(0);
			No_Music_Item.setScale(1);
		}
		else{
			Music_Item.setScale(1);
			No_Music_Item.setScale(0);			


		}

		if(!GameManager.getInstance().isSoundEnabled())
		{

			Sound_Item.setScale(0);
			No_Sound_Item.setScale(1);


		}
		else{

			Sound_Item.setScale(1);
			No_Sound_Item.setScale(0);

		}

		if(!GameManager.getInstance().isVibrationEnabled())
		{

			Vibration_Item.setScale(0);
			No_Vibration_Item.setScale(1);
		}
		else{


			Vibration_Item.setScale(1);
			No_Vibration_Item.setScale(0);
		}
    if(hasRateBeenGiven){
    	
    	hasRateBeenGiven=false;
		new Thread(new Runnable() {
			public void run() {
				HttpClient httpClient = new DefaultHttpClient();
				ResponseHandler<String> response=new BasicResponseHandler();
				String query= "id="+getDeviceDetails()+"rate="+rateDegree;

				try {
					
					HttpPost httpPost = new HttpPost("http://192.168.1.135/soma/index.php");

					List<NameValuePair> ratingByID = new ArrayList<NameValuePair>(2);
					ratingByID.add(new BasicNameValuePair("action",query));
					httpPost.setEntity(new UrlEncodedFormEntity(ratingByID));

					postRes = httpClient.execute(httpPost,response);
				

				} 
				catch (Exception e) {
					e.printStackTrace();
				} 

			}
		}).start(); 

		TimerHandler wait = new TimerHandler(10f,false,new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {

				activity.runOnUiThread(new Runnable() {
					public void run() {

						if(postRes!=null){
							
						  Toast.makeText(activity,"Murakoze guha SOMA inyenyeri "+rateDegree+".\nIkigero cyatugezeyo ubuhoro",Toast.LENGTH_LONG).show();  
						}
						else{
							Toast.makeText(activity,"Soma yagize ibibazo igihe yoherezaga ikigero wayihaye!\nReba niba telefoni yawe iri kuri interineti. \n\n:: HTTP_code= "+postRes,Toast.LENGTH_LONG+Toast.LENGTH_LONG).show();	  
						}
					}
				});	
			}

		});
		registerUpdateHandler(wait);
      }
	}


	private void checkRating(){

		if("1".equals(rateDegree)){
			this.RateStar0.setColor(Color.GREEN);	 
			this.RateStar1.setColor(Color.WHITE);
			this.RateStar2.setColor(Color.WHITE);
			this.RateStar3.setColor(Color.WHITE);
			this.RateStar4.setColor(Color.WHITE);
		}
		else if("2".equals(rateDegree)){
			this.RateStar0.setColor(Color.GREEN);
			this.RateStar1.setColor(Color.GREEN);
			this.RateStar2.setColor(Color.WHITE);
			this.RateStar3.setColor(Color.WHITE);
			this.RateStar4.setColor(Color.WHITE);

		}
		else if("3".equals(rateDegree)){
			this.RateStar0.setColor(Color.GREEN);
			this.RateStar1.setColor(Color.GREEN);
			this.RateStar2.setColor(Color.GREEN);
			this.RateStar3.setColor(Color.WHITE);
			this.RateStar4.setColor(Color.WHITE);
		}
		else if("4".equals(rateDegree)){
			this.RateStar0.setColor(Color.GREEN);
			this.RateStar1.setColor(Color.GREEN);
			this.RateStar2.setColor(Color.GREEN);
			this.RateStar3.setColor(Color.GREEN);
			this.RateStar4.setColor(Color.WHITE);
		}
		else if("5".equals(rateDegree)){
			this.RateStar0.setColor(Color.GREEN);
			this.RateStar1.setColor(Color.GREEN);
			this.RateStar2.setColor(Color.GREEN);
			this.RateStar3.setColor(Color.GREEN);
			this.RateStar4.setColor(Color.GREEN);
		}

		RateStar0.setAlpha(1);
		RateStar1.setAlpha(1);
		RateStar2.setAlpha(1);
		RateStar3.setAlpha(1);
		RateStar4.setAlpha(1);
	}

	private String getDeviceDetails(){

		if(context!=null){
			TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
			if(tm!=null){
				if(tm.getNetworkCountryIso()!=null){
				Origin=" "+"country iso: "+tm.getNetworkCountryIso()+", ";
				}
				else{
					Origin=" "+"country iso: NONE, ";	
				}
				if(tm.getLine1Number()!=null){
					Origin=" "+Origin+" phone number: "+tm.getLine1Number()+", ";
				}
				else{
					Origin=" "+Origin+" phone number: NONE, ";
				}
				if(tm.getNetworkOperatorName()!=null){
					Origin=" "+Origin+" operator name: "+tm.getNetworkOperatorName()+", ";
				}
				else{
				    Origin=" "+Origin+" operator name: NONE, ";
				}
				    Origin=" "+Origin+" phone type: "+tm.getPhoneType()+", ";
				if(tm.getDeviceSoftwareVersion()!=null){
					Origin=" "+Origin+" sofware: "+tm.getDeviceSoftwareVersion()+", ";
				}
				else{
					Origin=" "+Origin+" sofware: NONE, ";
				}
				if(tm.getDeviceId()!=null){
				deviceID =" device ID: "+ tm.getDeviceId()+", ";
				}
				else{
					deviceID =" device ID: NONE, ";
				}
				
				deviceID=deviceID.concat(Origin).toString();
			}
			return deviceID;
		}
		else{
			return "nta id";
		}
	}


	@Override
	public void disposeScene()
	{  
		
		this.clearTouchAreas();
		this.clearChildScene();
		this.detachChildren();
		this.detachSelf();
		this.dispose();
		System.gc();
	

	}

	
	@Override
	public void onBackKeyPressed()
	{
		SceneManager.getInstance().loadMenuScene(engine);
			SceneManager.getInstance().resumeMenuBackMusic();
	}
	@Override
	public void onMenuKeyPressed() {

		SceneManager.getInstance().loadMenuScene(engine);
			SceneManager.getInstance().resumeMenuBackMusic();
	}

}
